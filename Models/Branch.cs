﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Branch
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid ApplicationId { get; set; }
        [ForeignKey("Address")]
        public Guid AddressId { get; set; }
        public Address Address { get; set; }
        public bool Active { get; set; }
        public string ImageUrl { get; set; }
        public TimeSpan OpeningTime { get; set; }
        public TimeSpan ClosingTime { get; set; }
        public string IPAddress { get; set; }
        public float VAT { get; set; }
        [ForeignKey("Currency")]
        public Guid CurrencyId { get; set; }
        public Currency Currency { get; set; }

    }
   
}
