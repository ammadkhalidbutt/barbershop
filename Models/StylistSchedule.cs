﻿using Enums;
using Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class StylistSchedule
    {
        public Guid Id { get; set; }
        public WeekDays Day { get; set; }
        public TimeSpan StartingTime { get; set; }
        public TimeSpan EndingTime { get; set; }
        public bool IsDeleted { get; set; }
        [ForeignKey("Stylist")]
        public string StylistId { get; set; }
        public ApplicationUser Stylist { get; set; }
    }
}
