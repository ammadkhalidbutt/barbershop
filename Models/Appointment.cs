﻿using Enums;
using Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Appointment
    {
        public Guid Id { get; set; }

        [ForeignKey("Client")]
        public string ClientId { get; set; }
        public ApplicationUser Client { get; set; }

        [ForeignKey("Branch")]
        public Guid BranchId { get; set; }
        public Branch Branch { get; set; }

        [ForeignKey("Application")]
        public Guid ApplicationId { get; set; }
        public Application Application { get; set; }

        public DateTime BookingDate { get; set; }
        public DateTime AppointmentStartingTime { get; set; }
        public DateTime AppointmentEndingTime { get; set; } 
        public DateTime ActualStartingTime { get; set; }
        public DateTime ActualEndingTime { get; set; }
        public bool Active { get; set; }
        public bool IsDeleted { get; set; }
        public double Discount { get; set; }
        public AppointmentTreatmentStatus Status { get; set; }

        [ForeignKey("Stylist")]
        [Required]
        public string StylistId { get; set; }
        public ApplicationUser Stylist { get; set; }

        [ForeignKey("Treatment")]
        public Guid TreatmentId { get; set; }
        public Treatment Treatment { get; set; }

        public DateTime LastUpdatedOn { get; set; }

        public double TimeElapsed { get; set; }


    }

    public class AppointmentApproval
    {
        [Key, Column(Order = 1)]
        [ForeignKey("Appointment")]
        public Guid AppointmentId { get; set; }
        public Appointment Appointment { get; set; }

        [Key, Column(Order = 2)]
        [ForeignKey("Approver")]
        public string ApproverId { get; set; }
        public ApplicationUser Approver { get; set; }

        public DateTime ApprovalDateTime { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public AppointmentStatus Status { get; set; }

        public bool Active { get; set; }
    }

    public class AppointmentDetails
    {
        public Guid Id { get; set; }

        [ForeignKey("Appointment")]
        public Guid AppointmentId { get; set; }
        public Appointment Appointment { get; set; }

        public Guid TreatmentId { get; set; }

        [ForeignKey("User")]
        public string StylistId { get; set; }
        public ApplicationUser User { get; set; }
    }
   
}
