﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Log
    {
        public int Id { get; set; }
        public string ModelId { get; set; }
        public string StackTrace { get; set; }
        public string Action { get; set; }
        public DateTime DateTime { get; set; }
        public string AddedBy { get; set; }
    }
}
