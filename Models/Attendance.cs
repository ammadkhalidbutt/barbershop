﻿using Enums;
using Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Attendance
    {
        public Guid Id { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public DateTime EntryTime { get; set; }
        public string MacAddress { get; set; }
        public bool ManualEntry { get; set; }
        public string Error { get; set; }
        public bool Active { get; set; }
        [Index(IsUnique = true)]
        public int MachineId { get; set; }
        [ForeignKey("Branch")]
        public Guid BranchId { get; set; }
        public Branch Branch { get; set; }
        [ForeignKey("Application")]
        public Guid ApplicaitonId { get; set; }
        public Application Application { get; set; }

    }
}
