﻿using Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Product
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        [ForeignKey("User")]
        public string AddedBy { get; set; }
        public ApplicationUser User { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        [ForeignKey("CategoryId")]
        public ProductCategory Category { get; set; }
        public Guid CategoryId { get; set; }
        [ForeignKey("TaxId")]
        public Tax Tax { get; set; }
        public Guid TaxId { get; set; }
        public double Price { get; set; }
        public bool Active { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
    }
}
