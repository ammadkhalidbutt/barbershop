﻿using Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Machine
    {
        public Guid Id { get; set; }

        [ForeignKey("Application")]
        public Guid ApplicationId { get; set; }
        public Application Application { get; set; }

        [ForeignKey("Branch")]
        public Guid BranchId { get; set; }
        public Branch Branch { get; set; }

        public DateTime CreatedOn { get; set; }

        [ForeignKey("Creator")]
        public string CreatedBy { get; set; }
        public ApplicationUser Creator { get; set; }

        public bool Active { get; set; }
        public string MacAddress { get; set; }

        public DateTime LastUpdatedOn { get; set; }
    }
}
