﻿using Enums;
using Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class InvoiceRecord
    {
        public Guid Id { get; set; }
        [ForeignKey("Client")]
        public string ClientId { get; set; }
        public ApplicationUser Client { get; set; }
        public InvoiceStatus Status { get; set; }
        public DateTime DateTime { get; set; }
        public double GrandTotal { get; set; }
        public float AmountRecieved { get; set; }
        public int PointsUsed { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public float AmountGiven { get; set; }
        public string InvoiceNumber { get; set; }
        public bool IsPaidByPin { get; set; }
    }
}
