﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Models.ViewModels;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Models.ViewModels
{
    public class BranchViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public Guid ApplicationId { get; set; }
        public string IPAddress { get; set; }
        public Guid AddressId { get; set; }
        [Required]
        public AddressViewModel Address { get; set; }
        public bool Active { get; set; }
        public HttpPostedFileBase Logo { get; set; }
        public string LogoUrl { get; set; }
        [Required]
        public TimeSpan OpeningTime { get; set; }
        [Required]
        public TimeSpan ClosingTime { get; set; }
        [Required]
        public float VAT { get; set; }
        public CurrencyViewModel Currency { get; set; }
    }
    public class BranchSearchViewModel : GeneralSearchViewModel
    {
        public Guid BranchId { get; set; }
        public Guid ApplicationId { get; set; }
        public string Name { get; set; }
        public string IPAddress { get; set; }

    }
}
