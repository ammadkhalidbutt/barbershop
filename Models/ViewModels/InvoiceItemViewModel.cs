﻿using Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class InvoiceItemViewModel
    {
        public Guid Id { get; set; }
        public uint ReferenceNumber { get; set; }
        public float ItemPrice { get; set; }
        public float? DiscountedPrice { get; set; }
        public DateTime DateTime { get; set; }
        public bool IsDeleted { get; set; }
        public Guid ItemId { get; set; }
        public ApplicationUser PersonAddingRecord { get; set; }
        public DateTime UpdatedOn { get; set; }
        public ApplicationUser PersonUpdatingRecord { get; set; }
        public InvoiceRecord InvoiceRecord { get; set; }
        public AppointmentViewModel Appointment { get; set; }

    }

    public class InvoiceItemSearchViewModel : GeneralSearchViewModel
    {
        public Guid Id { get; set; }
        public Appointment Appointment { get; set; }
        public uint ReferenceNumber { get; set; }
        public ApplicationUser PersonAddingRecord { get; set; }
        public DateTime DateTime { get; set; }
        public ApplicationUser PersonUpdatingRecord { get; set; }
        public InvoiceRecord InvoiceRecord { get; set; }
    }
}
