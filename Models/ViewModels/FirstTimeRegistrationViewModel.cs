﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class FirstTimeRegistrationViewModel
    {
        public FirstTimeRegistrationViewModel()
        {
            Branch = new BranchViewModel();
            Application = new ApplicationViewModel();
            Currency = new CurrencyViewModel();
        }
        public string Action { get; set; }
        public BranchViewModel Branch { get; set; }
        public ApplicationViewModel Application { get; set; }
        public CurrencyViewModel Currency { get; set; }
    }
}
