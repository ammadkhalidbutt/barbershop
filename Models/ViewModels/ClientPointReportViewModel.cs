﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class ClientPointReportViewModel
    {
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public int Points { get; set; }
        public int TotalPoints { get; set; }
        public DateTime Date { get; set; }
    }

    public class ClientPointReportSearchViewModel : GeneralSearchViewModel
    {
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public Guid TreatmentId { get; set; }
        public string TreatmentName { get; set; }
        public bool DashboardView { get; set; }
    }
}
