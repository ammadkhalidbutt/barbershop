﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class TaxViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public float Percentage { get; set; }
        public bool IsExclusive { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string Action { get; set; }
        public bool IsDeleted { get; set; }
    }
}
