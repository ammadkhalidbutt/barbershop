﻿using Enums;
using Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class InvoiceReportViewModel
    {
        public Guid Id { get; set; }
        public ApplicationUser Client { get; set; }
        public RegisterViewModel User { get; set; }
        public InvoiceStatus Status { get; set; }
        public BranchViewModel Branch { get; set; }
        public DateTime DateTime { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public double GrandTotal { get; set; }
        public double AmountRecieved { get; set; }
        public double AmountGiven { get; set; }
        public int PointsUsed { get; set; }
        public double VATPercentage { get; set; }
        public double VAT
        {
            get
            {
                if (InvoiceItems.Count() > 0)
                {
                    double tax = 0;
                    for(var i = 0; i<InvoiceItems.Count(); i++)
                    {
                        tax += InvoiceItems[i].TreatmentPrice / 100 * InvoiceItems[i].TreatmentTax;
                    }
                    return tax;
                }
                return 0;
            }
        }
        public bool IsTreatmentView { get; set; }
        public int UserPoints { get; set; }
        public double TreatmentsPrice { get { return Math.Round(InvoiceItems.Sum(m => m.TreatmentPrice), 2); } }
        public double Discount { get { return Math.Round(InvoiceItems.Sum(m => m.Discount), 2); } }
        public double CommissionPaid { get { var commissionPaid = Math.Round(InvoiceItems.Sum(m => m.StylistCommission), 2); return commissionPaid; } }
        public List<TreatmentReportViewModel> InvoiceItems { get; set; }
        public bool IsPaidByPin { get; set; }
        public string InvoiceNumber { get; set; }
    }

    public class InvoiceReportSearchViewModel : GeneralSearchViewModel
    {
        public Guid Id { get; set; }
        public DateTime DateTime { get; set; }
        public string ClientName { get; set; }
        public string ClientId { get; set; }
        public string StylistName { get; set; }
        public string StylistId { get; set; }
        public string TreatmentName { get; set; }
        public Guid TreatmentId { get; set; }
        private DateTime _endingDateTime;
        public bool IsTreatmentView { get; set; }
        public Guid InvoiceId { get; set; }
        public Guid InvoiceItemId { get; set; }
        public DateTime EndingDateTime
        {
            get
            {
                _endingDateTime = _endingDateTime == new DateTime() && DateTime != new DateTime() ? DateTime.AddDays(7).AddMinutes(-1) : _endingDateTime;
                return _endingDateTime;
            }
            set
            {
                _endingDateTime = value;
            }
        }
        public bool DashboardView { get; set; }


    }
}
