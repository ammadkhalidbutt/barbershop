﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class TreatmentReportViewModel
    {
        public Guid InvoiceItemId { get; set; }
        public string StylistId { get; set; }
        public string StylistName { get; set; }
        public double StylistCommission { get; set; }
        public int PointsUsed { get; set; }
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public Guid TreatmentId { get; set; }
        public string TreatmentName { get; set; }
        public double TreatmentPrice { get; set; }
        public float TreatmentTax { get; set; }
        public DateTime InvoiceDateTime { get; set; }
        public double AmountRecieved { get; set; }
        public float Discount { get; set; }
        public DateTime DateTime { get; set; }
    }

    public class TreatmentReportSearchViewModel : GeneralSearchViewModel
    {
        public Guid InvoiceItemId { get; set; }
        public Guid InvoiceId { get; set; }
        public DateTime DateTime { get; set; }
        private DateTime _endingDateTime;
        public DateTime EndingDateTime
        {
            get
            {
                _endingDateTime = _endingDateTime == new DateTime() && DateTime != new DateTime() ? DateTime.AddDays(1).AddMinutes(-1) : _endingDateTime;
                return _endingDateTime;
            }
            set
            {
                _endingDateTime = value;
            }
        }
        public Guid TreatmentId { get; set; }
        public string TreatmentName { get; set; }
        public string StylistId { get; set; }
        public string StylistName { get; set; }
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public bool DashboardView { get; set; }
    }
}
