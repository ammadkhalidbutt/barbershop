﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class StylistTreatmentViewModel
    {
        public Guid TreatmentId { get; set; }
        public string StylistId { get; set; }
        public string TreatmentName { get; set; }
        public DateTime Date { get; set; }
        public bool Active { get; set; }
        public string Action { get; set; }
        public float PercentageCommission { get; set; }
        public bool IsDeleted { get; set; }
        public bool Speciality { get; set; }
        public List<Select2OptionModel> StylistsList { get; set; }
        public string[] SelectedSytlists { get; set; }
    }

    public class StylistTreatmentSearchViewModel : GeneralSearchViewModel
    {
        public Guid TreatmentId { get; set; }
        public string StylistId { get; set; }
    }
}
