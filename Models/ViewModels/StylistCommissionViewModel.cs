﻿using Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class StylistCommissionViewModel
    {
        public Guid Id { get; set; }
        public Guid AppointmentId { get; set; }
        public Appointment Appointment { get; set; }
        public Guid TreatmentId { get; set; }
        public Treatment Treatment { get; set; }
        public string StylistId { get; set; }
        public ApplicationUser Stylist { get; set; }
        public float Amount { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class StylistCommissionSearchViewModel : GeneralSearchViewModel
    {
        public StylistCommissionSearchViewModel()
        {
            Appointment = new Appointment();
            Treatment = new Treatment();
            Stylist = new ApplicationUser();
        }
        public Guid Id { get; set; }
        public Appointment Appointment { get; set; }
        public Treatment Treatment { get; set; }
        public ApplicationUser Stylist { get; set; }
        public float Amount { get; set; }
        private DateTime _startingDate;
        public DateTime StartingDate {
            get
            {
                _startingDate = _startingDate != new DateTime() ? _startingDate.Date : _startingDate;
                return _startingDate;
            }
            set
            {
                _startingDate = value;
            }
        }
        private DateTime _endingDate;
        public DateTime EndingDate {
            get
            {
                _endingDate = _endingDate == new DateTime() ? StartingDate.AddDays(1).AddMinutes(-1) : _endingDate;
                return _endingDate;
            }
            set
            {
                _endingDate = value;
            }
        }
    }
}
