﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class AttendanceDashboardViewModel
    {
        public List<UserAttendanceViewModel> ClientList { get; set; }
        public List<UserAttendanceViewModel> StylistList { get; set; }
        public List<TreatmentViewModel> TreatmentList { get; set; }
    }
}
