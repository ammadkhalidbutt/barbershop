﻿using Enums;
using Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class InvoiceRecordViewModel
    {
        public InvoiceRecordViewModel()
        {
            InvoiceItems = new List<InvoiceItemViewModel>();
        }
        public Guid Id { get; set; }
        public ApplicationUser Client { get; set; }
        public string StylistId { get; set; }
        public InvoiceStatus Status { get; set; }
        public AppointmentTreatmentStatus AppointmentStatus { get; set; }
        public DateTime DateTime { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public double TotalPayment { get; set; }
        public double GrandTotal
        {
            get
            {
                return Total + Total * (Vat / 100);
            }
        }
        public double Total { get; set; }
        public double Vat { get; set; }
        public double AmountRecieved { get; set; }
        public double AmountGiven { get; set; }
        public int PointsUsed { get; set; }
        public int PointsPurchased { get; set; }
        public int UserPoints { get; set; }
        public string InvoiceNumber { get; set; }
        public bool IsPaidByPin { get; set; }
        public List<InvoiceItemViewModel> InvoiceItems { get; set; }
    }

    public class InvoiceRecordSearchViewModel : GeneralSearchViewModel
    {
        public Guid Id { get; set; }
        public ApplicationUser Client { get; set; }
        public DateTime DateTime { get; set; }
        public InvoiceStatus Status { get; set; }
        public Guid InvoiceItemId { get; set; }
        public Guid InvoiceId { get; set; }
    }
}
