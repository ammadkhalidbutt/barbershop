﻿using Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class ClientPointViewModel
    {
        public Guid Id { get; set; }
        public string ClientId { get; set; }
        public ApplicationUser Client { get; set; }
        public int Points { get; set; }
        public DateTime Date { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public uint ReferenceId { get; set; }
        public InvoiceRecord InvoiceRecord { get; set; }
        public Guid AppointmentId { get; set; }
        public Appointment Appointment { get; set; }
        public bool Purchased { get; set; }
    }

    public class ClientPointSearchViewModel : GeneralSearchViewModel
    {
        public ClientPointSearchViewModel()
        {
            Appointment = new Appointment();
            Client = new ApplicationUser();
        }
        public ApplicationUser Client { get; set; }
        public uint ReferenceNumber { get; set; }
        public Guid InvoiceId { get; set; }
        public int Points { get; set; }
        public DateTime Date { get; set; }
        public DateTime FromDate { get; set; }
        public bool IsDeleted { get; set; }
        public bool Purchased { get; set; }
        public Appointment Appointment { get; set; }
    }
}

