﻿using Enums;
using Models.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Models.ViewModels
{
    public class AppointmentViewModel
    {
        public AppointmentViewModel()
        {
            Client = new ApplicationUser();
            Stylist = new ApplicationUser();
            Treatment = new TreatmentViewModel();
            Product = new ProductViewModel();
        }
        public Guid? Id { get; set; }
        [Required]
        public string ClientId { get; set; }

        public ApplicationUser Client { get; set; }

        public Address ClientAddress { get; set; }

        public string ClientName { get; set; }

        public float TreatmentTax { get; set; }

        public float PointWorth { get; set; }

        public Branch Branch { get; set; }

        public Address BranchAddress { get; set; }

        public Guid ApplicationId { get; set; }

        [IgnoreDataMember]
        public List<SelectListItem> ClientsList { get; set; }
        [IgnoreDataMember]
        public List<SelectListItem> TreatmentsList { get; set; }
        [IgnoreDataMember]
        public List<SelectListItem> StylistsList { get; set; }

        public DateTime BookingDate { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        [Required]
        public DateTime AppointmentStartingTime { get; set; }
        public DateTime AppointmentEndingTime { get; set; }
        public DateTime ActualStartingTime { get; set; }
        public DateTime ActualEndingTime { get; set; }
        public bool Active { get; set; }
        public bool IsDeleted { get; set; }
        public AppointmentStatus Status { get; set; }
        public AppointmentTreatmentStatus AppointmentTreatmentStatus { get; set; }
        [Range(0, 100)]
        public float Discount { get; set; }
        public float AmountRecieved { get; set; }
        [Required]
        public Guid TreatmentId { get; set; }
        public TreatmentViewModel Treatment { get; set; }
        public ProductViewModel Product { get; set; }
        [Required]
        public string StylistId { get; set; }
        public ApplicationUser Stylist { get; set; }
        public string Action { get; set; }
        public int TotalTime { get; set; }
        public float ElapsedTime { get; set; }
        public ApplicationUser Approver { get; set; }
        public ClientPoint ClientPoints { get; set; }
        public bool IsDashboard { get; set; }
        public bool IsForStart { get; set; }
        public SlotStatus SlotStatus { get; set; }
        public bool IsReccursive { get; set; }
        public string Description { get; set; }
        public string Subject { get { return Client.Name; } }
        public DateTime Start { get { return AppointmentStartingTime; } }
        public DateTime End { get { return AppointmentEndingTime; } }
        public string ThemeColor { get { return "blue"; } }
        public bool IsFullDay { get; set; }
        public bool IsEditAllowed { get; set; }
        public double GrandTotal { get; set; }
        public int PointsUsed { get; set; }
    }



    public class AppointmentDisplayModel
    {
        public List<AppointmentViewModel> Appointments { get; set; }
    }

    public class AppointmentApprovalViewModel
    {
        public Guid AppointmentId { get { return Appointment.Id; } }
        public string ApproverId { get { return Approval.ApproverId; } }
        public DateTime ApprovalDateTime { get { return Approval.ApprovalDateTime; } }
        public DateTime LastUpdatedOn { get { return Approval.LastUpdatedOn; } }

        [Required]
        public AppointmentStatus Status { get; set; }
        public string Action { get; set; }

        private ApplicationUser _stylist;
        private ApplicationUser _client;
        private Treatment _treatment;
        private ApplicationUser _approver;
        private AppointmentApproval _approval;
        private Appointment _appointment;

        public ApplicationUser Stylist { get { _stylist = _stylist ?? new ApplicationUser(); return _stylist; } set { _stylist = value; } }
        public ApplicationUser Client { get { _client = _client ?? new ApplicationUser(); return _client; } set { _client = value; } }
        public Treatment Treatment { get { _treatment = _treatment ?? new Treatment(); return _treatment; } set { _treatment = value; } }
        public ApplicationUser Approver { get { _approver = _approver ?? new ApplicationUser(); return _approver; } set { _approver = value; } }
        public AppointmentApproval Approval { get { _approval = _approval ?? new AppointmentApproval(); return _approval; } set { _approval = value; } }
        public Appointment Appointment { get { _appointment = _appointment ?? new Appointment(); return _appointment; } set { _appointment = value; } }



    }

    public class AppointmentTimeSlot
    {
        public DateTime StartingTime { get; set; }
        public DateTime EndingTime { get; set; }
        public TimeSpan Duration { get { return EndingTime.Subtract(StartingTime); } }
        public SlotStatus Status { get; set; }
        public string Stylist { get; set; }
        public string StylistId { get; set; }
        public string Client { get; set; }
        public string Treatment { get; set; }
        public string Description { get; set; }
        public Guid LeaveReferenceId { get; set; }
        public Guid Id { get; set; }
    }

    public class AppointmentSlotSearchViewModel
    {
        public Guid DiscardAppointment { get; set; }
        public DateTime DesiredDate { get; set; }
        public int TimeRequiredForTreatment { get; set; }
        public string SytlistId { get; set; }
        public string ClientId { get; set; }
        public bool GetAllSlots { get; set; }

    }

    public class AppointmentHistoryViewModel
    {
        public Guid Id { get; set; }
        public string ClientId { get; set; }
        public string StylistId { get; set; }
        public Guid TreatmentId { get; set; }
        public string TreatmentName { get; set; }
        public string StylistName { get; set; }
        public string ClientName { get; set; }
        public double Cost { get; set; }
        public double Discount { get; set; }
        public int PointsUsed { get; set; }
        public int PointsGained { get; set; }
        public DateTime AppointmentStart { get; set; }
        public DateTime AppointmentEnd { get; set; }
        public DateTime Date { get; set; }
    }

    public class AppointmentSearchViewModel : GeneralSearchViewModel
    {
        public AppointmentSearchViewModel()
        {
            MultiAppointmentTreatmentStatus = new List<int>();
        }
        public Guid Id { get; set; }
        public Guid DiscardAppointment { get; set; }
        public string DiscardAppointmentString { get; set; }
        public int TotalTime { get; set; }
        public bool SkipDateCheck { get; set; }
        public DateTime BookingDate { get; set; }
        private DateTime _bookingDateEnding;
        public DateTime BookingDateEnding { get { _bookingDateEnding = _bookingDateEnding == new DateTime() ? BookingDate.AddDays(1).AddMinutes(-1) : _bookingDateEnding; return _bookingDateEnding; } set { _bookingDateEnding = value; } }
        public string ApproverId { get; set; }
        public string StylistId { get; set; }
        public Guid TreatmentId { get; set; }
        public string ApproverName { get; set; }
        public string StylistName { get; set; }
        public string TreatmentName { get; set; }
        public AppointmentStatus Status { get; set; }
        public bool GetDetailedResult { get; set; }
        public bool TimeSlotsResult { get; set; }
        public bool VerifyTime { get; set; }
        public AppointmentTreatmentStatus AppointmentTreatmentStatus { get; set; }
        public List<int> MultiAppointmentTreatmentStatus { get; set; }
        public string TreatmentStatuses { get; set; }
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public DateTime StartingDateTime { get; set; }
        public DateTime EndingDateTime { get; set; }
        public string CalledFor { get; set; }
        public bool NotIndex { get; set; }
        public bool GetAllSlots { get; set; }
        public bool IsEditAllowed { get; set; }
    }

    public class AppointmentApprovalSearchViewModel : GeneralSearchViewModel
    {
        public Guid AppointmentId { get; set; }
        public AppointmentStatus Status { get; set; }
        public string ApproverId { get; set; }
        public Guid ApprovedAppointmentId { get; set; }
    }

    public class AppointmentTimeSlotsViewModel
    {
        public List<AppointmentTimeSlot> AppointedSlots { get; set; }
        public List<AppointmentTimeSlot> FreeSlots { get; set; }
    }


    public class AppointmentViewModelStylistDashboard
    {
        public string StylistId { get; set; }
        public string ClientId { get; set; }
        public Guid StartedTreatment { get; set; }
        public List<string> TreatmentList { get; set; }
    }

}
