﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class AddressViewModel
    {
        public Guid Id { get; set; }
        public string City { get; set; }
        [Display(Name = "Post Code")]
        public string Area { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public bool Active { get; set; }
    }

    public class AddressSearchViewModel : GeneralSearchViewModel
    {
        public Guid Id { get; set; }
        public string City { get; set; }
        public string Area { get; set; }
        public bool Active { get; set; }
    }
}
