﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class StylistCommissionReportViewModel
    {
        public Guid CommissionId { get; set; }
        public string StylistName { get; set; }
        public string StylistId { get; set; }
        private double _commission;
        public double Commission { get { return Math.Round(_commission, 2); } set { _commission = value; } }
        public DateTime Date { get; set; }
        public int NumberOfTreatments { get; set; }
    }

    public class StylistCommissionReportSearchViewModel : GeneralSearchViewModel
    {
        public string StylistName { get; set; }
        public string StylistId { get; set; }
        public Guid TreatmentId { get; set; }
        public string TreatmentName { get; set; }
        public DateTime DateTime { get; set; }
        private DateTime _endingDateTime;
        public DateTime EndingDateTime
        {
            get
            {
                _endingDateTime = _endingDateTime == new DateTime() && DateTime != new DateTime() ? DateTime.AddDays(1).AddMinutes(-1) : _endingDateTime;
                return _endingDateTime;
            }
            set
            {
                _endingDateTime = value;
            }
        }
        public bool DashboardView { get; set; }

    }
}
