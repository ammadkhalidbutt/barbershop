﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
   public class FullCalendarViewModel
    {
        public int EventID { get; set; }
        public string  Subject { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string ThemeColor { get; set; }
        public bool IsFullDay { get; set; }
    }

    public class FullCalendarSchedular
    {
        public List<FullCalendarResources> resources { get; set; }
        public List<FullCalendarEvents> events { get; set; }
        public AppointmentViewModel Appointment { get; set; }
    }

    public class FullCalendarResources
    {
        public string id { get; set; }
        public string title { get; set; }
        public string eventColor { get; set; }
    }

    public class FullCalendarEvents
    {
        public string id { get; set; }
        public string resourceId { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string title { get; set; }
        public string bordercolor { get; set; }
        public string treatment { get; set; }
        public string stylist { get; set; }
        public string client { get; set; }
        public string description { get; set; }
        public bool isRecurring { get; set; }
        public string bookingDate { get; set; }
        public string serealizedObject;
    }
}
