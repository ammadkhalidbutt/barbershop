﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class SearchResultViewModel<T>
    {

        public int TotalCount { get; set; }
        public List<T> ResultList
        {
            get;set;
        }
    }
    
}
