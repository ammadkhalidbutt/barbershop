﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class PointViewModel
    {
        public Guid Id { get; set; }
        public string Expiry { get; set; }
        public float Worth { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public string Action { get; set; }
    }

}
