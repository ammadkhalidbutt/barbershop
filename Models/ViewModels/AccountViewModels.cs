﻿using Enums;
using Foolproof;
using Microsoft.AspNet.Identity.EntityFramework;
using Models;
using QRCoder;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace Models.ViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class UserAttendanceViewModel
    {
        public string Id { get; set; }
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        public Gender Gender { get; set; }
        [Required]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Refernce Id")]
        public string ReferenceId { get; set; }
        public string Role { get; set; }
        public string Action { get; set; }
        public string ImageUrl { get; set; }
        public DateTime AttendanceDate { get; set; }
        public string FormattedName
        {
            get
            {
                return Name + (string.IsNullOrEmpty(PhoneNumber) ? "" : (" - " + PhoneNumber));
            }
        }
    }

    public class RegisterViewModel
    {
        public string Id { get; set; }
        [EmailAddress(ErrorMessage = "Email is not valid")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        //[RegularExpression(@"^((?=.*[A-Z])(?=.*\d)(?=.*[a-z])|(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%&\/=?_.-])|(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%&\/=?_.-])|(?=.*\d)(?=.*[a-z])(?=.*[!@#$%&\/=?_.-])).{6,15}$", ErrorMessage = "Passwords must have at least one digit ('0'-'9'). Passwords must have at least one uppercase ('A'-'Z').")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        public DateTime ComparativeDate { get { return DateTime.Now; } }
        private DateTime _DOB;
        [Required]
        [Display(Name = "Date of Birth")]
        [DataType(DataType.DateTime)]
        [LessThan("ComparativeDate", ErrorMessage = "Date of birth must be less than today.")]
        public DateTime DOB
        {
            get
            {
                _DOB = _DOB == new DateTime() ? new DateTime(1980, 01, 01) : _DOB;
                return _DOB;
            }
            set
            {
                _DOB = value;
            }
        }
        public Gender Gender { get; set; }
        [Required]
        [Display(Name = "Phone Number")]
        //[Remote("DoesPhoneNumberExist", "Account", AdditionalFields = "Id",
        //        ErrorMessage = "Phone number already exists")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Refernce Id")]
        public string ReferenceId { get; set; }
        [Display(Name = "Branch")]
        public Guid BranchId { get; set; }
        [Display(Name = "Application")]
        public Guid ApplicaitonId { get; set; }
        public string Role { get; set; }
        public string Action { get; set; }
        public bool IsRegistered { get; set; }
        public bool Active { get; set; }
        public bool Error { get; set; }
        public ICollection<IdentityUserRole> Roles { get; set; }
        public HttpPostedFileBase Image { get; set; }
        public string ImageUrl { get; set; }
        public AddressViewModel Address { get; set; }
        public int Age
        {
            get
            {
                return DateTime.Now.Year - DOB.Year;
            }
        }

        public string StylistId { get; set; }

        public string TreatmentName { get; set; }
        public float Percentage { get; set; }
        public Guid TreatmentId { get; set; }
        public bool Speciality { get; set; }
        public DateTime TreatmentDate { get; set; }
        public string QRCodeImage
        {
            get
            {
                string QrCodeImage = "";
                using (MemoryStream ms = new MemoryStream())
                {
                    if (!string.IsNullOrEmpty(this.Id))
                    {
                        QRCodeGenerator qrGenerator = new QRCodeGenerator();
                        QRCodeData qrCodeData = qrGenerator.CreateQrCode(this.Id, QRCodeGenerator.ECCLevel.Q);
                        QRCode qrCode = new QRCode(qrCodeData);
                        using (Bitmap qrCodeImage = qrCode.GetGraphic(20))
                        {
                            qrCodeImage.Save(ms, ImageFormat.Png);
                            QrCodeImage = "data:image/png;base64," + Convert.ToBase64String(ms.ToArray());
                        }
                    }

                }
                return QrCodeImage;
            }
        }
        public int Points { get; set; }

        public bool CheckIn { get; set; }

    }

    public class AccountSearchViewModel : GeneralSearchViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string RoleId { get; set; }
        public Guid TreatmentId { get; set; }
        public bool GetDeleted { get; set; }
        public string PhoneNumber { get; set; }
        public Guid AvoidTreatmentId { get; set; }
        public bool AppendedView { get; set; }
        public string Role { get; set; }
        public bool IsClient { get; set; }
        public bool GetGroupedRecords { get; set; }
        public bool GetAllResults { get; set; }
    }

    public class RoleViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class RoleSearchViewModel : GeneralSearchViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [RegularExpression(@"^((?=.*[A-Z])(?=.*\d)(?=.*[a-z])|(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%&\/=?_.-])|(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%&\/=?_.-])|(?=.*\d)(?=.*[a-z])(?=.*[!@#$%&\/=?_.-])).{6,15}$", ErrorMessage = "Passwords must have at least one digit ('0'-'9'). Passwords must have at least one uppercase ('A'-'Z').")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }

        public string UserId { get; set; }
        public string Role { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
