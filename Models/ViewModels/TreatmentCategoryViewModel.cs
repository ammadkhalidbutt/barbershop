﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Models.ViewModels
{
    public class TreatmentCategoryViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public HttpPostedFileBase UploadedImage { get; set; }
        public string Image { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string Action { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class TreatmentCategorySearchViewModel : GeneralSearchViewModel
    {
        public Guid Id { get; set; }
        public string StylistId { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
    }
}
