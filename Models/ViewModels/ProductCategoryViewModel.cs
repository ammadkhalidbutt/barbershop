﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Models.ViewModels
{
    public class ProductCategoryViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public HttpPostedFileBase UploadedImage { get; set; }
        public string Image { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string Action { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class ProductCategorySearchViewModel : GeneralSearchViewModel
    {
        public ProductCategorySearchViewModel()
        {
            IdList = new List<Guid>();
        }
        public List<Guid> IdList { get; set; }
        public string Name { get; set; }
    }
}
