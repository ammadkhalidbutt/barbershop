﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class CurrencyViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class CurrencySearchViewModel : GeneralSearchViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
