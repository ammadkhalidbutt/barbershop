﻿using Enums;
using Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class StylistScheduleViewModel
    {
        public Guid Id { get; set; }
        public ApplicationUser Stylist { get; set; }
        public TimeSpan StartingTime { get; set; }
        public TimeSpan EndingTime { get; set; }
        public WeekDays Day { get; set; }
    }

    public class StylistScheduleListViewModel
    {
        public StylistScheduleListViewModel()
        {
            JqsList = new List<JqsDayViewModel>();
            ScheduleList = new List<StylistScheduleViewModel>();
        }
        public List<JqsDayViewModel> JqsList { get; set; }
        public List<StylistScheduleViewModel> ScheduleList { get; set; }
        public string Action { get; set; }
        public string StylistId { get; set; }
    }

    public class StylistScheduleSearchViewModel : GeneralSearchViewModel
    {
        public StylistScheduleSearchViewModel()
        {
            Stylist = new ApplicationUser();
        }
        public Guid Id { get; set; }
        public ApplicationUser Stylist { get; set; }
        public WeekDays Day { get; set; }
        public TimeSpan StartingTime { get; set; }
        public bool OneTimeOnly { get; set; }
        public bool IsDeleted { get; set; }
    }
}
