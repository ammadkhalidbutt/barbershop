﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class ApplicationViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
    }

    public class ApplicationSearchViewModel : GeneralSearchViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
