﻿using Enums;
using Foolproof;
using Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Models.ViewModels
{
    public class LeaveViewModel
    {
        public LeaveViewModel()
        {
            StylistList = new List<SelectListItem>();
        }
        public Guid Id { get; set; }
        public Guid ReferenceId { get; set; }
        public ApprovalStatus Status { get; set; }
        [Required(ErrorMessage = "Select leave type")]
        public LeaveType Type { get; set; }
        public ApplicationUser Stylist { get; set; }
        public List<SelectListItem> StylistList { get; set; }
        [Display(Name = "Delete")]
        public bool IsDeleted { get; set; }
        public string Action { get; set; }
        public Recursion Recursion { get; set; }
        public bool IsContinuous { get; set; }
        public bool IsDashboard { get; set; }
        public string Note { get; set; }
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Select a date")]
        public DateTime StartingDate { get; set; }
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Select a date")]
        [GreaterThanOrEqualTo("StartingDate", ErrorMessage = "Ending date must be greater than starting date")]
        public DateTime EndingDate { get; set; }
        [Required(ErrorMessage = "Select a time")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        [DataType(DataType.Time)]
        public TimeSpan DailyStartTime { get; set; }
        [Required(ErrorMessage = "Select a time")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        [DataType(DataType.Time)]
        [GreaterThanOrEqualTo("DailyStartTime", ErrorMessage = "Ending time must be greater than starting time")]
        public TimeSpan DailyEndTime { get; set; }
        public string DaysArray { get; set; }
        public string RecursionParameter { get; set; }
        public List<string> RecursiveDays { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }

    }

    public class TimeLessThanAttribute : ValidationAttribute
    {
        private readonly string _comparisonProperty;

        public TimeLessThanAttribute(string comparisonProperty)
        {
            _comparisonProperty = comparisonProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ErrorMessage = ErrorMessageString;
            var currentValue = (TimeSpan)value;

            var property = validationContext.ObjectType.GetProperty(_comparisonProperty);

            if (property == null)
                throw new ArgumentException("Property with this name not found");

            var comparisonValue = (TimeSpan)property.GetValue(validationContext.ObjectInstance);

            if (currentValue > comparisonValue)
                return new ValidationResult(ErrorMessage);

            return ValidationResult.Success;
        }
    }


    public class LeaveSearchViewModel : GeneralSearchViewModel
    {
        public LeaveSearchViewModel()
        {
            Stylist = new ApplicationUser { Id=""};
        }
        public Guid Id { get; set; }
        public Guid ReferenceId { get; set; }
        public ApplicationUser Stylist { get; set; }
        public DateTime StartingTime { get; set; }
        public DateTime EndingTime { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get { return StartDate.Date.AddDays(1).AddMilliseconds(-1); } }
        public bool IsDeleted { get; set; }
        public Recursion Recursion { get; set; }
        public Recursion RecursionBeside { get; set; }
        public bool IsContinuous { get; set; }
        public DateTime RecursionEndDate { get; set; }

    }
}
