﻿using Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Models.ViewModels
{
    public class DiscountViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string PromoCode { get; set; }
        public string ImageUrl { get; set; }
        public HttpPostedFileBase Image { get; set; }
        [Required]
        public double DiscountPercentage { get; set; }
        public BriefEntityViewModel Entity { get; set; }
        public DiscountEntityTypeCatalog EntityType { get; set; }
        public int Priority { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime StartDateTime { get; set; } = DateTime.Today;
        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime EndDateTime { get; set; } = DateTime.Today.AddDays(1).AddMilliseconds(-1);
        public string Description { get; set; }
        [Required]
        public string NotificationTitle { get; set; }
        [Required]
        public string NotificationBody { get; set; }
        public bool IsExpired
        {
            get
            {
                return DateTime.Now > EndDateTime;
            }
        }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool IsApproved { get; set; }
        public Guid BranchId { get; set; }
        public Guid ApplicationId { get; set; }

        public string Action { get; set; }
        public List<SelectListItem> EntityList { get; set; }
    }
    public class DiscountSearchViewModel : GeneralSearchViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid ClientId { get; set; }
    }


}
