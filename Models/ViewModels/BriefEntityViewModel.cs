﻿using System;

namespace Models.ViewModels
{
    public class BriefEntityViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
