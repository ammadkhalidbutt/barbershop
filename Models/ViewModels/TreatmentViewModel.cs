﻿using Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Models.ViewModels
{
    public class TreatmentViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string AddedBy { get; set; }
        public ApplicationUser User { get; set; }
        public int Points { get; set; }
        public float Price { get; set; }
        public bool Active { get; set; }
        public bool IsDeleted { get; set; }
        //for treatments created dynamically in appointment as client requested.
        public bool IsDynamicallyAdded { get; set; }
        [Required]
        [Display(Name ="Time Required")]
        public int ApproximateTime { get; set; }
        public DateTime Date { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public string ImageUrl { get; set; }
        public HttpPostedFileBase Image { get; set; }
        public string Description { get; set; }
        public Tax Tax { get; set; }
        [Required]
        public Guid TaxId { get; set; }
        [Required]
        public Guid CategoryId { get; set; }

        public string Action { get; set; }
        public TreatmentCategory Category { get; set; }
        public List<SelectListItem> CategoryList { get; set; }
    }

    public class TreatmentSearchViewModel : GeneralSearchViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string StylistId { get; set; }
        public string StylistName { get; set; }
        public Guid CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int Points { get; set; }
    }


}
