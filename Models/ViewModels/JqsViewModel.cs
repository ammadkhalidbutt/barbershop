﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class JqsPeriodViewModel
    {
        public string Id { get; set; }
        public string start { set; get; }
        public string end { get; set; }
        public string title { get; set; }
    }

    public class JqsDayViewModel
    {
        public JqsDayViewModel()
        {
            periods = new List<JqsPeriodViewModel>();
        }
        public int day;
        public int Day { get { return day; } set { day = value; } }
        public List<JqsPeriodViewModel> periods { get; set; }
        //public List<JqsPeriodViewModel> Periods { get { return periods; } set { periods = periods == new List<JqsPeriodViewModel>() ? value : periods ; } }
    }
}
