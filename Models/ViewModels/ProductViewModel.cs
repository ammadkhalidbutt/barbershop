﻿using Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Models.ViewModels
{
    public class ProductViewModel
    {
        public ProductViewModel()
        {
            Active = true;
        }
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string AddedBy { get; set; }
        public ApplicationUser User { get; set; }
        public ProductCategory Category { get; set; }
        public Guid CategoryId { get; set; }
        public double Price { get; set; }
        public bool Active { get; set; }
        public int Quantity { get; set; }
        public bool IsDeleted { get; set; }
        //for treatments created dynamically in appointment as client requested.
        public bool IsDynamicallyAdded { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public HttpPostedFileBase Image { get; set; }
        public Tax Tax { get; set; }
        [Required]
        public Guid TaxId { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public string UpdatedBy { get; set; }
        public string Action { get; set; }
        public DateTime UpdatedOn { get; set; }
    }

    public class ProductSearchViewModel : GeneralSearchViewModel
    {
        public ProductSearchViewModel()
        {
            ProductIds = new List<Guid>();
        }
        public Guid Id { get; set; }
        public Guid CategoryId { get; set; }
        public string Name { get; set; }
        public List<Guid> ProductIds { get; set; }
    }
}
