﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class LoggedInUserInformation
    {
        public string UserId { get; set; }
        public Guid AppId { get; set; }
        public Guid BranchId { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
    }
}
