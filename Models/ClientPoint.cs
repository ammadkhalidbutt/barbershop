﻿using Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ClientPoint
    {
        public Guid Id { get; set; }
        [ForeignKey("Client")]
        public string ClientId { get; set; }
        public ApplicationUser Client { get; set; }
        [ForeignKey("InvoiceRecord")]
        public Guid InvoiceId { get; set; }
        public InvoiceRecord InvoiceRecord { get; set; }
        public int Points { get; set; }
        public DateTime Date { get; set; }
        public bool IsDeleted { get; set; }
        public bool Purchased { get; set; }
        public DateTime LastUpdatedOn { get; set; }

    }
}
