﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Enums;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Models.Identity
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        public Gender Gender { get; set; }
        public bool Active { get; set; }
        public string QRCode { get; set; }
        [ForeignKey("Branch")]
        public Guid BranchId { get; set; }
        public Branch Branch { get; set; }
        [ForeignKey("Application")]
        public Guid ApplicationId { get; set; }
        public Application Application { get; set; }
        [ForeignKey("Address")]
        public Guid AddressId { get; set; }
        public Address Address { get; set; }
        public string ImageUrl { get; set; }
        public Guid ReferenceId { get; set; }
        public string DeviceId { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Treatment> Treatments { get; set; }
        public DbSet<StylistTreatment> StylistTreatments { get; set; }
        public DbSet<Attendance> UserAttendances { get; set; }
        public DbSet<ClientPoint> ClientPoints { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<AppointmentApproval> AppointmentApprovals { get; set; }
        public DbSet<AppointmentDetails> AppointmentDetails { get; set; }
        public DbSet<StylistCommission> StylistCommissions { get; set; }
        public DbSet<Application> Applications { get; set; }
        public DbSet<Machine> Machines { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Point> Points { get; set; }
        public DbSet<InvoiceRecord> InvoiceRecords { get; set; }
        public DbSet<InvoiceItem> InvoiceItems { get; set; }
        public DbSet<StylistSchedule> StylistSchedules { get; set; }
        public DbSet<Leave> Leaves { get; set; }
        public DbSet<TreatmentCategory> TreatmentCategories { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Tax> Taxes { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<Discount> Discounts { get; set; }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}