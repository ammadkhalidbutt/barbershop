﻿using Enums;
using Foolproof;
using Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Leave
    {
        public Guid Id { get; set; }

        public Guid ReferenceId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Required(ErrorMessage = "Select a date")]
        public DateTime StartingDate { get; set; }

        [DataType(DataType.Date)]
        [GreaterThanOrEqualTo("StartingDate")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Required(ErrorMessage = "Select a date")]
        public DateTime EndingDate { get; set; }

        public ApprovalStatus Status { get; set; }
        [Required(ErrorMessage = "Select leave type")]
        public LeaveType Type { get; set; }
        [ForeignKey("Stylist")]
        public string StylistId { get; set; }
        public ApplicationUser Stylist { get; set; }
        [Display(Name = "Delete")]
        public bool IsDeleted { get; set; }
        public Recursion Recursion { get; set; }
        public bool IsContinuous { get; set; }
        public string Note { get; set; }
        public string RecursionParameter { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
