﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Discount
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string PromoCode { get; set; }
        public string ImageUrl { get; set; }
        [Required]
        public double DiscountPercentage { get; set; }
        public Guid EntityId { get; set; }
        public int EntityType { get; set; }
        public int Priority { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string Description { get; set; }
        public string NotificationTitle { get; set; }
        public string NotificationBody { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool IsApproved { get; set; }
        public Guid ApplicationId { get; set; }
        public Guid BranchId { get; set; }

    }
}
