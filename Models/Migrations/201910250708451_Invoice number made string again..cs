namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Invoicenumbermadestringagain : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.InvoiceRecords", "InvoiceNumber", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.InvoiceRecords", "InvoiceNumber", c => c.Int(nullable: false, identity: true));
        }
    }
}
