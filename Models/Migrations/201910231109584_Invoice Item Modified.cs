namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InvoiceItemModified : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceItems", "ItemId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvoiceItems", "ItemId");
        }
    }
}
