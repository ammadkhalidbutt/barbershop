namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    City = c.String(),
                    Area = c.String(),
                    Street = c.String(),
                    House = c.Int(nullable: false),
                    Active = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Applications",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Name = c.String(),
                    Active = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.AppointmentApprovals",
                c => new
                {
                    AppointmentId = c.Guid(nullable: false),
                    ApproverId = c.String(nullable: false, maxLength: 128),
                    ApprovalDateTime = c.DateTime(nullable: false),
                    LastUpdatedOn = c.DateTime(nullable: false),
                    Status = c.Int(nullable: false),
                    Active = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => new { t.AppointmentId, t.ApproverId })
                .ForeignKey("dbo.Appointments", t => t.AppointmentId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.ApproverId, cascadeDelete: false)
                .Index(t => t.AppointmentId)
                .Index(t => t.ApproverId);

            CreateTable(
                "dbo.Appointments",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    ClientId = c.String(maxLength: 128),
                    BranchId = c.Guid(nullable: false),
                    ApplicationId = c.Guid(nullable: false),
                    BookingDate = c.DateTime(nullable: false),
                    AppointmentStartingTime = c.DateTime(nullable: false),
                    AppointmentEndingTime = c.DateTime(nullable: false),
                    Active = c.Boolean(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    Discount = c.Single(nullable: false),
                    Status = c.Int(nullable: false),
                    StylistId = c.String(nullable: false, maxLength: 128),
                    TreatmentId = c.Guid(nullable: false),
                    LastUpdatedOn = c.DateTime(nullable: false),
                    //TimeElapsed = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Applications", t => t.ApplicationId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.ClientId)
                .ForeignKey("dbo.AspNetUsers", t => t.StylistId, cascadeDelete: false)
                .ForeignKey("dbo.Treatments", t => t.TreatmentId, cascadeDelete: false)
                .Index(t => t.ClientId)
                .Index(t => t.BranchId)
                .Index(t => t.ApplicationId)
                .Index(t => t.StylistId)
                .Index(t => t.TreatmentId);

            CreateTable(
                "dbo.Branches",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Name = c.String(),
                    ApplicationId = c.Guid(nullable: false),
                    AddressId = c.Guid(nullable: false),
                    Active = c.Boolean(nullable: false),
                    ImageUrl = c.String(),
                    OpeningTime = c.Time(nullable: false, precision: 7),
                    ClosingTime = c.Time(nullable: false, precision: 7),
                    VAT = c.Single(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.AddressId, cascadeDelete: true)
                .Index(t => t.AddressId);

            CreateTable(
                "dbo.AspNetUsers",
                c => new
                {
                    Id = c.String(nullable: false, maxLength: 128),
                    Name = c.String(),
                    DOB = c.DateTime(nullable: false),
                    Gender = c.Int(nullable: false),
                    Active = c.Boolean(nullable: false),
                    QRCode = c.String(),
                    BranchId = c.Guid(nullable: false),
                    ApplicationId = c.Guid(nullable: false),
                    AddressId = c.Guid(nullable: false),
                    ImageUrl = c.String(),
                    Email = c.String(maxLength: 256),
                    EmailConfirmed = c.Boolean(nullable: false),
                    PasswordHash = c.String(),
                    SecurityStamp = c.String(),
                    PhoneNumber = c.String(),
                    PhoneNumberConfirmed = c.Boolean(nullable: false),
                    TwoFactorEnabled = c.Boolean(nullable: false),
                    LockoutEndDateUtc = c.DateTime(),
                    LockoutEnabled = c.Boolean(nullable: false),
                    AccessFailedCount = c.Int(nullable: false),
                    UserName = c.String(nullable: false, maxLength: 256),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.AddressId, cascadeDelete: false)
                .ForeignKey("dbo.Applications", t => t.ApplicationId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId, cascadeDelete: false)
                .Index(t => t.BranchId)
                .Index(t => t.ApplicationId)
                .Index(t => t.AddressId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");

            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    UserId = c.String(nullable: false, maxLength: 128),
                    ClaimType = c.String(),
                    ClaimValue = c.String(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                {
                    LoginProvider = c.String(nullable: false, maxLength: 128),
                    ProviderKey = c.String(nullable: false, maxLength: 128),
                    UserId = c.String(nullable: false, maxLength: 128),
                })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                {
                    UserId = c.String(nullable: false, maxLength: 128),
                    RoleId = c.String(nullable: false, maxLength: 128),
                })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);

            CreateTable(
                "dbo.Treatments",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Date = c.DateTime(nullable: false),
                    Name = c.String(),
                    AddedBy = c.String(maxLength: 128),
                    Points = c.Int(nullable: false),
                    Price = c.Single(nullable: false),
                    Active = c.Boolean(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    ApproximateTime = c.Int(nullable: false),
                    LastUpdatedOn = c.DateTime(nullable: false),
                    ImageUrl = c.String(),
                    Description = c.String(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.AddedBy)
                .Index(t => t.AddedBy);

            CreateTable(
                "dbo.AppointmentDetails",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    AppointmentId = c.Guid(nullable: false),
                    TreatmentId = c.Guid(nullable: false),
                    StylistId = c.String(maxLength: 128),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Appointments", t => t.AppointmentId, cascadeDelete: false)
                .ForeignKey("dbo.Treatments", t => t.TreatmentId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.StylistId)
                .Index(t => t.AppointmentId)
                .Index(t => t.TreatmentId)
                .Index(t => t.StylistId);

            CreateTable(
                "dbo.ClientPoints",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    ClientId = c.String(maxLength: 128),
                    InvoiceId = c.Guid(nullable: false),
                    Points = c.Int(nullable: false),
                    Date = c.DateTime(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    LastUpdatedOn = c.DateTime(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ClientId)
                .ForeignKey("dbo.InvoiceRecords", t => t.InvoiceId, cascadeDelete: false)
                .Index(t => t.ClientId)
                .Index(t => t.InvoiceId);

            CreateTable(
                "dbo.InvoiceRecords",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    ClientId = c.String(maxLength: 128),
                    Status = c.Int(nullable: false),
                    DateTime = c.DateTime(nullable: false),
                    GrandTotal = c.Double(nullable: false),
                    AmountRecieved = c.Single(nullable: false),
                    PointsUsed = c.Int(nullable: false),
                    LastUpdatedOn = c.DateTime(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ClientId)
                .Index(t => t.ClientId);

            CreateTable(
                "dbo.InvoiceItems",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    AppointmentId = c.Guid(nullable: false),
                    TreatmentPrice = c.Single(nullable: false),
                    DiscountedPrice = c.Single(nullable: false),
                    DateTime = c.DateTime(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    AddedBy = c.String(maxLength: 128),
                    UpdatedOn = c.DateTime(nullable: false),
                    UpdatedBy = c.String(maxLength: 128),
                    InvoiceId = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Appointments", t => t.AppointmentId, cascadeDelete: false)
                .ForeignKey("dbo.InvoiceRecords", t => t.InvoiceId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.AddedBy)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedBy)
                .Index(t => t.AppointmentId)
                .Index(t => t.AddedBy)
                .Index(t => t.UpdatedBy)
                .Index(t => t.InvoiceId);

            CreateTable(
                "dbo.Machines",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    ApplicationId = c.Guid(nullable: false),
                    BranchId = c.Guid(nullable: false),
                    CreatedOn = c.DateTime(nullable: false),
                    CreatedBy = c.String(maxLength: 128),
                    Active = c.Boolean(nullable: false),
                    MacAddress = c.String(),
                    LastUpdatedOn = c.DateTime(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Applications", t => t.ApplicationId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy)
                .Index(t => t.ApplicationId)
                .Index(t => t.BranchId)
                .Index(t => t.CreatedBy);

            CreateTable(
                "dbo.Points",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    Expiry = c.String(),
                    Worth = c.Int(nullable: false),
                    LastUpdatedOn = c.DateTime(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.AspNetRoles",
                c => new
                {
                    Id = c.String(nullable: false, maxLength: 128),
                    Name = c.String(nullable: false, maxLength: 256),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");

            CreateTable(
                "dbo.StylistCommissions",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    AppointmentId = c.Guid(nullable: false),
                    TreatmentId = c.Guid(nullable: false),
                    StylistId = c.String(maxLength: 128),
                    Amount = c.Single(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Appointments", t => t.AppointmentId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.StylistId)
                .ForeignKey("dbo.Treatments", t => t.TreatmentId, cascadeDelete: false)
                .Index(t => t.AppointmentId)
                .Index(t => t.TreatmentId)
                .Index(t => t.StylistId);

            CreateTable(
                "dbo.StylistTreatments",
                c => new
                {
                    TreatmentId = c.Guid(nullable: false),
                    StylistId = c.String(nullable: false, maxLength: 128),
                    Date = c.DateTime(nullable: false),
                    Active = c.Boolean(nullable: false),
                    PercentageCommission = c.Single(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                    Speciality = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => new { t.TreatmentId, t.StylistId })
                .ForeignKey("dbo.AspNetUsers", t => t.StylistId, cascadeDelete: false)
                .ForeignKey("dbo.Treatments", t => t.TreatmentId, cascadeDelete: false)
                .Index(t => t.TreatmentId)
                .Index(t => t.StylistId);

            CreateTable(
                "dbo.Attendances",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    UserId = c.String(maxLength: 128),
                    EntryTime = c.DateTime(nullable: false),
                    MacAddress = c.String(),
                    ManualEntry = c.Boolean(nullable: false),
                    Error = c.String(),
                    Active = c.Boolean(nullable: false),
                    MachineId = c.Int(nullable: false),
                    BranchId = c.Guid(nullable: false),
                    ApplicaitonId = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Applications", t => t.ApplicaitonId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.MachineId, unique: true)
                .Index(t => t.BranchId)
                .Index(t => t.ApplicaitonId);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Attendances", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Attendances", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.Attendances", "ApplicaitonId", "dbo.Applications");
            DropForeignKey("dbo.StylistTreatments", "TreatmentId", "dbo.Treatments");
            DropForeignKey("dbo.StylistTreatments", "StylistId", "dbo.AspNetUsers");
            DropForeignKey("dbo.StylistCommissions", "TreatmentId", "dbo.Treatments");
            DropForeignKey("dbo.StylistCommissions", "StylistId", "dbo.AspNetUsers");
            DropForeignKey("dbo.StylistCommissions", "AppointmentId", "dbo.Appointments");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Machines", "CreatedBy", "dbo.AspNetUsers");
            DropForeignKey("dbo.Machines", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.Machines", "ApplicationId", "dbo.Applications");
            DropForeignKey("dbo.InvoiceItems", "UpdatedBy", "dbo.AspNetUsers");
            DropForeignKey("dbo.InvoiceItems", "AddedBy", "dbo.AspNetUsers");
            DropForeignKey("dbo.InvoiceItems", "InvoiceId", "dbo.InvoiceRecords");
            DropForeignKey("dbo.InvoiceItems", "AppointmentId", "dbo.Appointments");
            DropForeignKey("dbo.ClientPoints", "InvoiceId", "dbo.InvoiceRecords");
            DropForeignKey("dbo.InvoiceRecords", "ClientId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ClientPoints", "ClientId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AppointmentDetails", "StylistId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AppointmentDetails", "TreatmentId", "dbo.Treatments");
            DropForeignKey("dbo.AppointmentDetails", "AppointmentId", "dbo.Appointments");
            DropForeignKey("dbo.AppointmentApprovals", "ApproverId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AppointmentApprovals", "AppointmentId", "dbo.Appointments");
            DropForeignKey("dbo.Appointments", "TreatmentId", "dbo.Treatments");
            DropForeignKey("dbo.Treatments", "AddedBy", "dbo.AspNetUsers");
            DropForeignKey("dbo.Appointments", "StylistId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Appointments", "ClientId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.AspNetUsers", "ApplicationId", "dbo.Applications");
            DropForeignKey("dbo.AspNetUsers", "AddressId", "dbo.Addresses");
            DropForeignKey("dbo.Appointments", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.Branches", "AddressId", "dbo.Addresses");
            DropForeignKey("dbo.Appointments", "ApplicationId", "dbo.Applications");
            DropIndex("dbo.Attendances", new[] { "ApplicaitonId" });
            DropIndex("dbo.Attendances", new[] { "BranchId" });
            DropIndex("dbo.Attendances", new[] { "MachineId" });
            DropIndex("dbo.Attendances", new[] { "UserId" });
            DropIndex("dbo.StylistTreatments", new[] { "StylistId" });
            DropIndex("dbo.StylistTreatments", new[] { "TreatmentId" });
            DropIndex("dbo.StylistCommissions", new[] { "StylistId" });
            DropIndex("dbo.StylistCommissions", new[] { "TreatmentId" });
            DropIndex("dbo.StylistCommissions", new[] { "AppointmentId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Machines", new[] { "CreatedBy" });
            DropIndex("dbo.Machines", new[] { "BranchId" });
            DropIndex("dbo.Machines", new[] { "ApplicationId" });
            DropIndex("dbo.InvoiceItems", new[] { "InvoiceId" });
            DropIndex("dbo.InvoiceItems", new[] { "UpdatedBy" });
            DropIndex("dbo.InvoiceItems", new[] { "AddedBy" });
            DropIndex("dbo.InvoiceItems", new[] { "AppointmentId" });
            DropIndex("dbo.InvoiceRecords", new[] { "ClientId" });
            DropIndex("dbo.ClientPoints", new[] { "InvoiceId" });
            DropIndex("dbo.ClientPoints", new[] { "ClientId" });
            DropIndex("dbo.AppointmentDetails", new[] { "StylistId" });
            DropIndex("dbo.AppointmentDetails", new[] { "TreatmentId" });
            DropIndex("dbo.AppointmentDetails", new[] { "AppointmentId" });
            DropIndex("dbo.Treatments", new[] { "AddedBy" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUsers", new[] { "AddressId" });
            DropIndex("dbo.AspNetUsers", new[] { "ApplicationId" });
            DropIndex("dbo.AspNetUsers", new[] { "BranchId" });
            DropIndex("dbo.Branches", new[] { "AddressId" });
            DropIndex("dbo.Appointments", new[] { "TreatmentId" });
            DropIndex("dbo.Appointments", new[] { "StylistId" });
            DropIndex("dbo.Appointments", new[] { "ApplicationId" });
            DropIndex("dbo.Appointments", new[] { "BranchId" });
            DropIndex("dbo.Appointments", new[] { "ClientId" });
            DropIndex("dbo.AppointmentApprovals", new[] { "ApproverId" });
            DropIndex("dbo.AppointmentApprovals", new[] { "AppointmentId" });
            DropTable("dbo.Attendances");
            DropTable("dbo.StylistTreatments");
            DropTable("dbo.StylistCommissions");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Points");
            DropTable("dbo.Machines");
            DropTable("dbo.InvoiceItems");
            DropTable("dbo.InvoiceRecords");
            DropTable("dbo.ClientPoints");
            DropTable("dbo.AppointmentDetails");
            DropTable("dbo.Treatments");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Branches");
            DropTable("dbo.Appointments");
            DropTable("dbo.AppointmentApprovals");
            DropTable("dbo.Applications");
            DropTable("dbo.Addresses");
        }
    }
}
