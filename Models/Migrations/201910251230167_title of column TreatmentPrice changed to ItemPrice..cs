namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class titleofcolumnTreatmentPricechangedtoItemPrice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceItems", "ItemPrice", c => c.Single(nullable: false));
            DropColumn("dbo.InvoiceItems", "TreatmentPrice");
        }
        
        public override void Down()
        {
            AddColumn("dbo.InvoiceItems", "TreatmentPrice", c => c.Single(nullable: false));
            DropColumn("dbo.InvoiceItems", "ItemPrice");
        }
    }
}
