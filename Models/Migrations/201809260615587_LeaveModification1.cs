namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LeaveModification1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Leaves", "Recursion", c => c.Int(nullable: false));
            AddColumn("dbo.Leaves", "IsContinuous", c => c.Boolean(nullable: false));
            AddColumn("dbo.Leaves", "RecursionEndDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Leaves", "Note", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Leaves", "Note");
            DropColumn("dbo.Leaves", "RecursionEndDate");
            DropColumn("dbo.Leaves", "IsContinuous");
            DropColumn("dbo.Leaves", "Recursion");
        }
    }
}
