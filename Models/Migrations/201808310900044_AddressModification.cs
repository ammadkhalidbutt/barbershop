namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddressModification : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Addresses", "House", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Addresses", "House", c => c.Int(nullable: false));
        }
    }
}
