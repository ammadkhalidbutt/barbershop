namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InvoicenumberandpaidByPincheckaddedtoinvoice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceRecords", "InvoiceNumber", c => c.String());
            AddColumn("dbo.InvoiceRecords", "PaidByPin", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvoiceRecords", "PaidByPin");
            DropColumn("dbo.InvoiceRecords", "InvoiceNumber");
        }
    }
}
