namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Third : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Appointments", "ActualStartingTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.Appointments", "ActualEndingTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Appointments", "ActualEndingTime");
            DropColumn("dbo.Appointments", "ActualStartingTime");
        }
    }
}
