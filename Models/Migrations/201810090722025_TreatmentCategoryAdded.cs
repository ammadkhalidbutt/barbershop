namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TreatmentCategoryAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TreatmentCategories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Image = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Treatments", "CategoryId", c => c.Guid(nullable: true));
            CreateIndex("dbo.Treatments", "CategoryId");
            AddForeignKey("dbo.Treatments", "CategoryId", "dbo.TreatmentCategories", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Treatments", "CategoryId", "dbo.TreatmentCategories");
            DropIndex("dbo.Treatments", new[] { "CategoryId" });
            DropColumn("dbo.Treatments", "CategoryId");
            DropTable("dbo.TreatmentCategories");
        }
    }
}
