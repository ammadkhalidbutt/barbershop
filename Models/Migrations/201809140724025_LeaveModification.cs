namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LeaveModification : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Leaves", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Leaves", "Status", c => c.Boolean(nullable: false));
        }
    }
}
