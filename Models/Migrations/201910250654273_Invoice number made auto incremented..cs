namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Invoicenumbermadeautoincremented : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.InvoiceRecords", "InvoiceNumber", c => c.Int(nullable: true, identity: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.InvoiceRecords", "InvoiceNumber", c => c.String());
        }
    }
}
