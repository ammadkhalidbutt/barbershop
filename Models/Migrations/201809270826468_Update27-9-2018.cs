namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update2792018 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Leaves", "RecursionEndDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Leaves", "RecursionEndDate", c => c.DateTime(nullable: false));
        }
    }
}
