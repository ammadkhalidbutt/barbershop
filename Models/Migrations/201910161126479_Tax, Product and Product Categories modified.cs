namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TaxProductandProductCategoriesmodified : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductCategories", "UpdatedBy", c => c.String());
            AddColumn("dbo.ProductCategories", "UpdatedOn", c => c.DateTime(nullable: false));
            AddColumn("dbo.Products", "UpdatedBy", c => c.String());
            AddColumn("dbo.Products", "UpdatedOn", c => c.DateTime(nullable: false));
            AddColumn("dbo.Taxes", "CreatedOn", c => c.DateTime(nullable: false));
            AddColumn("dbo.Taxes", "CreatedBy", c => c.String());
            AddColumn("dbo.Taxes", "UpdatedBy", c => c.String());
            AddColumn("dbo.Taxes", "UpdatedOn", c => c.DateTime(nullable: false));
            AddColumn("dbo.Taxes", "IsDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Taxes", "IsDeleted");
            DropColumn("dbo.Taxes", "UpdatedOn");
            DropColumn("dbo.Taxes", "UpdatedBy");
            DropColumn("dbo.Taxes", "CreatedBy");
            DropColumn("dbo.Taxes", "CreatedOn");
            DropColumn("dbo.Products", "UpdatedOn");
            DropColumn("dbo.Products", "UpdatedBy");
            DropColumn("dbo.ProductCategories", "UpdatedOn");
            DropColumn("dbo.ProductCategories", "UpdatedBy");
        }
    }
}
