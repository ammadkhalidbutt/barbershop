namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FloatToDoubleInModelInitial : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Appointments", "Discount", c => c.Double(nullable: false));
            AlterColumn("dbo.Appointments", "TimeElapsed", c => c.Double(nullable: false));
            AlterColumn("dbo.Treatments", "Price", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Treatments", "Price", c => c.Single(nullable: false));
            AlterColumn("dbo.Appointments", "TimeElapsed", c => c.Single(nullable: false));
            AlterColumn("dbo.Appointments", "Discount", c => c.Single(nullable: false));
        }
    }
}
