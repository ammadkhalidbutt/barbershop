namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StylistSchedule : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StylistSchedules",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        StylistId = c.String(maxLength: 128),
                        StartingTime = c.Time(nullable: false, precision: 7),
                        EndingTime = c.Time(nullable: false, precision: 7),
                        Day = c.Int(nullable: false),
                        OneTimeOnly = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.StylistId)
                .Index(t => t.StylistId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StylistSchedules", "StylistId", "dbo.AspNetUsers");
            DropIndex("dbo.StylistSchedules", new[] { "StylistId" });
            DropTable("dbo.StylistSchedules");
        }
    }
}
