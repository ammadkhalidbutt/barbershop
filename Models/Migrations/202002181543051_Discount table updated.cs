namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Discounttableupdated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Discounts", "ImageUrl", c => c.String());
            AddColumn("dbo.Discounts", "DiscountPercentage", c => c.Double(nullable: false));
            AlterColumn("dbo.Discounts", "EntityId", c => c.String(nullable: false));
            DropColumn("dbo.Discounts", "Image");
            DropColumn("dbo.Discounts", "DiscountAmount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Discounts", "DiscountAmount", c => c.Single(nullable: false));
            AddColumn("dbo.Discounts", "Image", c => c.String());
            AlterColumn("dbo.Discounts", "EntityId", c => c.Int(nullable: false));
            DropColumn("dbo.Discounts", "DiscountPercentage");
            DropColumn("dbo.Discounts", "ImageUrl");
        }
    }
}
