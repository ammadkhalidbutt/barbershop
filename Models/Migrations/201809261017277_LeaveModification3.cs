namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LeaveModification3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Leaves", "ReferenceId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Leaves", "ReferenceId");
        }
    }
}
