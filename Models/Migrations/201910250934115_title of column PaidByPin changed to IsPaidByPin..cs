namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class titleofcolumnPaidByPinchangedtoIsPaidByPin : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceRecords", "IsPaidByPin", c => c.Boolean(nullable: false));
            DropColumn("dbo.InvoiceRecords", "PaidByPin");
        }
        
        public override void Down()
        {
            AddColumn("dbo.InvoiceRecords", "PaidByPin", c => c.Boolean(nullable: false));
            DropColumn("dbo.InvoiceRecords", "IsPaidByPin");
        }
    }
}
