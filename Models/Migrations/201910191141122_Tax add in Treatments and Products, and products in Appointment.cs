namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TaxaddinTreatmentsandProductsandproductsinAppointment : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AppointmentDetails", "TreatmentId", "dbo.Treatments");
            DropIndex("dbo.AppointmentDetails", new[] { "TreatmentId" });
            AddColumn("dbo.Treatments", "TaxId", c => c.Guid(nullable: true));
            AddColumn("dbo.Products", "TaxId", c => c.Guid(nullable: true));
            CreateIndex("dbo.Treatments", "TaxId");
            CreateIndex("dbo.Products", "TaxId");
            AddForeignKey("dbo.Treatments", "TaxId", "dbo.Taxes", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Products", "TaxId", "dbo.Taxes", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "TaxId", "dbo.Taxes");
            DropForeignKey("dbo.Treatments", "TaxId", "dbo.Taxes");
            DropIndex("dbo.Products", new[] { "TaxId" });
            DropIndex("dbo.Treatments", new[] { "TaxId" });
            DropColumn("dbo.Products", "TaxId");
            DropColumn("dbo.Treatments", "TaxId");
            CreateIndex("dbo.AppointmentDetails", "TreatmentId");
            AddForeignKey("dbo.AppointmentDetails", "TreatmentId", "dbo.Treatments", "Id", cascadeDelete: true);
        }
    }
}
