namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Discounttableadded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Discounts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PromoCode = c.String(),
                        Image = c.String(),
                        DiscountAmount = c.Single(nullable: false),
                        EntityId = c.Int(nullable: false),
                        EntityType = c.Int(nullable: false),
                        Priority = c.Int(nullable: false),
                        StartDateTime = c.DateTime(nullable: false),
                        EndDateTime = c.DateTime(nullable: false),
                        Description = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedBy = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        UpdatedOn = c.DateTime(nullable: false),
                        IsApproved = c.Boolean(nullable: false),
                        ApplicationId = c.Guid(nullable: false),
                        BranchId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Discounts");
        }
    }
}
