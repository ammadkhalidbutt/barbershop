namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LeaveAndSchedule : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Leaves",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        StartingDate = c.DateTime(nullable: false),
                        EndingDate = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Type = c.Int(nullable: false),
                        StylistId = c.String(maxLength: 128),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.StylistId)
                .Index(t => t.StylistId);
            
            AddColumn("dbo.Branches", "IPAddress", c => c.String());
            AddColumn("dbo.AspNetUsers", "ReferenceId", c => c.Guid(nullable: false));
            DropColumn("dbo.StylistSchedules", "OneTimeOnly");
        }
        
        public override void Down()
        {
            AddColumn("dbo.StylistSchedules", "OneTimeOnly", c => c.Boolean(nullable: false));
            DropForeignKey("dbo.Leaves", "StylistId", "dbo.AspNetUsers");
            DropIndex("dbo.Leaves", new[] { "StylistId" });
            DropColumn("dbo.AspNetUsers", "ReferenceId");
            DropColumn("dbo.Branches", "IPAddress");
            DropTable("dbo.Leaves");
        }
    }
}
