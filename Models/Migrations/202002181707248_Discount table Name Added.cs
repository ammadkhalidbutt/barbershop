namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DiscounttableNameAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Discounts", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Discounts", "Name");
        }
    }
}
