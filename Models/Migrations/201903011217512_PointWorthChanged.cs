namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PointWorthChanged : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Points", "Worth", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Points", "Worth", c => c.Int(nullable: false));
        }
    }
}
