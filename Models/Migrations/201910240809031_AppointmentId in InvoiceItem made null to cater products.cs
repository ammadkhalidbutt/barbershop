namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AppointmentIdinInvoiceItemmadenulltocaterproducts : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.InvoiceItems", "AppointmentId", "dbo.Appointments");
            DropIndex("dbo.InvoiceItems", new[] { "AppointmentId" });
            AlterColumn("dbo.InvoiceItems", "AppointmentId", c => c.Guid());
            CreateIndex("dbo.InvoiceItems", "AppointmentId");
            AddForeignKey("dbo.InvoiceItems", "AppointmentId", "dbo.Appointments", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InvoiceItems", "AppointmentId", "dbo.Appointments");
            DropIndex("dbo.InvoiceItems", new[] { "AppointmentId" });
            AlterColumn("dbo.InvoiceItems", "AppointmentId", c => c.Guid(nullable: false));
            CreateIndex("dbo.InvoiceItems", "AppointmentId");
            AddForeignKey("dbo.InvoiceItems", "AppointmentId", "dbo.Appointments", "Id", cascadeDelete: true);
        }
    }
}
