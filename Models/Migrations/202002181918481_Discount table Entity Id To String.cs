namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DiscounttableEntityIdToString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Discounts", "EntityId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Discounts", "EntityId", c => c.Guid(nullable: false));
        }
    }
}
