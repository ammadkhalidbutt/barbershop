namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TaxProductandProductCategoriesAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductCategories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Image = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Name = c.String(),
                        AddedBy = c.String(maxLength: 128),
                        CategoryId = c.Guid(nullable: false),
                        Price = c.Double(nullable: false),
                        Active = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        LastUpdatedOn = c.DateTime(nullable: false),
                        ImageUrl = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProductCategories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.AddedBy)
                .Index(t => t.AddedBy)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Taxes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Percentage = c.Single(nullable: false),
                        IsExclusive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "AddedBy", "dbo.AspNetUsers");
            DropForeignKey("dbo.Products", "CategoryId", "dbo.ProductCategories");
            DropIndex("dbo.Products", new[] { "CategoryId" });
            DropIndex("dbo.Products", new[] { "AddedBy" });
            DropTable("dbo.Taxes");
            DropTable("dbo.Products");
            DropTable("dbo.ProductCategories");
        }
    }
}
