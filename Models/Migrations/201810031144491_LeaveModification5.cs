namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LeaveModification5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Leaves", "CreatedBy", c => c.String());
            AddColumn("dbo.Leaves", "CreatedOn", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Leaves", "CreatedOn");
            DropColumn("dbo.Leaves", "CreatedBy");
        }
    }
}
