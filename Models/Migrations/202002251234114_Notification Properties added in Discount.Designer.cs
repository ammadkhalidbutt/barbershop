// <auto-generated />
namespace Models.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class NotificationPropertiesaddedinDiscount : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(NotificationPropertiesaddedinDiscount));
        
        string IMigrationMetadata.Id
        {
            get { return "202002251234114_Notification Properties added in Discount"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
