namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DiscounttableEntityIdUpdated : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Discounts", "DiscountPercentage", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Discounts", "DiscountPercentage", c => c.Single(nullable: false));
        }
    }
}
