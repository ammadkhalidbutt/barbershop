namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CurrencyAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Branches", "CurrencyId", c => c.Guid(nullable: true));
            CreateIndex("dbo.Branches", "CurrencyId");
            AddForeignKey("dbo.Branches", "CurrencyId", "dbo.Currencies", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Branches", "CurrencyId", "dbo.Currencies");
            DropIndex("dbo.Branches", new[] { "CurrencyId" });
            DropColumn("dbo.Branches", "CurrencyId");
            DropTable("dbo.Currencies");
        }
    }
}
