namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LeaveModification4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Leaves", "RecursionParameter", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Leaves", "RecursionParameter");
        }
    }
}
