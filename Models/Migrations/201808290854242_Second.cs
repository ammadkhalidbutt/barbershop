namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Second : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Appointments", "TimeElapsed", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Appointments", "TimeElapsed", c => c.Int(nullable: false));
        }
    }
}
