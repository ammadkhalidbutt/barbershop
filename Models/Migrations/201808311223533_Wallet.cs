namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Wallet : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClientPoints", "Purchased", c => c.Boolean(nullable: false));
            AddColumn("dbo.InvoiceRecords", "AmountGiven", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvoiceRecords", "AmountGiven");
            DropColumn("dbo.ClientPoints", "Purchased");
        }
    }
}
