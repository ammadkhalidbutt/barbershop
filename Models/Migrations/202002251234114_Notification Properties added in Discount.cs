
namespace Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotificationPropertiesaddedinDiscount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Discounts", "NotificationTitle", c => c.String());
            AddColumn("dbo.Discounts", "NotificationBody", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Discounts", "NotificationBody");
            DropColumn("dbo.Discounts", "NotificationTitle");
        }
    }
}
