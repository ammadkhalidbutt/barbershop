﻿using Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class InvoiceItem
    {
        public Guid Id { get; set; }
        [ForeignKey("Appointment")]
        public Guid? AppointmentId { get; set; }
        public Appointment Appointment { get; set; }

        public float ItemPrice { get; set; }
        public float DiscountedPrice { get; set; }
        public DateTime DateTime { get; set; }
        public bool IsDeleted { get; set; }

        [ForeignKey("PersonAddingRecord")]
        public string AddedBy { get; set; }
        public ApplicationUser PersonAddingRecord { get; set; }

        public DateTime UpdatedOn { get; set; }

        public Guid ItemId { get; set; }

        [ForeignKey("PersonUpdatingRecord")]
        public string UpdatedBy { get; set; }
        public ApplicationUser PersonUpdatingRecord { get; set; }

        [ForeignKey("InvoiceRecord")]
        public Guid InvoiceId { get; set; }
        public InvoiceRecord InvoiceRecord { get; set; }
    }
}
