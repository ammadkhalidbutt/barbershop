﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class GeneralSearchViewModel
    {
        public int RecordsPerPage { get; set; }
        public bool CalculateTotal { get; set; }
        public int CurrentPage { get; set; }
        public bool Select2 { get; set; }
        public bool Pagination { get; set; }
        public bool ReturnEmpty { get; set; }
    }
}
