﻿using Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class StylistTreatment
    {
        [Key, Column(Order = 0 )]
        [ForeignKey("Treatment")]
        public Guid TreatmentId { get; set; }
        public Treatment Treatment { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Stylist")]
        public string StylistId { get; set; }
        public ApplicationUser Stylist { get; set; }

        public DateTime Date { get; set; }
        public bool Active { get; set; }
        public float PercentageCommission { get; set; }
        public bool IsDeleted { get; set; }

        public bool Speciality { get; set; }
    }

    public class StylistCommission
    {
        public Guid Id { get; set; }
        [ForeignKey("Appointment")]
        public Guid AppointmentId { get; set; }
        public Appointment Appointment { get; set; }
        [ForeignKey("Treatment")]
        public Guid TreatmentId { get; set; }
        public Treatment Treatment { get; set; }
        [ForeignKey("Stylist")]
        public string StylistId { get; set; }
        public ApplicationUser Stylist { get; set; }
        public float Amount { get; set; }
        public bool IsDeleted { get; set; }
    }
}
