﻿using BusinessLogic;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BarberShop.Controllers
{
    public class PointController : Controller
    {
        // GET: Point
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Create(string href)
        {
            ViewBag.href = href;
            return View(new PointViewModel { Action = "Create" });
        }
        [HttpPost]
        public ActionResult Create(PointViewModel model)
        {
            var href = TempData["href"].ToString();
            var result = new Logic().Create(model);
            if(result!=new Guid())
            {
                return Redirect(href);
            }
            return View(model);
        }
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Edit()
        {
            var result = new Logic().GetPoint();
            result.Action = "Edit";
            return View("Create",result);
        }

        [HttpPost]
        public ActionResult Edit(PointViewModel model)
        {
            var result = new Logic().Edit(model);
            if(result!=new Guid())
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View("Create", model);
            }

        }
    }
}