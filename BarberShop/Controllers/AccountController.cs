﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using BarberShop.Models;
using Models.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using BusinessLogic;
using Models;
using QRCoder;
using Models.ViewModels;
using AutoMapper;
using System.Collections.Generic;
using Microsoft.Ajax.Utilities;

namespace BarberShop.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //    public static List<LoggedInUsers> OnlineUsers
        //    {
        //        get
        //        {
        //            if (System.Web.HttpContext.Current.Cache["OnlineUsers"] != null)
        //                return (List<LoggedInUsers>)System.Web.HttpContext.Current.Cache["OnlineUsers"];
        //            else
        //                List<LoggedInUsers> list = My_MSI.Net.Controllers.OnlineUsers.List;
        //            System.Web.HttpContext.Current.Cache.Add("OnlineUsers", list, null, DateTime.MaxValue, new TimeSpan(), CacheItemPriority.NotRemovable, null);
        //            return list;
        //        }
        //    }
        //}

        //private static System.Web.SessionState.HttpSessionState TheSession;
        //public static new System.Web.SessionState.HttpSessionState Session
        //{
        //    get { return TheSession; }
        //    set
        //    {
        //        TheSession = value;
        //    }
        //}

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            var branch = new Logic().GetBranches<BranchViewModel>(new BranchSearchViewModel());
            var application = new Logic().GetApplications<ApplicationViewModel>(new ApplicationSearchViewModel());
            Session["FirstTimeRegistration"] = false;
            if (!(branch.ResultList.Count() > 0 || application.ResultList.Count() > 0))
            {
                Session["FirstTimeRegistration"] = true;
            }
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var check = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Email = model.Email }).ResultList.FirstOrDefault();
            SignInStatus result = SignInStatus.Failure;
            //var passwordHash = UserManager.PasswordHasher.HashPassword("test");

            if (check != null)
            {
                result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            }
            else
            {
                ModelState.AddModelError("Password", "This user doesn't exist or has been removed from the system.");
            }
            switch (result)
            {
                case SignInStatus.Success:

                    {
                        //if the list exists, add this user to it
                        if (HttpRuntime.Cache["LoggedInUsers"] != null)
                        {
                            //get the list of logged in users from the cache
                            var loggedInUsers = (Dictionary<string, DateTime>)HttpRuntime.Cache["LoggedInUsers"];
                            //AccountController.OnlineUsers
                            //List<LoggedInUsers> list = My_MSI.Net.Controllers.OnlineUsers.List;
                            //System.Web.HttpContext.Current.Cache.Add("LoggedInUsers",list, )

                            if (!loggedInUsers.ContainsKey(model.Email))
                            {
                                //add this user to the list
                                loggedInUsers.Add(model.Email, DateTime.Now);
                                //add the list back into the cache
                                HttpRuntime.Cache["LoggedInUsers"] = loggedInUsers;
                            }
                        }

                        //the list does not exist so create it
                        else
                        {
                            //create a new list
                            var loggedInUsers = new Dictionary<string, DateTime>();
                            //add this user to the list
                            loggedInUsers.Add(model.Email, DateTime.Now);
                            //add the list into the cache
                            HttpRuntime.Cache["LoggedInUsers"] = loggedInUsers;
                        }

                        //return RedirectToLocal(returnUrl);
                    }
                    Session["Currency"] = Helpers.Extensions.GetCurrencyValue(model.Email);
                    var userId = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Email = model.Email }).ResultList.FirstOrDefault().Id;

                    //var userManager = new UserManager(new ApplicationDbContext());

                    new Helpers.LogGeneration().CreateGeneralLog(new ApplicationDbContext(), new Log { Action = "Login", DateTime = DateTime.Now, AddedBy = UserManager.GetRoles(userId).FirstOrDefault(), ModelId = model.Email });
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("Password", "Invalid login attempt.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Edit
        [AllowAnonymous]
        public ActionResult Edit(string id, string role)
        {
            Session["role"] = role;
            var result = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Id = id }).ResultList.FirstOrDefault();
            result.Action = "Edit";
            result.Role = role;
            return View(result);
        }

        public ActionResult UpdateClientOnAppointment(string Id)
        {
            Guid UserId = new Guid();
            Guid.TryParse(Id, out UserId);
            var model = new RegisterViewModel();
            model.Action = "Register";
            if (UserId != new Guid())
            {
                model = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Id = UserId.ToString() }).ResultList.FirstOrDefault();
                model.Action = "Edit";
            }
            model.Role = "Client";
            return PartialView("EditPartial", model);
        }

        public ActionResult EditPartial(RegisterViewModel model)
        {
            return PartialView(model);
        }

        //
        // POST: /Account/Edit
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Edit(RegisterViewModel model)
        {
            ModelState.Remove("Password");
            ModelState.Remove("Confirm Password");

            if (!(bool)DoesPhoneNumberExist(model.PhoneNumber, model.Id).Data)
            {
                ModelState.AddModelError("PhoneNumber", "Phone Number already exists.");
            }
            if (ModelState.IsValid)
            {
                if (model.Image != null)
                {
                    var pic = System.IO.Path.GetFileName(model.Image.FileName);
                    var savedPath = Server.MapPath("/Attachments/users");
                    var path = System.IO.Path.Combine(savedPath, pic);
                    model.Image.SaveAs(path);
                    model.ImageUrl = "/Attachments/users/" + pic;
                }
                
                model.Action = "Edit";
                var result = new Logic().Edit(model);
                //var role = Session["role"].ToString();
                var role = model.Role;
                if (Request.IsAjaxRequest())
                {
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (!string.IsNullOrEmpty(result))
                    {
                        return RedirectToAction("Index", new { Role = role });
                    }

                    return View(model);
                }
            }
            if (Request.IsAjaxRequest())
            {
                model.Error = true;
                return PartialView("EditPartial", model);
            }
            else
            {
                return View(model);
            }

        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register(string role, bool? isRegistered)
        {
            isRegistered = isRegistered == null ? false : isRegistered.Value;
            Session["role"] = role;
            var result = new RegisterViewModel { Address = new AddressViewModel(), IsRegistered = isRegistered.Value, Action = "Register", Role = role };

            if (isRegistered.Value)
            {
                return View(result);
            }
            else
            {
                return View("Edit", result);
            }

        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (Request != null && Request.IsAjaxRequest())// If ajax call Id becomes required. Dont know the reasons
            {
                ModelState.Remove("Id");
                ModelState.Remove("Address.Id");
                if (model.Role == "Client")
                {
                    if (string.IsNullOrEmpty(model.Password))
                    {
                        model.Password = "Test@1234567";
                        model.ConfirmPassword = "Test@1234567";
                    }
                    ModelState.Remove("Password");
                    ModelState.Remove("ConfirmPassword");
                    ModelState.Remove("DOB");
                }
            }
            if (!(bool)DoesPhoneNumberExist(model.PhoneNumber, model.Id).Data)
            {
                ModelState.AddModelError("PhoneNumber", "Phone Number already exists.");
            }
            var title = model.Role;
            model.Gender = model.Gender == 0 ? Enums.Gender.Unknown : model.Gender;
            var application = new Logic().GetApplications<ApplicationViewModel>(new ApplicationSearchViewModel());
            var branch = new Logic().GetBranches<BranchViewModel>(new BranchSearchViewModel());
            if (ModelState.IsValid && application.ResultList.Count() > 0 && branch.ResultList.Count() > 0)
            {
                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode("The text which should be encoded.", QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);

                if (model.Image != null)
                {
                    var pic = System.IO.Path.GetFileName(model.Image.FileName);
                    var savedPath = Server.MapPath("/Attachments/users");
                    var path = System.IO.Path.Combine(savedPath, pic);
                    model.Image.SaveAs(path);
                    model.ImageUrl = "/Attachments/users/" + pic;
                }
                else
                {
                    model.ImageUrl = "/images/default/default.jpg";
                }

                ApplicationUser user;
                model.Address.Id = Guid.NewGuid();
                model.Address.Active = true;
                model.Role = string.IsNullOrEmpty(model.Role) ? string.IsNullOrEmpty(title) ? "Client" : title : model.Role;
                model.Email = string.IsNullOrEmpty(model.Email) ? string.Format("{0}{1}@saloon.com", model.Name.Replace(" ", string.Empty), model.PhoneNumber.Replace("+", string.Empty).Replace(" ", string.Empty)) : model.Email;
                var config = new MapperConfiguration(cfg => { cfg.CreateMap<AddressViewModel, Address>(); });
                var iMapper = config.CreateMapper();
                var address = iMapper.Map<AddressViewModel, Address>(model.Address);
                var appCheck = new Logic().GetApplications<ApplicationViewModel>(new ApplicationSearchViewModel());
                var firstTimeRegistration = HttpContext == null || Session["FirstTimeRegistration"] == null ? false : (bool)Session["FirstTimeRegistration"];
                if (firstTimeRegistration)
                {
                    user = new ApplicationUser { UserName = model.Email, Email = model.Email, Address = address, Active = true, ApplicationId = application.ResultList.First().Id, BranchId = branch.ResultList.First().Id, DOB = model.DOB, ImageUrl = model.ImageUrl, Name = model.Name, PhoneNumber = model.PhoneNumber, Gender = model.Gender, QRCode = qrCode.ToString() };
                    CreateRoles();
                    model.Role = "SuperAdministrator";
                    Session["FirstTimeRegistration"] = false;
                }
                else
                {
                    user = new ApplicationUser { UserName = model.Email, Email = model.Email, Active = true, Address = address, ApplicationId = application.ResultList.First().Id, BranchId = branch.ResultList.First().Id, DOB = model.DOB, ImageUrl = model.ImageUrl, Name = model.Name, PhoneNumber = model.PhoneNumber, Gender = model.Gender, QRCode = qrCode.ToString() };
                }
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    using (ApplicationDbContext db = new ApplicationDbContext())
                    {
                        new Helpers.LogGeneration().CreateGeneralLog(db, new Log { Action = "Register" + model.Role, AddedBy = user.Id, DateTime = DateTime.Now, ModelId = user.Id });
                        var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                        var roleCheck = userManager.AddToRole(user.Id, model.Role);
                    }

                    if (model.CheckIn)
                    {
                        new Logic().ScanClients(user.Id, "Checked In");
                    }

                    if (Request != null && Request.IsAjaxRequest())
                    {
                        return Json(user, JsonRequestBehavior.AllowGet);
                    }

                    else
                    {
                        //await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);

                        // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                        // Send an email with this link
                        // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                        if (firstTimeRegistration)
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        else if (title != "" && title == "Stylist")
                        {
                            return RedirectToAction("Index", "Account", new { role = "Stylist" });
                        }
                        else if (title != "" && title == "Client")
                        {
                            return RedirectToAction("Index", "Account", new { role = "Client" });
                        }
                        else if (title != "" && title == "Administrator")
                        {
                            return RedirectToAction("Index", "Account", new { role = "Administrator" });
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("Email", result.Errors.Where(m => m.Contains("Email")).FirstOrDefault());
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            Session["Title"] = title;
            if (Request != null && Request.IsAjaxRequest())
            {
                return PartialView("EditPartial", model);
            }
            else
            {
                if (model.IsRegistered)
                {
                    return View(model);
                }
                else
                {
                    return View("Edit", model);
                }
            }
        }

        //
        // GET: /Account/Details
        [Authorize]
        public ActionResult Details(RegisterViewModel model)
        {
            var result = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Id = model.Id }).ResultList.FirstOrDefault();
            result.Role = model.Role;
            return View(result);
        }

        //
        // GET: /Account/AppRegister
        [AllowAnonymous]
        public ActionResult AppRegister()
        {
            var model = new FirstTimeRegistrationViewModel();
            model.Action = "Register";
            return View(model);
        }

        //
        // POST: /Account/AppRegister
        [HttpPost]
        [AllowAnonymous]
        public ActionResult AppRegister(FirstTimeRegistrationViewModel model)
        {
            if (model.Branch.Logo != null)
            {
                var pic = System.IO.Path.GetFileName(model.Branch.Logo.FileName);
                var savedDirectory = Server.MapPath("/Attachments/logo");
                var path = System.IO.Path.Combine(savedDirectory, pic);
                model.Branch.Logo.SaveAs(path);
                model.Branch.LogoUrl = "/Attachments/logo/" + pic;
            }

            var validatedGuid = new Guid();
            if (!(Guid.TryParse(model.Application.Id.ToString(), out validatedGuid) && validatedGuid != new Guid()))
            {
                model.Application.Id = Guid.NewGuid();
            }

            var applicationId = new Logic().Create(model.Application);
            model.Branch.ApplicationId = applicationId;
            var currencyId = new Logic().Create(model.Currency);
            model.Branch.Currency = new CurrencyViewModel { Id = currencyId };
            var branchId = new Logic().Create(model.Branch);
            Session["FirstTimeRegistration"] = true;
            return RedirectToAction("Register", new { role = "Stylist", isRegistered = true });
        }

        //
        // Edit App
        [Authorize(Roles = "SuperAdministrator")]
        public ActionResult EditApp()
        {
            var branchId = new Helpers.LoggedInUserInfo().GetLoggedInUser().BranchId;
            var result = new Logic().GetBranches<BranchViewModel>(new BranchSearchViewModel { BranchId = branchId }).ResultList.First();
            var model = new FirstTimeRegistrationViewModel { Branch = result };
            model.Currency = model.Branch.Currency;
            model.Action = "Edit";
            return View("AppRegister", model);
        }
        [HttpPost]
        public ActionResult EditApp(FirstTimeRegistrationViewModel model)
        {
            if (model.Branch.Logo != null)
            {
                var pic = System.IO.Path.GetFileName(model.Branch.Logo.FileName);
                var savedDirectory = Server.MapPath("/images/logo");
                var path = System.IO.Path.Combine(savedDirectory, pic);
                model.Branch.Logo.SaveAs(path);
                model.Branch.LogoUrl = "/images/logo/" + pic;
            }
            model.Branch.Currency = model.Currency;
            var branch = new Logic().Edit(model.Branch);
            if (branch != new Guid())
                return RedirectToAction("Index", "Home");
            return View("AppRegister", model);
        }

        //
        // 

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/Delete
        //public ActionResult Delete(string id, string role)
        //{
        //    var user = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Id = id });
        //    user.ResultList.FirstOrDefault().Role = role;
        //    return View(user);
        //}

        //
        // POST: /Account/Delete
        public ActionResult Delete(RegisterViewModel model)
        {
            var result = new Logic().Delete(model.Id);
            return RedirectToAction("Index", "Account", new { role = model.Role });
        }

        //
        // GET: /Account/Index
        [Authorize]
        public ActionResult Index(string role)
        {
            ViewBag.role = role;
            return View(new AccountSearchViewModel { Role = role });
        }

        //
        // GET: /Account/_IndexPartial
        public PartialViewResult _IndexPartial(AccountSearchViewModel search)
        {
            search.CurrentPage = search.CurrentPage == 0 ? 1 : search.CurrentPage;
            search.RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = search.Role }).ResultList.First().Id;
            search.RecordsPerPage = 16;
            search.Pagination = true;

            var result = new List<RegisterViewModel>();
            if (User.IsInRole("SuperAdministrator") || User.IsInRole("Administrator") || (User.IsInRole("Client") && search.Role == "Stylist"))
            {
                search.GetGroupedRecords = search.Role != "Administrator" ? true : false;
                var result1 = new Logic().GetUsers<RegisterViewModel>(search).ResultList;
                result = result1.GroupBy(x => x.Id).Select(o => o.FirstOrDefault()).ToList();
            }
            else if (!(User.IsInRole("Client")) && search.Role == "Client")
            {
                result = new Logic().GetUsers<RegisterViewModel>(search).ResultList;
            }
            else
            {
                search.Id = User.Identity.GetUserId();
                result = new Logic().GetUsers<RegisterViewModel>(search).ResultList.GroupBy(x => x.Email).Select(o => o.FirstOrDefault()).ToList();
            }
            ViewBag.Role = search.Role;
            ViewBag.AppendedView = search.AppendedView;
            result.ForEach(m => m.Role = search.Role);
            return PartialView(result);
        }

        public ActionResult AddReferenceNumber()
        {
            return View(new RegisterViewModel());
        }

        [HttpPost]
        public ActionResult AddReferenceNumber(RegisterViewModel model)
        {
            var result = new Logic().Edit(model);
            if (string.IsNullOrEmpty(result))
            {
                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Account", new { role = "Client" });
            }
        }

        //
        // GET: /Account/Clients
        [Authorize(Roles = "Administrator, SuperAdministrator, Client, Stylist")]
        public ActionResult Clients()
        {
            return View();
        }

        //
        // GET: /Account/_ClientPartial
        public PartialViewResult _ClientPartial(AccountSearchViewModel search)
        {
            var result = new List<RegisterViewModel>();
            if (User.IsInRole("SuperAdministrator") || User.IsInRole("Administrator") || User.IsInRole("Stylist"))
            {
                result = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { IsClient = true, RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "Client" }).ResultList.First().Id, Id = search.Id }).ResultList;
            }
            else
            {
                search.Id = User.Identity.GetUserId();
                result = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { IsClient = true, RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "Client" }).ResultList.First().Id, Id = search.Id }).ResultList;
            }

            return PartialView(result);
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            //code == null ? View("Error") :
            return View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }

            model.Code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }


        //
        //Reset Password Custom
        public ActionResult ResetUserPasswordCustom(string id, string role)
        {
            var user = new Logic().GetUserDetails(id);
            var record = new ResetPasswordViewModel { Email = user.Email };
            record.Role = role;
            record.Code = UserManager.GeneratePasswordResetToken(id);
            record.UserId = id;
            return View(record);
        }

        [HttpPost]
        public ActionResult ResetUserPasswordCustom(ResetPasswordViewModel model)
        {
            var code = model.Code.Replace(" ", "+");
            var result = UserManager.ResetPassword(model.UserId, code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("Details", "Account", new { id = model.UserId, role = model.Role });
            }
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            var userEmail = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Id = new Helpers.LoggedInUserInfo().GetLoggedInUserId() }).ResultList.FirstOrDefault();
            new Helpers.LogGeneration().CreateGeneralLog(new ApplicationDbContext(), new Log { Action = "Log Off", DateTime = DateTime.Now, AddedBy = UserManager.GetRoles(new Helpers.LoggedInUserInfo().GetLoggedInUserId()).FirstOrDefault(), ModelId = userEmail.Email });
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

        #region CustomMethods

        public JsonResult DoesPhoneNumberExist(string PhoneNumber, string Id)
        {
            var getRecord = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { PhoneNumber = PhoneNumber }).ResultList.FirstOrDefault();
            bool validatePhoneNumber = getRecord != null && (getRecord.Id != Id || string.IsNullOrEmpty(Id)) ? false : true;
            return Json(validatePhoneNumber, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetUsersCount(string role)
        {
            var search = new AccountSearchViewModel { Select2 = true };
            search.Role = string.IsNullOrEmpty(role) ? "Client" : role;
            var check = new Logic().GetUsersCount(search);
            return Json(check, JsonRequestBehavior.AllowGet);
        }

        public void CreateRoles()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
                if (!(roleManager.RoleExists("Administrator") && roleManager.RoleExists("User") && roleManager.RoleExists("Client") && roleManager.RoleExists("Worker")))
                {
                    roleManager.Create(new IdentityRole("SuperAdministrator"));
                    roleManager.Create(new IdentityRole("Administrator"));
                    roleManager.Create(new IdentityRole("Stylist"));
                    roleManager.Create(new IdentityRole("Client"));
                    roleManager.Create(new IdentityRole("Worker"));
                }
            }
        }
        #endregion
    }
}