﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BusinessLogic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Models;
using Models.Identity;
using Models.ViewModels;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace BarberShop.Controllers
{
    public class ExcelImportController : Controller
    {
        [Authorize]
        public ActionResult Clients()
        {
            return View();
        }
        // GET: ExcelImport
        public async Task<bool> ImportClients()
        {
            try
            {
                var filePath = string.Empty;
                foreach (string upload in Request.Files)
                {
                    if (Request.Files[upload].FileName != "")
                    {
                        string path = AppDomain.CurrentDomain.BaseDirectory + "/App_Data/uploads/";
                        string filename = Path.GetFileName(Request.Files[upload].FileName);
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        Request.Files[upload].SaveAs(Path.Combine(path, filename));
                        filePath = Path.Combine(path, filename);
                    }
                }
                var strError = string.Empty;

                byte[] file = System.IO.File.ReadAllBytes(filePath);
                using (MemoryStream ms = new MemoryStream(file))
                using (ExcelPackage package = new ExcelPackage(ms))
                {
                    if (package.Workbook.Worksheets.Count == 0)
                    {
                        strError = "Your Excel file does not contain any work sheets";
                        return false;
                    }

                    else
                    {
                        var workSheet = package.Workbook.Worksheets[1];

                        {
                            var loggedInUserInfo = new Helpers.LoggedInUserInfo().GetLoggedInUser();
                            var clientList = new List<ApplicationUser>();
                            var addressList = new List<Address>();
                            var clientRoleList = new List<IdentityUserRole>();
                            var roleId = new Logic().GetRoleId<Select2OptionModel>(new RoleSearchViewModel { Name = "Client", Select2 = true }).ResultList.First().id;
                            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
                            var hashPassword = userManager.PasswordHasher;
                            var existingClients = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel());

                            for (int row = workSheet.Dimension.Start.Row + 1;
                                     row <= workSheet.Dimension.End.Row;
                                     row++)
                            {
                                var excel = new RegisterViewModel();

                                for (int col = workSheet.Dimension.Start.Column;
                                         col <= workSheet.Dimension.End.Column;
                                         col++)
                                {
                                    if (workSheet.Cells[row, col] != null && workSheet.Cells[row, col].Value != null)
                                    {
                                        switch (col)
                                        {
                                            case 1: excel.Name = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 2: excel.PhoneNumber = workSheet.Cells[row, col].Value.ToString(); break;
                                            case 3:
                                                excel.Email = workSheet.Cells[row, col].Value.ToString();
                                                break;
                                            default: break;
                                        }
                                    }

                                }


                                string email = string.IsNullOrEmpty(excel.Email) ? String.Format("{0}{1}@saloon.com", excel.Name.Replace(" ", string.Empty), excel.PhoneNumber.Replace("+", string.Empty).Replace(" ", string.Empty)) : excel.Email;
                                if (clientList.Where(x => x.Email == email || x.PhoneNumber == excel.PhoneNumber || x.UserName == excel.Email).Count() == 0 && existingClients.ResultList.Where(x => x.Email == email || x.PhoneNumber == excel.PhoneNumber).Count() == 0)
                                {
                                    var Address = new Address { Id = Guid.NewGuid(), Active = true, Area = "", City = "", House = "", Street = "" };
                                    addressList.Add(Address);
                                    if (!string.IsNullOrEmpty(excel.PhoneNumber))
                                    {
                                        var clientModel = new ApplicationUser
                                        {
                                            Id = Guid.NewGuid().ToString(),
                                            Active = true,
                                            Address = null,
                                            AddressId = Address.Id,
                                            Email = email, 
                                            UserName = email,
                                            Name = excel.Name,
                                            PasswordHash = hashPassword.HashPassword("Pass@123"),
                                            PhoneNumber = excel.PhoneNumber,
                                            DOB = DateTime.Now.AddYears(-10),
                                            ImageUrl = "/images/users/default-user.png",
                                            ApplicationId = loggedInUserInfo.ApplicationId,
                                            BranchId = loggedInUserInfo.BranchId
                                        };
                                        clientList.Add(clientModel);
                                        clientRoleList.Add(new IdentityUserRole
                                        {
                                            RoleId = roleId,
                                            UserId = clientModel.Id
                                        });
                                    }
                                }
                            }
                            GC.Collect();
                            GC.WaitForPendingFinalizers();
                            var troublingId = clientList.Where(m => m.Email == "eefjevanhaaften@gmail.com").Select(m => m.Id).FirstOrDefault();
                            clientRoleList.RemoveAll(m => m.UserId == troublingId);
                            clientList.RemoveAll(m => m.Email == "eefjevanhaaften@gmail.com");
                            var check = new Logic().AddClientsInBulk(clientList, clientRoleList, addressList);
                        }
                        return true;
                    }
                }
            }


            catch (Exception ex)
            {
                string message = "Message " + ex.Message + "\n Stack Trace" + ex.StackTrace + "\n Inner Exception" + ex.InnerException;
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Import Client", StackTrace = message, DateTime = DateTime.Now });
                return false;
            }
        }
    }
}