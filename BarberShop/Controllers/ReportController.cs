﻿using BusinessLogic;
using Enums;
using Microsoft.AspNet.Identity;
using Models;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace BarberShop.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        //public ActionResult Index()
        //{
        //    var result = RenderRazorViewToString(this, "ReportPartialView", new List<AppointmentViewModel>());
        //     var pdfFile = PdfSharpConvert(result);
        //    return Content(result);
        //}

        //public string RenderRazorViewToString(Controller controller, string partialViewName, object model)
        //{
        //    ViewEngineResult result = ViewEngines.Engines.FindPartialView(controller.ControllerContext, partialViewName);
        //    if (result.View != null)
        //    {
        //        controller.ViewData.Model = model;
        //        StringBuilder sb = new StringBuilder();
        //        using(StringWriter sw = new StringWriter())
        //        {
        //            ViewContext viewContext = new ViewContext(controller.ControllerContext, result.View, ViewData, TempData, sw);
        //            result.View.Render(viewContext, sw);
        //            result.ViewEngine.ReleaseView(viewContext, result.View);
        //            return sw.GetStringBuilder().ToString();
        //        }

        //    }

        //    {
        //        ViewEngineResult result1 = ViewEngines.Engines.FindPartialView(controller.ControllerContext, partialViewName);
        //        if (result1.View != null)
        //        {
        //            controller.ViewData.Model = model;
        //            StringBuilder sb = new StringBuilder();
        //            using(StringWriter sw = new StringWriter(sb))
        //            {
        //                using(HtmlTextWriter output = new HtmlTextWriter(sw))
        //                {
        //                    ViewContext viewContext = new ViewContext(controller.ControllerContext, result.View, ViewData, TempData, output);
        //                    result.View.Render(viewContext, output);
        //                    var html = sb.ToString();
        //                }
        //            }
        //        }
        //    }
        //    return "";
        //}

        //public PartialViewResult ReportPartialView()
        //{
        //    var records = new Logic().GetAppointments<AppointmentViewModel>(new AppointmentSearchViewModel { BookingDate = DateTime.Now, Status = AppointmentStatus.Approved, AppointmentTreatmentStatus = AppointmentTreatmentStatus.Ended }).ResultList;
        //    return PartialView(records);
        //}

        //public static Byte[] PdfSharpConvert(String html)
        //{
        //    Byte[] res = null;
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(html, PdfSharp.PageSize.A4);
        //        pdf.Save(ms);
        //        res = ms.ToArray();
        //    }
        //    return res;
        //}

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult GetTreatmentReport()
        {
            return View();
        }

        public PartialViewResult TreatmentReport(TreatmentReportSearchViewModel model)
        {

            if (User.IsInRole("Stylist"))
            {
                model.StylistId = User.Identity.GetUserId();
            }
            var report = new Logic().GetTreatmentReport<TreatmentReportViewModel>(model).ResultList;
            ViewBag.dashboardView = model.DashboardView ? true : false;
            return PartialView(report);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult GetInvoiceReport()
        {
            return View();
        }

        public PartialViewResult InvoiceReport(InvoiceReportSearchViewModel model)
        {
            if (User.IsInRole("Stylist"))
            {
                model.StylistId = User.Identity.GetUserId();
            }
            model.DateTime = (model.DateTime == new DateTime() ? DateTime.Today : model.DateTime);
            model.EndingDateTime = (model.EndingDateTime == new DateTime() ? DateTime.Today.AddDays(7) : model.EndingDateTime);
            var report = new Logic().GetInvoiceReport<InvoiceReportViewModel>(model).ResultList;
            ViewBag.dashboardView = model.DashboardView ? true : false;
            return PartialView(report);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult GetClientPointReport()
        {
            return View();
        }

        public PartialViewResult ClientPointReport(ClientPointReportSearchViewModel model)
        {
            var report = new Logic().GetClientPointReport<ClientPointReportViewModel>(model).ResultList;
            ViewBag.dashboardView = model.DashboardView ? true : false;
            return PartialView(report);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult GetStylistCommissionReport()
        {
            return View();
        }

        public PartialViewResult StylistCommissionReport(StylistCommissionReportSearchViewModel model)
        {

            if (User.IsInRole("Stylist"))
            {
                model.StylistId = User.Identity.GetUserId();
            }
            model.DateTime = (model.DateTime == new DateTime() ? DateTime.Today : model.DateTime);
            model.EndingDateTime = (model.EndingDateTime == new DateTime() ? DateTime.Today.AddDays(1) : model.EndingDateTime);
            var report = new Logic().GetStylistCommissionReport<StylistCommissionReportViewModel>(model).ResultList;
            ViewBag.dashboardView = model.DashboardView ? true : false;
            return PartialView(report);
        }

        public PartialViewResult GetPresentUsers(string role, string action)
        {
            var result = new Logic().GetPresentUsers(role, action);
            ViewBag.role = role;
            if (User.IsInRole("Stylist") && role == "Stylist")
            {
                var userEmail = new Helpers.LoggedInUserInfo().GetLoggedInUser().Email;
                result = result.Where(m => m.Email == userEmail).ToList();
            }
            return PartialView(result);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult GetClientTreatmentHistory(string id)
        {
            ViewBag.id = id;
            var appointments = new Logic().GetAppointments<AppointmentViewModel>(new AppointmentSearchViewModel { ClientId = id, AppointmentTreatmentStatus = AppointmentTreatmentStatus.Ended, Status = AppointmentStatus.Completed }).ResultList;

            return View(appointments);
        }

        public JsonResult ValidateEmailFromDatabase(string Email, string Id)
        {
            var result = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Email = Email, GetAllResults = true }).ResultList;
            if (result.Count() > 0)
            {
                if (result.FirstOrDefault().Active && result.FirstOrDefault().Id != Id)
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}