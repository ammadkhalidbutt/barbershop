﻿using BusinessLogic;
using Models;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BarberShop.Controllers
{
    public class StylistTreatmentController : Controller
    {
        // GET: TreatmentStylist
        [Authorize]
        public ActionResult Index(Guid id)
        {
            Session["treatmentId"] = id;
            var result = (new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { TreatmentId = id, RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "Stylist" }).ResultList.First().Id }).ResultList);
            return View(result);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Create()
        {
            Session["action"] = "Create";
            var treatmentId = Session["treatmentId"] != null ? Guid.Parse(Session["treatmentId"].ToString()) : new Guid();
            //var result = new List<StylistTreatmentViewModel>();
            //var resultItem = new StylistTreatmentViewModel
            //{
            //    StylistsList = new Logic().GetUsers<Select2OptionModel>(new AccountSearchViewModel { RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "User" }).ResultList.First().Id, Select2 = true, AvoidTreatmentId = treatmentId }).ResultList.GroupBy(x => x.id).ToList().Select(m => new Select2OptionModel { id = m.Max(x => x.id), text = m.Max(x => x.text) }).ToList(),
            //    TreatmentId = treatmentId
            //};
            //result.Add(resultItem);

            var result = new StylistTreatmentViewModel
            {
                StylistsList = new Logic().GetUsers<Select2OptionModel>(new AccountSearchViewModel { RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "Stylist" }).ResultList.First().Id, Select2 = true, AvoidTreatmentId = treatmentId }).ResultList.GroupBy(x => x.id).ToList().Select(m => new Select2OptionModel { id = m.Max(x => x.id), text = m.Max(x => x.text) }).ToList(),
                TreatmentId = treatmentId,
                Action = "AddStylistTreatment"
            };
            return View(result);
        }

        [HttpPost]
        public ActionResult Create(StylistTreatmentViewModel model)
        {
            //foreach(var stylistTreatment in model)
            //{
            //    var record = new StylistTreatmentViewModel { Active = true, IsDeleted = false, Date = DateTime.Now, PercentageCommission = stylistTreatment.PercentageCommission, Speciality = stylistTreatment.Speciality, TreatmentId = stylistTreatment.TreatmentId, StylistId = stylistTreatment.StylistId };
            //    var result = new Logic().Create(record);
            //    if (result == new Guid())
            //        break;
            //}
            //foreach(var stylist in model.SelectedSytlists)
            //{
            //    var record = new StylistTreatmentViewModel { Active = true, IsDeleted = false, Date = DateTime.Now, PercentageCommission = model.PercentageCommission, Speciality = model.Speciality, TreatmentId = model.TreatmentId, StylistId = stylist };
            //    var result = new Logic().Create(record);
            //    if (result == new Guid())
            //        break;
            //}

            var result = new Logic().Create(model);
            if (result != new Guid())
            {
                return RedirectToAction("Index", new { id = model.TreatmentId });
            }
            else
            {
                return View(model);
            }

        }
        [Authorize(Roles = "SuperAdministrator, Administrator")]
        public ActionResult AddStylistTreatment(string id)
        {
            return View(new StylistTreatmentViewModel { StylistId = id, Action = "AddStylistTreatment" });
        }

        public ActionResult _AddStylistTreatment(StylistTreatmentViewModel model)
        {
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult AddStylistTreatment(StylistTreatmentViewModel model)
        {
            var result = new Logic().Create(model);
            if (Request.IsAjaxRequest())
            {
                if (result != new Guid())
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (result != new Guid())
                {
                    return RedirectToAction("Details", "Account", new { Id = model.StylistId });
                }
                else
                {
                    return View();
                }
            }
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Edit(Guid? treatmentId, string stylistId)
        {
            Session["action"] = "Edit";
            var record = GetStylistTreatmentForEdit(stylistId, treatmentId.Value);
            record.Action = "Edit";

            if (Request.IsAjaxRequest())
            {
                return View("_AddStylistTreatment", record);
            }
            return View("Create", record);
        }

        [HttpPost]
        public ActionResult Edit(StylistTreatmentViewModel model)
        {
            var result = new Logic().Edit(model);

            if (Request.IsAjaxRequest())
            {
                if (result != new Guid())
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (result != new Guid())
                {
                    var record = GetStylistTreatmentForEdit(model.StylistId, model.TreatmentId);
                    record.Action = "Edit";
                    return View("Create", record);
                }
                else
                {
                    return View();
                }
            }

        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Delete(string stylistId, Guid treatmentId)
        {
            var result = new Logic().Delete(new StylistTreatmentViewModel { StylistId = stylistId, TreatmentId = treatmentId });
            if (Request.IsAjaxRequest())
            {
                if (result != new Guid())
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (result != new Guid())
                {
                    return RedirectToAction("Index", new { id = treatmentId });
                }
                else
                {
                    var record = new Logic().Read<StylistTreatmentViewModel>(new StylistTreatmentSearchViewModel { StylistId = stylistId, TreatmentId = treatmentId });
                    return View("Create", record);
                }
            }
        }


        public ActionResult GetStylistTreatments(string stylistId)
        {
            return PartialView(new Logic().Read<StylistTreatmentViewModel>(new StylistTreatmentSearchViewModel { StylistId = stylistId }).ResultList);
        }

        public ActionResult Test()
        {
            //Session["PreviouslySelectedStylist"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult Test(List<StylistTreatmentViewModel> modelList)
        {
            foreach (var stylistTreatment in modelList)
            {
                var record = new StylistTreatmentViewModel { Active = true, IsDeleted = false, Date = DateTime.Now, PercentageCommission = stylistTreatment.PercentageCommission, Speciality = stylistTreatment.Speciality, TreatmentId = stylistTreatment.TreatmentId, StylistId = stylistTreatment.StylistId };
                var result = new Logic().Create(record);
                if (result == new Guid())
                    break;
            }
            return RedirectToAction("Index", new { id = modelList.FirstOrDefault().TreatmentId });
        }

        public StylistTreatmentViewModel GetStylistTreatmentForEdit(string stylistId, Guid treatmentId)
        {
            var record = new Logic().Read<StylistTreatmentViewModel>(new StylistTreatmentSearchViewModel { StylistId = stylistId, TreatmentId = treatmentId }).ResultList.First();
            record.StylistsList = new Logic().GetUsers<Select2OptionModel>(new AccountSearchViewModel { RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "Stylist" }).ResultList.First().Id, Select2 = true }).ResultList;
            return record;
        }
    }
}