﻿using BusinessLogic;
using Helpers.Select2;
using Models;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BarberShop.Controllers
{
    public class TreatmentCategoryController : Controller
    {
        // GET: TreatmentCategory
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult IndexPartial(TreatmentCategorySearchViewModel search)
        {
            var result = new Logic().GetTreatmentCategories<TreatmentCategoryViewModel>(search).ResultList;
            return PartialView(result);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Create()
        {
            return View(new TreatmentCategoryViewModel { Action = "Create"});
        }

        public PartialViewResult CreatePartial(TreatmentCategoryViewModel model)
        {
            return PartialView(model);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Create(TreatmentCategoryViewModel model)
        {

            if (model.UploadedImage != null)
            {
                var pic = System.IO.Path.GetFileName(model.UploadedImage.FileName);
                var mappedPath = Server.MapPath("/Attachments/treatmentCategories");
                var path = System.IO.Path.Combine(mappedPath, pic);
                model.UploadedImage.SaveAs(path);
                model.Image = "/Attachments/treatment-categories/" + pic;
            }
            else
            {
                model.Image = "/images/treatment/default-treatment.jpg";
            }
            var result = new Logic().Create(model);
            if (result != new Guid())
            {
                return RedirectToAction("Index");
            }
            else
            {
                return PartialView("CreatePartial", model);
            }
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Edit(Guid id)
        {
            var result = new Logic().GetTreatmentCategories<TreatmentCategoryViewModel>(new TreatmentCategorySearchViewModel { Id = id }).ResultList.FirstOrDefault();
            result.Action = "Edit";
            return View("Create", result);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Edit(TreatmentCategoryViewModel model)
        {
            if (model.UploadedImage != null)
            {
                var pic = System.IO.Path.GetFileName(model.UploadedImage.FileName);
                var mappedPath = Server.MapPath("/images/treatmentCategories");
                var path = System.IO.Path.Combine(mappedPath, pic);
                model.UploadedImage.SaveAs(path);
                model.Image = "/images/treatmentCategories/" + pic;
            }
            var result = new Logic().Edit(model);
            if(result != new Guid())
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View("Create", model);
            }
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Delete(Guid id)
        {
            var result = new Logic().Delete(new TreatmentCategoryViewModel { Id = id });
                return RedirectToAction("Index");
        }

        public JsonResult GetCategories(string prefix, int pageSize, int pageNumber)
        {
            var result = new Logic().GetTreatmentCategories<Select2OptionModel>(new TreatmentCategorySearchViewModel { Select2 = true, Name = prefix}).ResultList;
            return Json(new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, result), JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult GetDetailModal(Guid id)
        {
            var category = new TreatmentCategorySearchViewModel { Id = id };
            return PartialView(new Logic().GetTreatmentCategories<TreatmentCategoryViewModel>(category).ResultList.First());
        }
    }
}