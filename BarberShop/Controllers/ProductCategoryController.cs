﻿using BusinessLogic;
using Helpers.Select2;
using Models;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BarberShop.Controllers
{
    public class ProductCategoryController : BaseController
    {
        // GET: ProductCategory
        public ActionResult Index(ProductCategorySearchViewModel search)
        {
            return View();
        }

        public PartialViewResult IndexPartial(ProductCategorySearchViewModel search)
        {
            var result = new Logic().GetProductCategories<ProductCategoryViewModel>(search).ResultList;
            return PartialView(result);
        }

        // GET: ProductCategory/Details/5
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ProductCategory/Create
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Create()
        {
            return View(new ProductCategoryViewModel { Action = "Create" });
        }

        public PartialViewResult CreatePartial(ProductCategoryViewModel model)
        {
            return PartialView(model);
        }

        // POST: ProductCategory/Create
        [HttpPost]
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Create(ProductCategoryViewModel model)
        {
            try
            {
                // TODO: Add insert logic here

                if (model.UploadedImage != null)
                {
                    var pic = System.IO.Path.GetFileName(model.UploadedImage.FileName);
                    var mappedPath = Server.MapPath("/Attachments/productCategories");
                    if (!Directory.Exists(mappedPath))
                        Directory.CreateDirectory(mappedPath);
                    var path = System.IO.Path.Combine(mappedPath, pic);
                    model.UploadedImage.SaveAs(path);
                    model.Image = "/Attachments/productCategories/" + pic;
                }
                else
                {
                    model.Image = "/images/default/default.jpg";
                }
                var check = new Logic(LoggedInUserId).Create(model);
                if(check != new Guid())
                return RedirectToAction("Index");
                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

        // GET: ProductCategory/Edit/5
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Edit(Guid id)
        {
            var record = new Logic(LoggedInUserId).GetProductCategory(id);
            record.Action = "Edit";
            return View("Create",record);
        }

        // POST: ProductCategory/Edit/5
        [HttpPost]
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Edit(ProductCategoryViewModel model)
        {
            try
            {
                // TODO: Add update logic here
                if (model.UploadedImage != null)
                {
                    var pic = System.IO.Path.GetFileName(model.UploadedImage.FileName);
                    var mappedPath = Server.MapPath("/Attachments/productCategories");
                    var path = System.IO.Path.Combine(mappedPath, pic);
                    model.UploadedImage.SaveAs(path);
                    model.Image = "/Attachments/productCategories/" + pic;
                }
                var check = new Logic(LoggedInUserId).Edit(model);
                if(check != new Guid())
                return RedirectToAction("Index");
                return View("Create", model);

            }
            catch
            {
                return View("Create", model);
            }
        }

        // GET: ProductCategory/Delete/5
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Delete(Guid id)
        {
            var check = new Logic(LoggedInUserId).Delete(new ProductCategoryViewModel { Id = id });
            return RedirectToAction("Index");
        }

        public PartialViewResult GetDetailModal(Guid id)
        {
            return PartialView(new Logic().GetProductCategory(id));
        }

        #region Json Methods

        public JsonResult GetCategoriesForSelect2(string prefix, int pageSize, int pageNumber)
        {
            var result = new Logic().GetProductCategories<Select2OptionModel>(new ProductCategorySearchViewModel { Select2 = true, Name = prefix }).ResultList;
            return Json(new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, result), JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
