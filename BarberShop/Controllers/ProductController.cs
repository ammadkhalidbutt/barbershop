﻿using BusinessLogic;
using Helpers.Select2;
using Models;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BarberShop.Controllers
{
    public class ProductController : BaseController
    {
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult _IndexPartial(ProductSearchViewModel search)
        {
            var result = new Logic().GetProducts<ProductViewModel>(search).ResultList;
            return PartialView(result);
        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Product/Create
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Create()
        {
            return View(new ProductViewModel { Action = "Create"});
        }

        // POST: Product/Create
        [HttpPost]
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Create(ProductViewModel model)
        {
            try
            {
                // TODO: Add insert logic here

                if (model.Image != null)
                {
                    var pic = System.IO.Path.GetFileName(model.Image.FileName);
                    var mappedPath = Server.MapPath("/Attachments/product");  
                    if (!Directory.Exists(mappedPath))
                    {
                        Directory.CreateDirectory(mappedPath);
                    }
                    var path = System.IO.Path.Combine(mappedPath, pic);
                    model.Image.SaveAs(path);
                    model.ImageUrl = "/Attachments/product/" + pic;
                }
                else
                {
                    model.ImageUrl = "/images/default/default.jpg";
                }
                var result = new Logic(LoggedInUserId).Create(model);
                if (result != new Guid())
                {
                    return RedirectToAction("Index", "Product");
                }

                return View(model);
            }
            catch(Exception ex)
            {
                return View(model);
            }
        }

        [HttpGet]
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Edit(Guid id)
        {
            var result = new Logic().GetProduct(id);
            result.Action = "Edit";
            return View("Create", result);
        }

        // POST: Product/Edit/5
        [HttpPost]
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Edit(ProductViewModel model)
        {
            try
            {
                // TODO: Add update logic here

                if (model.Image != null)
                {
                    var pic = System.IO.Path.GetFileName(model.Image.FileName);
                    var mappedPath = Server.MapPath("/images/product");
                    if (!Directory.Exists(mappedPath))
                    {
                        Directory.CreateDirectory(mappedPath);
                    }
                    var path = System.IO.Path.Combine(mappedPath, pic);
                    model.Image.SaveAs(path);
                    model.ImageUrl = "/images/product/" + pic;
                }
                var result = new Logic(LoggedInUserId).Edit(model);
                if (result != new Guid())
                {
                    return RedirectToAction("Index", "Product");
                }

                return View(model);
            }
            catch(Exception ex)
            {
                return View();
            }
        }


        // POST: Product/Delete/5
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Delete(Guid id)
        {
            try
            {
                // TODO: Add delete logic here
                var result = new Logic(LoggedInUserId).Delete(new ProductViewModel { Id = id });
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        public PartialViewResult GetDetailModal(Guid id)
        {
            return PartialView(new Logic().GetProduct(id));
        }

        #region JSON methods

        public JsonResult GetProductsForSelect2(string prefix, int pageSize, int pageNumber)
        {
            var result = new Logic().GetProducts<Select2OptionModel>(new ProductSearchViewModel { Select2 = true, Name = prefix }).ResultList;
            return Json(new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, result), JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
