﻿using BusinessLogic;
using Enums;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using Models;
using Models.Identity;
using Models.ViewModels;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BarberShop.Controllers
{
    public class UtilityImportController : Controller
    {

        // GET: UtilityImport
        [Authorize(Roles = "SuperAdministrator, Administrator")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public bool Index(string difference)
        {
            try
            {
                var filePath = string.Empty;

                foreach (string upload in Request.Files)
                {
                    if (Request.Files[upload].FileName != "")
                    {
                        string path = AppDomain.CurrentDomain.BaseDirectory + "/App_Data/uploads/";
                        string filename = Path.GetFileName(Request.Files[upload].FileName);
                        Request.Files[upload].SaveAs(Path.Combine(path, filename));
                        filePath = Path.Combine(path, filename);
                    }
                }
                var strError = string.Empty;

                byte[] file = System.IO.File.ReadAllBytes(filePath);
                using (MemoryStream ms = new MemoryStream(file))
                using (ExcelPackage package = new ExcelPackage(ms))
                {
                    if (package.Workbook.Worksheets.Count == 0)
                    {
                        strError = "Your Excel file does not contain any work sheets";
                        return false;
                    }

                    else
                    {
                        var workSheets = package.Workbook.Worksheets;
                        foreach (var sheet in workSheets)
                        {

                            switch (sheet.Name)
                            {
                                case "Administrator":
                                    ImportAdministrator(sheet); GC.Collect();
                                    GC.WaitForPendingFinalizers(); break;
                                case "Stylist":
                                    ImportStylist(sheet); GC.Collect();
                                    GC.WaitForPendingFinalizers(); break;
                                case "Point":
                                    ImportPoint(sheet); GC.Collect();
                                    GC.WaitForPendingFinalizers(); break;
                                case "Treatment":
                                    ImportTreatment(sheet); GC.Collect();
                                    GC.WaitForPendingFinalizers(); break;
                                case "Treatment Stylist":
                                    ImportTreatmentStylist(sheet); GC.Collect();
                                    GC.WaitForPendingFinalizers(); break;
                                case "Client":
                                    ImportClient(sheet); GC.Collect();
                                    GC.WaitForPendingFinalizers(); break;
                                case "Appointment":
                                    ImportAppointment(sheet); GC.Collect();
                                    GC.WaitForPendingFinalizers(); break;
                            }

                        }
                        return true;
                    }
                }
            }

            catch (Exception ex)
            {
                return false;
            }
        }

        //public void ImportApplication(ExcelWorksheet worksheet)
        //{
        //    try
        //    {
        //        var applicationCheck = new Logic().GetApplications<ApplicationViewModel>(new ApplicationSearchViewModel()).ResultList;
        //        if (applicationCheck.Count() == 0)
        //        {
        //            var application = new ApplicationViewModel();
        //            for (var row = worksheet.Dimension.Start.Row + 1;
        //                row <= worksheet.Dimension.Start.Row + 1;
        //                row++)
        //            {
        //                for (var col = worksheet.Dimension.Start.Column;
        //                    col <= worksheet.Dimension.End.Column;
        //                    col++)
        //                {
        //                    if (worksheet.Cells[row, col] != null && worksheet.Cells[row, col].Value != null)
        //                    {

        //                        switch (col)
        //                        {
        //                            case 1: application.Name = worksheet.Cells[row, col].Value.ToString(); break;
        //                            default: break;
        //                        }
        //                        var record = new Logic().Create(application);

        //                    }
        //                }
        //            }

        //        }
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }



        //}

        //public void ImportBranch(ExcelWorksheet worksheet)
        //{
        //    try
        //    {
        //        var branchCheck = new Logic().GetBranches<BranchViewModel>(new BranchSearchViewModel()).ResultList;

        //        if (branchCheck.Count() == 0)
        //        {

        //            for (var row = worksheet.Dimension.Start.Row + 1;
        //            row <= worksheet.Dimension.End.Row;
        //            row++)
        //            {
        //                var branch = new BranchViewModel();
        //                var address = new AddressViewModel();
        //                for (var col = worksheet.Dimension.Start.Column;
        //                    col <= worksheet.Dimension.End.Column;
        //                    col++)
        //                {
        //                    if (worksheet.Cells[row, col] != null && worksheet.Cells[row, col].Value != null)
        //                    {

        //                        switch (col)
        //                        {
        //                            case 1: branch.Name = worksheet.Cells[row, col].Value.ToString(); break;
        //                            case 2: branch.ApplicationId = new Logic().GetApplications<ApplicationViewModel>(new ApplicationSearchViewModel()).ResultList.FirstOrDefault().Id; break;
        //                            case 3: branch.OpeningTime = (TimeSpan)worksheet.Cells[row, col].Value; break;
        //                            case 4: branch.ClosingTime = (TimeSpan)worksheet.Cells[row, col].Value; break;
        //                            case 5: branch.VAT = (float)worksheet.Cells[row, col].Value; break;
        //                            case 6: address.City = worksheet.Cells[row, col].Value.ToString(); break;
        //                            case 7: address.Area = worksheet.Cells[row, col].Value.ToString(); break;
        //                            case 8: address.Street = worksheet.Cells[row, col].Value.ToString(); break;
        //                            case 9: address.House = worksheet.Cells[row, col].Value.ToString(); break;
        //                        }

        //                    }
        //                }
        //                branch.Address = address;
        //                var record = new Logic().Create(branch);
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        throw;
        //    }

        //}

        public void ImportAdministrator(ExcelWorksheet worksheet)
        {
            try
            {
                for (var row = worksheet.Dimension.Start.Row + 1;
                    row <= worksheet.Dimension.End.Row;
                    row++)
                {
                    var admin = new RegisterViewModel { Address = new AddressViewModel() };
                    for (var col = worksheet.Dimension.Start.Column;
                        col <= worksheet.Dimension.End.Column;
                        col++)
                    {
                        if (worksheet.Cells[row, col] != null && worksheet.Cells[row, col].Value != null)
                        {
                            switch (col)
                            {
                                case 1: admin.Name = worksheet.Cells[row, col].Value.ToString(); break;
                                case 2: admin.Email = worksheet.Cells[row, col].Value.ToString(); break;
                                case 3: admin.DOB = DateTime.Parse(worksheet.Cells[row, col].Value.ToString()); break;
                                case 4: admin.Gender = (Gender)Enum.Parse(typeof(Gender), worksheet.Cells[row, col].Value.ToString()); break;
                                case 5: admin.PhoneNumber = worksheet.Cells[row, col].Value.ToString(); break;
                                case 6: admin.Address.City = worksheet.Cells[row, col].Value.ToString(); break;
                                case 7: admin.Address.Area = worksheet.Cells[row, col].Value.ToString(); break;
                                case 8: admin.Address.Street = worksheet.Cells[row, col].Value.ToString(); break;
                                case 9: admin.Address.House = worksheet.Cells[row, col].Value.ToString(); break;
                            }

                        }
                    }
                    admin.Role = "Administrator";
                    AddUser(admin);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public void ImportStylist(ExcelWorksheet worksheet)
        {
            try
            {
                for (var row = worksheet.Dimension.Start.Row + 1;
                    row <= worksheet.Dimension.End.Row;
                    row++)
                {
                    var stylist = new RegisterViewModel { Address = new AddressViewModel() };
                    for (var col = worksheet.Dimension.Start.Column;
                        col <= worksheet.Dimension.End.Column;
                        col++)
                    {
                        if (worksheet.Cells[row, col] != null && worksheet.Cells[row, col].Value != null)
                        {
                            switch (col)
                            {
                                case 1: stylist.Name = worksheet.Cells[row, col].Value.ToString(); break;
                                case 2: stylist.Email = worksheet.Cells[row, col].Value.ToString(); break;
                                case 3: stylist.DOB = DateTime.Parse(worksheet.Cells[row, col].Value.ToString()); break;
                                case 4: stylist.Gender = (Gender)Enum.Parse(typeof(Gender), worksheet.Cells[row, col].Value.ToString()); break;
                                case 5: stylist.PhoneNumber = worksheet.Cells[row, col].Value.ToString(); break;
                                case 6: stylist.Address.City = worksheet.Cells[row, col].Value.ToString(); break;
                                case 7: stylist.Address.Area = worksheet.Cells[row, col].Value.ToString(); break;
                                case 8: stylist.Address.Street = worksheet.Cells[row, col].Value.ToString(); break;
                                case 9: stylist.Address.House = worksheet.Cells[row, col].Value.ToString(); break;
                            }

                        }
                    }
                    stylist.Role = "Stylist";
                    AddUser(stylist);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public void ImportPoint(ExcelWorksheet worksheet)
        {
            try
            {
                for (var row = worksheet.Dimension.Start.Row + 1;
                            row <= worksheet.Dimension.Start.Row + 1;
                            row++)
                {
                    var point = new PointViewModel();
                    for (var col = worksheet.Dimension.Start.Column;
                        col <= worksheet.Dimension.End.Column;
                        col++)
                    {
                        if (worksheet.Cells[row, col] != null && worksheet.Cells[row, col].Value != null)
                        {

                            switch (col)
                            {
                                case 1: point.Worth = int.Parse(worksheet.Cells[row, col].Value.ToString()); break;
                            }
                            if (new Logic().GetPoint() != null)
                            {
                                var editRecord = new Logic().Edit(point);
                            }
                            else
                            {
                                var record = new Logic().Create(point);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public void ImportTreatment(ExcelWorksheet worksheet)
        {
            try
            {
                for (var row = worksheet.Dimension.Start.Row + 1;
          row <= worksheet.Dimension.End.Row;
          row++)
                {
                    var treatment = new TreatmentViewModel();
                    for (var col = worksheet.Dimension.Start.Column;
                        col <= worksheet.Dimension.End.Column;
                        col++)
                    {
                        if (worksheet.Cells[row, col] != null && worksheet.Cells[row, col].Value != null)
                        {
                            switch (col)
                            {
                                case 1: treatment.Name = worksheet.Cells[row, col].Value.ToString(); break;
                                case 2: treatment.AddedBy = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Email = worksheet.Cells[row, col].Value.ToString() }).ResultList.FirstOrDefault().Id; break;
                                case 3: treatment.Points = int.Parse(worksheet.Cells[row, col].Value.ToString()); break;
                                case 4: treatment.Price = int.Parse(worksheet.Cells[row, col].Value.ToString()); break;
                                case 5: treatment.ApproximateTime = int.Parse(worksheet.Cells[row, col].Value.ToString()); break;
                                case 6: treatment.Description = worksheet.Cells[row, col].Value.ToString(); break;
                            }

                        }
                    }
                    var check = new Logic().GetTreatments<TreatmentViewModel>(new TreatmentSearchViewModel { Name = treatment.Name }).ResultList;
                    if (check.Count() > 0)
                    {
                        treatment.Id = check.FirstOrDefault().Id;
                        var editResult = new Logic().Edit(treatment);
                    }
                    else
                    {
                        var createResult = new Logic().Create(treatment);
                    }
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public void ImportTreatmentStylist(ExcelWorksheet worksheet)
        {
            try
            {
                for (var row = worksheet.Dimension.Start.Row + 1;
         row <= worksheet.Dimension.End.Row;
         row++)
                {
                    var treatmentStylist = new StylistTreatmentViewModel();
                    for (var col = worksheet.Dimension.Start.Column;
                        col <= worksheet.Dimension.End.Column;
                        col++)
                    {
                        if (worksheet.Cells[row, col] != null && worksheet.Cells[row, col].Value != null)
                        {
                            switch (col)
                            {
                                case 1: treatmentStylist.TreatmentId = new Logic().GetTreatments<TreatmentViewModel>(new TreatmentSearchViewModel { Name = worksheet.Cells[row, col].Value.ToString() }).ResultList.FirstOrDefault().Id; break;
                                case 2: treatmentStylist.StylistId = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Email = worksheet.Cells[row, col].Value.ToString() }).ResultList.FirstOrDefault().Id; break;
                                case 3: treatmentStylist.PercentageCommission = int.Parse(worksheet.Cells[row, col].Value.ToString()); break;
                                case 4: treatmentStylist.Speciality = bool.Parse(worksheet.Cells[row, col].Value.ToString()); break;
                            }
                        }
                    }
                    var check = new Logic().Read<StylistTreatmentViewModel>(new StylistTreatmentSearchViewModel { TreatmentId = treatmentStylist.TreatmentId, StylistId = treatmentStylist.StylistId }).ResultList;
                    if (!(check.Count() > 0))
                    {
                        treatmentStylist.SelectedSytlists = new String[] { treatmentStylist.StylistId };
                        treatmentStylist.TreatmentName = worksheet.Cells[row, 1].Value.ToString();
                        var record = new Logic().Create(treatmentStylist);
                    }
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public void ImportClient(ExcelWorksheet worksheet)
        {
            try
            {
                for (var row = worksheet.Dimension.Start.Row + 1;
          row <= worksheet.Dimension.End.Row;
          row++)
                {
                    var client = new RegisterViewModel { Address = new AddressViewModel() };
                    for (var col = worksheet.Dimension.Start.Column;
                        col <= worksheet.Dimension.End.Column;
                        col++)
                    {
                        if (worksheet.Cells[row, col] != null && worksheet.Cells[row, col].Value != null)
                        {

                            switch (col)
                            {
                                case 1: client.Name = worksheet.Cells[row, col].Value.ToString(); break;
                                case 2: client.Email = worksheet.Cells[row, col].Value.ToString(); break;
                                case 3: client.DOB = DateTime.Parse(worksheet.Cells[row, col].Value.ToString()); break;
                                case 4: client.Gender = (Gender)Enum.Parse(typeof(Gender), worksheet.Cells[row, col].Value.ToString()); break;
                                case 5: client.PhoneNumber = worksheet.Cells[row, col].Value.ToString(); break;
                                case 6: client.Address.City = worksheet.Cells[row, col].Value.ToString(); break;
                                case 7: client.Address.Area = worksheet.Cells[row, col].Value.ToString(); break;
                                case 8: client.Address.Street = worksheet.Cells[row, col].Value.ToString(); break;
                                case 9: client.Address.House = worksheet.Cells[row, col].Value.ToString(); break;
                            }
                        }
                    }
                    client.Role = "Client";
                    AddUser(client);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public void ImportAppointment(ExcelWorksheet worksheet)
        {
            try
            {
                for (var row = worksheet.Dimension.Start.Row + 1;
                     row <= worksheet.Dimension.End.Row;
                     row++)
                {
                    var appointment = new AppointmentViewModel { Client = new ApplicationUser(), Treatment = new TreatmentViewModel(), Approver = new ApplicationUser() };
                    for (var col = worksheet.Dimension.Start.Column;
                        col <= worksheet.Dimension.End.Column;
                        col++)
                    {
                        if (worksheet.Cells[row, col] != null && worksheet.Cells[row, col].Value != null)
                        {

                            switch (col)
                            {
                                case 1: appointment.ClientId = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Email = worksheet.Cells[row, col].Value.ToString() }).ResultList.FirstOrDefault().Id; break;
                                case 2: appointment.BookingDate = DateTime.Parse(worksheet.Cells[row, col].Value.ToString()); break;
                                case 3: appointment.AppointmentStartingTime = DateTime.Parse(worksheet.Cells[row, col].Value.ToString()); break;
                                case 4: appointment.Discount = float.Parse(worksheet.Cells[row, col].Value.ToString()); break;
                                case 5: appointment.StylistId = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Email = worksheet.Cells[row, col].Value.ToString() }).ResultList.FirstOrDefault().Id; break;
                                case 6: appointment.TreatmentId = new Logic().GetTreatments<TreatmentViewModel>(new TreatmentSearchViewModel { Name = worksheet.Cells[row, col].Value.ToString() }).ResultList.FirstOrDefault().Id; break;
                                case 7: appointment.Status = (AppointmentStatus)Enum.Parse(typeof(AppointmentStatus), worksheet.Cells[row, col].Value.ToString()); break;
                                case 8: appointment.AppointmentTreatmentStatus = (AppointmentTreatmentStatus)Enum.Parse(typeof(AppointmentTreatmentStatus), worksheet.Cells[row, col].Value.ToString()); break;
                                case 9: appointment.Approver.Id = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Email = worksheet.Cells[row, col].Value.ToString() }).ResultList.FirstOrDefault().Id; break;
                            }

                        }
                    }
                    var record = new Logic().Create(appointment);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }


        #region Functions

        public void AddUser(RegisterViewModel model)
        {
            model.Password = "Asdfjkl;1";
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var check = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Email = model.Email }).ResultList;
            if (check.Count() > 0)
            {
                model.Id = check.FirstOrDefault().Id;
                var editResult = new Logic().Edit(model);
                var provider = new DpapiDataProtectionProvider("BarberShop");
                userManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(
                    provider.Create("PasswordReset"));
                var token = userManager.GeneratePasswordResetToken(model.Id);
                userManager.ResetPassword(model.Id, token, model.Password);
            }
            else
            {
                var createResult = new AccountController(manager, null).Register(model);
            }
        }

        #endregion


    }

}