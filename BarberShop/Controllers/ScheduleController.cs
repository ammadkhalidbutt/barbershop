﻿using AutoMapper;
using BusinessLogic;
using Enums;
using Models.Identity;
using Models.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BarberShop.Controllers
{
    public class ScheduleController : Controller
    {
        // GET: Schedule
        [Authorize]
        public ActionResult Index(StylistScheduleSearchViewModel search)
        {
            if (string.IsNullOrEmpty(search.Stylist.Id))
            {
                return View();
            }
            else
            {
                return View(new Logic().GetStylistSchedule<StylistScheduleViewModel>(search).ResultList);
            }
        }

        public ActionResult Create(string id)
        {
            var scheduleList = new Logic().GetStylistSchedule<StylistScheduleViewModel>(new StylistScheduleSearchViewModel { Stylist = new ApplicationUser { Id = id } }).ResultList.GroupBy(x => x.Day);
            var result = new StylistScheduleListViewModel { Action = scheduleList.Count() == 0 ? "Create" : "Edit", StylistId = id };
            foreach (var schedule in scheduleList)
            {
                var jqsModel = new JqsDayViewModel();
                jqsModel.day = (int)schedule.FirstOrDefault().Day - 1;
                foreach (var period in schedule)
                {
                    jqsModel.periods.Add(new JqsPeriodViewModel { start = period.StartingTime.ToString(), end = period.EndingTime.ToString() });
                }
                result.JqsList.Add(jqsModel);
            }
            return View(result);
        }

        [HttpPost]
        public ActionResult Create()
        {
            System.IO.StreamReader sr = new System.IO.StreamReader(Request.InputStream);
            string line = "";
            line = sr.ReadToEnd();
            //Console.WriteLine(line);

            dynamic dynObj = JsonConvert.DeserializeObject(line);
            var jo = dynObj["JqsList"].ToString();
            var jqsList = JArray.Parse(jo);
            var stylistId = dynObj["StylistId"].ToString();
            var action = dynObj["Action"].ToString();
            if (action == "Edit")
            {
                var delete = new Logic().Delete(new StylistScheduleViewModel { Stylist = new ApplicationUser { Id = stylistId } });
            }

            foreach (var schedule in jqsList)
            {
                foreach (var period in schedule.periods)
                {
                    var record = new StylistScheduleViewModel { Day = (WeekDays)Enum.Parse(typeof(WeekDays), (schedule["day"] + 1).ToString()), StartingTime = TimeSpan.Parse(period["start"].ToString()), EndingTime = TimeSpan.Parse(period["end"].ToString()), Stylist = new ApplicationUser { Id = stylistId } };
                    var result = new Logic().Create(record);
                }
            }


            return RedirectToAction("Index", "Account", new { role = "Stylist" });
        }

        public ActionResult Edit(string id)
        {
            var result = new StylistScheduleListViewModel { Action = "Edit", ScheduleList = new Logic().GetStylistSchedule<StylistScheduleViewModel>(new StylistScheduleSearchViewModel { Stylist = new ApplicationUser { Id = id } }).ResultList };
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<StylistScheduleViewModel, JqsPeriodViewModel>().ForMember(d => d.Id, x => x.MapFrom(s => s.Stylist.Id)); });
            var iMapper = config.CreateMapper();
            //result.JqsList.First().periods.Add(iMapper.Map<StylistScheduleViewModel, JqsPeriodViewModel>(result.ScheduleList.First()));
            return View("Create", result);
        }

        [HttpPost]
        public ActionResult Edit(StylistScheduleListViewModel model)
        {
            return View();
        }
    }
}