﻿using BusinessLogic;
using Helpers.Select2;
using Models;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BarberShop.Controllers
{
    public class Select2Controller : Controller
    {
        // GET: Select2
        public JsonResult Branch(string prefix, int pageSize, int pageNumber, Guid application)
        {

            var search = new BranchSearchViewModel { ApplicationId = application, Name = prefix, Select2 = true };
            var branches = new Logic().GetBranches<Select2OptionModel>(search);
            var result = new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, branches.ResultList);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Application(string prefix, int pageSize, int pageNumber)
        {
            var search = new ApplicationSearchViewModel { Name = prefix, Select2 = true };
            var applications = new Logic().GetApplications<Select2OptionModel>(search);
            var result = new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, applications.ResultList);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}