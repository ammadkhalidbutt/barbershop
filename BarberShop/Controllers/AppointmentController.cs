﻿using AutoMapper;
using BusinessLogic;
using Enums;
using Helpers.Select2;
using Microsoft.AspNet.Identity;
using Models;
using Models.Identity;
using Models.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;

namespace BarberShop.Controllers
{
    public class AppointmentController : Controller
    {
        // GET: Appointment
        public ActionResult Index(AppointmentSearchViewModel model)
        {
            if (!string.IsNullOrEmpty(model.TreatmentStatuses))
            {
                try
                {
                    model.MultiAppointmentTreatmentStatus = new List<int>();
                    foreach (var s in model.TreatmentStatuses.Split(','))
                    {
                        model.MultiAppointmentTreatmentStatus.Add((int)Enum.Parse(typeof(AppointmentTreatmentStatus), s));
                    }
                }
                catch
                {

                }
            }
            //added for stylish dashboard, as no authorization is required for that view, therefore this acts as a bypass.
            PartialViewResult check;
            var userId = User.Identity.GetUserId();
            if (Request.UrlReferrer != null)
            {
                var url = Request.UrlReferrer.AbsolutePath;
                if (User.IsInRole("SuperAdministrator") || User.IsInRole("Administrator") || url.Contains("Dashboard"))
                {
                    return View(GetAppointments(model).Model);
                }
                else if (User.IsInRole("Stylist"))
                {
                    model.StylistId = userId;
                }
                else if (User.IsInRole("Client"))
                {
                    model.ClientId = userId;
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            check = GetAppointments(model);
            return View(check.Model);
        }

        public ActionResult AppointmentDashboard()
        {
            var stylistId = User.IsInRole("Stylist") ? User.Identity.GetUserId() : "";
            var stylists = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Id = stylistId, RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "Stylist" }).ResultList.FirstOrDefault().Id }).ResultList.GroupBy(x => x.Email).Select(o => o.FirstOrDefault());
            var model = new FullCalendarSchedular { events = new List<FullCalendarEvents>(), resources = new List<FullCalendarResources>() };
            model.resources = stylists.Select(m => new FullCalendarResources { id = m.Id, title = m.Name, eventColor = "transparent" }).Distinct().ToList();
            return View(model);
        }


        public ActionResult AppointmentDetails(FullCalendarEvents EventModel)
        {
            return PartialView("_AppointmentDetails", EventModel);
        }
        public ActionResult AppointmentToolTip(FullCalendarEvents EventModel)
        {
            return PartialView("_AppointmentToolTip", EventModel);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator, Stylist")]
        public ActionResult Create()
        {
            ViewBag.ViewFor = "Create Appointment";
            return View(new AppointmentViewModel { BookingDate = DateTime.Now, AppointmentStartingTime = DateTime.Now, Action = "Create", Client = new ApplicationUser(), ClientsList = new List<SelectListItem>(), StylistsList = new List<SelectListItem>(), TreatmentsList = new List<SelectListItem>() });
        }

        public PartialViewResult CreatePartial(AppointmentViewModel model)
        {
            return PartialView(model);
        }
        public PartialViewResult CreatePartialModal(Guid? Id, DateTime? Date, Guid? StylistId)
        {
            ViewBag.ViewFor = Id == null || Id == new Guid() ? "Modal Create" : "Modal Edit";
            var BookingDate = Date ?? DateTime.Now;
            var stylist = StylistId != null || StylistId != new Guid() ? new Logic().GetUsers<Select2OptionModel>(new AccountSearchViewModel { Id = StylistId.ToString(), Select2 = true }).ResultList.Select(m => new SelectListItem { Text = m.text, Value = m.id }).ToList() : new List<SelectListItem>();
            AppointmentViewModel model = new AppointmentViewModel { BookingDate = BookingDate, AppointmentStartingTime = BookingDate, Action = "Create", Client = new ApplicationUser(), ClientsList = new List<SelectListItem>(), StylistsList = stylist, TreatmentsList = new List<SelectListItem>() };
            if (Id != null)
            {
                var AppointmentId = Id ?? new Guid();
                model = new Logic().GetAppointments<AppointmentViewModel>(new AppointmentSearchViewModel { Id = AppointmentId, StartingDateTime = BookingDate, EndingDateTime = BookingDate.AddDays(1).AddMilliseconds(-10), Select2 = false }).ResultList.First();
                model.Action = "Edit";
                model.ClientsList = new List<SelectListItem>() { (new SelectListItem { Value = model.Client.Id, Text = model.Client.Name, Selected = true }) };
                model.TreatmentsList = new List<SelectListItem>() { (new SelectListItem { Value = model.Treatment.Id.ToString(), Text = model.Treatment.Name, Selected = true }) };
                model.StylistsList = new List<SelectListItem>() { (new SelectListItem { Value = model.Stylist.Id, Text = model.Stylist.Name, Selected = true }) };
            }
            model.IsDashboard = true;
            return PartialView("CreatePartial", model);
        }

        public PartialViewResult CreatePartialForStart(Guid? Id, DateTime? Date, string ClientId, string StylistId, DateTime? LastAppointmentEndingDateTime)
        {

            var BookingDate = Date ?? DateTime.Now.Date;
            var lastAppointmentEndingDateTime = LastAppointmentEndingDateTime ?? DateTime.Now;
            ViewBag.ViewFor = Id == null || Id == new Guid() ? "Start Create" : "Start Edit";
            var stylist = !string.IsNullOrEmpty(StylistId) ? new Logic().GetUsers<Select2OptionModel>(new AccountSearchViewModel { Id = StylistId, Select2 = true }).ResultList.Select(m => new SelectListItem { Text = m.text, Value = m.id }).ToList() : new List<SelectListItem>();
            var client = !string.IsNullOrEmpty(ClientId) ? new Logic().GetUsers<Select2OptionModel>(new AccountSearchViewModel { Id = ClientId, Select2 = true }).ResultList.Select(m => new SelectListItem { Text = m.text, Value = m.id }).ToList() : new List<SelectListItem>();
            AppointmentViewModel model = new AppointmentViewModel { BookingDate = BookingDate, AppointmentStartingTime = lastAppointmentEndingDateTime, Action = "Create", Client = new ApplicationUser { Name = client.FirstOrDefault().Text, Id = ClientId }, ClientId = ClientId, StylistId = StylistId, ClientsList = client, Stylist = new ApplicationUser { Id = StylistId, Name = stylist.FirstOrDefault().Text }, StylistsList = stylist, TreatmentsList = new List<SelectListItem>() };
            if (Id != null)
            {
                var AppointmentId = Id ?? new Guid();
                model = new Logic().GetAppointments<AppointmentViewModel>(new AppointmentSearchViewModel { Id = AppointmentId, StartingDateTime = BookingDate, EndingDateTime = BookingDate.AddDays(1).AddMilliseconds(-10), Select2 = false }).ResultList.First();
                model.Action = "Edit";
                model.ClientsList = new List<SelectListItem>() { (new SelectListItem { Value = model.Client.Id, Text = model.Client.Name, Selected = true }) };
                model.TreatmentsList = new List<SelectListItem>() { (new SelectListItem { Value = model.Treatment.Id.ToString(), Text = model.Treatment.Name, Selected = true }) };
                model.StylistsList = new List<SelectListItem>() { (new SelectListItem { Value = model.Stylist.Id, Text = model.Stylist.Name, Selected = true }) };
            }
            model.IsDashboard = true;
            model.IsForStart = true;
            return PartialView("CreatePartial", model);
        }

        public PartialViewResult CreateMultiTreatmentAppointment(string StylistId)
        {

            var BookingDate = DateTime.Now.Date;
            var currentDateTime = DateTime.Now;
            var stylist = !string.IsNullOrEmpty(StylistId) ? new Logic().GetUsers<Select2OptionModel>(new AccountSearchViewModel { Id = StylistId, Select2 = true }).ResultList.Select(m => new SelectListItem { Text = m.text, Value = m.id }).ToList() : new List<SelectListItem>();
            AppointmentViewModel model = new AppointmentViewModel
            {
                BookingDate = BookingDate,
                AppointmentStartingTime = currentDateTime,
                AppointmentEndingTime = currentDateTime,
                ActualStartingTime = currentDateTime,
                ActualEndingTime = currentDateTime,
                Action = "Create",
                StylistId = StylistId,
                Stylist = new ApplicationUser
                {
                    Id = StylistId,
                    Name = stylist.FirstOrDefault().Text
                }
            };
            return PartialView("_MultiTreatmentAppointment", model);
        }

        [HttpPost]
        public ActionResult Create(AppointmentViewModel model)
        {
            ViewBag.ViewFor = "Create Appointment";
            model.Approver = new ApplicationUser { Id = User.Identity.GetUserId() };
            var result = new Logic().Create(model);
            return Json(result != new Guid(), JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult CreateMultiAppointment(AppointmentViewModel model)
        {
            model.Approver = new ApplicationUser { Id = User.Identity.GetUserId() };
            var result = new Logic().Create(model);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [Authorize(Roles = "Administrator, SuperAdministrator, Stylist")]
        public ActionResult Edit(Guid Id, DateTime bookingDate)
        {
            ViewBag.ViewFor = "Edit Appointment";
            var record = new Logic().GetAppointments<AppointmentViewModel>(new AppointmentSearchViewModel { Id = Id, StartingDateTime = bookingDate, EndingDateTime = bookingDate.AddDays(1).AddMilliseconds(-10), Select2 = false }).ResultList.First();
            record.Action = "Edit";
            record.ClientsList = new List<SelectListItem>() { (new SelectListItem { Value = record.Client.Id, Text = record.Client.Name, Selected = true }) };
            record.TreatmentsList = new List<SelectListItem>() { (new SelectListItem { Value = record.Treatment.Id.ToString(), Text = record.Treatment.Name, Selected = true }) };
            record.StylistsList = new List<SelectListItem>() { (new SelectListItem { Value = record.Stylist.Id, Text = record.Stylist.Name, Selected = true }) };
            return View("Create", record);
        }

        [HttpPost]
        public ActionResult Edit(AppointmentViewModel model)
        {
            ViewBag.ViewFor = "Edit Appointment";
            var request = Request.UrlReferrer.ToString().Contains("Dashboard");
            var result = new Logic().Edit(model);
            if (result != new Guid())
            {
                if (request || Request.IsAjaxRequest())
                    return Json(true, JsonRequestBehavior.AllowGet);
                else
                    return RedirectToAction("Index", new AppointmentSearchViewModel { Status = Enums.AppointmentStatus.Approved, AppointmentTreatmentStatus = Enums.AppointmentTreatmentStatus.Paused, BookingDate = DateTime.Now.Date, NotIndex = true });
            }
            if (request || Request.IsAjaxRequest())
                return Json(false, JsonRequestBehavior.AllowGet);
            else
                return View("Create", model);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator, Stylist")]
        public ActionResult Delete(AppointmentViewModel model)
        {
            var request = Request.UrlReferrer.ToString().Contains("Dashboard");

            var result = new Logic().Delete(model);
            if (result != new Guid())
            {
                if (request)
                    return Json(true, JsonRequestBehavior.AllowGet);
                else
                    return RedirectToAction("Index", new AppointmentSearchViewModel { Status = Enums.AppointmentStatus.Approved, AppointmentTreatmentStatus = Enums.AppointmentTreatmentStatus.Paused, BookingDate = DateTime.Now.Date, NotIndex = true });
            }
            else
            {
                if (request)
                    return Json(false, JsonRequestBehavior.AllowGet);
                else
                    return RedirectToAction("Index");
            }
        }


        //[Authorize(Roles = "Administrator, SuperAdministrator, Stylist")]
        public ActionResult Start(Guid appointmentId)
        {
            var record = new Logic().GetAppointments<AppointmentViewModel>(new AppointmentSearchViewModel { Id = appointmentId }).ResultList.First();
            if (record.AppointmentTreatmentStatus != AppointmentTreatmentStatus.Ended)
            {
                var setActualTime = new Logic().Edit(new AppointmentViewModel { Id = appointmentId, ActualStartingTime = DateTime.Now, AppointmentTreatmentStatus = AppointmentTreatmentStatus.Started });
                record.TotalTime = record.Treatment.ApproximateTime * 60;
                record.TotalTime -= (int)(record.ElapsedTime * 60);
            }
            return View(record);
        }

        public PartialViewResult LoadStartPartial(AppointmentViewModel model)
        {
            if (model.AppointmentTreatmentStatus == AppointmentTreatmentStatus.Paused)
            {
                new Logic().Edit(new AppointmentViewModel { Id = model.Id, ActualStartingTime = DateTime.Now, AppointmentTreatmentStatus = AppointmentTreatmentStatus.Started });
            }
            else if ((model.AppointmentTreatmentStatus == AppointmentTreatmentStatus.Pending))
            {
                new Logic().Edit(new AppointmentViewModel { Id = model.Id, AppointmentTreatmentStatus = AppointmentTreatmentStatus.Started });
            }

            model.TotalTime = model.Treatment.ApproximateTime * 60;
            model.TotalTime -= (int)(model.ElapsedTime * 60);
            return PartialView("StartPartial", model);
        }

        public PartialViewResult StartPartial(AppointmentViewModel model)
        {
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Start(AppointmentViewModel model)
        {
            var percentageCommission = new Logic().Read<StylistTreatmentViewModel>(new StylistTreatmentSearchViewModel { StylistId = model.Stylist.Id, TreatmentId = model.Treatment.Id }).ResultList.First().PercentageCommission;
            var stylistCommission = new StylistCommissionViewModel { AppointmentId = model.Id.Value, StylistId = model.Stylist.Id, TreatmentId = model.Treatment.Id, Amount = ((float)(model.Treatment.Price / 100) * percentageCommission) };
            var commissionId = new Logic().Create(stylistCommission);
            model.AppointmentTreatmentStatus = AppointmentTreatmentStatus.Ended;
            model.ActualEndingTime = DateTime.Now;
            var savedRecord = new Logic().Edit(model);

            if (commissionId != new Guid() && savedRecord != new Guid())
            {
                return RedirectToAction("Index", "Appointment", new { Status = Enums.AppointmentStatus.Approved, AppointmentTreatmentStatus = Enums.AppointmentTreatmentStatus.Paused, BookingDate = DateTime.Now.Date, NotIndex = true });
            }
            return View(model);
        }


        public ActionResult MultiTreatmentStart(Guid stylistId)
        {
            RegisterViewModel Stylist = new RegisterViewModel();
            if (stylistId != null)
                Stylist = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Id = stylistId.ToString() }).ResultList.FirstOrDefault();
            ViewBag.Stylist = Stylist;
            var record = new Logic().GetAppointments<AppointmentViewModel>(new AppointmentSearchViewModel { StylistId = stylistId.ToString(), BookingDate = DateTime.Now.Date }).ResultList.Where(x => (x.AppointmentTreatmentStatus != AppointmentTreatmentStatus.Ended && x.Status != AppointmentStatus.Cancelled)).ToList(); ;
            return View(record
                .OrderBy(x => x.AppointmentTreatmentStatus == AppointmentTreatmentStatus.Pending)
                .ThenBy(x => x.AppointmentTreatmentStatus == AppointmentTreatmentStatus.Paused)
                .ThenBy(x => x.AppointmentTreatmentStatus == AppointmentTreatmentStatus.Started).ToList());
        }
        public ActionResult GetMultiAppointment(Guid id)
        {
            var record = new Logic().GetAppointments<AppointmentViewModel>(new AppointmentSearchViewModel { Id = id }).ResultList.FirstOrDefault();
            return View("_MultiTreatmentStartPartial", record);
        }


        public ActionResult GetCurrentClientAppointments(string stylistId, string clientId, Guid discardAppointmentId)
        {
            var result = new Logic().GetAppointments<AppointmentViewModel>(new AppointmentSearchViewModel
            {
                ClientId = clientId,
                StylistId = stylistId, /*DiscardAppointment = discardAppointmentId,*/
                BookingDate = DateTime.Now.Date
            }).ResultList
            .Where(x => x.Client.Id == clientId &&
                        (x.AppointmentTreatmentStatus != AppointmentTreatmentStatus.Ended && x.Status == AppointmentStatus.Approved)).ToList();

            var lastAppointmentEndingDateTime = result.Count() > 0 ? result.OrderByDescending(x => x.AppointmentEndingTime).FirstOrDefault().AppointmentEndingTime.AddMinutes(1) : DateTime.Now;
            ViewBag.LastAppointmentEndingDateTime = lastAppointmentEndingDateTime;
            return PartialView(result);
        }

        public ActionResult _GetCurrentStylistAppointments(string stylistId, string clientId)
        {
            var check = new Logic().GetAppointments<AppointmentViewModel>(new AppointmentSearchViewModel
            {
                ClientId = clientId,
                StylistId = stylistId, /*DiscardAppointment = discardAppointmentId,*/
                BookingDate = DateTime.Now.Date
            }).ResultList;
            var result = check
            .Where(x =>
                    (
                        (x.AppointmentTreatmentStatus == AppointmentTreatmentStatus.Ended && (x.Status == AppointmentStatus.Approved || x.Status == AppointmentStatus.Completed)) ||
                        (x.Status == AppointmentStatus.Cancelled)
                    )
            ).ToList();

            var lastAppointmentEndingDateTime = result.Count() > 0 ? result.OrderByDescending(x => x.AppointmentEndingTime).FirstOrDefault().AppointmentEndingTime.AddMinutes(1) : DateTime.Now;
            ViewBag.LastAppointmentEndingDateTime = lastAppointmentEndingDateTime;
            return PartialView(result);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult UnpaidAppointments()
        {
            var result = new Logic().GetUnpaidAppointments();
            result.ForEach(x => x.Status = InvoiceStatus.Pending);
            return View(result);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Payment(string id)
        {
            var appointment = new Logic().GetAppointments<AppointmentViewModel>(new AppointmentSearchViewModel { ClientId = id, GetDetailedResult = true, /*BookingDate = DateTime.Now.Date,*/ AppointmentTreatmentStatus = AppointmentTreatmentStatus.Ended, Status = AppointmentStatus.Approved, NotIndex = true }).ResultList;
            var Invoice = new InvoiceRecordViewModel();
            var point = new Logic().GetClientPoints<ClientPointViewModel>(new ClientPointSearchViewModel { Client = new ApplicationUser { Id = id } }).ResultList;
            Invoice.UserPoints = point.Count() > 0 ? point.Sum(x => x.Points) : 0;
            if (appointment.Count() > 0)
            {
                appointment.First().PointWorth = new Logic().GetPoint().Worth;
                var invoiceItems = appointment.Where(m => m.Id != new Guid()).Select(m => new InvoiceItemViewModel
                {
                    Appointment = m
                }).ToList();
                Invoice.InvoiceItems = invoiceItems;
            }

            return View(Invoice);
        }

        [HttpPost]
        public ActionResult Payment(InvoiceRecordViewModel model)
        {
            new Logic(User.Identity.GetUserId()).Payment(model);

            return RedirectToAction("UnpaidAppointments", "Appointment");
        }


        public PartialViewResult GetAppointments(AppointmentSearchViewModel search)
        {
            if (!search.NotIndex)
            {
                search = new AppointmentSearchViewModel { Status = Enums.AppointmentStatus.Approved, BookingDate = DateTime.Now.Date, CalledFor = "Approved Treatments", NotIndex = true };
            }
            new Helpers.LogGeneration().CreateGeneralLog(new ApplicationDbContext(), new Log { Action = "None", DateTime = DateTime.Now.AddDays(1), ModelId = Guid.NewGuid().ToString(), AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId() });
            Session["action"] = string.IsNullOrEmpty(search.CalledFor) ? "Pending Treatments" : search.CalledFor;
            return PartialView(new Logic().GetAppointments<AppointmentViewModel>(search).ResultList);

        }


        public PartialViewResult GetBookingModal()
        {
            var model = new FullCalendarEvents { resourceId = "1e93fd13-866c-4899-8050-a930149f6254", start = "2018-09-28 15:09:00.000" };
            return PartialView(model);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult InvoiceReportView(InvoiceReportSearchViewModel search)
        {
            var result = new InvoiceReportViewModel();
            if (search.IsTreatmentView)
            {
                result.InvoiceItems = new Logic().GetTreatmentReport<TreatmentReportViewModel>(new TreatmentReportSearchViewModel { InvoiceItemId = search.InvoiceItemId }).ResultList;
                result.DateTime = result.InvoiceItems.FirstOrDefault().InvoiceDateTime;
            }
            else
            {
                result = new Logic().GetInvoiceReport<InvoiceReportViewModel>(search).ResultList.FirstOrDefault();
            }
            result.Branch = new Logic().GetBranches<BranchViewModel>(new BranchSearchViewModel()).ResultList.FirstOrDefault();
            result.User = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Id = search.IsTreatmentView ? result.InvoiceItems.FirstOrDefault().ClientId : result.Client.Id }).ResultList.FirstOrDefault();
            result.IsTreatmentView = search.IsTreatmentView;

            return View(result);
        }

        public ActionResult _GetProductCategoryList()
        {
            var result = new Logic().GetProductCategories<ProductCategoryViewModel>(new ProductCategorySearchViewModel());
            return View(result.ResultList);
        }

        public ActionResult _GetProducts(Guid categoryId)
        {
            var result = new Logic().GetProducts<ProductViewModel>(new ProductSearchViewModel { CategoryId = categoryId });
            return View(result.ResultList);
        }

        public ActionResult _GetTreatmentCategoryList()
        {
            var result = new Logic().GetTreatmentCategories<TreatmentCategoryViewModel>(new TreatmentCategorySearchViewModel());
            return View(result.ResultList);
        }

        public ActionResult _GetTreatments(Guid categoryId)
        {
            var result = new Logic().GetTreatments<TreatmentViewModel>(new TreatmentSearchViewModel { CategoryId = categoryId });
            return View(result.ResultList);
        }



        //
        // Region for JSON methods
        #region json Methods

        public JsonResult GetProductDetails(Guid itemId)
        {
            var result = new Logic().GetProduct(itemId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTreatmentDetails(Guid itemId)
        {
            var result = new Logic().GetTreatment(itemId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Finish(AppointmentViewModel model)
        {
            var stylistTreatment = new Logic().Read<StylistTreatmentViewModel>(new StylistTreatmentSearchViewModel { StylistId = model.Stylist.Id, TreatmentId = model.Treatment.Id }).ResultList;
            if (stylistTreatment.Count() > 0)
            {
                var percentageCommission = stylistTreatment.FirstOrDefault().PercentageCommission;
                var stylistCommission = new StylistCommissionViewModel { AppointmentId = model.Id.Value, StylistId = model.Stylist.Id, TreatmentId = model.Treatment.Id, Amount = ((float)(model.Treatment.Price / 100) * percentageCommission) };
                var commissionId = new Logic().Create(stylistCommission);
                model.AppointmentTreatmentStatus = AppointmentTreatmentStatus.Ended;
                model.ActualEndingTime = DateTime.Now;
                var savedRecord = new Logic().Edit(model);

                if (commissionId != new Guid() && savedRecord != new Guid())
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FetchCalendarData(DateTime date)
        {
            // Check if role is stylist and set Stylist id in SearchModel otherwise leave empty
            var events = new Logic().GetFreeSlotsForAllStylist(new AppointmentSlotSearchViewModel { DesiredDate = date, GetAllSlots = true });
            events.ForEach(cc => cc.serealizedObject = new JavaScriptSerializer().Serialize(cc));
            return Json(events, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUserDetail(string id)
        {
            var detail = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Id = id }).ResultList.FirstOrDefault();
            return Json(detail, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUsers(string prefix, int pageSize, int pageNumber)
        {
            var users = new List<Select2OptionModel>();
            //if (User.IsInRole("SuperAdministrator") || User.IsInRole("Administrator"))
            //{
            //    users = new Logic().GetUsers<Select2OptionModel>(new AccountSearchViewModel { RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "Client" }).ResultList.First().Id, Select2 = true, Name = prefix }).ResultList;
            //}
            //else
            //{
            //    users = new Logic().GetUsers<Select2OptionModel>(new AccountSearchViewModel { RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "Client" }).ResultList.First().Id, Select2 = true, Name = prefix, Id = User.Identity.GetUserId() }).ResultList;
            //}
            users = new Logic().GetUsers<Select2OptionModel>(new AccountSearchViewModel { IsClient = true, RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "Client" }).ResultList.First().Id, Select2 = true, Name = prefix, Pagination = true, RecordsPerPage = 20, CurrentPage = 1 }).ResultList;
            return Json(new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, users.GroupBy(m => m.text).Select(m => m.FirstOrDefault()).ToList()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAdministrators(string prefix, int pageSize, int pageNumber)
        {
            var userId = (User.IsInRole("SuperAdministrator") || User.IsInRole("Administrator")) ? "" : User.Identity.GetUserId();
            var users = new Logic().GetUsers<Select2OptionModel>(new AccountSearchViewModel { RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "Administrator" }).ResultList.First().Id, Id = userId, Select2 = true, Name = prefix, Pagination = true, RecordsPerPage = 20, CurrentPage = 1 }).ResultList;
            return Json(new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, users.GroupBy(m => m.text).Select(m => m.FirstOrDefault()).ToList()), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTreatments(string prefix, int pageSize, int pageNumber, string stylistId, bool isForAddingStylistTreatment = false)
        {
            var treatments = new List<Select2OptionModel>();
            if (isForAddingStylistTreatment && !string.IsNullOrEmpty(stylistId))
            {
                var existingTreatmentIds = new Logic().Read<StylistTreatmentViewModel>(new StylistTreatmentSearchViewModel { StylistId = stylistId }).ResultList.Select(m => m.TreatmentId).ToList();
                //var exitstingTreatments = new Logic().GetTreatments<TreatmentViewModel>(new TreatmentSearchViewModel { StylistId = stylistId }).ResultList.Select(m => m.Id).ToList();
                var allTreatments = new Logic().GetTreatments<Select2OptionModel>(new TreatmentSearchViewModel { Select2 = true, Name = prefix, Pagination = true, RecordsPerPage = 20, CurrentPage = 1 }).ResultList;

                treatments = allTreatments.Where(m => !(existingTreatmentIds.Contains(Guid.Parse(m.id)))).ToList();
            }
            else
            {
                var treatmentsList = new Logic().GetTreatments<TreatmentViewModel>(new TreatmentSearchViewModel { Name = prefix, StylistId = stylistId, Pagination = true, RecordsPerPage = 20, CurrentPage = 1 }).ResultList;
                treatments = treatmentsList.Select(m => new Select2OptionModel
                {
                    id = m.Id.ToString(),
                    text = m.Name,
                    additionalAttributesModel = m,
                }).ToList();
            }
            return Json(new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, treatments), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetStylists(string prefix, int pageSize, int pageNumber, Guid? treatmentId)
        {
            var users = new List<Select2OptionModel>();
            var userId = (User.IsInRole("SuperAdministrator") || User.IsInRole("Administrator") || User.IsInRole("Client")) ? "" : User.Identity.GetUserId();
            if (treatmentId == null)
            {
                users = new Logic().GetUsers<Select2OptionModel>(new AccountSearchViewModel { RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "Stylist" }).ResultList.First().Id, Id = userId, TreatmentId = treatmentId.GetValueOrDefault(), Select2 = true, ReturnEmpty = false, Name = prefix, Pagination = true, RecordsPerPage = 20, CurrentPage = 1 }).ResultList;
            }
            else
            {
                users = new Logic().GetUsers<Select2OptionModel>(new AccountSearchViewModel { RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "Stylist" }).ResultList.First().Id, Id = userId, TreatmentId = treatmentId.GetValueOrDefault(), Select2 = true, ReturnEmpty = true, Name = prefix, Pagination = true, RecordsPerPage = 20, CurrentPage = 1 }).ResultList;
            }
            return Json(new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, users), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ValidateStartingTime(AppointmentSearchViewModel model)
        {
            var treatment = new Logic().GetTreatments<TreatmentViewModel>(new TreatmentSearchViewModel { Id = model.TreatmentId });
            model.TotalTime = treatment.ResultList.First().ApproximateTime;
            var branch = new Logic().GetBranches<BranchViewModel>(new BranchSearchViewModel()).ResultList.FirstOrDefault();
            var freeSlots = new List<AppointmentTimeSlot>();
            var appointedSlots = new List<AppointmentTimeSlot>();
            appointedSlots.Add(new AppointmentTimeSlot { StartingTime = model.BookingDate, EndingTime = model.BookingDate.Add(branch.OpeningTime), Status = SlotStatus.Schedule });
            appointedSlots.Add(new AppointmentTimeSlot { StartingTime = model.BookingDate.Add(branch.ClosingTime), EndingTime = model.BookingDate.AddDays(1), Status = SlotStatus.Schedule });
            var availaibilityResult = "false";
            var path = Request.UrlReferrer.ToString().Contains("Start");
            if (path)
            {
                freeSlots.Add(new AppointmentTimeSlot { StartingTime = model.BookingDate.Add(branch.OpeningTime), EndingTime = model.BookingDate.Add(branch.ClosingTime), Status = SlotStatus.Available });
            }
            else
            {
                var timeSlots = new Logic().GetFreeSlots(new AppointmentSlotSearchViewModel { DesiredDate = model.BookingDate, SytlistId = model.StylistId, ClientId = model.ClientId, TimeRequiredForTreatment = model.TotalTime, DiscardAppointment = !string.IsNullOrEmpty(model.DiscardAppointmentString) ? Guid.Parse(model.DiscardAppointmentString) : new Guid() });
                freeSlots = timeSlots.FreeSlots;
                appointedSlots.AddRange(timeSlots.AppointedSlots);
            }
            var appointmentDesiredTime = model.BookingDate.Add(model.StartingDateTime.TimeOfDay);

            //needs clearification
            freeSlots.AddRange(appointedSlots);


            foreach (var freeSlot in freeSlots)
            {
                if ((appointmentDesiredTime.Ticks >= freeSlot.StartingTime.Ticks && appointmentDesiredTime.Ticks < (freeSlot.EndingTime.AddMinutes(-(model.TotalTime)).Ticks) && freeSlot.Status == SlotStatus.Available))// Slot is Available
                {
                    availaibilityResult = "";
                    break;
                }
                else if ((appointmentDesiredTime.Ticks >= freeSlot.StartingTime.Ticks && appointmentDesiredTime.Ticks <= freeSlot.EndingTime.Ticks && freeSlot.Status != SlotStatus.Available))// Unavailable Slot
                {
                    switch (freeSlot.Status)
                    {
                        case SlotStatus.Leave: availaibilityResult = "Stylist is on leave at this time"; break;
                        case SlotStatus.Appointment: availaibilityResult = "This time is booked for another appointment"; break;
                        case SlotStatus.UnScheduled: availaibilityResult = "This time is out of the stylist's schedule"; break;
                        case SlotStatus.Schedule: availaibilityResult = "This time is out of saloon schedule"; break;
                    }
                    break;
                }
            }
            if (availaibilityResult == "false")// if there is an available slot but time required for it is not sufficient
            {
                var closestSlot = freeSlots.OrderBy(t => Math.Abs((t.StartingTime - appointmentDesiredTime).Ticks))
                             .First();
                switch (closestSlot.Status)
                {
                    case SlotStatus.Leave: availaibilityResult = string.Format("Not enough time as stylist has a leave at {0} and treatment requires {1} minutes least", closestSlot.StartingTime.ToString("hh:mm tt"), model.TotalTime); break;
                    case SlotStatus.Appointment: availaibilityResult = string.Format("Not enough time as another appointment is booked for {0} and treatment requires {1} minutes least", closestSlot.StartingTime.ToString("hh:mm tt"), model.TotalTime); break;
                    case SlotStatus.UnScheduled: availaibilityResult = string.Format("Not enough time for treatment as stylist's schedule ends at {0} and treatment requires {1} minutes least", closestSlot.StartingTime.ToString("hh:mm tt"), model.TotalTime); break;
                    case SlotStatus.Schedule: availaibilityResult = string.Format("Saloon closes at {0} and treatment requires {1} minutes least", closestSlot.StartingTime.ToString("hh:mm tt"), model.TotalTime); break;
                }
            }
            return Json(availaibilityResult, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult GetEvents(AppointmentSearchViewModel model)
        {
            model.VerifyTime = true;
            model.DiscardAppointment = string.IsNullOrEmpty(model.DiscardAppointmentString) ? model.DiscardAppointment : Guid.Parse(model.DiscardAppointmentString);
            model.BookingDate = model.StartingDateTime.Date;
            var appointments = new Logic().GetAppointments<FullCalendarViewModel>(model).ResultList;
            return Json(appointments, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUnpaidInvoices()
        {
            var result = new Logic().GetUnpaidAppointments();
            result.ForEach(x => x.Status = InvoiceStatus.Pending);
            if (User.IsInRole("Stylist"))
            {
                result = result.Where(m => m.StylistId == User.Identity.GetUserId()).ToList();
            }
            return Json(result.Count(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateElapsedTime(string Id, double ElpasedTime, bool PauseTreatment)//For Single Start Page
        {
            // In Elapsed Time send DbElapsedTime Plus view Elapsed time from view. Edit Logic changed
            Guid id = new Guid();
            var result = new Guid();
            AppointmentTreatmentStatus AppStatus = AppointmentTreatmentStatus.Paused;
            if (!PauseTreatment)
            {
                AppStatus = AppointmentTreatmentStatus.Started;
            }
            if (Guid.TryParse(Id, out id))
            {
                result = new Logic().Edit(new AppointmentViewModel { Id = id, ElapsedTime = (float)ElpasedTime, ActualEndingTime = DateTime.Now, AppointmentTreatmentStatus = AppStatus });
            }//var unpaidInvoices = new Logic().GetUnpaidAppointments().Count();
            var response = result != new Guid() ? true : false;
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult ChangeAppointmentState(Guid id, double elpasedTime, AppointmentTreatmentStatus appointmentStatus)// For MultiStartPage
        {
            var result = new Logic().Edit(new AppointmentViewModel { Id = id, ElapsedTime = (float)elpasedTime, ActualEndingTime = DateTime.Now, AppointmentTreatmentStatus = appointmentStatus });
            var response = result != new Guid() ? true : false;
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAppointment(Guid id)
        {
            var result = new Logic().GetAppointments<AppointmentViewModel>(new AppointmentSearchViewModel { Id = id }).ResultList.FirstOrDefault();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAppointmentCount(AppointmentSearchViewModel search)
        {
            if (User.IsInRole("Stylist") && string.IsNullOrEmpty(search.StylistId))
            {
                search.StylistId = User.Identity.GetUserId();
            }
            search.BookingDate = DateTime.Now.Date;
            search.BookingDateEnding = DateTime.Now.Date.AddDays(1).AddMilliseconds(-1);
            return Json(new Logic().GetAppointments<AppointmentViewModel>(search).ResultList.Count(), JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetAllAppointmentsCount()
        {
            AppointmentSearchViewModel search = new AppointmentSearchViewModel { NotIndex = true };
            if (User.IsInRole("Stylist") && string.IsNullOrEmpty(search.StylistId))
            {
                search.StylistId = User.Identity.GetUserId();
            }
            search.BookingDate = DateTime.Now.Date;
            search.BookingDateEnding = DateTime.Now.Date.AddDays(1).AddMilliseconds(-1);
            var allAppointments = new Logic().GetAppointmentCount(search);
            var appointmentsCount = new
            {
                InProgress = allAppointments.Where(m => m.AppointmentTreatmentStatus == AppointmentTreatmentStatus.Started).Count(),
                Finished = allAppointments.Where(m => m.AppointmentTreatmentStatus == AppointmentTreatmentStatus.Ended).Count(),
                Paused = allAppointments.Where(m => m.AppointmentTreatmentStatus == AppointmentTreatmentStatus.Paused).Count(),
                Cancelled = allAppointments.Where(m => m.Status == AppointmentStatus.Cancelled).Count(),
                Pending = allAppointments.Where(m => m.AppointmentTreatmentStatus == AppointmentTreatmentStatus.Pending).Count(),
            };
            return Json(appointmentsCount, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}