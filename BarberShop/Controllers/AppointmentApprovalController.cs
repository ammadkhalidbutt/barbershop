﻿using BusinessLogic;
using Enums;
using Microsoft.AspNet.Identity;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BarberShop.Controllers
{
    public class AppointmentApprovalController : Controller
    {
        // GET: AppointmentApproval
        [Authorize(Roles ="Administrator, SuperAdministrator")]
        public ActionResult Create(Guid id)
        {
            return View(new Logic().GetApprovals<AppointmentApprovalViewModel>(new AppointmentApprovalSearchViewModel { AppointmentId = id }).ResultList.First());
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        [HttpPost]
        public ActionResult Create(AppointmentApprovalViewModel model)
        {
            model.Approval.ApproverId = User.Identity.GetUserId();
            var result = new Logic().Create(model);
            if(result != new Guid())
            {
                return RedirectToAction("Index", "Appointment", new { Status = Enums.AppointmentStatus.Processing, AppointmentTreatmentStatus = Enums.AppointmentTreatmentStatus.Paused, CalledFor = "Unapproved Treatments", NotIndex = true });
            }
            return View(model);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Edit(Guid id)
        {
            var result = new Logic().GetApprovals<AppointmentApprovalViewModel>(new AppointmentApprovalSearchViewModel { AppointmentId = id }).ResultList.First();
            result.Action = "Edit";
            return View("Create", result);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Edit(AppointmentApprovalViewModel model)
        {
            model.Approval.LastUpdatedOn = DateTime.Now;
            var result = new Logic().Edit(model);
            if(result != new Guid())
            {
                var statusStr = string.Format("{0},{1},{2}", AppointmentTreatmentStatus.Pending.ToString(), AppointmentTreatmentStatus.Paused.ToString(), AppointmentTreatmentStatus.Started.ToString());
                return RedirectToAction("Index", "Appointment", new { ClientId = model.Client.Id, SkipDateCheck = true, Status = AppointmentStatus.Approved, TreatmentStatuses = statusStr, NotIndex = true, IsEditAllowed = true });
            }

            else
            {
                return View("Create", result); 
            }

        }

        [HttpPost]
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Delete(AppointmentApprovalViewModel model)
        {
            var result = new Logic().Delete(model);
            if(result != new Guid())
            {
                return RedirectToAction("Index", "Appointment");
            }
            else
            {
                return RedirectToAction("Create", model);
            }
        }
    }
}