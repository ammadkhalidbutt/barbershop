﻿using BusinessLogic;
using Enums;
using Helpers.Select2;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Models;
using Models.Identity;
using Models.ViewModels;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace BarberShop.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            var day = Enum.Parse(typeof(WeekDays), DateTime.Now.DayOfWeek.ToString());
            return View();
        }
        //[Authorize(Roles = "Administrator, SuperAdministrator, Stylist")]
        public ActionResult StylistDashboard()
        {
            //var userIdentity = (ClaimsIdentity)User.Identity;
            //var claims = userIdentity.Claims;
            //var roleClaimType = userIdentity.RoleClaimType;

            //// or...
            //var roles = claims.Where(c => c.Type == roleClaimType).ToList();

            return View();
        }

        public PartialViewResult AttendanceDashboardPartial()
        {
            var model = new AttendanceDashboardViewModel();
            model.StylistList = new Logic().GetStylistsForStylistDashboard(new AccountSearchViewModel { Role = "Stylist", GetGroupedRecords = true });
            model.ClientList = new Logic().GetClientsForStylistDashboard("");
            model.TreatmentList = new List<TreatmentViewModel>();
            return PartialView(model);
        }

        [Authorize]
        public ActionResult CheckInClients()
        {
            ViewBag.Action = "CheckInClients";
            return View();
        }

        public PartialViewResult CheckInClientsPartial(string title, bool forDashboard = false)
        {
            ViewBag.Title = title;
            ViewBag.ForDashboard = forDashboard;
            //ViewBag.Action = forDashboard ? "CheckInClients" : "";
            return PartialView();
        }

        [HttpPost]
        public ActionResult CheckInClients(string id)
        {
            if (Helpers.Extensions.IsUserActive(id))
            {
                var presentClientsIds = new Logic().GetPresentUsers("Client", "").OrderByDescending(x => x.AttendanceDate).Where(m => m.Action == "Checked In").Select(m => m.Id).ToList();
                if (!presentClientsIds.Contains(id))
                {
                    var result = new Logic().ScanClients(id, "Checked In");
                    var request = Request.UrlReferrer.ToString().Contains("Dashboard");
                    if (!string.IsNullOrEmpty(result))
                    {
                        ViewBag.Action = "CheckInClients";
                        if (request || Request.IsAjaxRequest())
                            return Json(true, JsonRequestBehavior.AllowGet);
                        else
                            return RedirectToAction("Index");
                    }
                    else
                    {
                        if (request || Request.IsAjaxRequest())
                            return Json(false, JsonRequestBehavior.AllowGet);
                        else
                            return View();
                    }
                }
                else
                {
                    return Json("Client already checked in", JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json("Client does not exist", JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult ScanInClients(string id)
        {
            if (Helpers.Extensions.IsUserActive(id))
            {
                var presentClients = new Logic().GetPresentUsers("Client", "Checked In").OrderByDescending(x => x.AttendanceDate).Where(m => m.Action == "Checked In").ToList();
                if (!presentClients.Select(m => m.Id).Contains(id))
                {
                    var result = new Logic().ScanClients(id, "Checked In");
                    var client = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Id = result }).ResultList.FirstOrDefault();
                    var request = Request.UrlReferrer.ToString().Contains("Dashboard");
                    if (!string.IsNullOrEmpty(result))
                    {
                        ViewBag.Action = "CheckInClients";
                        if (request || Request.IsAjaxRequest())
                            return Json(string.Format("Welcome {0}, you have successfully <span class='badge badge-success'><b>checked in.</b></span>", client.Name), JsonRequestBehavior.AllowGet);
                        else
                            return RedirectToAction("Index");
                    }
                    else
                    {
                        if (request || Request.IsAjaxRequest())
                            return Json("Please try again.", JsonRequestBehavior.AllowGet);
                        else
                            return View();
                    }
                }
                else
                {
                    var result = new Logic().ScanClients(id, "Checked Out");
                    var request = Request.UrlReferrer.ToString().Contains("Dashboard");
                    if (!string.IsNullOrEmpty(result))
                    {
                        ViewBag.Action = "CheckInClients";
                        if (request || Request.IsAjaxRequest())
                            return Json(string.Format("Good Bye {0}, you have successfully  <span class='badge badge-warning'><b>checked out.</b></span>", presentClients.Where(m => m.Id == id).Select(m => m.Name).FirstOrDefault()), JsonRequestBehavior.AllowGet);
                        else
                            return RedirectToAction("Index");
                    }
                    else
                    {
                        if (request || Request.IsAjaxRequest())
                            return Json("Please try again.", JsonRequestBehavior.AllowGet);
                        else
                            return View();
                    }
                }

            }
            else
            {
                return Json("Client does not exist", JsonRequestBehavior.AllowGet);
            }

        }

        [Authorize]
        public ActionResult CheckOutClients()
        {
            ViewBag.Action = "CheckOutClients";
            return View("CheckInClients");
        }

        [HttpPost]
        public ActionResult CheckOutClients(string id)
        {
            if (Helpers.Extensions.IsUserActive(id))
            {
                var checkedInClientIds = new Logic().GetPresentUsers("Client", "All Clients").OrderByDescending(x => x.AttendanceDate).Where(m => m.Action == "Checked In").Select(m => m.Id).ToList();
                if (checkedInClientIds.Contains(id))
                {
                    ViewBag.Action = "CheckOutClients";
                    var result = new Logic().ScanClients(id, "Checked Out");
                    var request = Request.UrlReferrer.ToString().Contains("Dashboard");
                    if (!string.IsNullOrEmpty(result))
                    {
                        if (request || Request.IsAjaxRequest())
                            return Json(true, JsonRequestBehavior.AllowGet);
                        else
                            return RedirectToAction("Index");
                    }
                    else
                    {
                        if (request || Request.IsAjaxRequest())
                            return Json(false, JsonRequestBehavior.AllowGet);
                        else
                            return View();
                    }
                }
                else
                {
                    return Json("Client not checked in", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("Client does not exist", JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult ScanClients()
        {
            return View();
        }

        //[Authorize]
        public ActionResult BookAppointment(AppointmentViewModelStylistDashboard model)
        {
            try
            {
                var startingTreatment = new Guid();
                var loggedInUserBranchId = User.Identity.IsAuthenticated ?
                                            new Helpers.LoggedInUserInfo().GetLoggedInUser().BranchId :
                                            new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { Id = model.StylistId }).ResultList.First().BranchId;
                var approverId = User.Identity.IsAuthenticated ? User.Identity.GetUserId() : model.StylistId;
                var branch = new Logic().GetBranches<BranchViewModel>(new BranchSearchViewModel { BranchId = loggedInUserBranchId }).ResultList.First();
                foreach (var item in model.TreatmentList)
                {
                    var result = new Logic().Create(new AppointmentViewModel
                    {
                        AppointmentStartingTime = DateTime.Now.TimeOfDay > branch.ClosingTime ? new DateTime().Add(branch.ClosingTime) : DateTime.Now.TimeOfDay < branch.OpeningTime ? new DateTime().Add(branch.OpeningTime) : new DateTime().Add(DateTime.Now.TimeOfDay),
                        BookingDate = DateTime.Now,
                        StylistId = model.StylistId,
                        ClientId = model.ClientId,
                        TreatmentId = Guid.Parse(item),
                        Approver = new ApplicationUser { Id = approverId }
                    });
                    if (model.StartedTreatment == new Guid() && startingTreatment == new Guid())
                    {
                        startingTreatment = result;
                    }
                    else if (Guid.Parse(item) == model.StartedTreatment && model.StartedTreatment != new Guid())
                        startingTreatment = result;

                    if (result == new Guid())
                        return Json(result, JsonRequestBehavior.AllowGet);
                }
                return Json(startingTreatment, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new Guid(), JsonRequestBehavior.AllowGet);

            }
        }

        //[HttpPost]
        public PartialViewResult GetPresentClientPartial(bool forDashboard = false, string action = "checked in")
        {
            ViewBag.ForDashboard = forDashboard;
            var result = new List<UserAttendanceViewModel>();
            if (forDashboard)
            {
                result = new Logic().GetClientsForStylistDashboard("");
            }
            else
            {
                result = new Logic().GetPresentUsers("Client", "All Clients");
                result = result.GroupBy(m => m.Id).Select(x => x.OrderByDescending(s => s.AttendanceDate).FirstOrDefault()).ToList();
                result = result.Where(m => m.Action.ToLower() == action.ToLower()).ToList();
            }
            if (forDashboard)
                return PartialView("AttendanceDashboardClientsPartial", result);
            return PartialView(result);
        }


        public ActionResult Stats()
        {
            return View();
        }

        public ActionResult Calendar()
        {
            return View();
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult AttendanceDashboardStylistsPartial(List<UserAttendanceViewModel> model)
        {
            return PartialView(model);
        }

        public ActionResult AttendanceDashboardClientsPartial(List<UserAttendanceViewModel> model)
        {
            return PartialView(model);
        }

        public ActionResult AttendanceDashboardTreatmentsPartial(List<TreatmentViewModel> model)
        {
            return PartialView(model);
        }

        public ActionResult _TreatmentCategoryPartial(List<TreatmentCategoryViewModel> model)
        {
            return PartialView(model);
        }


        #region json methods


        //
        //this method allows us to set a session variable
        public JsonResult SetSessionVariable(string key, string value)
        {
            try
            {
                Session[key] = value;
                //var record = string.IsNullOrEmpty(value) ? new Select2OptionModel() : new Logic().GetUsers<Select2OptionModel>(new AccountSearchViewModel { Id = value, Select2 = true }).ResultList.FirstOrDefault();
                //var selectList = ViewBag.PreviouslySelectedStylist = record == new Select2OptionModel() ? new List<SelectListItem>() : new List<SelectListItem>() { new SelectListItem { Value = record.id, Text = record.text, Selected = true } };
                return Json(value, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


        //public JsonResult GetTreatmentList(TreatmentSearchViewModel model)
        //{
        //    if (!string.IsNullOrEmpty(model.StylistId))
        //    {
        //        var result = new Logic().GetTreatments<TreatmentViewModel>(model).ResultList;
        //        return Json(result, JsonRequestBehavior.AllowGet);
        //    }
        //    return Json(new List<TreatmentViewModel>(), JsonRequestBehavior.AllowGet);
        //}

        public ActionResult GetStylistTreatments(string stylistId, Guid categoryId)
        {
            var treatments = new Logic().GetTreatments<TreatmentViewModel>(new TreatmentSearchViewModel { StylistId = stylistId, CategoryId = categoryId }).ResultList;
            return PartialView("_TreatmentsPartial", treatments);
        }

        public ActionResult GetTreatmentCategoryList(TreatmentSearchViewModel model)
        {
            if (!string.IsNullOrEmpty(model.StylistId))
            {
                var result = new Logic().GetTreatments<TreatmentViewModel>(model).ResultList.Select(m => new TreatmentCategoryViewModel
                {
                    Id = m.Category.Id,
                    Name = m.Category.Name
                }).DistinctBy(x => x.Id).ToList();
                return PartialView("_TreatmentCategoryPartial", result);
            }
            return Json(new List<TreatmentViewModel>(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchStylist(AccountSearchViewModel model)
        {
            model.GetGroupedRecords = true;
            var result = new Logic().GetStylistsForStylistDashboard(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchClient(AccountSearchViewModel model)
        {
            return Json(new Logic().GetClientsForStylistDashboard(model.Name), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUsers(string prefix, int pageSize, int pageNumber, string title)
        {

            var presentClients = new Logic().GetPresentUsers("Client", "Checked In").ToList();
            var result = new List<Select2OptionModel>();
            // It will give all users other than checked in. Need to be fixed in future
            if (title.ToLower() == "checked in")
            {
                var users = new Logic().GetUsers<Select2OptionModel>(new AccountSearchViewModel { IsClient = true, RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "Client" }).ResultList.First().Id, Select2 = true, Name = prefix, Pagination = true, CurrentPage = 1, RecordsPerPage = 20 }).ResultList;
                result = users.Where(m => !(presentClients.Select(x => x.Id).Contains(m.id))).Select(m => m).ToList();
            }
            else
            {
                result = presentClients.Where(m => m.Action.ToLower() == "checked in" && (string.IsNullOrEmpty(prefix) || m.Name.Contains(prefix))).Select(x => new Select2OptionModel
                {
                    id = x.Id,
                    text = x.Name
                }).ToList();
            }

            return Json(new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, result.GroupBy(m => m.text).Select(m => m.FirstOrDefault()).ToList()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUnApprovedAppointments(AppointmentSearchViewModel model)
        {
            if (User.IsInRole("Stylist"))
            {
                model.StylistId = User.Identity.GetUserId();
            }
            var appointmentCount = new Logic().GetAppointments<AppointmentViewModel>(model).ResultList.Count();
            return Json(appointmentCount, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCompletedTreatments(AppointmentSearchViewModel model)
        {
            if (User.IsInRole("Stylist"))
            {
                model.StylistId = User.Identity.GetUserId();
            }
            var unpaidInvoices = new Logic().GetUnpaidAppointments().Count();
            return Json(unpaidInvoices, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStylistCommission()
        {
            var stylistCommissions = new Logic().GetStylistCommissions(new StylistCommissionSearchViewModel { StartingDate = DateTime.Now, Stylist = new ApplicationUser { Id = User.IsInRole("Stylist") ? User.Identity.GetUserId() : "" } });
            stylistCommissions = Math.Round(stylistCommissions, 2);
            return Json(stylistCommissions, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckTreatments(AppointmentSearchViewModel model)
        {
            return Json(true, JsonRequestBehavior.AllowGet);// Removed all the checks for multi treatment. Done on client Demand.
            var result = new Logic().CheckTreatments(model);
            if (result == "true")
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //public JsonResult GetNewClients()
        //{
        //    var newClients = 
        //    return (, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult GetQrImage(string id)
        {

            string QrCodeImage = "";
            using (MemoryStream ms = new MemoryStream())
            {
                QRCodeGenerator qrGenerator = new QRCodeGenerator();
                QRCodeData qrCodeData = qrGenerator.CreateQrCode(id, QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);
                using (Bitmap qrCodeImage = qrCode.GetGraphic(20))
                {
                    qrCodeImage.Save(ms, ImageFormat.Png);
                    QrCodeImage = "data:image/png;base64," + Convert.ToBase64String(ms.ToArray());
                }
            }
            return Json(QrCodeImage, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}