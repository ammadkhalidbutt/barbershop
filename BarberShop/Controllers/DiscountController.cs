﻿using BusinessLogic;
using Enums;
using Helpers;
using Helpers.Select2;
using Microsoft.AspNet.Identity;
using Models;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace BarberShop.Controllers
{
    [Authorize]
    public class DiscountController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult _IndexPartial(DiscountSearchViewModel search)
        {
            var result = new Logic().GetDiscounts(new DiscountSearchViewModel { Name = search.Name }).ResultList;
            return PartialView(result);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Create()
        {
            var verifyPointExistence = new Logic().GetPoint();
            if (verifyPointExistence == null)
            {
                return RedirectToAction("Create", "Point", new { href = Request.RawUrl });
            }
            Session["action"] = "Create";
            return View(new DiscountViewModel { EntityList = new List<SelectListItem>(), Action = "Create" });
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Create(DiscountViewModel model)
        {

            if (model.Image != null)
            {
                var pic = System.IO.Path.GetFileName(model.Image.FileName);
                var mappedPath = Server.MapPath("/Attachments/discount");
                var path = System.IO.Path.Combine(mappedPath, pic);
                model.Image.SaveAs(path);
                model.ImageUrl = "/Attachments/discount/" + pic;
            }
            else
            {
                model.ImageUrl = "/images/default/default.jpg";
            }
            model.EntityType = DiscountEntityTypeCatalog.All;// Remove When Other options are requested by client
            model.CreatedBy = User.Identity.GetUserId();
            model.CreatedOn = DateTime.Now;
            model.UpdatedBy = User.Identity.GetUserId();
            model.UpdatedOn = DateTime.Now;
            var result = new Logic().Create(model);
            if (result != new Guid())
            {
                return RedirectToAction("Index", "Discount");
            }

            return View(model);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Edit(Guid id)
        {
            Session["action"] = "Edit";
            var result = new Logic().GetDiscount(id);
            result.Action = "Edit";
            return View("Create", result);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Edit(DiscountViewModel model)
        {
            if (model.Image != null)
            {
                var pic = System.IO.Path.GetFileName(model.Image.FileName);
                var mappedPath = Server.MapPath("/images/discount");
                var path = System.IO.Path.Combine(mappedPath, pic);
                model.Image.SaveAs(path);
                model.ImageUrl = "/images/discount/" + pic;
            }
            var result = new Logic().Edit(model);
            if (result != new Guid())
            {
                return RedirectToAction("Index", "Discount");
            }
            return View("Create", model);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Delete(Guid id)
        {
            var result = new Logic().Delete(new DiscountViewModel { Id = id });
            if (result != null)
            {
                return RedirectToAction("Index", "Discount");
            }
            return RedirectToAction("Index", "Discount");
        }
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public JsonResult Approve(DiscountViewModel discountModel)
        {
            try
            {
                var result = new Logic().Approve(discountModel);
                if (result)
                {
                    var messageBody = "";
                    if (string.IsNullOrEmpty(discountModel.NotificationBody) == false && string.IsNullOrEmpty(discountModel.NotificationTitle) == false)
                    {
                        messageBody = WebUtility.HtmlDecode(discountModel.NotificationBody.Replace("{PromoCode}", discountModel.PromoCode));
                        messageBody= Regex.Replace(messageBody, "<.*?>", String.Empty);
                    }
                    if (!string.IsNullOrEmpty(messageBody))
                    {
                        var deviceId = "/topics/promotions";
                        new FirebaseNotification().SendNotification(deviceId, discountModel.NotificationTitle, messageBody, discountModel.Id);
                    }
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


        public PartialViewResult GetDetailModal(Guid id)
        {
            return PartialView(new Logic().GetDiscount(id));
        }

        public JsonResult GetEntities(DiscountEntityTypeCatalog entityType, string prefix, int pageSize, int pageNumber)
        {
            List<Select2OptionModel> result = new List<Select2OptionModel>();
            switch (entityType)
            {
                case DiscountEntityTypeCatalog.Category:
                    result = new Logic().GetTreatmentCategories<Select2OptionModel>(new TreatmentCategorySearchViewModel { Select2 = true, Name = prefix }).ResultList;
                    break;
                case DiscountEntityTypeCatalog.Item:
                    result = new Logic().GetTreatments<Select2OptionModel>(new TreatmentSearchViewModel { Select2 = true, Name = prefix }).ResultList;
                    break;
                case DiscountEntityTypeCatalog.Customer:
                    {
                        AccountSearchViewModel search = new AccountSearchViewModel();
                        search.CurrentPage = pageNumber;
                        search.RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "Client" }).ResultList.First().Id;
                        search.RecordsPerPage = pageSize;
                        search.Pagination = true;
                        search.Name = prefix;
                        result = new Logic().GetUsers<RegisterViewModel>(search).ResultList.Select(x => new Select2OptionModel()
                        {
                            id = x.Id,
                            text = x.Name,
                        }).ToList();
                        break;
                    }
                default:
                    break;
            }

            return Json(new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, result), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDiscountedItems(string promoCode, string treatments, Guid customerId)
        {
            List<Guid> treatmentsLst = treatments.Split(',').Select(m => Guid.Parse(m)).ToList();
            try
            {
                var discountedItems = new Logic().GetDiscountedItems(customerId, promoCode, treatmentsLst);
                return Json(new { success = true, response = discountedItems }, JsonRequestBehavior.AllowGet);
            }
            catch (KnownException ex)
            {
                return Json(new { success = false, response = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, response = "There was some problem. Please try again." }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDiscounts(string prefix, int pageSize, int pageNumber, string stylistId, bool isForAddingStylistTreatment = false)
        {
            var discountsList = new Logic().GetDiscounts(new DiscountSearchViewModel { Name = prefix, Pagination = true, RecordsPerPage = 20, CurrentPage = 1 }).ResultList;
            var discounts = discountsList.Select(m => new Select2OptionModel
            {
                id = m.Id.ToString(),
                text = m.Name,
                additionalAttributesModel = m,
            }).ToList();
            return Json(new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, discounts), JsonRequestBehavior.AllowGet);
        }

    }
}
