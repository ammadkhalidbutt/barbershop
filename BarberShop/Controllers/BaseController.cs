﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace BarberShop.Controllers
{
    public class BaseController : Controller
    {
        public string LoggedInUserId { get { return User.Identity.GetUserId(); } }
    }
}