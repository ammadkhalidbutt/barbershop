﻿using BusinessLogic;
using Helpers.Select2;
using Models;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BarberShop.Controllers
{
    public class TaxController : BaseController
    {
        // GET: Tax
        public ActionResult Index()
        {
            return View(new Logic().GetTaxes(new Guid()));
        }

        // GET: Tax/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Tax/Create
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Create()
        {
            return View(new TaxViewModel { Action = "Create" });
        }

        // POST: Tax/Create
        [HttpPost]
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Create(TaxViewModel model)
        {
            try
            {
                // TODO: Add insert logic here
                var result = new Logic(LoggedInUserId).Create(model);
                if (result != new Guid())
                    return RedirectToAction("Index");
                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Tax/Edit/5
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Edit(Guid id)
        {
            var tax = new Logic().GetTax(id);
            tax.Action = "Edit";
            return View("Create", tax);
        }

        // POST: Tax/Edit/5
        [HttpPost]
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Edit(TaxViewModel model)
        {
            try
            {
                // TODO: Add update logic here
                var result = new Logic(LoggedInUserId).Edit(model);
                if(result != new Guid())
                return RedirectToAction("Index");
                return View("Create", model);
            }
            catch
            {
                return View("Create", model);
            }
        }

        // GET: Tax/Delete/5
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Delete(Guid id)
        {
            var check = new Logic().Delete(new TaxViewModel { Id = id });
                return RedirectToAction("Index");
        }

        public PartialViewResult _DeleteModal(Guid id)
        {
            var result = new Logic().GetTax(id);
            return PartialView(result);
        }


        #region JSON methods

        public JsonResult GetTaxesForSelect2(string prefix, int pageSize, int pageNumber)
        {
            var result = new Logic().GetTaxes(new Guid()).Select(m => new Select2OptionModel { id = m.Id.ToString(), text = m.Name }).ToList();
            return Json(new Select2Repository().GetSelect2PagedResult(pageSize, pageNumber, result), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Helping Methods



        #endregion
    }
}
