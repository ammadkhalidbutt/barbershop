﻿using BusinessLogic;
using Microsoft.AspNet.Identity;
using Models;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BarberShop.Controllers
{
    public class TreatmentController : Controller
    {
        // GET: Treatment

        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult _IndexPartial(TreatmentSearchViewModel search)
        {
            var result = new Logic().GetTreatments<TreatmentViewModel>(new TreatmentSearchViewModel { Id = search.Id, StylistId = search.StylistId }).ResultList;
            return PartialView(result);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Create()
        {
            var verifyPointExistence = new Logic().GetPoint();
            if (verifyPointExistence == null)
            {
                return RedirectToAction("Create", "Point", new { href = Request.RawUrl });
            }
            Session["action"] = "Create";
            
            var categoryList = new Logic().GetTreatmentCategories<Select2OptionModel>(new TreatmentCategorySearchViewModel { Select2 = true }).ResultList.Select(m => new SelectListItem { Text = m.text, Value = m.id }).ToList();
            return View(new TreatmentViewModel { CategoryList = categoryList, Action = "Create" });
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Create(TreatmentViewModel model)
        {

            if (model.Image != null)
            {
                var pic = System.IO.Path.GetFileName(model.Image.FileName);
                var mappedPath = Server.MapPath("/Attachments/treatment");
                var path = System.IO.Path.Combine(mappedPath, pic);
                model.Image.SaveAs(path);
                model.ImageUrl = "/Attachments/treatment/" + pic;
            }
            else
            {
                model.ImageUrl = "/images/default/default.jpg";
            }
            model.AddedBy = User.Identity.GetUserId();
            var result = new Logic().Create(model);
            if (result != new Guid())
            {
                return RedirectToAction("Index", "Treatment");
            }

            return View(model);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Edit(Guid id)
        {
            Session["action"] = "Edit";
            var result = new Logic().GetTreatment(id);
            result.Action = "Edit";
            return View("Create", result);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Edit(TreatmentViewModel model)
        {
            if (model.Image != null)
            {
                var pic = System.IO.Path.GetFileName(model.Image.FileName);
                var mappedPath = Server.MapPath("/images/treatment");
                var path = System.IO.Path.Combine(mappedPath, pic);
                model.Image.SaveAs(path);
                model.ImageUrl = "/images/treatment/" + pic;
            }
            var result = new Logic().Edit(model);
            if (result != new Guid())
            {
                return RedirectToAction("Index", "Treatment");
            }
            return View("Create", model);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator")]
        public ActionResult Delete(Guid id)
        {
            var result = new Logic().Delete(new TreatmentViewModel { Id = id });
            if (result != null)
            {
                return RedirectToAction("Index", "Treatment");
            }
            return RedirectToAction("Index", "Treatment");
        }

        public PartialViewResult GetDetailModal(Guid id)
        {
            return PartialView(new Logic().GetTreatment(id));
        }

        #region JSON Methods

        public JsonResult GetTreatmentsCount()
        {
            var search = new TreatmentSearchViewModel { Select2 = true };
            if (User.IsInRole("Stylist"))
            {
                search.StylistId = User.Identity.GetUserId();
            }
            return Json(new Logic().GetTreatmentsCount(search), JsonRequestBehavior.AllowGet);
        }

        #endregion


    }
}