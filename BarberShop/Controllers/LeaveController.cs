﻿using BusinessLogic;
using Microsoft.AspNet.Identity;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Enums;
using System.Web.Script.Serialization;

namespace BarberShop.Controllers
{
    public class LeaveController : Controller
    {
        // GET: Leave
        [Authorize(Roles = "Administrator, SuperAdministrator, Stylist")]
        public ActionResult DetailIndex()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, SuperAdministrator, Stylist")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, SuperAdministrator, Stylist")]
        public PartialViewResult _IndexPartial(LeaveSearchViewModel search)
        {
            var leaves = new List<LeaveViewModel>();
            if (User.IsInRole("Administrator") || User.IsInRole("SuperAdministrator"))
            {
                leaves = new Logic().GetLeaves<LeaveViewModel>(search).ResultList;
            }
            else if (User.IsInRole("Stylist"))
            {
                var stylistId = User.Identity.GetUserId();
                leaves = new Logic().GetLeaves<LeaveViewModel>(new LeaveSearchViewModel { Stylist = new ApplicationUser { Id = stylistId } }).ResultList;
            }
            leaves = leaves.GroupBy(x => x.ReferenceId).Select(x => x.FirstOrDefault()).OrderBy(x => x.StartingDate).ToList();
            return PartialView(leaves);
        }

        [Authorize(Roles = "Administrator, SuperAdministrator, Stylist")]
        public ActionResult Create(string id)
        {
            var result = new LeaveViewModel();
            if (string.IsNullOrEmpty(id))
            {
                result = new LeaveViewModel { Action = "Create", StartingDate = DateTime.Now.Date, DailyStartTime = DateTime.Now.TimeOfDay, DailyEndTime = DateTime.Now.AddMinutes(1).TimeOfDay, EndingDate = DateTime.Now.Date, StylistList = new List<SelectListItem>(), Recursion = Recursion.Never };
                return View(result);
            }
            else
            {
                var stylist = new Logic().GetUserDetails(id);
                result = new LeaveViewModel { Action = "Edit", StartingDate = DateTime.Now.Date, DailyStartTime = DateTime.Now.TimeOfDay, DailyEndTime = DateTime.Now.AddMinutes(1).TimeOfDay, EndingDate = DateTime.Now.Date, StylistList = new List<SelectListItem> { new SelectListItem { Text = stylist.Name, Value = stylist.UserId } }, Recursion = Recursion.Never };
                return View();
            }
        }

        public PartialViewResult CreatePartial(LeaveViewModel model)
        {
            return PartialView(model);
        }
        public PartialViewResult CreatePartialModal(Guid? Id, DateTime? Date, Guid? StylistId)
        {
            var stylist = StylistId != null || StylistId != new Guid() ? new Logic().GetUsers<Select2OptionModel>(new AccountSearchViewModel { Id = StylistId.ToString(), Select2 = true }).ResultList.Select(m => new SelectListItem { Text = m.text, Value = m.id }).ToList() : new List<SelectListItem>();
            LeaveViewModel leave = new LeaveViewModel { Action = "Create", StartingDate = Date ?? DateTime.Now.Date, DailyStartTime = Date.HasValue ? Date.Value.TimeOfDay : DateTime.Now.TimeOfDay, DailyEndTime = Date.HasValue ? Date.Value.TimeOfDay : DateTime.Now.TimeOfDay, EndingDate = Date ?? DateTime.Now.Date, StylistList = stylist, Recursion = Recursion.Never };
            if (Id != null)
            {
                var LeaveId = Id ?? new Guid();
                leave = new Logic().GetLeaves<LeaveViewModel>(new LeaveSearchViewModel { ReferenceId = LeaveId }).ResultList.FirstOrDefault();
                if (leave != null)
                {
                    leave.DaysArray = string.Join(",", leave.RecursiveDays);
                    leave.Action = "Edit";
                    leave.DailyStartTime = leave.StartingDate.TimeOfDay;
                    leave.DailyEndTime = leave.EndingDate.TimeOfDay;
                    leave.StylistList = new List<SelectListItem> { new SelectListItem { Value = leave.Stylist.Id, Text = leave.Stylist.Name } };
                }
            }
            leave.IsDashboard = true;
            return PartialView("CreatePartial", leave);
        }

        [HttpPost]
        public ActionResult Create(LeaveViewModel model)
        {
            if (User.IsInRole("Stylist"))
            {
                model.Stylist = new ApplicationUser { Id = User.Identity.GetUserId() };
            }
            if (!ModelState.IsValid)
                return Json(false, JsonRequestBehavior.AllowGet);
            var result = new Logic().Create(model);
            return Json(result != new Guid(), JsonRequestBehavior.AllowGet);

        }

        //
        // was commented for reason yet unknown.
        [Authorize(Roles = "Administrator, SuperAdministrator, Stylist")]
        public ActionResult Edit(Guid id)
        {
            var leave = new Logic().GetLeaves<LeaveViewModel>(new LeaveSearchViewModel { ReferenceId = id }).ResultList.FirstOrDefault();
            if (leave != null)
            {
                leave.DaysArray = string.Join(",", leave.RecursiveDays);
                leave.Action = "Edit";
                leave.DailyStartTime = leave.StartingDate.TimeOfDay;
                leave.DailyEndTime = leave.EndingDate.TimeOfDay;
                leave.StylistList = new List<SelectListItem> { new SelectListItem { Value = leave.Stylist.Id, Text = leave.Stylist.Name } };
                return View("Create", leave);
            }
            return RedirectToAction("Index", "Leave");
        }

        [HttpPost]
        public ActionResult Edit(LeaveViewModel model)
        {
            if (model.ReferenceId == new Guid())
            {
                model.ReferenceId = model.Id;
            }
            var result = new Logic().Edit(model);
            var request = Request.UrlReferrer.ToString().Contains("Dashboard");

            if (result != new Guid())
            {
                if (request)
                    return Json(true, JsonRequestBehavior.AllowGet);
                else
                    return RedirectToAction("Index");
            }
            else
            {
                if (request)
                    return Json(false, JsonRequestBehavior.AllowGet);
                else
                    return View("Create", model);
            }
        }

        //public ActionResult Delete(Guid id)
        //{
        //    return View(new Logic().GetLeaves<LeaveViewModel>(new LeaveSearchViewModel { ReferenceId = id }).ResultList.FirstOrDefault());
        //}
        [HttpGet]
        [Authorize(Roles = "Administrator, SuperAdministrator, Stylist")]
        public ActionResult Delete(LeaveViewModel model)
        {
            if (model.ReferenceId == new Guid())
            {
                model.ReferenceId = model.Id;
            }
            var result = new Logic().Delete(model);
            var request = Request.UrlReferrer.ToString().Contains("Dashboard");
            if (result != new Guid())
            {
                if (request)
                    return Json(true, JsonRequestBehavior.AllowGet);
                else
                    return RedirectToAction("Index");
            }
            else
            {
                if (request)
                    return Json(false, JsonRequestBehavior.AllowGet);
                else
                    return View(model);
            }
        }

        public ActionResult Practice()
        {
            var stylists = new Logic().GetUsers<RegisterViewModel>(new AccountSearchViewModel { RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "Stylist" }).ResultList.FirstOrDefault().Id }).ResultList.GroupBy(x => x.Email).Select(o => o.FirstOrDefault());
            var model = new FullCalendarSchedular { events = new List<FullCalendarEvents>(), resources = new List<FullCalendarResources>() };
            model.resources = stylists.Select(m => new FullCalendarResources { id = m.Id, title = m.Name, eventColor = "transparent" }).Distinct().ToList();
            return View(model);
        }
    }
}