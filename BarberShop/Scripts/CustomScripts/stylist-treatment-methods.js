﻿/// <reference path="select2methods.js" />

function getStylist(id, url) {
    if (id == "" || id == null) {
        $("#stylist-name-span").html("");
        $("#email-span").html("");
        $("#phone-number-span").html("");
    }
    else {
        $.ajax({
            url: url,
            data: { id: id },
            success: function (response) {
                $("#stylist-name-span").html(response.Name);
                $("#email-span").html(response.Email);
                $("#phone-number-span").html(response.PhoneNumber);
            }
        });
    }

}

function initializeTreatments(stylistId, url, reinitialize) {
    if (reinitialize) {
        $("#treatment-name").val("");
    }
    var pageSize = 20;
    var id = "#treatment-name";
    var dataArray = function (params) {
        params.page = params.page || 1;
            return {
                prefix: params.term,
                pageSize: pageSize,
                pageNumber: params.page,
                stylistId: stylistId,
                isForAddingStylistTreatment: true
            };
    };
    Select2AutoCompleteAjax(id, url, dataArray, pageSize,"Select Treatment");
}
function loadStylistTreatments(url) {
    $("#stylist-treatments-div").load(url, { stylistId: $("#stylist-id").val() }, function () { $("#myTable").DataTable(); });
    
}