﻿
var action = "checked in";
var title = $("#title").val();
var isForDashboard = $("#for-dashboard").val();

$("#check-in-button").html($("#title").val());
InitializeSelect2();
$("body").on("click", "#check-in-button", function () {
    var id = $(".client-list-check-in-select2").val();
    CheckInClient(id, "CheckInClients", "#check-in-client-error-div");
    $(".client-list-check-in-select2").val("");
    $(".client-list-check-in-select2").html("");
    InitializeSelect2();

});

$("body").on("click", "#check-out-button", function () {
    var id = $(".client-list-check-out-select2").val();
    CheckInClient(id, "CheckOutClients", "check-out", "#check-in-client-error-div");
    $(".client-list-check-out-select2").val("");
    $(".client-list-check-out-select2").html("");
    InitializeSelect2();

});

$("#qr-check-in-image").hide();
$("#qr-check-out-image").hide();
$("#check-in-client-id").focus();
$("#check-out-client-id").focus();

$("body").on("keypress focus", "#check-in-client-id", function (e) {
    if (e.which === 13) {
        var id = $("#check-in-client-id").val();
        CheckInClient(id, "CheckInClient", "#check-in-client-error-div");
        InitializeSelect2();
    }
});

$("body").on("keypress focus", "#check-out-client-id", function (e) {
    if (e.which === 13) {
        var id = $("#check-out-client-id").val();
        CheckInClient(id, "CheckOutClient", "check-out", "#check-in-client-error-div");
        InitializeSelect2();
    }
});

$("body").on("focus", "#check-in-client-id", function (event) {
    $("#" + event.target.id).on("keypress", function (e) {

    });
});

$("body").on("focus", "#check-out-client-id", function (event) {
    $("#" + event.target.id).on("keypress", function (e) {

    });
});

$("body").on("input", "#check-in-client-id", function () {
    var id = $("#check-in-client-id").val();
    $.ajax({
        url: '/Home/GetQrImage',
        data: { id: id },
        type: "POST"
    }).done(function (res) {
        $("#qr-check-in-image").attr("src", res);
        $("#qr-check-in-image").show();
    });
});

$("body").on("input", "#check-out-client-id", function () {
    var id = $("#check-out-client-id").val();
    $.ajax({
        url: '/Home/GetQrImage',
        data: { id: id },
        type: "POST"
    }).done(function (res) {
        $("#qr-check-out-image").attr("src", res);
        $("#qr-check-out-image").show();
    });
});

$("body").on("click", "#select-manually-check-in", function () {
    $("#check-in-by-select").show();
    $("#check-in-by-scan").hide();
    //$("#check-in-by-scan").show();
    $(this).hide();
});

$("body").on("click", "#select-manually-check-out", function () {
    $("#check-out-by-select").show();
    $("#check-out-by-scan").hide();
    //$("#check-out-by-scan").show();
    $(this).hide();
});

$("body").on("click", "#check-in-by-scan", function () {
    $("#check-in-by-select").hide();
    $("#check-in-by-scan").show();
    $("#check-in-manually").show();
    $(this).hide();
});

$("body").on("click", "#check-out-by-scan", function () {
    $("#check-out-by-select").hide();
    $("#check-out-by-scan").show();
    $("#check-out-manually").show();
    $(this).hide();
});

$("body").on("click", ".cancel", function () {
    window.location.href = '/Home/Index';
});

function InitializeSelect2() {

    var pageSize = 20;

    //
    //
    //Method which is to be called for populating options in dropdown //dynamically

    var id = ".client-list-check-in-select2";
    var url = "/Home/GetUsers";
    var dataArray = function (params) {
        params.page = params.page || 1;
        return {
            prefix: params.term,
            pageSize: pageSize,
            pageNumber: params.page,
            title: "checked in"
        };
    };
    Select2AutoCompleteAjax(id, url, dataArray, pageSize, "--- Search Clients ---");

    id = ".client-list-check-out-select2";
    url = "/Home/GetUsers";
    dataArray = function (params) {
        params.page = params.page || 1;
        return {
            prefix: params.term,
            pageSize: pageSize,
            pageNumber: params.page,
            title: "checked out"
        };
    };

    Select2AutoCompleteAjax(id, url, dataArray, pageSize, "--- Checked In Clients ---");

}

function CheckInClient(id, url, errorDiv) {
    $.ajax({
        url: '/Home/' + url,
        data: { id: id, forDashboard: isForDashboard },
        type: "POST"
    }).done(function (res) {
        if (res === true) {
            $("#error-span").remove();
            $("#checked-in-client-table-div").load('/Home/GetPresentClientPartial', { forDashboard: isForDashboard, action: "checked in" });
            $("#checked-out-client-table-div").load('/Home/GetPresentClientPartial', { forDashboard: isForDashboard, action: "checked out" });

            $(".myTable").DataTable();

        }
        else if (res === false) {
            DisplayError("Checked in failed");
        }
        else {
            DisplayError(res);
        }
    });

}

function DisplayError(msg) {
    $("#error-span").remove();
    $('#error-div').append('<span class="text-danger" id="error-span"> *' + msg + '</span>').delay(500);
}