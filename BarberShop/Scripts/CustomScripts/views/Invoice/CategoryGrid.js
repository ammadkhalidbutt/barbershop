﻿
function LoadGrid(category, item, backToCategoriesId) {
    //
    //this method is made to get treatments on the basis of selected category
    $("body").on("click", "." + category.class, function () {
        var categoryId = $(this).val();
        $.ajax({
            url: category.itemListUrl,
            data: { categoryId: categoryId },
            type: "POST",
            success: function (res) {
                $("#" + category.tableId).html(res);
                SetItemsCheckbox(item.selectedIdClass, item.class, item.checkBoxClass);
            }
        });
    });

    //
    //this is the new method to add treatments in an appointment after selecting treatment category.
    $("body").on("click", "." + item.class, function () {
        event.preventDefault();

        var element = this;
        var itemId = $(element).val();

        var checkboxElement = $(element).children("." + item.checkBoxClass);
        checkboxElement.prop("checked", !(checkboxElement.prop("checked")));

        var selectedTreatmentIds = [];

        $("." + item.selectedIdClass).each(function (i, v) { selectedTreatmentIds.push(v.value); });
        var productListCheck = !(selectedTreatmentIds.length > 0 && ($.inArray(itemId, selectedTreatmentIds)) >= 0);
        var index = $(".invoice-item-row").length;

        if (checkboxElement.is(':checked')) {

            $("#" + item.selectedIdSpanId).append('<input type="text" name="one" value="' + itemId + '" attr-id="' + itemId + '" class="' + item.selectedIdClass + '" />');
            $.ajax({
                url: item.itemDetailUrl,
                data: { itemId: itemId },
                type: "POST",
                success: function (res) {
                    AddInvoiceItem(res, index, item.itemType);
                    $(element).attr("class", "btn btn-danger category-grid-button " + item.class);
                }
            });

        }
        else {
            RemoveInvoiceItem(itemId, item.selectedIdClass);
            ChangeItemAfterDelete(element, item.class, item.checkBoxClass);

        }
    });

    //
    //to return from item view to item category
    $("body").on("click", backToCategoriesId, function () {
        LoadCategories(category.categoryUrl, category.tableId);
    });


    $(document).on("click", ".remove-item", function () {
        event.preventDefault();
        var itemId = $(this).attr("attr-id");
        RemoveInvoiceItem(itemId, item.selectedIdClass);
        $("." + item.class).each(function (i, v) {
            if ($(v).attr("attr-id") === itemId) {
                ChangeItemAfterDelete(v, item.class, item.checkBoxClass);

            }
        });
    });

}


function LoadCategories(url, tableId) {

    $.ajax({
        url: url,
        type: "POST",
        success: function (res) {
            $("#" + tableId).html(res);
        }
    });

}


//
//Reset treatments from selected treatments
function SetItemsCheckbox(selectedIdClass, itemClass, checkboxClass) {
    var selectedProductList = [];
    $("." + selectedIdClass).each(function (i, v) { selectedProductList.push(v.value); });
    $("." + itemClass).each(function (i, v) {
        if ($.inArray(v.value, selectedProductList) >= 0) {
            console.log($(v).val());
            $(v).children("." + checkboxClass).prop("checked", true);
            $(v).attr("class", "btn btn-danger category-grid-button " + itemClass);
        }
    });
}


//
//functionality of removing an item removing row and calling RemoveTableRow to further implement the change.
function RemoveInvoiceItem(itemId, selectItemClass) {
    $("." + selectItemClass).each(function (i, v) { if (v.value === itemId) v.remove(); });
    $("#" + itemId).remove();
    RemoveTableRow("invoice-item-table", "invoice-item-row");
    $(".invoice-item-row").last().next().remove();
    $("#invoice-item-table-body").append(AddLastRow());
}


function RemoveTableRow(tableId, rowClass) {
    $("#" + tableId).find("tbody").children("tr." + rowClass).each(function (ind, val) {
        var rowId = $(val).attr("id");

        //if (rowId !== undefined && rowId !== null && rowId !== "") {
        //    ChangeRowElementValues("id", rowId, ind, val, '-', '');
        //}
        RenameRowElements(rowClass, ind);
        $(val).children(".row-index").html(parseInt(ind+1));
    });
}

function ChangeRowElementValues(property, propertyValue, index, element, startingChar, endingChar) {
    var indexStarting = propertyValue.lastIndexOf(startingChar);
    var indexEnding = propertyValue.lastIndexOf(endingChar);
    var newValue = "";
    if (indexStarting > 0 && indexEnding > 0) {
        newValue = newValue.concat(propertyValue.substring(0, [indexStarting + 1]), index, propertyValue.substring([indexEnding]));
    }
    else if (indexStarting > 0) {
        newValue = newValue.concat(propertyValue.substring(0, [indexStarting + 1]), index);
    }
    $(element).attr(property, newValue);

}

function RenameRowElements(val, ind) {
    $(val).children().each(function (i, v) {
        var name = $(v).attr("name");
        if (name !== undefined && name !== null && name !== "") {
            ChangeRowElementValues("name", name, ind, v, '[', ']');
        }
        RenameRowElements(v, ind);
    });
}

function ChangeItemAfterDelete(element, itemClass, checkboxClass) {
    $(element).attr("class", "btn btn-success category-grid-button " + itemClass);
    $(element).children("." + checkboxClass).prop("checked", false);
    RecalculateAllValues();
}


//
//Reset all figures if any discount is made or any item is included or removed.
function RecalculateAllValues() {

    var totalPrice = 0;

    var discountedPrices = $(".discounted-price").get();
    discountedPrices.forEach(function (index, element) {
        totalPrice += parseFloat(discountedPrices[element].textContent);
    });

    var finalPrice = totalPrice;
    var cashRemainder = (parseFloat($("#AmountGiven").val()) - finalPrice);

    $(".final-price").html(finalPrice.toFixed(2).toString());
    $("input.grand-total").val(finalPrice);

    $(".total-price").html(finalPrice.toFixed(2).toString());
    $(".remainder-after-cash").html(cashRemainder.toFixed(2).toString());

    $(".final-remainder").html(finalPrice.toFixed(2).toString());
    $("span.grand-total").html(finalPrice.toFixed(2).toString());
    $(".amount-recieved").val(parseFloat($(".final-price").html()));
    if (ValidateRemainder()) {
        $("#error-span").remove();
    }
}


function AddLastRow() {
    var totalAmount = 0;
    $(".discounted-price").each(function (i, v) { totalAmount += parseFloat($(v).html()); });
    var trStart = '<tr>';
    var labelTd = '<td class="text-right" colspan="6" style="font-weight:600; font-size:24px"><b>Total</b></td>';
    var totalTd = '<td class="text-right" colspan="6" style="font-weight:600; font-size:24px"><b> <span class="final-price">' + totalAmount + '</span> €.</b></td>';
    var trEnd = '</tr>';
    var row = trStart + labelTd + totalTd + trEnd;
    return row;
}

