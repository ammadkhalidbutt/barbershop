﻿$(function () {
    //to display create appointment modal
    $("body").on("click", "#create-appointment-button", function () {
        var url = "/Appointment/CreateMultiTreatmentAppointment";
        var stylistId = $("#stylist-id").val();
        $.ajax({
            url: url,
            data: { 'StylistId': stylistId }
        }).done(function (data, textStatus, jqXHR) {
            $("#create-appointment-modal").html(data);
            $("#create-modal").modal("show");
        });
    });
    $("body").on("click", "#submit-appointment", function () {
        $("#appointment-form").removeData("validator").removeData("unobtrusiveValidation");//remove the form validation
        $.validator.unobtrusive.parse($("#appointment-form"));//add the form validation
        if ($("#appointment-form").valid()) {
            $.ajax({
                type: "POST",
                url: "/Appointment/CreateMultiAppointment",
                data: $("#appointment-form").serializeArray(),
                success: function (result) {
                    if (result !== '00000000-0000-0000-0000-000000000000 ') {
                        $.ajax({
                            type: "POST",
                            url: "/Appointment/GetMultiAppointment",
                            data: { 'id': result },
                            success: function (htmlResult) {
                                $("#treatments-wrapper").append(htmlResult);
                                StartTimer($(".appointment-timer." + result), false);
                                $("#create-modal").modal("hide");
                            },
                            error: function (data) {
                                return false;
                            }
                        });

                    }
                },
                error: function (data) {
                    return false;
                }
            });
        }
    });
});
