﻿$(function () {
    //--------------------Initializing All Timers on Page---------------------
    $(".appointment-timer").each(function () {
        var startTimer = false;
        var appointmentId = GetAppointmentId(this);
        var appointmentStatus = $('.appointment-status.' + appointmentId).val();
        if (appointmentStatus === "Started") {
            startTimer = true;
        }
        StartTimer(this, startTimer, appointmentStatus);
    });

    $(document).on("click", ".resume-timer", function () {
        StartTimer(this, true, null);
        ChangeState(this, "Started");
        var badge = '<span class="badge badge-success float-right @Model.Id status-badge">Started</span>';
        $("." + this.classList[this.classList.length - 1] + ".status-badge").html(badge);
    });

    $(document).on("click", ".pause-timer", function () {
        PauseTimer(this);
        ChangeState(this, "Paused");
        var badge = '<span class="badge badge-warning float-right @Model.Id status-badge">Paused</span>';
        $("." + this.classList[this.classList.length - 1] + ".status-badge").html(badge);

    });

    $(document).on("click", ".start-timer", function () {
        StartTimer(this, true, null);
        ChangeState(this, "Started");
        var badge = '<span class="badge badge-success float-right @Model.Id status-badge">Started</span>';
        $("." + this.classList[this.classList.length - 1] + ".status-badge").html(badge);
    });

    $(document).on("click", ".cancel-appointment", function () {
        var appointmentId = GetAppointmentId(this);
        CancelAppointment(appointmentId);
    });

    $(document).on("click", ".finish-appointment", function () {
        var appointmentId = GetAppointmentId(this);
        var treatmentId = $('.treatment-id.' + appointmentId).val();
        var stylistId = $('.stylist-id.' + appointmentId).val();
        var price = $('.treatment-price.' + appointmentId).val();
        FinishAppointment(appointmentId, treatmentId, stylistId, price);
    });
});

//=================================================Helper Functions======================================
function StartTimer(element, startTimer, appointmentStatus) {
    var appointmentId = GetAppointmentId(element);
    //--------------------Setting Required Fields--------------------
    $('.timer-start-time.' + appointmentId).val($.now());

    //------------------------Getting Required fields to start timer-----------------
    var totalTime = $('.total-time.' + appointmentId).val();
    totalTime = totalTime * 60;// In Seconds
    var elapsedTime = 0;
    if (appointmentStatus !== null && appointmentStatus === "Started")
        elapsedTime = GetElapsedTime(element, $('.timer-end-time.' + appointmentId).val());
    else
        elapsedTime = GetElapsedTime(element, null);
    var timerSeconds = totalTime - elapsedTime;

    if (timerSeconds < 0)//if elapsed time is greater than total time stop counter
    {
        timerSeconds = 1;// Set to 1 other wise it become digital clock
        $('.pause-timer.' + appointmentId).hide();
    }
    $('.appointment-timer.' + appointmentId).timeTo(timerSeconds, {
        displayDays: false,
        theme: "white",
        displayCaptions: false,
        fontSize: 30,
        start: startTimer,
        countdown: true
    }, function () {
        $('.pause-timer.' + GetAppointmentId(element)).hide();
    });
    ReloadAppointmentDiv();
}

function PauseTimer(element) {
    var appointmentId = GetAppointmentId(element);
    var totalElapsedTime = GetElapsedTime(element, null);
    $('.timer-elapsed-time.' + appointmentId).val(totalElapsedTime);
    $('.appointment-timer.' + appointmentId).timeTo('stop');
    ReloadAppointmentDiv();
}

function GetTimerElement(element) {
    var appointmentId = GetAppointmentId(element);
    return $('.appointment-timer' + appointmentId);
}

function ChangeState(element, currentState) {
    var appointmentId = GetAppointmentId(element);
    var elapsedTime = $('.timer-elapsed-time.' + appointmentId).val();
    if (currentState === "Started") {
        $(element).replaceWith("<a class='btn btn-warning btn-rounded pause-timer " + appointmentId + "'><i class='fa fa-pause'></i> Pause</a>");
    }
    else {
        $(element).replaceWith("<a class='btn btn-warning btn-rounded resume-timer " + appointmentId + "'><i class='fa fa-play'></i> Resume</a>");
    }
    $('.appointment-status.' + appointmentId).val(currentState);
    ChangeAppointmentState(appointmentId, elapsedTime, currentState);

}

function GetElapsedTime(element, lastUpdatedTime) {
    var appointmentId = GetAppointmentId(element);

    if (lastUpdatedTime !== null) {// If timer is started and someone leaves the page without pausing timer then it should be the moment he/she left the page till now plus elapsed time.
        var startedTimerElapsedTime = 0;
        startedTimerElapsedTime = $.now() - lastUpdatedTime;
        startedTimerElapsedTime = startedTimerElapsedTime / 1000; // In Seconds
        $('.timer-elapsed-time.' + appointmentId).val(startedTimerElapsedTime + parseFloat($('.timer-elapsed-time.' + appointmentId).val()));
    }

    var currentElapsedTime = $.now() - $('.timer-start-time.' + appointmentId).val();
    currentElapsedTime = currentElapsedTime / 1000;// In Seconds
    var totalElapsedTime = currentElapsedTime + parseFloat($('.timer-elapsed-time.' + appointmentId).val());
    return totalElapsedTime;
}

function GetAppointmentId(element) {
    var classesArr = $(element).attr('class').split(' ');
    var id = classesArr[classesArr.length - 1];
    if (id.length !== 36) {
        $(classesArr).each(function (index, value) {
            if (value.length === 36) {
                id = value;
            }
        });
    }
    if (parseInt(id.length) === 36)
        return '' + id;
    else
        return '';
}

