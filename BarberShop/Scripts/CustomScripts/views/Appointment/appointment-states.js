﻿function ChangeAppointmentState(id, elapsedTime, appointmentStatus) {
    var data = JSON.stringify({
        'id': id,
        'elpasedTime': elapsedTime,
        'appointmentStatus': appointmentStatus
    });
    $.ajax({
        type: "post",
        url: '/Appointment/ChangeAppointmentState',
        data: data,
        contentType: 'application/json',
        success: function (result) {

        }
    });
}
function CancelAppointment(id) {
    var r = confirm("Are you sure you want to cancel this appointment");
    if (r === true) {
        var data = {
            Id: id,
            Status: "Cancelled",
            AppointmentTreatmentStatus: "Cancelled"
        };
        $.ajax({
            url: '/Appointment/Edit',
            type: "POST",
            data: { model: data }
        }).done(function (res) {
            if (res) {
                $('.single-appointment-wrapper.' + id).remove();
                ReloadAppointmentDiv();
            }
        });
    }
}
function FinishAppointment(id, treatmentId, stylistId, price) {
    var r = confirm("Are you sure you want to submit?");
    if (r === true) {
        var appObj = {
            Id: id,
            Treatment: {
                Id: treatmentId,
                Price: price
            },
            Stylist:
            {
                Id: stylistId
            }
        };
        $.ajax({
            url: "/Appointment/Finish",
            type: "post",
            data: { model: appObj },
            success: function (res) {
                if (res) {
                    $('.single-appointment-wrapper.' + id).remove();
                    ReloadAppointmentDiv();
                }
            }
        });
    }
}

function PreserveCurrentStates() {
    $(".appointment-timer").each(function () {
        var appointmentId = GetAppointmentId(this);
        var appointmentStatus = $('.appointment-status.' + appointmentId).val();
        if (appointmentStatus === "Started") {

            var elapsedTime = GetElapsedTime(this);
            var data = JSON.stringify({
                'id': appointmentId,
                'elpasedTime': elapsedTime,
                'appointmentStatus': appointmentStatus
            });
            $.ajax({
                type: "post",
                url: '/Appointment/ChangeAppointmentState',
                data: data,
                contentType: 'application/json',
                success: function (result) {

                }
            });
        }
    });
}
function ReloadAppointmentDiv() {
    var stylistId = $("#stylist-id").val();
    $("#finished-treatment-table-div").load('/Appointment/_GetCurrentStylistAppointments', { stylistId: stylistId }, function () {
        $("#myTable").DataTable();
    });

}