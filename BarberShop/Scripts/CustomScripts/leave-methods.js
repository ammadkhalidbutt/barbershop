﻿

$(function () {
    InitializeSelect2();
    //Datetime picker initialization

    $(".date-time-picker").datetimepicker({
        format: 'Y-m-d h:m A',
        ampm: true
    });

    $(".date-picker").datetimepicker({
        timepicker: false,
        format: 'Y-m-d'
    });

    $(".time-picker").datetimepicker({
        datepicker: false,
        format: 'H:i',
        step: 15,
    });
    //
    //Initial setup
    var recursionStatus = $("#recursion").val();
    $(".weekly-recursion").hide();
    $(".continuous").hide();
    if (!($('#is-continuous').is(":checked"))) { $(".continuous-recursion").show(); }
    else {
        $(".continuous-recursion").hide();
    }
    if ($("#action").val() == "Edit") {
        CheckRecursionStatus();
    }

    //
    //Recursion dropdown functionality
    $("#recursion").on("change", function () {
        CheckRecursionStatus();
    });

    //
    //Continuous checkbox functionality
    $("#is-continuous").on("change", function () {
        if ($("#is-continuous").is(":checked")) {
            $(".continuous-recursion").hide();
        }
        else {
            $(".continuous-recursion").show();
        }
    });

    //
    //Weekdays calculation from page to server
    $("#submit-leave").on("mouseenter", function () {
        var days = new Array();
        for (i = 1; i <= 7; i++) {
            var day = $("#day-" + i).hasClass("active");
            if (day) {
                days.push(i);
            }
        }
        $("#days-selected").val(days);
        console.log(days);

    });


    //
    //Weekdays calculation from server to page
    var dayList = $("#days-selected").val();
    if (dayList != "" && dayList!=null) {
        var days = dayList.split(",");
        for (var i = 0; i < days.length; i++) {
            $("#day-" + days[i]).addClass("active");
        }
    }
    $("*[data-val-is-dependentproperty]").each(function () {
        $("#" + $(this).attr("data-val-is-dependentproperty")).on('blur', function () {
            $("input[data-val-is-dependentproperty='" + $(this).attr('id') + "']").valid();
        });
    });

});


//
//Functions

function InitializeSelect2() {
    var pageSize = 20;
    var optionListUrl = '/Appointment/GetUsers';
    //
    //
    //Method which is to be called for populating options in dropdown //dynamically

    var id = "#stylist-name";
    var url = "/Appointment/GetStylists";
    var dataArray = function (params) {
        params.page = params.page || 1;
        return {
            prefix: params.term,
            pageSize: pageSize,
            pageNumber: params.page
        }
    };
    Select2AutoCompleteAjax(id, url, dataArray, pageSize,"Select Stylist");

}
function CheckRecursionStatus() {
    var value = $("#recursion").val();
    switch (value) {
        case "2": $(".weekly-recursion").hide(); $(".continuous").show(); break;
        case "3": $(".weekly-recursion").show(); $(".continuous").show(); break;
        case "4": $(".weekly-recursion").show(); $(".continuous").show(); break;
        default: $(".daily-recursion").show(); $(".weekly-recursion").hide(); $(".continuous").hide(); break;
    }
}

function SubmitLeaveForm() {
    if (!$("#leave-form").valid()) {
        return $.Deferred().resolve(false);
    }
    var ajaxResult = $.ajax({
        type: "POST",
        url: '/Leave/' + $("#action").val(),
        data: $("#leave-form").serializeArray(),
        success: function (result) {
            return result;
        },
        error: function (data) {
            return false;
        }
    });
    return ajaxResult;

}
