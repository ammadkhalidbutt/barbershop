﻿var events = [];
var selectedEvent = null;

function GenerateCalender(events) {

    $('#calender').fullCalendar('destroy');
    $('#calender').fullCalendar({
        defaultView: 'agendaDay',
        contentHeight: 400,
        defaultDate: $("#BookingDate").val(),
        eventOverlap: false,
        minTime: "08:00:00",
        maxTime: "20:00:00",
        slotDuration: "00:15:00",
        timeFormat: 'h(:mm)a',
        header: {
            left: '',
            center: 'title',
            right: ''
        },
        eventLimit: true,
        eventColor: '#378006',
        events: events,
        eventClick: function (calEvent, jsEvent, view) {
            selectedEvent = calEvent;
            $('#myModal #eventTitle').text(calEvent.title);
            var $description = $('<div/>');
            $description.append($('<p/>').html('<b>Start:</b>' + calEvent.start.format("DD-MMM-YYYY HH:mm a")));
            if (calEvent.end != null) {
                $description.append($('<p/>').html('<b>End:</b>' + calEvent.end.format("DD-MMM-YYYY HH:mm a")));
            }
            $description.append($('<p/>').html('<b>Description:</b>' + calEvent.description));
            $('#myModal #pDetails').empty().html($description);

            $('#myModal').modal();
        },
        selectable: true,
        disableDragging: true,
        editable: false,
        select: function (start, end) {
            selectedEvent = {
                eventID: 0,
                title: '',
                description: '',
                start: start,
                end: end,
                allDay: false,
                color: ''
            };
        }
    })
}

function FetchEventAndRenderCalendar() {
    events = [];

    //var action = $("#Action").val();
    //var data;
    //if ($("#Action").val() == "Edit") {
    //    data = {
    //        StartingDateTime: $("#BookingDate").val(),
    //        StylistId: $("#stylist-name").val(),
    //        ClientId: $("#client-name").val(),
    //        Action: $("#Action").val(),
    //        DiscardAppointmentString: $("#Id").val()
    //    };
    //}
    //else {
    //    data = {
    //        StartingDateTime: $("#BookingDate").val(),
    //        ClientId: $("#client-name").val(),
    //        StylistId: $("#stylist-name").val()
    //    };
    //}

    //$.ajax({
    //    type: "POST",
    //    url: "/Appointment/GetEvents",
    //    data: { model: data },
    //    success: function (data) {

    //        $.each(data, function (i, v) {
    //            events.push({
    //                eventID: v.EventID,
    //                title: v.Subject,
    //                description: v.Description,
    //                start: moment(v.Start),
    //                end: v.End != null ? moment(v.End) : null,
    //                color: v.ThemeColor,
    //                allDay: v.IsFullDay
    //            });
    //        })

    //        GenerateCalender(events);
    //    },
    //    error: function (error) {
    //        $("#calender").html("");
    //        alert('failed');
    //    }
    //})
}


function InitializeSelect2() {
    var pageSize = 20;
    var optionListUrl = '/Appointment/GetUsers';
    //
    //
    //Method which is to be called for populating options in dropdown //dynamically

    var id = "#client-name";
    var url = '/Appointment/GetUsers';
    var dataArray = function (params) {
        params.page = params.page || 1;
        return {
            prefix: params.term,
            pageSize: pageSize,
            pageNumber: params.page
        };
    };
    Select2AutoCompleteAjax(id, url, dataArray, pageSize,"Select Client");

    id = "#treatment-name";
    var stylistId = $("#stylist-name").val();
    if (stylistId !== "") {
        url = '/Appointment/GetTreatments';
        dataArray = function (params) {
            params.page = params.page || 1;
            return {
                prefix: params.term,
                pageSize: pageSize,
                pageNumber: params.page,
                stylistId: stylistId
            };
        };
        Select2AutoCompleteAjax(id, url, dataArray, pageSize,"Select Treatment");
    }



    id = "#stylist-name";
    treatmentId = $("#treatment-name").val();
    url = '/Appointment/GetStylists';
    dataArray = function (params) {
        params.page = params.page || 1;
        return {
            prefix: params.term,
            pageSize: pageSize,
            pageNumber: params.page,
            treatmentId: treatmentId
        };
    };
    Select2AutoCompleteAjax(id, url, dataArray, pageSize,"Select Stylist");

}

async function VerifyTime() {
    var action = $("#Action").val();
    var data;
    if ($("#action").val() === "Edit") {
        data = {
            StartingDateTime: $("#start").val(),
            ClientId: $("#client-name").val(),
            StylistId: $("#stylist-name").val(),
            BookingDate: $("#BookingDate").val(),
            Action: $("#Action").val(),
            DiscardAppointmentString: $("#Id").val(),
            TreatmentId: $("#treatment-name").val()
        };
    }
    else {
        data = {
            StartingDateTime: $("#start").val(),
            ClientId: $("#client-name").val(),
            BookingDate: $("#BookingDate").val(),
            StylistId: $("#stylist-name").val(),
            TreatmentId: $("#treatment-name").val()
        };
    }
    var url = '/Appointment/ValidateStartingTime';
    if ($("#stylist-name").val() === "") {
        TimeVerificationError("Select stylist first");
        return false;
    }
    else if ($("#treatment-name").val() === "") {
        TimeVerificationError("Select treatment first");
        return false;
    }
    else if ($("#client-name").val() === "") {
        TimeVerificationError("Select client first");
        return false;
    }
    else {
        var check = false;
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            data: { model: data },
            beforeSend: function () {
                $("#submit-appointment").attr("disabled", "disabled");
                $("#loader").show();
            },
            success: function (response) {
                if (response !== "") {

                    TimeVerificationError(response + ".");
                    check = false;
                }
                else {
                    $("#loader").hide(500);
                    FetchEventAndRenderCalendar();
                    $("#error-div").html("");
                    $("#submit-appointment").removeAttr("disabled");
                    check = true;

                }
            },
            error: function () {
                TimeVerificationError("Fill the above fields first");
                $("#calender").html("");
                check = false;
            },
            done: function () {
                check = 'hello';
            }
        });
        return check;
    }
}

function TimeVerificationError(msg) {
    $('#error-div').append('<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i><span>' + "  " + msg + '</span></div>').delay(500);
    $("#submit-appointment").attr("disabled", "disabled");
    FetchEventAndRenderCalendar();
}

function VerifyDate() {
    var dateSelected = $("#BookingDate").val();
    dateSelected = new Date(dateSelected);
    var date = new Date().setHours(0, 0, 0);
    if (dateSelected < date) {
        $("#submit-appointment").attr("disabled", "disabled");
        TimeVerificationError("Enter date greater or equal than today")
    }
    else {
        $("#submit-appointment").removeAttr("disabled");
        $("#error-div").html("");
    }
}

function GetUserDetails(id) {
    if (id === "" || id === null) {
        $("#load-stylist-email").val("");
        $("#load-stylist-phone").val("");
    }
    else {
        $.ajax({
            type: "POST",
            url: '/Appointment/GetUserDetail',
            data: { id: id },
            success: function (res) {
                $("#load-stylist-email").val(res.Email);
                $("#load-stylist-phone").val(res.PhoneNumber);
            }
        });
    }
}



//Partial View Scripts
$(document).ready(function () {
    $("#loader").hide();
    InitializeSelect2();
    if ($("#action").val() === "Edit") {
        GetUserDetails($("#client-name").val());
        $("#edit-div").show();
    }
    else {
        $("#create-div").show();
    }
    FetchEventAndRenderCalendar();
    $("#calendar").hide();
    //
    //functionality of changing client name
    $("body").off("change", "#client-name");
    $("body").on("change", "#client-name", function () {
        GetUserDetails($("#client-name").val());
        if ($("#client-name").val() != "") {
            //$("#appointment-start-error").remove();
            $("#edit-div").show();
            $("#create-div").hide();
        }
        else {
            $("#edit-div").hide();
            $("#create-div").show();
        }
        VerifyTime();
    });
    $("body").off("click", "#client-name");
    $("body").on("click", "#client-name", function () {
        GetUserDetails($(this).val());
    });


    VerifyTime();
    var pageSize = 20;


    //Initializing Stylist dropdown
    $("body").off("change", "#treatment-name");
    $("body").on("change", "#treatment-name",async function () {
        var verificationCheck;
        verificationCheck = await VerifyTime() === "" ? true : false;

        if (verificationCheck) {
            var id = "#stylist-name";
            var treatmentId = $("#treatment-name").val();
            var url = "/Appointment/GetStylists";
            var dataArray = function (params) {
                params.page = params.page || 1;
                return {
                    prefix: params.term,
                    pageSize: pageSize,
                    pageNumber: params.page,
                    treatmentId: treatmentId
                };
            };
            Select2AutoCompleteAjax(id, url, dataArray, pageSize,"Select Stylist");
        }

    });


    //
    //loading Calendar on changing Stylist
    $("body").off("change", "#stylist-name");
    $("body").on("change", "#stylist-name", async function () {
        $("#treatment-name").val("").html("");
        var stylistId = $(this).val();
        id = "#treatment-name";
        url = '/Appointment/GetTreatments';
        dataArray = function (params) {
            params.page = params.page || 1;
            return {
                prefix: params.term,
                pageSize: pageSize,
                pageNumber: params.page,
                stylistId: stylistId
            };
        };
        Select2AutoCompleteAjax(id, url, dataArray, pageSize,"Select Treatment");
        //var verificationCheck = await VerifyTime() === "" ? true : false;
    });


    //
    //on changing the date
    $("body").off("change", "#BookingDate");
    $("body").on("change", "#BookingDate", function () {
        VerifyDate();
        VerifyTime();
    });



    //
    //on changing time
    $("body").off("change", "#start");
    $("body").on("change", "#start", function (event) {
        VerifyTime();
    });
    $(document).off('show.bs.modal', '.modal');
    $(document).on('show.bs.modal', '.modal', function () {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function () {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });

    $("body").off("click", ".update-user-button");
    $("body").on("click", ".update-user-button", function (event) {
        var selectedClient = $("#client-name").val();
        $.ajax({
            type: "POST",
            url: '/Account/UpdateClientOnAppointment',
            data: { Id: selectedClient },
            success: function (loadedView) {
                $("#client-modal-body").html(loadedView);
                $("#client-modal").modal("show");
            }
        });
    });

    $("body").off("click", "#submit-user");
    $("body").on("click", "#submit-user", function (event) {
        SubmitUserForm().done(function (obj) {

            if (obj != null) {
                // Set the value, creating a new option if necessary
                if ($('#client-name').find("option[value='" + obj.Id + "']").length) {
                    $('#client-name').val(obj.Id).trigger('change');
                } else {
                    // Create a DOM Option and pre-select by default
                    var newOption = new Option(obj.Name, obj.Id, true, true);
                    // Append it to the select
                    $('#client-name').append(newOption).trigger('change');
                }
                $("#client-modal").modal("hide");
            }
        });
    });
});

function SubmitAppointmentForm() {

    if (!$("#appointment-form").valid()) {
        return $.Deferred().resolve(false);
    }
    var ajaxResult = $.ajax({
        type: "POST",
        url: '/Appointment/' + $("#action").val(),
        data: $("#appointment-form").serializeArray(),
        success: function (result) {
            return result;
        },
        error: function (data) {
            return false;
        }
    });
    return ajaxResult;
}
//
//
//Functions


