﻿function GetSearchFilterAjaxResult(url, data, popup) {
    $.ajax({
        type: "post",
        url: url,
        data: { search: data },
        beforeSend: function () {
            //$(".filter-div").hide(1000);
            $(".loader-div").show(1000);
        },
        success: function (data) {
            $(".loader-div").hide(1000);
            if (!data) {
                $(".report-div").html("No data found");
            }
            else {
                $(".report-div").html(data);
                $(".report-div").show(2000).delay(3000);
                if (popup) {
                $('.image-popup-vertical-fit').magnificPopup({
                    type: 'image',
                    closeOnContentClick: true,
                    mainClass: 'mfp-img-mobile',
                    image: {
                        verticalFit: true,
                        titleSrc: false,
                    }

                    });
                }
            }
        }
    });

}

function InitialOrientation() {
    //$(".filter-div").hide();
    $(".loader-div").hide();
}
