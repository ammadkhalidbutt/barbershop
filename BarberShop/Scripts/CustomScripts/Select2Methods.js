﻿function Select2AutoCompleteAjax(id, url, dataArray, pageSize, placeHolder) {
    try {
        $(id).select2(
            {
                ajax: {
                    delay: 150,
                    url: url,
                    dataType: 'json',
                    //dropdownParent: "#check-in-client-modal", 

                    data: dataArray,
                    processResults: function (data, params) {
                        //alert(data.Results);
                        params.page = params.page || 1;
                        return {
                            results: data.Results,
                            pagination: {
                                more: (params.page * pageSize) < data.Total
                            }
                        };
                    }
                },
                placeholder: placeHolder,
                minimumInputLength: 0,
                allowClear: true
            });
    }
    catch (err) {
        console.log(err);
    }
}