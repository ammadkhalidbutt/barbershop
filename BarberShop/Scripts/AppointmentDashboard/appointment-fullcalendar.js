﻿$(function () {
    var date = new Date();
    $('#full-calendar').fullCalendar({
        schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
        defaultView: 'agendaDay',
        defaultDate: date,
        editable: true,
        selectable: true,
        slotDuration: '00:5:00',
        minTime: "08:00:00",
        maxTime: "23:00:00",
        eventLimit: true, // allow "more" link when too many events
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'agendaDay'
        },
        views: {
            agendaTwoDay: {
                type: 'agenda',
                duration: { days: 1 },

                // views that are more than a day will NOT do this behavior by default
                // so, we need to explicitly enable it
                groupByResource: true

                //// uncomment this line to group by day FIRST with resources underneath
                //groupByDateAndResource: true
            }
        },

        // uncomment this line to hide the all-day slot
        allDaySlot: false,
        resources: res,
        viewRender: function (element) {
            var moment = $('#full-calendar').fullCalendar('getDate');
            var date = moment.format('YYYY-MM-DD');
            $('td').removeClass('xdsoft_current');
            $('td[data-date="' + Number(date.split('-')[2]).toString() + '"][data-month="' + (Number(date.split('-')[1]) - 1).toString() + '"][data-year="' + Number(date.split('-')[0]).toString() + '"]').addClass('xdsoft_current');
            UpdateCalendar(date);
        },
        eventClick: function (calEvent, jsEvent, view) {
            if ($(jsEvent.originalEvent.target.parentElement) != null && $(jsEvent.originalEvent.target.parentElement).hasClass('close')) {// Delete Clicked
                var r = confirm("Are you sure you want to delete this " + calEvent.title + "?");
                if (r === true) {
                    DeleteEvent(calEvent.id, calEvent.title);
                }
            }
            else if($(jsEvent.originalEvent.target).hasClass('fa-play'))
            {
                window.open("/Appointment/Start?appointmentId=" + calEvent.id);
            }
            else {
                if (calEvent.title == "Leave" || calEvent.title == "Appointment") {
                    ShowAddModal(calEvent.id, calEvent.bookingDate, calEvent.resourceId, calEvent.title, true);
                }
            }
        },
        dayClick: function (date, jsEvent, view, resource) {
            //Available Appointment
            var clickedDateTime = moment(new Date(date._d).toISOString().replace('Z', '')).format("YYYY-MM-DD HH:mm:ss");
            if (resource) {
                ShowAddModal(null, clickedDateTime, resource.id, "Appointment", false);
            }
        },
        eventRender: function (e, element, view) {
            if (e.title == "Available") {
                return false;
            }
            element.find(".fc-event-title").remove();
            element.find(".fc-event-time").remove();
            $.ajax({
                url: "/Appointment/AppointmentDetails",
                data: JSON.parse(e.serealizedObject)

            })
                .then(function (content) {
                    // Set the tooltip content upon successful retrieval
                    element.html(content);
                }, function (xhr, status, error) {
                    // Upon failure... set the tooltip content to error
                    element.append('content.text', status + ': ' + error);
                });
            element.qtip({
                content: {
                    text: function (event, api) {
                        $.ajax({
                            url: "/Appointment/AppointmentToolTip",
                            data: JSON.parse(e.serealizedObject)

                        })
                            .then(function (content) {
                                // Set the tooltip content upon successful retrieval
                                api.set('content.text', content);
                            }, function (xhr, status, error) {
                                // Upon failure... set the tooltip content to error
                                api.set('content.text', status + ': ' + error);
                            });

                        return 'Loading...'; // Set some initial text
                    }
                },
                show: {
                    solo: true // Only show one tooltip at a time
                },
                style: "different-tip-color",
                position: {
                    my: "bottom center",
                    at: "top center",
                }
            });
        }
    });
});
function UpdateCalendar(Date) {
    $.ajax({
        url: "/Appointment/FetchCalendarData",
        data: { 'date': Date },
    })
        .then(function (events) {
            $("#full-calendar").fullCalendar('removeEvents');
            $("#full-calendar").fullCalendar('addEventSource', events);
        }, function (xhr, status, error) {
            var eve = [];
            $('#full-calendar').fullCalendar('updateEvent', eve);
        });
};
function DeleteEvent(id, type) {

    if (type == "Leave") {
        url = "/Leave/Delete";
    }
    else {
        url = "/Appointment/Delete";
    }
    $.ajax({
        url: url,
        data: { 'Id': id }
    }).done(function (result, textStatus, jqXHR) {
        if (result) {
            var moment = $('#full-calendar').fullCalendar('getDate');
            var date = moment.format('YYYY-MM-DD');
            UpdateCalendar(date);
        }
    });
};