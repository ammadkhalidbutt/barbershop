﻿$(function () {
    $("body").off("click", "#appointment-modal");
    $("body").on("click", "#appointment-modal", function (event) {
        ShowAddModal(null, null, null, "Appointment", false);

    });
    $("body").off("click", "#leave-modal");
    $("body").on("click", "#leave-modal", function (event) {
        ShowAddModal(null, null, null, "Leave", false);

    });
    $("body").off("click", "#appointment-button");
    $("body").on("click", "#appointment-button", function (event) {
        var type = "Appointment";
        if ($(this).hasClass('disabled')) {
            return;
        }
        if ($("#modal-appointment-panel").html().length > 0) {
            var disableOther = $("#leave-button").hasClass('disabled');
            ModalTypeChanged(type, disableOther);
            return;
        }
        ShowAddModal(null, null, null, type, false);

    });
    $("body").off("click", "#leave-button");
    $("body").on("click", "#leave-button", function (event) {
        var selectedStylist = null;
        var selectedTime = null;
        var type = "Leave";
        if ($(this).hasClass('disabled')) {
            return;
        }
        if ($("#modal-leave-panel").html().length > 0) {
            var disableOther = $("#appointment-button").hasClass('disabled');
            ModalTypeChanged(type, disableOther);
            return;
        }
        if ($("#modal-appointment-panel").html().length > 0) {// Select already selected stylist and time from appointment
            try {
                selectedStylist = $("#appointment-form #stylist-name").val();
                selectedTime = moment(new Date($("#appointment-form #BookingDate").val() + "T" + $("#appointment-form #start").val())).format("YYYY-MM-DD HH:mm:ss");
            }
            catch (err) {
                selectedStylist = null;
                selectedTime = null;
            }
        }
        ShowAddModal(null, selectedTime, selectedStylist, type, false);
    });
    $("body").off("click", "#submit-appointment");
    $("body").on("click", "#submit-appointment", function (event) {
        SubmitAppointmentForm().done(function (result) {
            if (result) {
                var moment = $('#full-calendar').fullCalendar('getDate');
                var date = moment.format('YYYY-MM-DD');
                $("#add-modal").modal("hide");
                UpdateCalendar(date);
            }
        });
    });
    $("body").off("click", "#submit-leave");
    $("body").on("click", "#submit-leave", function (event) {
        SubmitLeaveForm().done(function (result) {
            if (result) {
                var moment = $('#full-calendar').fullCalendar('getDate');
                var date = moment.format('YYYY-MM-DD');
                $("#add-modal").modal("hide");
                UpdateCalendar(date);
            }
        });
    });
});
function ShowAddModal(id, date, resourceId, type, disableOther) {
    if ($("#add-modal").is(":hidden")) {
        $("#modal-leave-panel").html("");
        $("#modal-appointment-panel").html("");
    }
    var url = "";
    if (date == "") {
        var moment = $('#full-calendar').fullCalendar('getDate');
        date = moment.format('YYYY-MM-DD');
    }
    if (type == "Leave") {
        url = "/Leave/CreatePartialModal";
    }
    else {
        url = "/Appointment/CreatePartialModal";
    }
    $.ajax({
        url: url,
        data: { 'Id': id, 'Date': date, 'StylistId': resourceId }
    }).done(function (data, textStatus, jqXHR) {
        ModalTypeChanged(type, disableOther);
        if (type == "Leave") {
            $("#add-modal .modal-body #modal-leave-panel").html(data);
            var $form = $("#add-modal .modal-body #modal-leave-panel").find('form');
        }
        else {
            $("#add-modal .modal-body #modal-appointment-panel").html(data);
            var $form = $("#add-modal .modal-body #modal-appointment-panel").find('form');
        }
        $form.removeData("validator");
        $form.removeData("unobtrusiveValidation");
        $.validator.unobtrusive.parse($form);
        if ($("#add-modal").is(":hidden")) {
            $("#add-modal").modal("show");
        }
    });
};
function ModalTypeChanged(type, disableOther) {
    // Initialize Every time to default state
    $("#modal-leave-panel").hide();
    $("#modal-appointment-panel").hide();
    $("#appointment-button").removeClass('disabled');
    $("#leave-button").removeClass('disabled');

    // Perform actiona according to parameters
    if (type == "Leave") {
        $(".radio-group label").removeClass('btn-appointment');
        $(".radio-group label").addClass('btn-leave');

        $("#appointment-button").addClass('not-active');
        if (disableOther) {
            $("#appointment-button").addClass('disabled');
        }
        $("#leave-button").removeClass('not-active');
        $("#modal-leave-panel").show();
    }
    else {
        $(".radio-group label").removeClass('btn-leave');
        $(".radio-group label").addClass('btn-appointment');
        $("#leave-button").addClass('not-active');
        $("#appointment-button").removeClass('not-active');
        if (disableOther) {
            $("#leave-button").addClass('disabled');
        }
        $("#modal-appointment-panel").show();
    }
}