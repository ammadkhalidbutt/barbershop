﻿using System.Web;
using System.Web.Optimization;

namespace BarberShop
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-3.4.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery.unobtrusive-ajax.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/PopperCore").Include(
                        "~/Scripts/popper.min.js",
                        "~/Scripts/bootstrap.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/QTip").Include(
                        "~/Scripts/jquery.qtip.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Slimscrollbar").Include(
                        "~/Scripts/dist/js/perfect-scrollbar.jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Wave").Include(
                        "~/Scripts/dist/js/waves.js"));

            bundles.Add(new ScriptBundle("~/bundles/Sidebarmenu").Include(
                        "~/Scripts/dist/js/sidebarmenu.js"));

            bundles.Add(new ScriptBundle("~/bundles/CustomJs").Include(
                        "~/Scripts/dist/js/custom.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Morris").Include(
                        "~/Scripts/raphael-min.js",
                        "~/Scripts/morris.min.js",
                        "~/Scripts/jquery.sparkline.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Toast").Include(
                        "~/Scripts/jquery.toast.js"));

            bundles.Add(new ScriptBundle("~/bundles/JqueryDateTimePicker").Include(
                        "~/Scripts/php-date-formatter.min.js",
                        "~/Scripts/jquery.mousewheel.js",
                        "~/Scripts/jquery.datetimepicker.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/foolProof").Include(
                       "~/Client Scripts/customfoolproof.validation.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Peity").Include(
                        "~/Scripts/jquery.peity.min.js",
                        "~/Scripts/jquery.peity.init.js",
                        "~/Scripts/dist/js/dashboard1.js"));

            bundles.Add(new ScriptBundle("~/bundles/sticky-kit").Include(
                        "~/Scripts/sticky-kit.min.js",
                        "~/Scripts/jquery.sparkline.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/magnific-popup").Include(
                        "~/Scripts/jquery.magnific-popup.min.js",
                        "~/Scripts/jquery.magnific-popup-init.js"));

            bundles.Add(new ScriptBundle("~/bundles/Select2").Include(
                        "~/Scripts/select2.min.js",
                        "~/Scripts/CustomScripts/Select2Methods.js"));

            bundles.Add(new ScriptBundle("~/bundles/DataTables").Include(
                        "~/Scripts/jquery.dataTables.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/TimeTo").Include(
                        "~/Scripts/jquery.time-to.min.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/Moment").Include(
                      "~/Scripts/moment.js"
                      ));
            bundles.Add(new ScriptBundle("~/bundles/FullCalendar").Include(
                        "~/Scripts/jquery-ui.min.js",
                        "~/Scripts/moment.js",
                        "~/Scripts/fullcalendar.min.js"
                        //"~/Content/assets/node_modules/calendar/dist/cal-init.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/JqueryUi").Include(
                        "~/Scripts/jquery-ui.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/FullCalendarScheduler").Include(
                        "~/Scripts/moment.min.js",
                        "~/Scripts/fullcalendar.min.js",
                        "~/Scripts/scheduler.min.js"
                        //"~/Content/assets/node_modules/calendar/dist/cal-init.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/BootstrapDateTimePicker").Include(
                        "~/Scripts/bootstrap-datetimepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqs").Include(
                        "~/Scripts/jquery-ui-1.12.1.min.js",
                        "~/Scripts/jquery.schedule.js"));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                       "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap-bundle").Include(
                      //"~/Scripts/bootstrap.js",
                      "~/Scripts/bootstrap.bundle.min.js"));

            bundles.Add(new StyleBundle("~/Content/favicon").Include(
                       "~/Content/all.css"));

            bundles.Add(new StyleBundle("~/Content/QTip").Include(
                       "~/Content/jquery.qtip.min.css"));

            bundles.Add(new StyleBundle("~/Content/jqs").Include(
                       "~/Content/jquery-ui.min.css",
                       "~/Content/jquery.schedule.min.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/syoTimer.css"));

            bundles.Add(new StyleBundle("~/Content/bs").Include(
                      "~/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/CustomThemeCss").Include(
                      "~/Content/style.min.css",
                      "~/Content/style2.css",
                      "~/Content/custom-style.css"));

            bundles.Add(new StyleBundle("~/Content/Select2DropDown").Include(
                      "~/Content/select2.min.css"));
            
            bundles.Add(new StyleBundle("~/Content/JqueryDateTimePicker").Include(
                      "~/Content/jquery.datetimepicker.min.css"));

            bundles.Add(new StyleBundle("~/Content/CustomThemeMainCss").Include(
                      "~/Content/morris.css",
                      "~/Content/jquery.toast.css",
                      "~/Content/dashboard1.css"));

            bundles.Add(new StyleBundle("~/Content/TimeTo").Include(
                      "~/Content/timeTo.css"));

            bundles.Add(new StyleBundle("~/Content/Popup").Include(
                      "~/Content/magnific-popup.css",
                      "~/Content/user-card.css"));

            bundles.Add(new StyleBundle("~/Content/DataTable").Include(
                        "~/Content/jquery.dataTables.min.css",
                        "~/Content/buttons.dataTables.css"));

            bundles.Add(new StyleBundle("~/Content/FullCalendar").Include(
                        "~/Content/fullcalendar.min.css"));

            bundles.Add(new StyleBundle("~/Content/FLC").Include(
                        "~/Content/fullcalendar.min.css"
                       /* "~/node_modules/fullcalendar-scheduler/dist/scheduler.min.css"*/));

            bundles.Add(new StyleBundle("~/Content/BootstrapDateTimePicker").Include(
                        "~/Content/bootstrap-datetimepicker.min.css"));
        }
    }    
}
