﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enums
{
    public enum Gender
    {
        [Display(Name = "Male")]
        Male = 1,
        [Display(Name = "Female")]
        Female = 2,
        [Display(Name = "Unknown")]
        Unknown = 3
    }

    public enum AppointmentStatus
    {
        [Display(Name = "Approved")]
        Approved = 1,
        [Display(Name = "Cancelled")]
        Cancelled = 2,
        [Display(Name = "Processing")]
        Processing = 3,
        [Display(Name = "Completed")]
        Completed = 4
    }

    public enum AppointmentTreatmentStatus
    {
        [Display(Name = "In Progress")]
        Started = 1,
        [Display(Name = "Finished")]
        Ended = 2,
        [Display(Name = "Paused")]
        Paused = 3,
        [Display(Name = "Pending")]
        Pending = 4,
        [Display(Name = "Cancelled")]
        Cancelled = 5
    }

    public enum InvoiceStatus
    {
        [Display(Name = "Pending")]
        Pending = 1,
        [Display(Name = "Paid")]
        Paid = 2
    }

    public enum WeekDays
    {
        Sunday = 1,
        Monday = 2,
        Tuesday = 3,
        Wednesday = 4,
        Thursday = 5,
        Friday = 6,
        Saturday = 7,

    }
    public enum LeaveType
    {
        Sick = 1,
        Annual = 2,
        FamilyResponsibility = 3
    }

    public enum ApprovalStatus
    {
        Approved = 1,
        Cancelled = 2,
        Processing = 3
    }

    public enum SlotStatus
    {
        Appointment = 1,
        Leave = 2,
        Schedule = 3,
        Available = 4,
        UnScheduled = 5,
    }

    public enum Recursion
    {
        Never = 1,
        Daily = 2,
        Weekly = 3,
        EveryOtherWeek = 4
    }

    public enum DiscountEntityTypeCatalog
    {
        Customer = 1,
        Item = 2,
        Category = 3,
        All = 4
    }
}
