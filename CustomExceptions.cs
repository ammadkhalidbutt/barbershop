﻿using System;
namespace Enums
{
    [Serializable]
    public class KnownException : Exception
    {
        public KnownException(string message)
        {
            this.Message = message;

        }
    }
}
