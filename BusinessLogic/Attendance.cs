﻿using EntityProvider;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public string ScanClients(string id, string action)
        {
            if (!string.IsNullOrEmpty(id))
                return new DataAccess().ScanClients(id, action);
            else
                return "";
        }

        public List<UserAttendanceViewModel> GetPresentUsers(string role, string action)
        {
            return new DataAccess().GetPresentUsers(role, action).OrderByDescending(x => x.AttendanceDate).ToList();
        }

        public List<UserAttendanceViewModel> GetScannedClient(string role, string action)
        {
            return new DataAccess().GetPresentUsers(role, action).OrderByDescending(x => x.AttendanceDate).ToList();
        }

        public List<UserAttendanceViewModel> GetClientsForStylistDashboard(string userName)
        {
            return new DataAccess().GetPresentUsers("Client", "Checked In").Where(m => string.IsNullOrEmpty(userName) || m.Name.ToLower().Contains(userName.ToLower())).OrderByDescending(x => x.AttendanceDate).ToList();
        }

        public List<UserAttendanceViewModel> GetStylistsForStylistDashboard(AccountSearchViewModel search)
        {
            search.RoleId = new Logic().GetRoleId<RoleViewModel>(new RoleSearchViewModel { Name = "Stylist" }).ResultList.First().Id;

            return new DataAccess().GetUsers<RegisterViewModel>(search).ResultList
                .Select(m => new UserAttendanceViewModel
                {
                    DOB = m.DOB,
                    Email = m.Email,
                    Gender = m.Gender,
                    Id = m.Id,
                    Name = m.Name,
                    PhoneNumber = m.PhoneNumber,
                    ReferenceId = m.ReferenceId,
                    Role = m.Role
                }).ToList();
        }
    }
}
