﻿using EntityProvider;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public SearchResultViewModel<T> GetTreatmentReport<T>(TreatmentReportSearchViewModel search)
        {
            return new DataAccess().GetTreatmentReport<T>(search);
        }

        public SearchResultViewModel<T> GetInvoiceReport<T>(InvoiceReportSearchViewModel search)
        {
            return new DataAccess().GetInvoiceReport<T>(search);
        }
    
        public SearchResultViewModel<T> GetClientPointReport<T>(ClientPointReportSearchViewModel search)
        {
            return new DataAccess().GetClientPointReport<T>(search);
        }

        public SearchResultViewModel<T> GetStylistCommissionReport<T>(StylistCommissionReportSearchViewModel search)
        {
            return new DataAccess().GetStylistCommissionReport<T>(search);
        }

    }
}
