﻿using EntityProvider;
using Models;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public PointViewModel GetPoint()
        {
            return new DataAccess().GetPoint();
        }

        public Guid Create(PointViewModel model)
        {
            model.Id = Guid.NewGuid();
            model.LastUpdatedOn = DateTime.Now;
            return new DataAccess().Create(model);
        }

        public Guid Edit(PointViewModel model)
        {
            model.LastUpdatedOn = DateTime.Now;
            return new DataAccess().Edit(model);
        }
    }
}
