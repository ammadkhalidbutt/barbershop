﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityProvider;
using Enums;
using Models;
using Models.Identity;
using Models.ViewModels;

namespace BusinessLogic
{
    public partial class Logic
    {
        public Guid Payment(InvoiceRecordViewModel invoice)
        {

            try
            {

                var invoiceCheck = CreateInvoice(invoice);

                AddInvoiceItems(invoice);

                return invoiceCheck;
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        #region Helping Methods

        /// <summary>
        /// Creates an invoice.
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        private Guid CreateInvoice(InvoiceRecordViewModel invoice)
        {
            try
            {
                invoice.InvoiceNumber = invoice.IsPaidByPin ? "P-" : "C-";
                invoice.Client = new ApplicationUser { Id = invoice.InvoiceItems.First().Appointment.Client.Id };
                invoice.Status = InvoiceStatus.Paid;
                invoice.InvoiceItems.ForEach(m => m.Appointment.AppointmentTreatmentStatus = AppointmentTreatmentStatus.Ended);
                invoice.InvoiceItems.ForEach(m => m.Appointment.Status = AppointmentStatus.Completed);
                invoice.TotalPayment = invoice.GrandTotal - (invoice.InvoiceItems.FirstOrDefault().Appointment.PointWorth * invoice.PointsPurchased);
                var invoiceCheck = Create(invoice);
                SettlePoints(invoice);
                return invoiceCheck;
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }

        /// <summary>
        /// To settle points, if paid by points then no points added else points added, as well as purchased.
        /// </summary>
        /// <param name="invoice"></param>
        private void SettlePoints(InvoiceRecordViewModel invoice)
        {
            var clientPointsPurchased = invoice.PointsPurchased > 0 ?
                Create(new ClientPointViewModel 
                {
                    Purchased = true,
                    ClientId = invoice.InvoiceItems.FirstOrDefault().Appointment.Client.Id,
                    Date = DateTime.Now, IsDeleted = false, 
                    Points = invoice.PointsPurchased,
                    InvoiceRecord = new InvoiceRecord { Id = invoice.Id } 
                }) 
                :
                new Guid();
            var clientPointsSubtraction = invoice.PointsUsed > 0 ? 
                Create(new ClientPointViewModel 
                {
                    AppointmentId = invoice.InvoiceItems.First().Appointment.Id.Value,
                    ClientId = invoice.InvoiceItems.First().Appointment.Client.Id,
                    Date = DateTime.Now,
                    IsDeleted = false, Points = -(int)(invoice.PointsUsed), 
                    InvoiceRecord = new InvoiceRecord { Id = invoice.Id } 
                })
                :
                new Guid();
        }

        /// <summary>
        /// To add treatments booked and completed by normal appointment flow or dynamically added on invoice view.
        /// </summary>
        /// <param name="invoiceItems"></param>
        private void AddInvoiceItems(InvoiceRecordViewModel invoice)
        {
            foreach (var inv in invoice.InvoiceItems)
            {
                inv.Appointment.Id = inv.Appointment.Treatment.IsDynamicallyAdded || inv.Appointment.Product.IsDynamicallyAdded ? null : inv.Appointment.Id;

                if (inv.Appointment.Treatment.Id != new Guid())
                {
                    var invoiceItem = Create(new InvoiceItemViewModel
                    {
                        InvoiceRecord = new InvoiceRecord { Id = invoice.Id },
                        PersonAddingRecord = new ApplicationUser { Id = LoggedInUserId },
                        Appointment = inv.Appointment,
                        ItemId = inv.Appointment.Treatment.Id,
                        DiscountedPrice = inv.Appointment.Discount,
                        ItemPrice = (float)inv.Appointment.Treatment.Price,
                        PersonUpdatingRecord = new ApplicationUser { Id = LoggedInUserId },
                        UpdatedOn = DateTime.Now
                    });
                    if (!inv.Appointment.Treatment.IsDynamicallyAdded)
                    {
                        var appointment = Edit(inv.Appointment);
                        var approval = Edit(new AppointmentApprovalViewModel { Appointment = new Appointment { Id = inv.Appointment.Id.Value, Status = inv.Appointment.AppointmentTreatmentStatus }, Approver = new ApplicationUser { Id = inv.Appointment.Approver.Id }, Status = AppointmentStatus.Completed });
                    }
                    var clientPointsAddition = invoice.PointsUsed <= 0 ? 
                        Create(new ClientPointViewModel { ClientId = inv.Appointment.Client.Id, Date = DateTime.Now, IsDeleted = false, Points = inv.Appointment.Treatment.Points, InvoiceRecord = new InvoiceRecord { Id = invoice.Id } }) 
                        :
                        new Guid();
                }
                else if(inv.Appointment.Product.Id != new Guid())
                {
                    AddInvoiceProducts(inv, invoice.Id);
                }
            }
        }

        private void AddInvoiceProducts(InvoiceItemViewModel invoiceItem, Guid invoiceId)
        {
            var dividedDiscount = invoiceItem.Appointment.Discount / invoiceItem.Appointment.Product.Quantity;

            for(var i = 0; i < invoiceItem.Appointment.Product.Quantity; i++)
            {
                var check = Create(new InvoiceItemViewModel
                {
                    InvoiceRecord = new InvoiceRecord { Id = invoiceId },
                    PersonAddingRecord = new ApplicationUser { Id = LoggedInUserId },
                    Appointment = invoiceItem.Appointment,
                    ItemId = invoiceItem.Appointment.Product.Id,
                    DiscountedPrice = dividedDiscount,
                    ItemPrice = (float)invoiceItem.Appointment.Product.Price,
                    PersonUpdatingRecord = new ApplicationUser { Id = LoggedInUserId },
                    UpdatedOn = DateTime.Now
                });
            }
        }

        #endregion
    }
}
