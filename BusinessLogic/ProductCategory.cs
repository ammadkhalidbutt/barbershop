﻿using EntityProvider;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public ProductCategoryViewModel GetProductCategory(Guid id)
        {
            return new DataAccess().GetProductCategory(id);
        }

        public SearchResultViewModel<T> GetProductCategories<T>(ProductCategorySearchViewModel search)
        {
            return new DataAccess().GetProductCategories<T>(search);
        }

        public Guid Create(ProductCategoryViewModel model)
        {
            model.Id = Guid.NewGuid();
            model.CreatedBy = LoggedInUserId;
            model.UpdatedBy = LoggedInUserId;
            model.CreatedOn = DateTime.Now;
            model.UpdatedOn = DateTime.Now;
            return new DataAccess().Create(model);
        }

        public Guid Edit(ProductCategoryViewModel model)
        {
            model.UpdatedBy = LoggedInUserId;
            model.UpdatedOn = DateTime.Now;
            return new DataAccess().Edit(model);
        }

        public bool Delete(ProductCategoryViewModel model)
        {
            model.IsDeleted = true;
            model.UpdatedBy = LoggedInUserId;
            model.UpdatedOn = DateTime.Now;
            var result = new DataAccess().Edit(model);
            return result != new Guid() ? true : false;
        }

        public bool Remove(ProductCategoryViewModel model)
        {
            return new DataAccess().Remove(model);
        }
    }
}
