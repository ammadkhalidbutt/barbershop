﻿using EntityProvider;
using Models.ViewModels;
using System;

namespace BusinessLogic
{
    public partial class Logic
    {
        public SearchResultViewModel<T> GetTreatmentCategories<T>(TreatmentCategorySearchViewModel search)
        {
            return new DataAccess().GetTreatmentCategories<T>(search);
        }

        public Guid Create(TreatmentCategoryViewModel model)
        {
            model.Id = Guid.NewGuid();
            model.CreatedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId();
            model.CreatedOn = DateTime.Now;
            return new DataAccess().Create(model);
        }

        public Guid Edit(TreatmentCategoryViewModel model)
        {
            return new DataAccess().Edit(model);
        }

        public Guid Delete(TreatmentCategoryViewModel model)
        {
            return new DataAccess().Delete(model);
        }
    }
}
