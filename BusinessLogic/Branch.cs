﻿using EntityProvider;
using Models;
using Models.ViewModels;
using System;
using Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public Guid Create(BranchViewModel model)
        {
            model.Id = Guid.NewGuid();
            model.Active = true;
            model.Address.Id = Guid.NewGuid();
            return new DataAccess().Create(model);
        }

        public Guid Edit(BranchViewModel model)
        {
            return new DataAccess().Edit(model);
        }

        public SearchResultViewModel<T> GetBranches<T>(BranchSearchViewModel search)
        {
            return new DataAccess().GetBranches<T>(search);
        }
    }
}
