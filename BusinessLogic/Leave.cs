﻿using EntityProvider;
using Enums;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public SearchResultViewModel<T> GetLeaves<T>(LeaveSearchViewModel search)
        {
            //List<LeaveViewModel> leaves = new DataAccess().GetLeaves<LeaveViewModel>(search).ResultList.ToList();
            //leaves.FirstOrDefault()
            //switch (leaves.FirstOrDefault().)
            //{
            //    case
            //}
            return new DataAccess().GetLeaves<T>(search);
        }

        public Guid Create(LeaveViewModel model)
        {
            model.Type = Enums.LeaveType.Annual;
            model.Status = Enums.ApprovalStatus.Approved;
            var days = string.IsNullOrEmpty(model.DaysArray)? 
                new string[0] : 
                model.DaysArray.Split(',');
            var result = new Guid();
            var referenceId = Guid.NewGuid();
            model.StartingDate = model.StartingDate.Date.Add(model.DailyStartTime);
            model.EndingDate = model.EndingDate.Date.Add(model.DailyEndTime);
            model.CreatedOn = model.Action == "Edit" ? model.CreatedOn : DateTime.Now;
            model.CreatedBy = model.Action == "Edit" ? model.CreatedBy : new Helpers.LoggedInUserInfo().GetLoggedInUserId();
            model.ReferenceId = referenceId;
            if (days.Count() <= 1)
            {
                model.Id = Guid.NewGuid();
                model.ReferenceId = referenceId;
                result = new DataAccess().Create(model);
            }
            else
            {

                foreach (var day in days)
                {
                    model.Id = Guid.NewGuid();
                    model.RecursionParameter = day;
                    result = new DataAccess().Create(model);
                }
            }
            return result;
        }

        public Guid Edit(LeaveViewModel model)
        {
            var deletePrevious = new DataAccess().Delete(model);
            return Create(model);
        }

        public Guid Delete(LeaveViewModel model)
        {
            return new DataAccess().Delete(model);
        }

    }
}
