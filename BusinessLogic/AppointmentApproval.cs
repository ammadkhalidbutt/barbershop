﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityProvider;
using Models;
using Models.ViewModels;

namespace BusinessLogic
{
    public partial class Logic
    {
        public SearchResultViewModel<T> GetApprovals<T>(AppointmentApprovalSearchViewModel search)
        {
            return new DataAccess().GetApprovals<T>(search);
        }

        public Guid Create(AppointmentApprovalViewModel model)
        {
            model.Approval.ApprovalDateTime = DateTime.Now;
            model.Approval.LastUpdatedOn = DateTime.Now;
            model.Approval.Active = true;
            model.Approval.AppointmentId = model.Appointment.Id;
            return new DataAccess().Create(model);
        }
        
        public Guid Edit(AppointmentApprovalViewModel model)
        {
            model.Approval.LastUpdatedOn = DateTime.Now;
            return new DataAccess().Edit(model);
        }
        
        public Guid Delete(AppointmentApprovalViewModel model)
        {
            return new DataAccess().Delete(model);
        }

        
    }
}
