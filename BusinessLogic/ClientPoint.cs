﻿using EntityProvider;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public SearchResultViewModel<T> GetClientPoints<T>(ClientPointSearchViewModel search)
        {
            return new DataAccess().GetClientPoints<T>(search);
        }

        public Guid Create(ClientPointViewModel model)
        {
            model.Id = Guid.NewGuid();
            model.Date = DateTime.Now;
            model.LastUpdatedOn = DateTime.Now;
            model.IsDeleted = false;
            return new DataAccess().Create(model);
        }

        public Guid Edit(ClientPointViewModel model)
        {
            model.LastUpdatedOn = DateTime.Now;
            return new DataAccess().Edit(model);
        }

        public Guid Delete(ClientPointViewModel model)
        {
            model.LastUpdatedOn = DateTime.Now;
            return new DataAccess().Delete(model);
        }
    }
}
