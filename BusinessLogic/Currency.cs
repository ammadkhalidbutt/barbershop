﻿using EntityProvider;
using Models;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public SearchResultViewModel<T> GetCurrency<T>(CurrencySearchViewModel model)
        {
            return new DataAccess().GetCurrency<T>(model);
        }

        public Guid Create(CurrencyViewModel model)
        {
            model.Id = Guid.NewGuid();
            return new DataAccess().Create(model);
        }

        public Guid Edit(CurrencyViewModel model)
        {
            return new DataAccess().Edit(model);
        }

    }
}
