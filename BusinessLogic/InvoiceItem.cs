﻿using EntityProvider;
using Models;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public SearchResultViewModel<T> GetInvoices<T>(InvoiceItemSearchViewModel search)
        {
            return new DataAccess().GetInvoices<T>(search);
        }

        public Guid Create(InvoiceItemViewModel model)
        {
            model.Id = Guid.NewGuid();
            model.DateTime = DateTime.Now;
            return new DataAccess().Create(model);            
        }

        public Guid Edit(InvoiceItemViewModel model)
        {
            model.UpdatedOn = DateTime.Now;
            return new DataAccess().Edit(model);
        }

        public Guid Delete(InvoiceItemViewModel model)
        {
            return new DataAccess().Delete(model);
        }
    }
}
