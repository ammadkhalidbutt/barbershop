﻿using EntityProvider;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public SearchResultViewModel<T> GetInvoiceRecords<T>(InvoiceRecordSearchViewModel search)
        {
            return new DataAccess().GetInvoiceRecords<T>(search);
        }

        public Guid Create(InvoiceRecordViewModel model)
        {
            model.Id = Guid.NewGuid();
            model.DateTime = DateTime.Now;
            model.LastUpdatedOn = DateTime.Now;
            return new DataAccess().Create(model);
        }

        public Guid Edit(InvoiceRecordViewModel model)
        {
            model.LastUpdatedOn = DateTime.Now;
            return new DataAccess().Edit(model);
        }

        public Guid Delete(InvoiceRecordViewModel model)
        {
            model.LastUpdatedOn = DateTime.Now;
            return new DataAccess().Delete(model);
        }
    }
}
