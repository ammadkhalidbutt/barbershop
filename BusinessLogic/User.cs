﻿using EntityProvider;
using Microsoft.AspNet.Identity.EntityFramework;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public LoggedInUserInformation GetUserDetails(string id)
        {
            return new DataAccess().GetUserDetails(id);
        }

        public SearchResultViewModel<T> GetUsers<T>(AccountSearchViewModel model)
        {
            return new DataAccess().GetUsers<T>(model);
        }

        public string Edit(RegisterViewModel model)
        {
            model.Gender = model.Gender == 0 ? Enums.Gender.Unknown : model.Gender;
            return new DataAccess().Edit(model);
        }

        public string Delete(string id)
        {
            return new DataAccess().Delete(id);
        }

        public SearchResultViewModel<T> GetRoleId<T>(RoleSearchViewModel search)
        {
            return new DataAccess().GetRoles<T>(search);
        }

        public RegisterViewModel GetUserPointsForApi(AccountSearchViewModel search)
        {
            var user = new DataAccess().GetUsers<RegisterViewModel>(search).ResultList.FirstOrDefault();
            if (user != null)
            {
                ClientPointReportViewModel ClientPoints = new DataAccess().GetClientPointReport<ClientPointReportViewModel>(new ClientPointReportSearchViewModel { ClientId = search.Id }).ResultList.FirstOrDefault();
                if (ClientPoints != null)
                    user.Points = ClientPoints.Points;
                else
                    user.Points = 0;
            }
            return user;
        }

        public bool AddClientsInBulk(List<ApplicationUser> clientList, List<IdentityUserRole> roleList, List<Address> addressList)
        {
            return new DataAccess().AddClientsInBulk(clientList, roleList, addressList);
        }
    }
}
