﻿using EntityProvider;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Web;
using Microsoft.AspNet.Identity;

namespace BusinessLogic
{
    public partial class Logic
    {
        public TaxViewModel GetTax(Guid id)
        {
            return new DataAccess().GetTax(id);
        }

        public Guid Create(TaxViewModel model)
        {
            model.Id = Guid.NewGuid();
            model.CreatedBy = LoggedInUserId;
            model.CreatedOn = DateTime.Now;
            model.UpdatedBy = LoggedInUserId;
            model.UpdatedOn = DateTime.Now;
            return new DataAccess().Create(model);
        }

        public Guid Edit(TaxViewModel model)
        {
            model.UpdatedBy = LoggedInUserId;
            model.UpdatedOn = DateTime.Now;
            return new DataAccess().Edit(model);
        }

        public List<TaxViewModel> GetTaxes(Guid id)
        {
            return new DataAccess().GetTaxes(id);
        }

        public bool Delete(TaxViewModel model)
        {
            model.IsDeleted = true;
            model.UpdatedOn = DateTime.Now;
            model.UpdatedBy = LoggedInUserId;
            return (new DataAccess().Edit(model) != new Guid()) ? true : false;
        }

        public bool Remove(TaxViewModel model)
        {
            return new DataAccess().Remove(model);
        }
    }
}
