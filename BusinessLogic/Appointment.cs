﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityProvider;
using Enums;
using Models;
using Models.Identity;
using Models.ViewModels;

namespace BusinessLogic
{
    public partial class Logic
    {
        public SearchResultViewModel<T> GetAppointments<T>(AppointmentSearchViewModel search)
        {
            return new DataAccess().GetAppointments<T>(search);
        }

        public List<AppointmentViewModel> GetAppointmentCount(AppointmentSearchViewModel search)
        {
            return new DataAccess().GetAppointmentCount(search);
        }

        public Guid Create(AppointmentViewModel model)
        {
            var branchId = new Logic().GetBranches<BranchViewModel>(new BranchSearchViewModel()).ResultList.First().Id;
            var applicationId = new Logic().GetApplications<ApplicationViewModel>(new ApplicationSearchViewModel()).ResultList.First().Id;
            var treatmentDuration = new Logic().GetTreatments<TreatmentViewModel>(new TreatmentSearchViewModel { Id = model.TreatmentId }).ResultList.First().ApproximateTime;
            model.AppointmentStartingTime = model.BookingDate.Date.Add(model.AppointmentStartingTime != new DateTime() ? model.AppointmentStartingTime.TimeOfDay : DateTime.Now.TimeOfDay);
            model.AppointmentEndingTime = model.AppointmentStartingTime.AddMinutes(treatmentDuration);
            model.ActualEndingTime = DateTime.Now;
            model.ActualStartingTime = DateTime.Now;
            model.LastUpdatedOn = DateTime.Now;
            model.Active = true;
            model.Status = AppointmentStatus.Approved; // to bypass approval
            model.ApplicationId = applicationId;
            model.Branch = new Branch { Id = branchId };
            model.Id = Guid.NewGuid();
            model.AppointmentTreatmentStatus = model.AppointmentTreatmentStatus == 0? AppointmentTreatmentStatus.Pending: model.AppointmentTreatmentStatus;
            model.TotalTime = (Int32)((model.AppointmentEndingTime.Subtract(model.AppointmentStartingTime)).TotalMinutes);
            model.IsDeleted = false;
            return new DataAccess().Create(model);
        }

        public Guid Edit(AppointmentViewModel model)
        {
            model.AppointmentStartingTime = model.AppointmentStartingTime!=new DateTime() ? model.BookingDate.Add(model.AppointmentStartingTime.TimeOfDay) : new DateTime();
            model.AppointmentEndingTime = model.AppointmentStartingTime != new DateTime() ? model.AppointmentStartingTime.AddMinutes(model.Treatment.ApproximateTime) : new DateTime();
            model.LastUpdatedOn = DateTime.Now;
            return new DataAccess().Edit(model);
        }

        public Guid Delete(AppointmentViewModel model)
        {
            model.LastUpdatedOn = DateTime.Now;
            return new DataAccess().Delete(model);
        }

        public AppointmentTimeSlotsViewModel GetFreeSlots(AppointmentSlotSearchViewModel model)
        {
            return new DataAccess().GetFreeSlots(model);
        }
        public List<FullCalendarEvents> GetFreeSlotsForAllStylist(AppointmentSlotSearchViewModel model)
        {
            return new DataAccess().GetFreeSlotsForAllStylist(model);
        }

        public List<InvoiceRecordViewModel> GetUnpaidAppointments()
        {
            return new DataAccess().GetUnpaidAppointments();
        }

        public string CheckTreatments(AppointmentSearchViewModel model)
        {
            model.BookingDate = DateTime.Now;
            var stylistAppointments = GetAppointments<AppointmentViewModel>(new AppointmentSearchViewModel { StylistId = model.StylistId, BookingDate = DateTime.Now.Date}).ResultList.Where(m=>m.AppointmentTreatmentStatus == AppointmentTreatmentStatus.Started).ToList();
            if (stylistAppointments.Count() > 0)
            {
                return string.Format("Stylist is busy doing {0} with {1} at the moment.", stylistAppointments.First().Treatment.Name, stylistAppointments.First().Client.Name);
            }
            else
            {
                var clientAppointments = GetAppointments<AppointmentViewModel>(new AppointmentSearchViewModel { ClientId = model.ClientId, BookingDate = DateTime.Now.Date, TreatmentId = model.TreatmentId }).ResultList.Where(m => !(m.AppointmentTreatmentStatus == AppointmentTreatmentStatus.Ended || m.Status == AppointmentStatus.Cancelled || m.Status == AppointmentStatus.Completed)).ToList();
                if (clientAppointments.Count() > 0)
                {
                    if(clientAppointments.First().AppointmentTreatmentStatus == AppointmentTreatmentStatus.Pending)
                    {
                        return string.Format("{1} has already booked this treatment with {0}", clientAppointments.First().Stylist.Name, clientAppointments.First().Client.Name);
                    }
                    else
                    {
                        return string.Format("{2} is already having this treatment from {0} and is currently {1}", clientAppointments.First().Stylist.Name, clientAppointments.First().AppointmentTreatmentStatus.ToString(), clientAppointments.First().Client.Name);
                    }
                }
            }
            return "true";

        }

        public List<AppointmentHistoryViewModel> GetAppointmentHistory(AppointmentSearchViewModel search)
        {
            return new DataAccess().GetAppointmentHistory(search);
        }
    }

}
