﻿using EntityProvider;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public SearchResultViewModel<T> GetStylistSchedule<T>(StylistScheduleSearchViewModel search)
        {
            return new DataAccess().GetStylistSchedule<T>(search);
        }

        public Guid Create(StylistScheduleViewModel model)
        {
            model.Id = Guid.NewGuid();
            return new DataAccess().Create(model);
        }

        public Guid Edit(StylistScheduleViewModel model)
        {
            return new DataAccess().Edit(model);
        }

        public Guid Delete(StylistScheduleViewModel model)
        {
            return new DataAccess().Delete(model);
        }
    }
}
