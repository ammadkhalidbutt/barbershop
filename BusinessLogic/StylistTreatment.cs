﻿using EntityProvider;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public SearchResultViewModel<T> Read<T>(StylistTreatmentSearchViewModel search)
        {
            return new DataAccess().Read<T>(search);
        }

        public Guid Create(StylistTreatmentViewModel model)
        {
            model.Date = DateTime.Now;
            model.Active = true;
            model.StylistId = model.SelectedSytlists != null && model.SelectedSytlists.Count() > 0 ? model.SelectedSytlists.FirstOrDefault(): model.StylistId;
            return new DataAccess().Create(model);
        }

        public Guid Edit(StylistTreatmentViewModel model)
        {
            return new DataAccess().Edit(model);
        }

        public Guid Delete(StylistTreatmentViewModel model)
        {
            return new DataAccess().Delete(model);
        }


    }
}
