﻿using EntityProvider;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public ProductViewModel GetProduct(Guid id)
        {
            return new DataAccess().GetProduct(id);
        }

        public SearchResultViewModel<T> GetProducts<T>(ProductSearchViewModel search)
        {
            return new DataAccess().GetProducts<T>(search);
        }

        public Guid Create(ProductViewModel model)
        {
            model.Id = Guid.NewGuid();
            model.AddedBy = LoggedInUserId;
            model.Date = DateTime.Now;
            model.UpdatedBy = LoggedInUserId;
            model.UpdatedOn = DateTime.Now;
            model.LastUpdatedOn = DateTime.Now;
            return new DataAccess().Create(model);
        }

        public Guid Edit(ProductViewModel model)
        {
            model.UpdatedBy = LoggedInUserId;
            model.UpdatedOn = DateTime.Now;
            return new DataAccess().Edit(model);
        }
        
        public bool Delete(ProductViewModel model)
        {
            model.UpdatedBy = LoggedInUserId;
            model.UpdatedOn = DateTime.Now;
            model.IsDeleted = true;
            return new DataAccess().Edit(model) != new Guid() ? true : false;
        }

        public bool Remove(ProductViewModel model)
        {
            return new DataAccess().Remove(model);
        }
    }
}
