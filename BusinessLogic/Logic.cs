﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public string LoggedInUserId { get; set; }

        public Logic()
        {

        }

        public Logic(string loggedInUserId)
        {
            LoggedInUserId = loggedInUserId;
        }
    }
}
