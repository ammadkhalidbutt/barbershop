﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityProvider;
using Models;
using Models.ViewModels;

namespace BusinessLogic
{
    public partial class Logic
    {
        public SearchResultViewModel<T> GetTreatments<T>(TreatmentSearchViewModel search)
        {
            return new DataAccess().GetTreatments<T>(search);
        }

        public TreatmentViewModel GetTreatment(Guid id)
        {
            return new DataAccess().GetTreatment(id);
        }

        public Guid Create(TreatmentViewModel model)
        {
            model.Id = Guid.NewGuid();
            model.Active = true;
            model.Date = DateTime.Now;
            model.LastUpdatedOn = DateTime.Now;
            return new DataAccess().Create(model);
        }

        public Guid Edit(TreatmentViewModel model)
        {
            model.LastUpdatedOn = DateTime.Now;
            return new DataAccess().Edit(model);
        }

        public Guid Delete(TreatmentViewModel model)
        {
            return new DataAccess().Delete(model);
        }
    }
}
