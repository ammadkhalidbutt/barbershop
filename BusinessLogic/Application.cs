﻿using EntityProvider;
using Helpers;
using Models;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public Guid Create(ApplicationViewModel model)
        {
            model.Active = true;
            model.Id = model.Id == new Guid()? Guid.NewGuid() : model.Id;
            return new DataAccess().Create(model);
        }
        
        public SearchResultViewModel<T> GetApplications<T>(ApplicationSearchViewModel search)
        {
            return new DataAccess().GetApplications<T>(search);
        }
    }
}
