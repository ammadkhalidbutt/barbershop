﻿using EntityProvider;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic
    {
        public int GetTreatmentsCount(TreatmentSearchViewModel search)
        {
            return new DataAccess().GetTreatmentsCount(search);
        }
        public int GetUsersCount(AccountSearchViewModel search)
        {
            return new DataAccess().GetUsersCount(search);
        }
        public double GetStylistCommissions(StylistCommissionSearchViewModel search)
        {
            return new DataAccess().GetStylistCommissions(search);
        }
    }
}
