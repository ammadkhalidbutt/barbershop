﻿using EntityProvider;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Logic 
    {
        public SearchResultViewModel<T> GetAddress<T>(AddressSearchViewModel search)
        {
            return new DataAccess().GetAddress<T>(search);
        }
    }
}
