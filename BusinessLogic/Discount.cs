﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityProvider;
using Models;
using Models.ViewModels;

namespace BusinessLogic
{
    public partial class Logic
    {
        public SearchResultViewModel<DiscountViewModel> GetDiscounts(DiscountSearchViewModel search)
        {
            return new DataAccess().GetDiscounts(search);
        }

        public DiscountViewModel GetDiscount(Guid id)
        {
            return new DataAccess().GetDiscount(id);
        }

        public Guid Create(DiscountViewModel model)
        {
            var promoCodeChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            model.Id = Guid.NewGuid();
            model.IsActive = true;
            model.PromoCode = new string(
                Enumerable.Repeat(promoCodeChars, 8)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return new DataAccess().Create(model);
        }

        public Guid Edit(DiscountViewModel model)
        {
            return new DataAccess().Edit(model);
        }

        public Guid Delete(DiscountViewModel model)
        {
            return new DataAccess().Delete(model);
        }
        public bool Approve(DiscountViewModel model)
        {
            return new DataAccess().Approve(model);
        }
        public Dictionary<string, double> GetDiscountedItems(Guid customerId, string promoCode, List<Guid> treatments)
        {
            return new DataAccess().GetDiscountedItems(customerId, promoCode, treatments);
        }
    }
}
