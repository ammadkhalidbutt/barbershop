﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{
    public static class Extensions
    {
        public static SearchResultViewModel<T> Paginate<T>(this IQueryable<T> query, GeneralSearchViewModel search, ApplicationDbContext db)
        {
            SearchResultViewModel<T> searchResult = new SearchResultViewModel<T>();
            if (search.Pagination)
            {
                List<T> BranchList = query.Skip((search.CurrentPage - 1) * search.RecordsPerPage).Take(search.RecordsPerPage).ToList() as List<T>;
                searchResult.ResultList = BranchList ?? new List<T>();
                if (search.CalculateTotal)
                {
                    searchResult.TotalCount = query == null ? 0 : query.Count();
                }
            }
            else
            {
                try
                {
                    searchResult.ResultList = query.ToList() as List<T>;
                }
                catch (Exception ex)
                {
                    new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Search", DateTime = DateTime.Now, ModelId = "none", StackTrace = ex.StackTrace });
                }
            }
            return searchResult;

        }

        public static string GetCurrencyValue(string email)
        {
            using(ApplicationDbContext db = new ApplicationDbContext())
            {
                var userBranchId = db.Users.Where(m => m.Email == email).FirstOrDefault().BranchId;
                var currencyValue = db.Branches.Join(db.Currencies, b => b.CurrencyId, c => c.Id, (b, c) => new { b, c })
                    .Where(m=>m.b.Id == userBranchId)
                    .Select(m => m.c).FirstOrDefault().Value;
                return currencyValue;
            }
        }

        public static bool IsUserActive(string id)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var user = userManager.FindById(id);
            if (user != null && user != new ApplicationUser())
                return user.Active;
            else
                return false;
        }
    }
}
