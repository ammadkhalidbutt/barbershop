﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpers.Select2
{
    public class Select2Repository
    {
        List<Select2OptionModel> GetPagedListOptions(int pageSize, int pageNumber, List<Select2OptionModel> list, out int totalSearchRecords)
        {
            //var allSearchedResults = GetAllSearchResults(searchTerm);
            totalSearchRecords = list.Count;
            return list.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
        }

        public Select2PagedResult GetSelect2PagedResult(int pageSize, int pageNumber, List<Select2OptionModel> list)
        {
            var select2pagedResult = new Select2PagedResult();
            var totalResults = 0;
            select2pagedResult.Results = GetPagedListOptions(pageSize, pageNumber, list, out totalResults);
            select2pagedResult.Total = totalResults;
            return select2pagedResult;
        }

    }
}
