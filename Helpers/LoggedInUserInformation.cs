﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Models.Identity;
using System.Web;

namespace Helpers
{
    public class LoggedInUserInfo
    {
        public string GetLoggedInUserId() => HttpContext.Current.User.Identity.GetUserId();

        public ApplicationUser GetLoggedInUser()
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            return userManager.FindById(GetLoggedInUserId());
        }
    }
}
