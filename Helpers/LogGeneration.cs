﻿using Models;
using Models.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{
    public class LogGeneration
    {
        public bool CreateExceptionLog(DbContext db, Log log)
        {
            try
            {
                    ObjectContext objContext = ((IObjectContextAdapter)db).ObjectContext;
                    
                    //bool exists = db.Database
                    // .SqlQuery<int?>(@"
                    //     SELECT 1 FROM sys.tables AS T
                    //     INNER JOIN sys.schemas AS S ON T.schema_id = S.schema_id
                    //     WHERE S.Name = 'SchemaName' AND T.Name = 'AspNetUsers'")
                    // .SingleOrDefault() != null;

                var query = Convert.ToBoolean(db.Database.SqlQuery<int?>(@" 
                            IF EXISTS (SELECT 1 
                            FROM INFORMATION_SCHEMA.TABLES 
                            WHERE TABLE_TYPE='BASE TABLE' 
                            AND TABLE_NAME='ExceptionLogs') 
                            SELECT 1 AS res ELSE SELECT 0 AS res").SingleOrDefault());
                if (!query)
                {
                    var result =  db.Database.ExecuteSqlCommand(@"CREATE TABLE ExceptionLogs(
                                         Id UNIQUEIDENTIFIER DEFAULT NEWID() PRIMARY KEY,
                                         ModelId  varchar(256),
                                         StackTrace varchar(MAX),
                                         Action varchar(256),
                                         DateTime datetime NOT NULL)");
                }
                var insert = db.Database.ExecuteSqlCommand(@"Insert into ExceptionLogs (Id,ModelId,StackTrace,Action,DateTime) values ({0},{1},{2},{3},{4})", Guid.NewGuid(), log.ModelId, log.StackTrace, log.Action, log.DateTime);
            }
            catch (Exception ex)
            {

                throw;
            }
            return true; 
        }

        public bool CreateGeneralLog(DbContext db, Log log)
        {
            try
            {
                    ObjectContext objContext = ((IObjectContextAdapter)db).ObjectContext;

                //bool exists = db.Database
                // .SqlQuery<int?>(@"
                //         SELECT 1 FROM sys.tables AS T
                //         INNER JOIN sys.schemas AS S ON T.schema_id = S.schema_id
                //         WHERE S.Name = 'SchemaName' AND T.Name = 'Treatments'")
                // .SingleOrDefault() != null;

                var query = Convert.ToBoolean(db.Database.SqlQuery<int>(@" 
                            IF EXISTS (SELECT 1 
                            FROM INFORMATION_SCHEMA.TABLES 
                            WHERE TABLE_TYPE='BASE TABLE' 
                            AND TABLE_NAME='GeneralLogs') 
                            SELECT 1 AS res ELSE SELECT 0 AS res").SingleOrDefault());
                if (!query)
                {
                    var result =  db.Database.ExecuteSqlCommand(@"CREATE TABLE GeneralLogs(
                                         Id UNIQUEIDENTIFIER DEFAULT NEWID() PRIMARY KEY,
                                         ModelId  varchar(256),
                                         AddedBy  varchar(256),
                                         Action varchar(256),
                                         DateTime datetime NOT NULL)");
                }
                var insert = db.Database.ExecuteSqlCommand(@"Insert into GeneralLogs (Id,ModelId,AddedBy,Action,DateTime) values ({0},{1},{2},{3},{4})", Guid.NewGuid(), log.ModelId, log.AddedBy, log.Action, log.DateTime);
            }
            catch (Exception ex)
            {

                throw;
            }
            return true; 
        }
    }
}
