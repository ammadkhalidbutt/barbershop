﻿using System;
namespace Helpers
{
    [Serializable]
    public class KnownException : Exception
    {
        public KnownException(string message) : base(message)
        {

        }
    }
}
