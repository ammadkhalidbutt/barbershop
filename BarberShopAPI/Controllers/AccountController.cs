﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using BarberShopAPI.Models;
using BarberShopAPI.Providers;
using BarberShopAPI.Results;
using Models.ViewModels;
using BusinessLogic;
using AutoMapper;
using System.Linq;
using Models;
using System.IO;
using System.Net;
using System.Transactions;

namespace BarberShopAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        // GET api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfo")]
        public UserInfoViewModel GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            return new UserInfoViewModel
            {
                UserId = User.Identity.GetUserId(),
                Email = User.Identity.GetUserName(),
                HasRegistered = externalLogin == null,
                LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
            };
        }

        //
        // POST api/Account/GetClientPoints
        [HttpPost]
        public RegisterViewModel GetClientPoints(AccountSearchViewModel search)
        {
            return new Logic().GetUserPointsForApi(search);
        }

        //
        // POST api/Account/GetUser
        [HttpPost]
        public List<RegisterViewModel> GetUsers(AccountSearchViewModel search)
        {
            if(search.Role == "Client")
            {
            var result = new Logic().GetUsers<RegisterViewModel>(search).ResultList.GroupBy(x=>x.Email).ToList();
                result.ForEach(x => x.FirstOrDefault().Points = x.Sum(m => m.Points));
                var result1 = result.Select(x => x.FirstOrDefault()).ToList();
                return result1;
            }
            else
            {
                var result = new Logic().GetUsers<RegisterViewModel>(search).ResultList;
                return result;
            }
        }

        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        [Route("ManageInfo")]
        public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        {
            IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (user == null)
            {
                return null;
            }

            List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

            foreach (IdentityUserLogin linkedAccount in user.Logins)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = linkedAccount.LoginProvider,
                    ProviderKey = linkedAccount.ProviderKey
                });
            }

            if (user.PasswordHash != null)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = LocalLoginProvider,
                    ProviderKey = user.UserName,
                });
            }

            return new ManageInfoViewModel
            {
                LocalLoginProvider = LocalLoginProvider,
                Email = user.UserName,
                Logins = logins,
                ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
            };
        }

        // POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
            //    model.NewPassword);
            await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/SetPassword
        [Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/AddExternalLogin
        [Route("AddExternalLogin")]
        public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

            if (ticket == null || ticket.Identity == null || (ticket.Properties != null
                && ticket.Properties.ExpiresUtc.HasValue
                && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
            {
                return BadRequest("External login failure.");
            }

            ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

            if (externalData == null)
            {
                return BadRequest("The external login is already associated with an account.");
            }

            IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(),
                new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/RemoveLogin
        [Route("RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
            }
            else
            {
                result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
                    new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogin
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            if (error != null)
            {
                return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            ApplicationUser user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
                externalLogin.ProviderKey));

            bool hasRegistered = user != null;

            if (hasRegistered)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
                   OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(UserManager,
                    CookieAuthenticationDefaults.AuthenticationType);

                AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName);
                Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
            }
            else
            {
                IEnumerable<Claim> claims = externalLogin.GetClaims();
                ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                Authentication.SignIn(identity);
            }

            return Ok();
        }

        // GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        [AllowAnonymous]
        [Route("ExternalLogins")]
        public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        {
            IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
            List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

            string state;

            if (generateState)
            {
                const int strengthInBits = 256;
                state = RandomOAuthStateGenerator.Generate(strengthInBits);
            }
            else
            {
                state = null;
            }

            foreach (AuthenticationDescription description in descriptions)
            {
                ExternalLoginViewModel login = new ExternalLoginViewModel
                {
                    Name = description.Caption,
                    Url = Url.Route("ExternalLogin", new
                    {
                        provider = description.AuthenticationType,
                        response_type = "token",
                        client_id = Startup.PublicClientId,
                        redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
                        state = state
                    }),
                    State = state
                };
                logins.Add(login);
            }

            return logins;
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        [HttpPost]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var title = "Client";
            model.Gender = model.Gender == 0 ? Enums.Gender.Unknown : model.Gender;
            var application = new Logic().GetApplications<ApplicationViewModel>(new ApplicationSearchViewModel());
            var branch = new Logic().GetBranches<BranchViewModel>(new BranchSearchViewModel());
            var address = new Address();
            if (ModelState.IsValid && application.ResultList.Count() > 0 && branch.ResultList.Count() > 0)
            {
                if (model.Image != null)
                {
                    MemoryStream ms = new MemoryStream(model.Image, 0, model.Image.Length);
                    // Convert byte[] to Image
                    ms.Write(model.Image, 0, model.Image.Length);
                    System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                    var path = string.Format("/images/users/{0}{1}.png", model.Name, model.PhoneNumber);
                    var SaveFileSource = System.Web.Hosting.HostingEnvironment.MapPath(path);
                    System.IO.FileInfo file = new System.IO.FileInfo(SaveFileSource);
                    file.Directory.Create(); // If the directory already exists, this method does nothing.
                    if (File.Exists(SaveFileSource))
                    {
                        File.Delete(SaveFileSource);
                    }
                    image.Save(SaveFileSource, System.Drawing.Imaging.ImageFormat.Png);
                    model.ImageUrl = path;
                }
                else
                {
                    model.ImageUrl = "/images/users/default-user.png";
                }

                model.Address.Id = Guid.NewGuid();
                model.Address.Active = true;
                model.Role = string.IsNullOrEmpty(model.Role) ? string.IsNullOrEmpty(title) ? "Client" : title : model.Role;
                model.Email = string.IsNullOrEmpty(model.Email) ? String.Format("{0}{1}@saloon.com", model.Name, model.PhoneNumber) : model.Email;
                var config = new MapperConfiguration(cfg => { cfg.CreateMap<AddressViewModel, Address>(); });
                var iMapper = config.CreateMapper();
                address = iMapper.Map<AddressViewModel, Address>(model.Address);

            }
            var user = new ApplicationUser { UserName = model.Email, Email = model.Email, Active = true, Address = address, ApplicationId = application.ResultList.First().Id, BranchId = branch.ResultList.First().Id, DOB = model.DOB, ImageUrl = model.ImageUrl, Name = model.Name, PhoneNumber = model.PhoneNumber, Gender = model.Gender, QRCode = model.Id };

            IdentityResult result = new IdentityResult();
            try
            {
                 result = await UserManager.CreateAsync(user, model.Password);

            }
            catch (Exception ex)
            {

                throw;
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            else
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    new Helpers.LogGeneration().CreateGeneralLog(db, new Log { Action = "Register" + model.Role, AddedBy = model.Id, DateTime = DateTime.Now, ModelId = model.Id });
                    var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                    userManager.AddToRole(user.Id, "Client");
                }
            }

            return Ok();
        }


        //POST api/Account/Edit/{object}
        public HttpResponseMessage Edit(RegisterBindingModel model)
        {
            if (model.Image != null)
            {
                MemoryStream ms = new MemoryStream(model.Image, 0, model.Image.Length);
                // Convert byte[] to Image
                ms.Write(model.Image, 0, model.Image.Length);
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                var path = string.Format("/images/users/{0}{1}.png", model.Name, model.PhoneNumber);
                var SaveFileSource = System.Web.Hosting.HostingEnvironment.MapPath(path);
                System.IO.FileInfo file = new System.IO.FileInfo(SaveFileSource);
                file.Directory.Create(); // If the directory already exists, this method does nothing.
                if (File.Exists(SaveFileSource))
                {
                    File.Delete(SaveFileSource);
                }
                model.ImageUrl = path;
                image.Save(SaveFileSource, System.Drawing.Imaging.ImageFormat.Png);
            }
            model.Action = "Edit";
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<RegisterBindingModel, RegisterViewModel>().ForMember(x => x.Image, opt => opt.Ignore()); });
            var iMapper = config.CreateMapper();
            var record = iMapper.Map<RegisterBindingModel, RegisterViewModel>(model);
            var result = new Logic().Edit(record);
            if (string.IsNullOrEmpty(result))
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotAcceptable, "Check content and make sure all fields are correct");
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [AllowAnonymous]
        private async Task<ExternalLoginInfo> AuthenticationManager_GetExternalLoginInfoAsync_WithExternalBearer()
        {
            ExternalLoginInfo loginInfo = null;

            var result = await Authentication.AuthenticateAsync(DefaultAuthenticationTypes.ExternalBearer);

            if (result != null && result.Identity != null)
            {
                var idClaim = result.Identity.FindFirst(ClaimTypes.NameIdentifier);
                if (idClaim != null)
                {
                    loginInfo = new ExternalLoginInfo()
                    {
                        DefaultUserName = result.Identity.Name == null ? "" : result.Identity.Name.Replace(" ", ""),
                        Login = new UserLoginInfo(idClaim.Issuer, idClaim.Value)
                    };
                }
            }
            return loginInfo;
        }

        // POST api/Account/RegisterExternal
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterBindingModel model)
        {
            ModelState.Remove("model.Id");
            ModelState.Remove("model.Name");
            ModelState.Remove("model.PhoneNumber");
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var info = await Authentication.GetExternalLoginInfoAsync();
            var info = await AuthenticationManager_GetExternalLoginInfoAsync_WithExternalBearer();
            if (info == null)
            {
                return InternalServerError();
            }
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var application = new Logic().GetApplications<ApplicationViewModel>(new ApplicationSearchViewModel());
                var branch = new Logic().GetBranches<BranchViewModel>(new BranchSearchViewModel());
                var address = new Address();
                model.Role = "Client";
                if (application.ResultList.Count() > 0 && branch.ResultList.Count() > 0)
                {
                    model.Address = new AddressViewModel();
                    model.Address.Id = Guid.NewGuid();
                    model.Address.Active = true;
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<AddressViewModel, Address>(); });
                    var iMapper = config.CreateMapper();
                    address = iMapper.Map<AddressViewModel, Address>(model.Address);
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, Active = true, Address = address, ApplicationId = application.ResultList.First().Id, BranchId = branch.ResultList.First().Id, DOB = model.DOB, ImageUrl = model.ImageUrl, Name = model.Name, PhoneNumber = model.PhoneNumber, Gender = model.Gender, QRCode = model.Id };
                try
                {
                    IdentityResult result = await UserManager.CreateAsync(user);
                    if (!result.Succeeded)
                    {
                        return GetErrorResult(result);
                    }

                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (!result.Succeeded)
                    {
                        return GetErrorResult(result);
                    }
                    UserManager.AddToRole(user.Id, "Client");
                }
                catch (Exception ex)
                {
                    return BadRequest("Something went wrong.");
                }
                scope.Complete();
            }
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }
            public string Email { get; set; }
            public string Name { get; set; }
            public string ProfilePic { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }
                if (!string.IsNullOrEmpty(Email))
                {
                    claims.Add(new Claim(ClaimTypes.Email, Email, null, LoginProvider));
                }
                //if (!string.IsNullOrEmpty(Name))
                //{
                //    claims.Add(new Claim(ClaimTypes.Name, Name, null, LoginProvider));
                //}
                if (!string.IsNullOrEmpty(ProfilePic))
                {
                    claims.Add(new Claim(ClaimTypes.Uri, ProfilePic, null, LoginProvider));
                }
                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }
                ExternalLoginData ELD = new ExternalLoginData();

                ELD.LoginProvider = providerKeyClaim.Issuer;
                ELD.ProviderKey = providerKeyClaim.Value;
                ELD.Name = identity.FindFirstValue(ClaimTypes.Name);
                ELD.UserName = identity.FindFirstValue(ClaimTypes.Name);
                try
                {
                    ELD.Email = identity.Claims.FirstOrDefault(x => x.Type.Contains("emailaddress")).Value;
                }
                catch (Exception ex)
                {
                    ELD.Email = "";
                }

                return ELD;
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}
