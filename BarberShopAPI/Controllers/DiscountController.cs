﻿using BusinessLogic;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace BarberShopAPI.Controllers
{
    public class DiscountController : ApiController
    {
        public DiscountViewModel Get(Guid id)
        {
            DiscountSearchViewModel search = new DiscountSearchViewModel() { Id = id };
            return new Logic().GetDiscounts(search).ResultList.FirstOrDefault();
        }
        public List<DiscountViewModel> GetDiscounts(Guid clientId)
        {
            DiscountSearchViewModel search = new DiscountSearchViewModel() { ClientId = clientId, RecordsPerPage = 10, CurrentPage = 1 };
            return new Logic().GetDiscounts(search).ResultList;
        }
    }
}
