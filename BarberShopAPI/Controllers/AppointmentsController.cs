﻿using BusinessLogic;
using Models.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BarberShopAPI.Controllers
{
    public class AppointmentsController : ApiController
    {
        // GET: api/Appointments
        [HttpPost]
        public List<AppointmentViewModel> Get(AppointmentSearchViewModel search)
        {
            return new Logic().GetAppointments<AppointmentViewModel>(search).ResultList;
        }

        [HttpPost]
        public List<AppointmentHistoryViewModel> GetAppointmentHistory(AppointmentSearchViewModel search)
        {
            return new Logic().GetAppointmentHistory(search);
        }
    }
}
