﻿using BusinessLogic;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BarberShopAPI.Controllers
{
    [Authorize]
    public class TreatmentsController : ApiController
    {
        [HttpPost]
        public List<TreatmentViewModel> Get([FromBody] TreatmentSearchViewModel filters)
        {
            var branch = new Logic().GetBranches<BranchViewModel>(new BranchSearchViewModel()).ResultList.FirstOrDefault();
            var treatments = new Logic().GetTreatments<TreatmentViewModel>(filters).ResultList;
            treatments.ForEach(m => m.Price = m.Price + ((branch.VAT/100)* m.Price));
            return treatments;
        }

    }
}
