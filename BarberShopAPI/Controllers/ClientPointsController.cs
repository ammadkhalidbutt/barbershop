﻿using BusinessLogic;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BarberShopAPI.Controllers
{
    public class ClientPointsController : ApiController
    {
        public List<ClientPointViewModel> Get(ClientPointSearchViewModel search)
        {
            return new Logic().GetClientPoints<ClientPointViewModel>(search).ResultList;
        }
    }
}