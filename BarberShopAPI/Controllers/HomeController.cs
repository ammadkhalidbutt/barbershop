﻿using Models.ViewModels;
using System.Web.Mvc;

namespace BarberShopAPI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            try
            {

                ViewBag.Title = "Home Page";
                var userAuthenticated = false;
                var sessionValue = Session["UserInfo"] != null ? bool.TryParse(Session["UserInfo"].ToString(), out userAuthenticated) : false;
                if (!userAuthenticated)
                {
                    return RedirectToAction("Login");
                }
                return View();
            }
            catch (System.Exception)
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel userCredentials)
        {
            if (userCredentials.Email == "apiAdmin@salon.com" && userCredentials.Password == "Apisalon@123")
            {
                Session["UserInfo"] = true;
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError("", "Wrong Username or Password!");
                return View();
            }
        }
    }
}
