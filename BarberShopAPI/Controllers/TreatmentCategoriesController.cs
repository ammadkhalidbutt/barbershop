﻿using BusinessLogic;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BarberShopAPI.Controllers
{
    public class TreatmentCategoriesController : ApiController
    {
        // POST api/<controller>/{object}
        [HttpPost]
        public List<TreatmentCategoryViewModel> Get(TreatmentCategorySearchViewModel search)
        {
            return new Logic().GetTreatmentCategories<TreatmentCategoryViewModel>(search).ResultList;
        }
    }
}