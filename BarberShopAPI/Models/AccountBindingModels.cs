﻿using System;
using Foolproof;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Enums;
using Microsoft.AspNet.Identity.EntityFramework;
using Models.ViewModels;
using Newtonsoft.Json;

namespace BarberShopAPI.Models
{
    // Models used as parameters to AccountController actions.

    public class AddExternalLoginBindingModel
    {
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }

    public class ChangePasswordBindingModel
    {
        //[Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterBindingModel
    {
        public RegisterBindingModel()
        {
            Address = new AddressViewModel();
        }

        [Required]
        public string Id { get; set; }
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        public string Name { get; set; }
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[0-9]).{6,15}$", ErrorMessage = "Passwords must have at least one digit ('0'-'9').")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        public DateTime ComparativeDate { get { return DateTime.Now.AddYears(-10); } }
        private DateTime _DOB;
        [Display(Name = "Date of Birth")]
        [DataType(DataType.DateTime)]
        [LessThan("ComparativeDate")]
        public DateTime DOB
        {
            get
            {
                _DOB = _DOB == new DateTime() ? new DateTime(1980, 01, 01) : _DOB;
                return _DOB;
            }
            set
            {
                _DOB = value;
            }
        }
        public Gender Gender { get; set; }
        [Required]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Refernce Id")]
        public string ReferenceId { get; set; }
        [Display(Name = "Branch")]
        public Guid BranchId { get; set; }
        [Display(Name = "Application")]
        public Guid ApplicaitonId { get; set; }
        public string Role { get; set; }
        public string Action { get; set; }
        public bool IsRegistered { get; set; }
        public ICollection<IdentityUserRole> Roles { get; set; }
        public byte[] Image { get; set; }
        public string ImageUrl { get; set; }
        public AddressViewModel Address { get; set; }
        public int Age
        {
            get
            {
                return DateTime.Now.Year - DOB.Year;
            }
        }

        //public string StylistId { get; set; }

        //public string TreatmentName { get; set; }
        //public float Percentage { get; set; }
        //public Guid TreatmentId { get; set; }
        //public bool Speciality { get; set; }
        //public DateTime TreatmentDate { get; set; }
        //public int Points { get; set; }
    }

    public class RegisterExternalBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class RemoveLoginBindingModel
    {
        [Required]
        [Display(Name = "Login provider")]
        public string LoginProvider { get; set; }

        [Required]
        [Display(Name = "Provider key")]
        public string ProviderKey { get; set; }
    }

    public class SetPasswordBindingModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
