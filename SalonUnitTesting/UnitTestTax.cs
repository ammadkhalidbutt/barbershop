﻿using System;
using System.Security.Principal;
using System.Web;
using BusinessLogic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.ViewModels;
using Moq;

namespace SalonUnitTesting
{
    [TestClass]
    public class UnitTestTax
    {
        private Guid id;
        [TestMethod]
        public void TestCreate()
        {
            try
            {
                var context = new Mock<HttpContextBase>();
                var mockIdentity = new Mock<IIdentity>();
                context.SetupGet(x => x.User.Identity).Returns(mockIdentity.Object);
                mockIdentity.Setup(x => x.Name).Returns("test_name");
                var tax = new TaxViewModel
                {
                    CreatedBy = "d8474e5d-6dbc-4f8f-873a-93ab87bd7c03",
                    UpdatedBy = "d8474e5d-6dbc-4f8f-873a-93ab87bd7c03",
                    Name = "BWT",
                    Percentage = 8
                };
                var check = new Logic().Create(tax);
                id = check;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [TestMethod]
        public void TestGet()
        {
            try
            {
                var tax = new Logic().GetTax(id);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [TestMethod]
        public void TestGetTaxes()
        {
            try
            {
                var tax = new Logic().GetTaxes(new Guid());
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [TestMethod]
        public void EditTest()
        {
            try
            {
                var tax = new Logic().GetTax(id);
                tax.Name = "Bwt";
                tax.Percentage = 10;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [TestMethod]
        public void Delete()
        {
            try
            {
                var tax = new Logic().GetTax(id);
                var check = new Logic().Delete(tax);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [TestMethod]
        public void Remove()
        {
            try
            {
                var tax = new Logic().GetTax(id);
                var check = new Logic().Remove(tax);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
