﻿using AutoMapper;
using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<T> GetTreatmentCategories<T>(TreatmentCategorySearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    if (search.Select2)
                    {
                        var queryableResult = db.TreatmentCategories.Where(m =>
                                        m.IsDeleted == search.IsDeleted &&
                                        (search.Id == defaultGuid || m.Id == search.Id) &&
                                        (string.IsNullOrEmpty(search.Name) || m.Name.Contains(search.Name)))
                                        .Select(m => new Select2OptionModel
                                        {
                                            id = m.Id.ToString(),
                                            text = m.Name
                                        }).AsQueryable();
                        var result = queryableResult as IQueryable<T>;
                        return result.Paginate(search, db);
                    }
                    else
                    {
                        var queryableResult = db.TreatmentCategories.Where(m =>
                                         m.IsDeleted == search.IsDeleted &&
                                        (m.Id == search.Id || search.Id == new Guid()) &&
                                        (m.Name.Contains(search.Name) || string.IsNullOrEmpty(search.Name)))
                                        .Select(m => new TreatmentCategoryViewModel
                                        {
                                            Id = m.Id,
                                            CreatedBy = m.CreatedBy,
                                            CreatedOn = m.CreatedOn,
                                            Description = m.Description,
                                            Image = m.Image,
                                            Name = m.Name
                                        }).AsQueryable();
                        var result = queryableResult as IQueryable<T>;
                        return result.Paginate(search, db);
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Create(TreatmentCategoryViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<TreatmentCategoryViewModel, TreatmentCategory>(); });
                    var iMapper = config.CreateMapper();
                    var category = iMapper.Map<TreatmentCategoryViewModel, TreatmentCategory>(model);
                    var result = db.TreatmentCategories.Add(category);
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }

        public Guid Edit(TreatmentCategoryViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.TreatmentCategories.Find(model.Id);
                    if (record != null)
                    {
                        record.Name = string.IsNullOrEmpty(model.Name) ? record.Name : model.Name;
                        record.Description = string.IsNullOrEmpty(model.Description) ? record.Description : model.Description;
                        record.Image = string.IsNullOrEmpty(model.Image) ? record.Image : model.Image;
                        db.Entry(record).State = EntityState.Modified;
                        db.SaveChanges();
                        return record.Id;
                    }
                    else
                        return new Guid();
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }

        public Guid Delete(TreatmentCategoryViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.TreatmentCategories.Find(model.Id);
                    if (record.IsDeleted == false)
                    {
                        record.IsDeleted = true;
                        db.Entry(record).State = EntityState.Modified;
                        var treatments = db.Treatments.Where(m => m.CategoryId == model.Id && m.IsDeleted == false).ToList();
                        treatments.ForEach(m => m.IsDeleted = true);
                        db.Entry(treatments).State = EntityState.Modified;
                        db.SaveChanges();
                        return record.Id;
                    }
                    else
                        return new Guid();
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }
    }
}
