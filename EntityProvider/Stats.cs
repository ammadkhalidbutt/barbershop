﻿using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public int GetTreatmentsCount(TreatmentSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    return db.Treatments.Where(m => m.Active == true && m.IsDeleted == false).Select(m => m).Count();
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int GetUsersCount(AccountSearchViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    return (from u in db.Users
                            join r in db.Roles on u.Roles.Where(m => m.UserId == u.Id).Select(ri => ri.RoleId).FirstOrDefault() equals r.Id

                            where 
                              u.Active &&
                              (string.IsNullOrEmpty(model.Id) || u.Id == model.Id) &&
                              (string.IsNullOrEmpty(model.Name) || u.Name.Contains(model.Name) || u.PhoneNumber.Contains(model.Name)) &&
                              (string.IsNullOrEmpty(model.Email) || u.Email == model.Email) &&
                              (string.IsNullOrEmpty(model.PhoneNumber) || u.PhoneNumber.Contains(model.PhoneNumber)) &&
                              (string.IsNullOrEmpty(model.RoleId) || u.Roles.Any(x => x.RoleId == model.RoleId)) &&
                              (string.IsNullOrEmpty(model.Role) || r.Name == model.Role)
                            select u).Count();

                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public double GetStylistCommissions(StylistCommissionSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    return (from sc in db.StylistCommissions
                            join a in db.Appointments on sc.AppointmentId equals a.Id
                            join t in db.Treatments on sc.TreatmentId equals t.Id
                            join s in db.Users on sc.StylistId equals s.Id
                            where !sc.IsDeleted
                                && (sc.AppointmentId == search.Appointment.Id || search.Appointment.Id == new Guid())
                                && (a.BookingDate >= search.StartingDate && a.BookingDate <= search.EndingDate || search.StartingDate == new DateTime())
                                && (sc.StylistId == search.Stylist.Id || string.IsNullOrEmpty(search.Stylist.Id))
                                && (sc.TreatmentId == search.Treatment.Id || search.Treatment.Id == new Guid())
                                && (sc.Id == search.Id || search.Id == new Guid())
                            select sc).Sum(x => x.Amount);
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
