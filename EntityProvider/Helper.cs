﻿using Models.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        private string GetEntityName(string EntityType, Guid EntityId)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    switch (EntityType)
                    {
                        case "Category":
                            return (db.TreatmentCategories.Where(x => x.Id == EntityId).FirstOrDefault()).Name;
                        case "Item":
                            return (db.Treatments.Where(x => x.Id == EntityId).FirstOrDefault()).Name;
                        case "Customer":
                            return (db.Users.Where(x => x.Id == EntityId.ToString()).FirstOrDefault()).Name;
                        default:
                            return "";
                    }
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}
