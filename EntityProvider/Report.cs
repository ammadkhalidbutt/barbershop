﻿using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<T> GetTreatmentReport<T>(TreatmentReportSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var queryableResult = (from ii in db.InvoiceItems
                                           join ir in db.InvoiceRecords on ii.InvoiceId equals ir.Id
                                           join a in db.Appointments on ii.AppointmentId equals a.Id
                                           join b in db.Branches on a.BranchId equals b.Id
                                           join c in db.Users on a.ClientId equals c.Id
                                           join s in db.Users on a.StylistId equals s.Id
                                           join t in db.Treatments on a.TreatmentId equals t.Id
                                           join sc in db.StylistCommissions on new { tId = t.Id, sId = s.Id } equals new { tId = sc.TreatmentId, sId = sc.StylistId }
                                           where !(a.IsDeleted && ir.IsDeleted && ii.IsDeleted)
                                                 && (ii.DateTime >= search.DateTime && ii.DateTime <= search.EndingDateTime || search.DateTime == new DateTime())
                                                 && (c.Name.Contains(search.ClientName) || string.IsNullOrEmpty(search.ClientName))
                                                 && (c.Id == search.ClientId || string.IsNullOrEmpty(search.ClientId))
                                                 && (s.Name.Contains(search.StylistName) || string.IsNullOrEmpty(search.StylistName))
                                                 && (s.Id == search.StylistId || string.IsNullOrEmpty(search.StylistId))
                                                 && (t.Name.Contains(search.TreatmentName) || string.IsNullOrEmpty(search.TreatmentName))
                                                 && (t.Id == search.TreatmentId || search.TreatmentId == new Guid())
                                                 && (ii.Id == search.InvoiceItemId || search.InvoiceItemId == new Guid())
                                                 && (ir.Id == search.InvoiceId || search.InvoiceId == new Guid())

                                           select new TreatmentReportViewModel
                                           {
                                               ClientId = c.Id,
                                               ClientName = c.Name,
                                               InvoiceDateTime = ii.DateTime,
                                               InvoiceItemId = ii.Id,
                                               StylistCommission = (sc.Amount),
                                               StylistId = s.Id,
                                               StylistName = s.Name,
                                               TreatmentId = t.Id,
                                               TreatmentName = t.Name,
                                               TreatmentPrice = t.Price,
                                               Discount = ii.DiscountedPrice,
                                               PointsUsed = ir.PointsUsed,
                                               AmountRecieved = ((t.Price - ii.DiscountedPrice) - ((db.Points.FirstOrDefault().Worth * ir.PointsUsed) / db.InvoiceItems.Where(m => m.InvoiceId == ir.Id && !m.IsDeleted).Count()))
                                           }).OrderBy(x => x.InvoiceDateTime).Distinct().AsQueryable();
                    var result = queryableResult as IQueryable<T>;
                    return result.Paginate(search, db);

                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Get profit report", DateTime = DateTime.Now, ModelId = search.ClientId.ToString(), StackTrace = ex.StackTrace });
                throw;
            }
        }

        public SearchResultViewModel<T> GetInvoiceReport<T>(InvoiceReportSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var queryableResult = (from ir in db.InvoiceRecords
                                           join ii in db.InvoiceItems on ir.Id equals ii.InvoiceId
                                           join a in db.Appointments on ii.AppointmentId equals a.Id into aas
                                           from a in aas.DefaultIfEmpty()
                                           join c in db.Users on a.ClientId equals c.Id into cs
                                           from c in cs.DefaultIfEmpty()
                                           join s in db.Users on a.StylistId equals s.Id into ss
                                           from s in ss.DefaultIfEmpty()
                                           join t in db.Treatments on ii.ItemId equals t.Id into ts
                                           from t in ts.DefaultIfEmpty()
                                           join b in db.Branches on a.BranchId equals b.Id
                                           where
                                                 !(a.IsDeleted && ir.IsDeleted && ii.IsDeleted)
                                           &&
                                           (search.Id == new Guid() || ir.Id == search.Id)
                                                 && (search.DateTime == new DateTime() || (ir.DateTime >= search.DateTime && ir.DateTime <= search.EndingDateTime))
                                                 && (string.IsNullOrEmpty(search.ClientName) || c == null || c.Name.Contains(search.ClientName))
                                                 && (string.IsNullOrEmpty(search.ClientId) || c == null || c.Id == search.ClientId)
                                                 && (string.IsNullOrEmpty(search.StylistName) || s == null || s.Name.Contains(search.StylistName))
                                                 && (string.IsNullOrEmpty(search.StylistId) || s == null || s.Id == search.StylistId)
                                                 && (string.IsNullOrEmpty(search.TreatmentName) || t == null || t.Name.Contains(search.TreatmentName))
                                                 && (search.TreatmentId == new Guid() || t == null || t.Id == search.TreatmentId)
                                           select new InvoiceReportViewModel
                                           {
                                               Id = ir.Id,
                                               AmountRecieved = ir.AmountRecieved,
                                               AmountGiven = ir.AmountGiven,
                                               VATPercentage = b.VAT,
                                               DateTime = ir.DateTime,
                                               Client = c,
                                               GrandTotal = ir.GrandTotal,
                                               LastUpdatedOn = ir.LastUpdatedOn,
                                               PointsUsed = ir.PointsUsed,
                                               Status = ir.Status,
                                               InvoiceNumber = ir.InvoiceNumber,
                                               IsPaidByPin = ir.IsPaidByPin,
                                               InvoiceItems = (from iii in db.InvoiceItems
                                                               join iir in db.InvoiceRecords on iii.InvoiceId equals iir.Id
                                                               join ia in db.Appointments on iii.AppointmentId equals ia.Id into ias
                                                               from ia in ias.DefaultIfEmpty()
                                                               join si in db.Users on ia.StylistId equals si.Id into sis
                                                               from si in sis.DefaultIfEmpty()
                                                               join it in db.Treatments on iii.ItemId equals it.Id into its
                                                               from it in its.DefaultIfEmpty()
                                                               join ip in db.Products on iii.ItemId equals ip.Id into ips
                                                               from ip in ips.DefaultIfEmpty()
                                                               join sc in db.StylistCommissions on new { tId = it.Id, sId = si.Id, aId = ia.Id } equals new { tId = sc.TreatmentId, sId = sc.StylistId, aId = sc.AppointmentId } into scs
                                                               from sc in scs.DefaultIfEmpty()
                                                               where !(ia.IsDeleted && iir.IsDeleted && iii.IsDeleted)
                                                                   && (iii.InvoiceId == ir.Id)
                                                               select new TreatmentReportViewModel
                                                               {
                                                                   InvoiceItemId = iii.Id,
                                                                   StylistCommission = (sc != null ? sc.Amount : 0),
                                                                   InvoiceDateTime = iir.DateTime,
                                                                   StylistName = si != null ? si.Name : "",
                                                                   TreatmentId = it != null ? it.Id : ip != null ? ip.Id : defaultGuid,
                                                                   TreatmentName = it != null ? it.Name : ip != null ? ip.Name : "",
                                                                   TreatmentPrice = it != null ? it.Price : ip != null ? ip.Price : 0,
                                                                   TreatmentTax = it != null ? it.Tax.Percentage : ip != null ? ip.Tax.Percentage : 0,
                                                                   DateTime = iii.DateTime,
                                                                   Discount = iii.DiscountedPrice,
                                                               }).OrderBy(x => x.InvoiceDateTime).Distinct().ToList()

                                           }).GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).AsQueryable();
                    var result = queryableResult as IQueryable<T>;
                    return result.Paginate(search, db);
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Invoice report", DateTime = DateTime.Now, ModelId = search.Id.ToString(), StackTrace = ex.StackTrace });
                throw;
            }
        }

        public SearchResultViewModel<T> GetClientPointReport<T>(ClientPointReportSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var queryableResult = (from p in db.ClientPoints
                                           join c in db.Users on p.ClientId equals c.Id
                                           where (!p.IsDeleted)
                                              && (c.Active)
                                              && (p.ClientId == search.ClientId || string.IsNullOrEmpty(search.ClientId))
                                              && (c.Name.Contains(search.ClientName) || string.IsNullOrEmpty(search.ClientName))
                                           select new ClientPointReportViewModel
                                           {
                                               ClientId = p.ClientId,
                                               ClientName = c.Name,
                                               Date = DateTime.Now,
                                               Points = p.Points
                                           }).GroupBy(x => x.ClientId).Select(m => new ClientPointReportViewModel
                                           {
                                               ClientId = m.Max(x => x.ClientId),
                                               ClientName = m.Max(x => x.ClientName),
                                               Date = DateTime.Now,
                                               Points = m.Sum(x => x.Points)
                                           }).AsQueryable();
                    var result = queryableResult as IQueryable<T>;
                    return result.Paginate(search, db);
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Client point report", DateTime = DateTime.Now, ModelId = search.ClientId.ToString(), StackTrace = ex.StackTrace });
                throw;
            }
        }

        public SearchResultViewModel<T> GetStylistCommissionReport<T>(StylistCommissionReportSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var queryableResult = (from sc in db.StylistCommissions
                                           join a in db.Appointments on sc.AppointmentId equals a.Id
                                           join s in db.Users on a.StylistId equals s.Id
                                           join t in db.Treatments on a.TreatmentId equals t.Id
                                           where
                                           (a.Status == Enums.AppointmentTreatmentStatus.Ended)
                                           && !(sc.IsDeleted)
                                           && ((a.BookingDate >= search.DateTime || search.DateTime == new DateTime()) && (a.BookingDate <= search.EndingDateTime || search.EndingDateTime == new DateTime())
                                           && (a.StylistId == search.StylistId || string.IsNullOrEmpty(search.StylistId))
                                           && (s.Name.Contains(search.StylistName) || string.IsNullOrEmpty(search.StylistName))
                                           && (t.Name.Contains(search.TreatmentName) || string.IsNullOrEmpty(search.TreatmentName))
                                           && (t.Id == search.TreatmentId || search.TreatmentId == new Guid()))
                                           select new StylistCommissionReportViewModel
                                           {
                                               CommissionId = sc.Id,
                                               Commission = sc.Amount,
                                               Date = a.BookingDate,
                                               NumberOfTreatments = 1,
                                               StylistId = sc.StylistId,
                                               StylistName = s.Name
                                           }).OrderBy(x => x.StylistId).Distinct().GroupBy(x => x.StylistId)
                                           .Select(m => new StylistCommissionReportViewModel
                                           {
                                               CommissionId = m.FirstOrDefault().CommissionId,
                                               Commission = m.Sum(x => x.Commission),
                                               Date = DateTime.Now,
                                               NumberOfTreatments = m.Sum(x => x.NumberOfTreatments),
                                               StylistId = m.FirstOrDefault().StylistId,
                                               StylistName = m.FirstOrDefault().StylistName
                                           }).AsQueryable();
                    var result = queryableResult as IQueryable<T>;
                    return result.Paginate(search, db);

                }
            }
            catch (Exception ex)
            {
                return new SearchResultViewModel<T>();
            }
        }

        public List<RegisterViewModel> GetNewClients(DateTime date)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var query = db.Database.SqlQuery<RegisterViewModel>(@"").ToList();
                    return query;
                }
            }
            catch (Exception ex)
            {
                return new List<RegisterViewModel>();
            }
        }

    }
}
