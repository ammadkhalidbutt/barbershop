﻿using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<T> GetAddress<T>(AddressSearchViewModel search)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var queryableResult = db.Addresses.Where(m => m.Active
                                            && m.Id == search.Id || search.Id == new Guid()
                                            && m.Area == search.Area || string.IsNullOrEmpty(search.Area)
                                            && m.City == search.City || string.IsNullOrEmpty(search.City))
                                            .Select(m => new AddressViewModel
                                            {
                                                Active = m.Active,
                                                Area = m.Area,
                                                City = m.City,
                                                House = m.House,
                                                Id = m.Id,
                                                Street = m.Street
                                            }).AsQueryable().OrderBy(s=>s.City);
                    var result = queryableResult as IQueryable<T>;
                    return result.Paginate(search, db);
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Search Address", DateTime = DateTime.Now, ModelId = search.Id.ToString(), StackTrace = ex.StackTrace });
                throw;
            }
        } 
    }
}

