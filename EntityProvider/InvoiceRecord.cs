﻿using AutoMapper;
using Enums;
using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<T> GetInvoiceRecords<T>(InvoiceRecordSearchViewModel search)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    PointViewModel point = GetPoint();
                    var queryableResult = db.InvoiceRecords.Join(db.Users, ir => ir.ClientId, c => c.Id, (ir, c) => new { ir, c })
                                            .Where(m=>m.ir.DateTime == search.DateTime || search.DateTime == new DateTime()
                                                      && m.ir.ClientId == search.Client.Id || string.IsNullOrEmpty(search.Client.Id)
                                                      && m.ir.Status == search.Status || search.Status == 0
                                                      && m.ir.Id == search.InvoiceId || search.InvoiceId == new Guid())
                                            .Select(m => new InvoiceRecordViewModel
                                            {
                                                Client = m.c,
                                                Id = m.ir.Id,
                                                Status = m.ir.Status,
                                                InvoiceItems = (from ii in db.InvoiceItems
                                                                join a in db.Appointments on ii.AppointmentId equals a.Id
                                                                join ab in db.Users on ii.AddedBy equals ab.Id
                                                                join ub in db.Users on ii.UpdatedBy equals ub.Id
                                                                join b in db.Branches on a.BranchId equals b.Id
                                                                join ba in db.Addresses on b.AddressId equals ba.Id
                                                                join c in db.Users on a.ClientId equals c.Id
                                                                join ca in db.Addresses on c.AddressId equals ca.Id
                                                                join s in db.Users on a.StylistId equals s.Id
                                                                join t in db.Treatments on a.TreatmentId equals t.Id
                                                                join tax in db.Taxes on t.TaxId equals tax.Id
                                                                join ap in db.AppointmentApprovals on a.Id equals ap.AppointmentId
                                                                join apr in db.Users on ap.ApproverId equals apr.Id
                                                                where a.Active && !a.IsDeleted
                                                                    && (a.Id == search.Id || search.Id == new Guid())
                                                                select new InvoiceItemViewModel
                                                                {
                                                                    Appointment = new AppointmentViewModel
                                                                    {
                                                                        Active = a.Active,
                                                                        ApplicationId = a.ApplicationId,
                                                                        Branch = b,
                                                                        BranchAddress = ba,
                                                                        ClientId = a.ClientId,
                                                                        BookingDate = a.BookingDate,
                                                                        AppointmentStartingTime = a.AppointmentStartingTime,
                                                                        AppointmentEndingTime = a.AppointmentEndingTime,
                                                                        IsDeleted = a.IsDeleted,
                                                                        AppointmentTreatmentStatus = a.Status,
                                                                        Status = ap.Status,
                                                                        StylistId = a.StylistId,
                                                                        TreatmentId = a.TreatmentId,
                                                                        TotalTime = t.ApproximateTime,
                                                                        Id = a.Id,
                                                                        Client = c,
                                                                        ClientAddress = ca,
                                                                        Stylist = s,
                                                                        Treatment = new TreatmentViewModel
                                                                        {
                                                                            Id = t.Id,
                                                                            AddedBy = t.AddedBy,
                                                                            ApproximateTime = t.ApproximateTime,
                                                                            Category = t.Category,
                                                                            CategoryId = t.CategoryId,
                                                                            Date = t.Date,
                                                                            Description = t.Description,
                                                                            ImageUrl = t.ImageUrl,
                                                                            IsDeleted = t.IsDeleted,
                                                                            Name = t.Name,
                                                                            Points = t.Points,
                                                                            LastUpdatedOn = t.LastUpdatedOn,
                                                                            Price = (float)t.Price,
                                                                            TaxId = t.TaxId,
                                                                            Tax = tax,
                                                                            Active = t.Active
                                                                        },
                                                                        Approver = apr,
                                                                        PointWorth=point.Worth,
                                                                    },
                                                                    PersonAddingRecord = ab,
                                                                    PersonUpdatingRecord = ub,
                                                                    DateTime = ii.DateTime,
                                                                    DiscountedPrice = ii.DiscountedPrice,
                                                                    Id = ii.Id,
                                                                    IsDeleted = ii.IsDeleted,
                                                                    UpdatedOn = ii.UpdatedOn,
                                                                    ItemPrice = (float)t.Price
                                                                }).ToList(),
                                                AmountRecieved = m.ir.AmountRecieved
                                            }).AsQueryable();
                    var result = queryableResult as IQueryable<T>;
                    return result.Paginate(search, db);
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Search/Read invoice record", DateTime = DateTime.Now, ModelId = search.Id.ToString(), StackTrace = ex.StackTrace });
                throw;
            }
        }

        public Guid Create(InvoiceRecordViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var count = db.InvoiceRecords.Count();
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<InvoiceRecordViewModel, InvoiceRecord>().ForMember(s => s.ClientId, x => x.MapFrom(d => d.Client.Id)); });
                    var iMapper = config.CreateMapper();
                    var invoiceRecord = iMapper.Map<InvoiceRecordViewModel, InvoiceRecord>(model);
                    invoiceRecord.Client = null;
                    invoiceRecord.GrandTotal = model.TotalPayment;
                    invoiceRecord.InvoiceNumber = invoiceRecord.InvoiceNumber + (count + 1);
                    var result = db.InvoiceRecords.Add(invoiceRecord);
                    db.SaveChanges();
                    new Helpers.LogGeneration().CreateGeneralLog(new ApplicationDbContext(), new Log { Action = "Create invoice record", DateTime = DateTime.Now, ModelId = model.Id.ToString(), AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId() });

                    return result.Id;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Create invoice record", DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                throw;
            }
        }

        public Guid Edit(InvoiceRecordViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.InvoiceRecords.Find(model.Id);
                    record.Status = model.Status != 0 ? model.Status : record.Status;
                    record.DateTime = model.DateTime;
                    record.LastUpdatedOn = model.LastUpdatedOn;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    return record.Id;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Edit invoice record", DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                throw;
            }
        }

        public Guid Delete(InvoiceRecordViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.InvoiceRecords.Find(model.Id);
                    record.Status = model.Status != 0 ? model.Status : record.Status;
                    record.LastUpdatedOn = model.LastUpdatedOn;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    return record.Id;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Delete invoice record", DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                throw;
            }
        }
    }
}
