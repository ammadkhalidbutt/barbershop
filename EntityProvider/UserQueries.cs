﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Helpers;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Models;
using Models.Identity;
using Models.ViewModels;
using EntityFramework.BulkInsert.Extensions;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public LoggedInUserInformation GetUserDetails(string id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    return (from u in db.Users
                            join b in db.Branches on u.BranchId equals b.Id
                            join ap in db.Applications on b.ApplicationId equals ap.Id
                            where (u.Id == id && u.Active && b.Active && ap.Active)
                            select new LoggedInUserInformation
                            {
                                AppId = ap.Id,
                                BranchId = b.Id,
                                UserId = u.Id,
                                Email = u.Email,
                                Name = u.Name
                            }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public SearchResultViewModel<T> GetUsers<T>(AccountSearchViewModel filter)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    if (filter.Select2)
                    {
                        IQueryable<Select2OptionModel> queryableResult = null;
                        if (filter.IsClient)
                        {
                            queryableResult = (from u in db.Users
                                               join r in db.Roles on u.Roles.Where(m => m.UserId == u.Id).Select(ri => ri.RoleId).FirstOrDefault() equals r.Id
                                               where (string.IsNullOrEmpty(filter.Id) || u.Id == filter.Id) &&
                                               (string.IsNullOrEmpty(filter.Name) || u.Name.Contains(filter.Name) || u.PhoneNumber.Contains(filter.Name)) &&
                                               (string.IsNullOrEmpty(filter.Email) || u.Email == filter.Email) &&
                                               (string.IsNullOrEmpty(filter.PhoneNumber) || u.PhoneNumber.Contains(filter.PhoneNumber))
                                               && r.Id == filter.RoleId
                                               || filter.GetAllResults
                                               select new Select2OptionModel
                                               {
                                                   id = u.Id,
                                                   text = u.Name + (string.IsNullOrEmpty(u.PhoneNumber) ? "" : (" - " + u.PhoneNumber))
                                               }).OrderBy(x => x.text).AsQueryable();

                        }
                        else
                        {
                            queryableResult = (from u in db.Users
                                               join st in db.StylistTreatments on u.Id equals st.StylistId into stu
                                               from st in stu.DefaultIfEmpty()
                                               join t in db.Treatments on st.TreatmentId equals t.Id into tstu
                                               from t in tstu.DefaultIfEmpty()
                                               join r in db.Roles on u.Roles.Where(m => m.UserId == u.Id).Select(ri => ri.RoleId).FirstOrDefault() equals r.Id

                                               where (string.IsNullOrEmpty(filter.Id) || u.Id == filter.Id) &&
                                                 (string.IsNullOrEmpty(filter.Name) || u.Name.Contains(filter.Name) || u.PhoneNumber.Contains(filter.Name)) &&
                                                 (string.IsNullOrEmpty(filter.Email) || u.Email == filter.Email) &&
                                                 (string.IsNullOrEmpty(filter.PhoneNumber) || u.PhoneNumber.Contains(filter.PhoneNumber)) &&
                                                 (string.IsNullOrEmpty(filter.RoleId) || u.Roles.Any(x => x.RoleId == filter.RoleId)) &&
                                                 (string.IsNullOrEmpty(filter.Role) || r.Name == filter.Role) &&
                                                 (filter.TreatmentId == new Guid() || t.Id == filter.TreatmentId) &&
                                                 (filter.AvoidTreatmentId == new Guid() || st.TreatmentId != filter.AvoidTreatmentId) &&
                                                 (u.Active == !filter.GetDeleted && (st == null || (!st.IsDeleted && st.Active))) || filter.GetAllResults

                                               select new Select2OptionModel
                                               {
                                                   id = u.Id,
                                                   text = u.Name + (string.IsNullOrEmpty(u.PhoneNumber) ? "" : (" - " + u.PhoneNumber))
                                               }).GroupBy(x => x.id)
                                                   .Select(m => new Select2OptionModel
                                                   {
                                                       id = m.FirstOrDefault().id,
                                                       text = m.FirstOrDefault().text
                                                   }).OrderBy(x => x.text).AsQueryable();
                        }
                        var result = queryableResult as IQueryable<T>;
                        return result.Paginate(filter, db);
                    }
                    else
                    {
                        var queryableResult = db.Users.Join(db.Addresses, u => u.AddressId, a => a.Id, (u, a) => new { u, a })
                                                      .GroupJoin(db.StylistTreatments, u => u.u.Id, st => st.StylistId, (u, st) => new { u = u.u, a = u.a, sts = st })
                                                      .SelectMany(x => x.sts.DefaultIfEmpty(), (x, y) => new { u = x.u, a = x.a, st = y })
                                                      .GroupJoin(db.Treatments, st => st.st.TreatmentId, t => t.Id, (st, t) => new { st = st.st, u = st.u, a = st.a, ts = t })
                                                      .SelectMany(x => x.ts.DefaultIfEmpty(), (x, y) => new { t = y, st = x.st, u = x.u, a = x.a, })
                                                      .GroupJoin(db.ClientPoints, u => u.u.Id, cp => cp.ClientId, (u, cp) => new { st = u.st, u = u.u, a = u.a, t = u.t, cps = cp })
                                                      .SelectMany(x => x.cps.DefaultIfEmpty(), (x, y) => new { t = x.t, st = x.st, u = x.u, a = x.a, cp = y })
                                                      .Where(m =>
                                                     (m.u.Id == filter.Id || string.IsNullOrEmpty(filter.Id)) &&
                                                     (m.u.Name.Contains(filter.Name) || string.IsNullOrEmpty(filter.Name)) &&
                                                     (m.u.Email == filter.Email || string.IsNullOrEmpty(filter.Email)) &&
                                                     (string.IsNullOrEmpty(filter.PhoneNumber) || m.u.PhoneNumber.Contains(filter.PhoneNumber)) &&
                                                     (m.u.Roles.Any(x => x.RoleId == filter.RoleId) || string.IsNullOrEmpty(filter.RoleId))
                                                     &&
                                                     (m.t.Id == filter.TreatmentId || filter.TreatmentId == new Guid()) &&
                                                     (m.st.TreatmentId != filter.AvoidTreatmentId || filter.AvoidTreatmentId == new Guid())
                                                     &&
                                                     (m.u.Active == !filter.GetDeleted && ((!m.st.IsDeleted && m.st.Active) || m.st == null) || filter.GetAllResults)
                                                     )
                                                     .Select(m => new RegisterViewModel
                                                     {
                                                         Address = new AddressViewModel
                                                         {
                                                             Id = m.a.Id,
                                                             Active = m.a.Active,
                                                             Area = m.a.Area,
                                                             City = m.a.City,
                                                             House = m.a.House,
                                                             Street = m.a.Street
                                                         },
                                                         ApplicaitonId = m.u.ApplicationId,
                                                         BranchId = m.u.BranchId,
                                                         DOB = m.u.DOB,
                                                         Email = m.u.Email,
                                                         Gender = m.u.Gender,
                                                         Id = m.u.Id,
                                                         Roles = m.u.Roles,
                                                         Name = m.u.Name,
                                                         PhoneNumber = m.u.PhoneNumber,
                                                         ImageUrl = m.u.ImageUrl,
                                                         Active = m.u.Active,
                                                         StylistId = string.IsNullOrEmpty(m.st.StylistId) ? "" : m.st.StylistId,
                                                         TreatmentId = m.t.Id != null ? m.t.Id : new Guid(),
                                                         TreatmentName = m.t.Name != null ? m.t.Name : "",
                                                         TreatmentDate = m.st.Date != null ? m.st.Date : new DateTime(),
                                                         Speciality = m.st != null ? m.st.Speciality : false,
                                                         Percentage = m.st != null ? m.st.PercentageCommission : 0,
                                                         Points = m.cp != null ? m.cp.Points : 0
                                                     }).OrderBy(x => x.Name).AsQueryable();

                        if (filter.GetGroupedRecords)
                            queryableResult = queryableResult.GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).OrderBy(x => x.Name);

                        IQueryable<T> result = queryableResult as IQueryable<T>;
                        return result.Paginate(filter, db);
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public string Edit(RegisterViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var oldUser = db.Users.Find(model.Id);
                    oldUser.Name = string.IsNullOrEmpty(model.Name) ? oldUser.Name : model.Name;
                    oldUser.DOB = model.DOB == new DateTime() ? oldUser.DOB : model.DOB;
                    oldUser.ImageUrl = (string.IsNullOrEmpty(model.ImageUrl) || model.ImageUrl == oldUser.ImageUrl) ? oldUser.ImageUrl : model.ImageUrl;
                    oldUser.Email = string.IsNullOrEmpty(model.Email) ? oldUser.Email : model.Email;
                    oldUser.Gender = model.Gender == 0 ? oldUser.Gender : model.Gender;
                    oldUser.PhoneNumber = model.PhoneNumber == "" ? oldUser.PhoneNumber : model.PhoneNumber;
                    oldUser.ReferenceId = string.IsNullOrEmpty(model.ReferenceId) ? oldUser.ReferenceId : Guid.Parse(model.ReferenceId);
                    if (model.Address != null)
                    {
                        var address = db.Addresses.Find(oldUser.AddressId);
                        address.Area = string.IsNullOrEmpty(model.Address.Area) ? address.Area : model.Address.Area;
                        address.City = string.IsNullOrEmpty(model.Address.City) ? address.City : model.Address.City;
                        address.House = string.IsNullOrEmpty(model.Address.House) ? address.House : model.Address.House;
                        address.Street = string.IsNullOrEmpty(model.Address.Street) ? address.Street : model.Address.Street;
                        db.Entry(address).State = EntityState.Modified;
                    }
                    db.Entry(oldUser).State = EntityState.Modified;

                    db.SaveChanges();
                    return oldUser.Id;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Edit User/Client", DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                throw;
            }
        }

        public string Delete(string id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var user = db.Users.Find(id);
                    user.Active = false;
                    var address = db.Addresses.Find(user.AddressId);
                    address.Active = false;
                    db.Entry(address).State = EntityState.Modified;
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                    return user.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public SearchResultViewModel<T> GetRoles<T>(RoleSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    if (search.Select2)
                    {
                        var queryableResult = db.Roles.Where(m => (m.Id == search.Id || string.IsNullOrEmpty(search.Id))
                                                               && (m.Name.Contains(search.Name) || string.IsNullOrEmpty(search.Name)))
                                                      .Select(m => new Select2OptionModel
                                                      {
                                                          id = m.Id,
                                                          text = m.Name
                                                      }).OrderBy(x => x.text).AsQueryable();
                        var result = queryableResult as IQueryable<T>;
                        return result.Paginate(search, db);

                    }
                    else
                    {
                        var queryableResult = db.Roles.Where(m => (m.Id == search.Id || string.IsNullOrEmpty(search.Id))
                                                              && (m.Name.Contains(search.Name) || string.IsNullOrEmpty(search.Name)))
                                                     .Select(m => new RoleViewModel
                                                     {
                                                         Id = m.Id,
                                                         Name = m.Name
                                                     }).OrderBy(x => x.Name).AsQueryable();
                        var result = queryableResult as IQueryable<T>;
                        return result.Paginate(search, db);
                    }

                }
            }
            catch (Exception ex)
            {

                return new SearchResultViewModel<T>();
            }
        }

        public bool AddClientsInBulk(List<ApplicationUser> clientList, List<IdentityUserRole> roleList, List<Address> addressList)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.Configuration.ValidateOnSaveEnabled = false;
                    using (var scope = db.Database.BeginTransaction())
                    {
                        try
                        {
                            db.BulkInsert(addressList);
                            db.BulkInsert(clientList);
                            db.BulkSaveChanges(operation => operation.BatchTimeout = 1000);
                            for (var i = 0; i < clientList.Count(); i++)
                            {
                                db.Database.ExecuteSqlCommand(@"Insert into AspNetUserRoles (RoleId , UserId) values ({0},{1})", roleList[i].RoleId, roleList[i].UserId);
                            }
                            db.SaveChanges();
                            scope.Commit();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            scope.Rollback();
                            string message = "Message " + ex.Message + "\n Stack Trace" + ex.StackTrace + "\n Inner Exception" + ex.InnerException;
                            new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Import Client", StackTrace = message, DateTime = DateTime.Now });
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string message = "Message " + ex.Message + "\n Stack Trace" + ex.StackTrace + "\n Inner Exception" + ex.InnerException;
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Import Client", StackTrace = message, DateTime = DateTime.Now });
                return false;
            }
        }
    }
}
