﻿using AutoMapper;
using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<T> GetApplications<T>(ApplicationSearchViewModel search)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    if (!(string.IsNullOrEmpty(search.Name)) || search.Select2)
                    {
                        IQueryable<Select2OptionModel> queryableResult = db.Applications.Where(m => m.Active && m.Name.Contains(search.Name)).Select(m => new Select2OptionModel { id = m.Id.ToString(), text = m.Name }).AsQueryable();
                        var result = queryableResult as IQueryable<T>;
                        return result.Paginate(search, db);
                    }

                    else
                    {
                        IQueryable<ApplicationViewModel> queryableResult = db.Applications
                            .Where(m => (m.Id == search.Id || search.Id == defaultGuid) && (m.Name.Contains(search.Name) || string.IsNullOrEmpty(search.Name)) && m.Active)
                            .Select(m=>new ApplicationViewModel
                            {
                                Id=m.Id, Active=m.Active, Name=m.Name
                            }).AsQueryable();
                        var result = queryableResult.OrderBy(x => x.Name) as IQueryable<T>;
                        return result.Paginate(search, db);
                         
                    }
                                      
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Create(ApplicationViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<ApplicationViewModel, Application>(); });
                    IMapper iMapper = config.CreateMapper();
                    var application = iMapper.Map<ApplicationViewModel, Application>(model);
                    var result = db.Applications.Add(application);
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Edit(Application model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Applications.Find(model.Id);
                    result.Name = model.Name;
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Delete(Application model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Applications.Find(model.Id);
                    result.Active = false;
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
