﻿using AutoMapper;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public PointViewModel GetPoint()
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.Points.FirstOrDefault();
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<Point, PointViewModel>(); });
                    var iMapper = config.CreateMapper();
                    var point = iMapper.Map<Point, PointViewModel>(record);
                    return point;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Create(PointViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    if (db.Points.Count() == 0)
                    {
                        var config = new MapperConfiguration(cfg => { cfg.CreateMap<PointViewModel, Point>(); });
                        var iMapper = config.CreateMapper();
                        var point = iMapper.Map<PointViewModel, Point>(model);
                        var result = db.Points.Add(point);
                        db.SaveChanges();
                        new Helpers.LogGeneration().CreateGeneralLog(db, new Log { Action = "Create Point", DateTime = DateTime.Now, AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), ModelId = model.Id.ToString() });
                        return result.Id;
                    }
                    else
                    {
                        new Helpers.LogGeneration().CreateExceptionLog(db, new Log { Action = "Point already exists", DateTime = DateTime.Now, AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), ModelId = model.Id.ToString() });
                        return new Guid();
                    }
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Create Points", AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                return new Guid();
            }
        }

        public Guid Edit(PointViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.Points.Find(model.Id);
                    record.Expiry = string.IsNullOrEmpty(model.Expiry) ? record.Expiry : model.Expiry;
                    record.Worth = model.Worth == 0 ?record.Worth: model.Worth;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    new Helpers.LogGeneration().CreateGeneralLog(db, new Log { Action = "Edit Point", DateTime = DateTime.Now, AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), ModelId = model.Id.ToString() });
                    return record.Id;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Edit Points", AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                return new Guid();
            }
        }
    }
}
