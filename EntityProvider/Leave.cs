﻿using AutoMapper;
using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<T> GetLeaves<T>(LeaveSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var queryableResult = (from l in db.Leaves
                                           join s in db.Users on l.StylistId equals s.Id
                                           where (
                                                 l.IsDeleted == search.IsDeleted
                                                 && (l.Id == search.Id || search.Id == new Guid())
                                                 && (
                                                        search.StartDate == new DateTime()
                                                        || search.IsContinuous
                                                        || (
                                                                l.IsContinuous==false &&
                                                                (
                                                                    (l.StartingDate >= search.StartDate && l.StartingDate <= search.EndDate)
                                                                    || (l.EndingDate >= search.StartDate && l.EndingDate <= search.EndDate)
                                                                )
                                                            )
                                                    )
                                                 && (search.IsContinuous == false || (search.IsContinuous && l.IsContinuous && l.StartingDate <= search.EndDate))
                                                 && (s.Id == search.Stylist.Id || string.IsNullOrEmpty(search.Stylist.Id))
                                                 && (s.Name.Contains(search.Stylist.Name) || string.IsNullOrEmpty(search.Stylist.Name))
                                                 && (l.Recursion == search.Recursion || search.Recursion == 0)
                                                 && (l.Recursion != search.RecursionBeside || search.RecursionBeside == 0)//Added to Get Only Recusrisve Leaves of any type
                                                 && (l.ReferenceId == search.ReferenceId || search.ReferenceId == new Guid())
                                                 )
                                           select new LeaveViewModel
                                           {
                                               Id = l.Id,
                                               Stylist = s,
                                               StartingDate = l.StartingDate,
                                               EndingDate = l.EndingDate,
                                               Status = l.Status,
                                               Type = l.Type,
                                               Note = l.Note,
                                               Recursion = l.Recursion,
                                               IsContinuous = l.IsContinuous,
                                               ReferenceId = l.ReferenceId,
                                               RecursiveDays = db.Leaves.Where(x => x.ReferenceId == l.ReferenceId).Select(y => y.RecursionParameter).ToList()
                                           }).GroupBy(x => x.ReferenceId).ToList().Select(m => m.FirstOrDefault()).AsQueryable();
                    var result = queryableResult as IQueryable<T>;
                    return result.Paginate(search, db);


                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Create(LeaveViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<LeaveViewModel, Leave>().ForMember(d => d.StylistId, s => s.MapFrom(x => x.Stylist.Id)); });
                    var iMapper = config.CreateMapper();
                    var leave = iMapper.Map<LeaveViewModel, Leave>(model);
                    leave.Stylist = null;
                    //leave.CreatedOn = DateTime.Now;
                    var result = db.Leaves.Add(leave);
                    db.SaveChanges();
                    new Helpers.LogGeneration().CreateGeneralLog(new ApplicationDbContext(), new Log { Action = "Create Leave", DateTime = DateTime.Now, ModelId = model.Id.ToString(), AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId() });
                    return result.Id;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Create Leave", DateTime = DateTime.Now, ModelId = "none", StackTrace = ex.StackTrace });
                return new Guid();
            }
        }

        public Guid Edit(LeaveViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.Leaves.Find(model.Id);
                    if (record != null)
                    {
                        record.StartingDate = model.StartingDate != new DateTime() ? model.StartingDate : record.StartingDate;
                        record.EndingDate = model.EndingDate != new DateTime() ? model.EndingDate : record.EndingDate;
                        record.Type = model.Type != 0 ? model.Type : record.Type;
                        record.Status = model.Status != 0 ? model.Status : record.Status;
                        record.Note = string.IsNullOrEmpty(model.Note) ? record.Note : model.Note;
                        record.IsContinuous = model.IsContinuous;
                        record.Recursion = model.Recursion != 0 ? model.Recursion : record.Recursion;
                        record.StylistId = string.IsNullOrEmpty(model.Stylist.Id) ? record.StylistId : model.Stylist.Id;
                        db.Entry(record).State = EntityState.Modified;
                        db.SaveChanges();
                        new Helpers.LogGeneration().CreateGeneralLog(new ApplicationDbContext(), new Log { Action = "Edit Leave", DateTime = DateTime.Now, ModelId = model.Id.ToString(), AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId() });
                        return record.Id;
                    }
                    return new Guid();
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Edit Leave", DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                throw;
            }
        }

        public Guid Delete(LeaveViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.Leaves.Where(x => x.ReferenceId == model.ReferenceId && x.IsDeleted == false).ToList();
                    foreach (var item in record)
                    {
                        db.Leaves.Remove(item);
                        db.SaveChanges();
                        new Helpers.LogGeneration().CreateGeneralLog(new ApplicationDbContext(), new Log { Action = "Delete Leave", DateTime = DateTime.Now, ModelId = model.Id.ToString(), AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId() });
                    }
                    return model.ReferenceId;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Delete Leave", DateTime = DateTime.Now, ModelId = "none", StackTrace = ex.StackTrace });
                throw;
            }
        }
    }
}
