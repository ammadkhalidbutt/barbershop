﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Enums;
using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<T> GetApprovals<T>(AppointmentApprovalSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var queryableResult = db.Appointments.Join(db.Treatments, a => a.TreatmentId, t => t.Id, (a, t) => new { a = a, t = t })
                                               .Join(db.Users, a => a.a.StylistId, s => s.Id, (a, s) => new { a = a, s = s })
                                               .Join(db.Users, a => a.a.a.ClientId, c => c.Id, (a, c) => new { a = a, c = c })
                                               .GroupJoin(db.AppointmentApprovals, a => a.a.a.a.Id, ap => ap.AppointmentId, (a, ap) => new { a = a, ap = ap.FirstOrDefault() })
                                               .GroupJoin(db.Users, ap => ap.ap.ApproverId, apr => apr.Id, (ap, apr) => new { a = ap.a.a.a.a, s = ap.a.a.s, c = ap.a.c, t = ap.a.a.a.t, ap = ap.ap, apr = apr.FirstOrDefault() })
                                               .Where(m =>
                                                     (m.a.Id == search.AppointmentId || search.AppointmentId == new Guid()) &&
                                                     (m.ap.ApproverId == search.ApproverId || string.IsNullOrEmpty(search.ApproverId)) &&
                                                     (m.ap.AppointmentId == search.ApprovedAppointmentId || search.ApprovedAppointmentId == new Guid()))
                                               .Select(m => new AppointmentApprovalViewModel
                                               {
                                                   Appointment = m.a,
                                                   Approver = m.apr,
                                                   Approval = m.ap,
                                                   Client = m.c,
                                                   Stylist = m.s,
                                                   Treatment = m.t
                                               }).AsQueryable().OrderBy(m => m.Appointment.BookingDate);

                    var result = queryableResult as IQueryable<T>;
                    new Helpers.LogGeneration().CreateGeneralLog(new ApplicationDbContext(), new Log { ModelId = search.AppointmentId.ToString(), Action = "Search", DateTime = DateTime.Now, AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId() });
                    return result.Paginate(search, db);

                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { ModelId = search.AppointmentId.ToString(), Action = "Search", DateTime = DateTime.Now, StackTrace = ex.StackTrace });
                throw;
            }
        }

        public Guid Create(AppointmentApprovalViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.AppointmentApprovals.Add(model.Approval);
                    if (model.Status == AppointmentStatus.Approved)
                    {
                        var appointment = db.Appointments.Find(model.AppointmentId);
                        appointment.Status = AppointmentTreatmentStatus.Paused;
                        db.Entry(appointment).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return result.AppointmentId;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Edit(AppointmentApprovalViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.AppointmentApprovals.Find(model.AppointmentId, model.Approver.Id);
                    record.Status = model.Approval.Status != 0 ? model.Approval.Status : record.Status;
                    record.LastUpdatedOn = model.LastUpdatedOn;
                    db.Entry(record).State = EntityState.Modified;
                    var appointment = db.Appointments.Find(model.AppointmentId);
                    appointment.Status = model.Appointment.Status;
                    db.Entry(appointment).State = EntityState.Modified;
                    db.SaveChanges();
                    return record.AppointmentId;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Edit appointment approval", DateTime = DateTime.Now, ModelId = model.AppointmentId.ToString(), StackTrace = ex.StackTrace });
                throw;
            }
        }

        public Guid Delete(AppointmentApprovalViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.AppointmentApprovals.Find(model.AppointmentId);
                    var appointment = db.Appointments.Find(model.AppointmentId);
                    appointment.Active = false;
                    appointment.IsDeleted = true;
                    record.Active = false;
                    db.Entry(record).State = EntityState.Modified;
                    db.Entry(appointment).State = EntityState.Modified;
                    db.SaveChanges();
                    return record.AppointmentId;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
