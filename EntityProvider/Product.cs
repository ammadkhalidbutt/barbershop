﻿using AutoMapper;
using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public ProductViewModel GetProduct(Guid id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.Products.Include(m => m.Category).Include(m=>m.Tax).FirstOrDefault(x => x.Id == id);
                    if (record != null)
                    {
                        var config = new MapperConfiguration(cfg => { cfg.CreateMap<Product, ProductViewModel>(); });
                        var iMapper = config.CreateMapper();
                        var Product = iMapper.Map<Product, ProductViewModel>(record);
                        return Product;
                    }
                    return new ProductViewModel();
                }
            }
            catch (Exception ex)
            {

                return new ProductViewModel();
            }
        }

        public SearchResultViewModel<T> GetProducts<T>(ProductSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Products
                        .Include(m=>m.Tax)
                        .Include(m=>m.Category)
                        .Where(m =>
                                    m.IsDeleted == false
                                    &&
                                    (string.IsNullOrEmpty(search.Name) || m.Name == search.Name)
                                    &&
                                    (search.ProductIds.Count() == 0 || search.ProductIds.Contains(m.Id))
                                    &&
                                    (search.CategoryId == new Guid() || search.CategoryId == m.CategoryId)
                                   )
                                   .Select(m => new ProductViewModel
                                   {
                                       Id = m.Id,
                                       AddedBy = m.AddedBy,
                                       Active = m.Active,
                                       CategoryId = m.CategoryId,
                                       Date = m.Date,
                                       ImageUrl = m.ImageUrl,
                                       Description = m.Description,
                                       Price = m.Price,
                                       Name = m.Name,
                                       TaxId = m.TaxId,
                                       Tax = m.Tax,
                                       Category = m.Category
                                   }).AsQueryable();
                    if (search.Select2)
                    {
                        return (result.Select(m => new Select2OptionModel
                        {
                            id = m.Id.ToString(),
                            text = m.Name
                        }) as IQueryable<T>).Paginate(search, db);
                    }
                    else
                    {
                        return (result as IQueryable<T>).Paginate(search, db);
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Create(ProductViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<ProductViewModel, Product>(); });
                    var iMapper = config.CreateMapper();
                    var Product = iMapper.Map<ProductViewModel, Product>(model);
                    Product.Category = null;
                    var result = db.Products.Add(Product);
                    db.SaveChanges();
                    new Helpers.LogGeneration().CreateGeneralLog(db, new Log { Action = "Create Product", DateTime = DateTime.Now, AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), ModelId = model.Id.ToString() });
                    return result.Id;

                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Create Products", AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                return new Guid();
            }
        }

        public Guid Edit(ProductViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.Products.Find(model.Id);
                    record.Name = string.IsNullOrEmpty(model.Name) ? record.Name : model.Name;
                    record.Price = model.Price == 0 ? record.Price : model.Price;
                    record.ImageUrl = string.IsNullOrEmpty(model.ImageUrl) ? record.ImageUrl : model.ImageUrl;
                    record.LastUpdatedOn = model.UpdatedOn;
                    record.UpdatedBy = model.UpdatedBy;
                    record.Active = model.Active;
                    record.IsDeleted = model.IsDeleted;
                    record.TaxId = model.TaxId;
                    record.Description = string.IsNullOrEmpty(model.Description) ? record.Description : model.Description;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    new Helpers.LogGeneration().CreateGeneralLog(db, new Log { Action = "Edit Product", DateTime = DateTime.Now, AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), ModelId = model.Id.ToString() });
                    return record.Id;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Edit Product", AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                return new Guid();
            }
        }

        public bool Remove(ProductViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.Products.Find(model.Id);
                    db.Products.Remove(record);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
