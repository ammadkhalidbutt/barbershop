﻿using AutoMapper;
using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public Guid Create(StylistCommissionViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<StylistCommissionViewModel, StylistCommission>(); });
                    var iMapper = config.CreateMapper();
                    var stylistCommission = iMapper.Map<StylistCommissionViewModel, StylistCommission>(model);
                    var result = db.StylistCommissions.Add(stylistCommission);
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Edit(StylistCommissionViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.StylistCommissions.Find(model.Id);
                    record.Amount = model.Amount;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    return record.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Delete(StylistCommissionViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.StylistCommissions.Find(model.Id);
                    record.IsDeleted = true;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    return record.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
