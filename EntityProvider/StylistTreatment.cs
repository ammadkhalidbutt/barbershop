﻿using AutoMapper;
using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<T> Read<T>(StylistTreatmentSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var queryableResult = db.StylistTreatments.Join(db.Treatments, st=>st.TreatmentId, t=>t.Id, (st,t)=>new { st,t})
                        .Where(m => m.st.IsDeleted == false && m.t.IsDeleted == false                       
                                    && (m.st.TreatmentId == search.TreatmentId || search.TreatmentId == new Guid())
                                    && (m.st.StylistId == search.StylistId || string.IsNullOrEmpty(search.StylistId))  
                                    && m.st.Active && !m.st.IsDeleted)
                                                     .Select(m => new StylistTreatmentViewModel
                                                     {
                                                         Active = m.st.Active,
                                                         Date = m.st.Date,
                                                         IsDeleted = m.st.IsDeleted,
                                                         PercentageCommission = m.st.PercentageCommission,
                                                         Speciality = m.st.Speciality,
                                                         TreatmentId = m.st.TreatmentId,
                                                         TreatmentName = m.t.Name,
                                                         StylistId = m.st.StylistId
                                                     }).OrderBy(m => m.Date).AsQueryable();
                    var result = queryableResult as IQueryable<T>;
                    return result.Paginate(search, db);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Create(StylistTreatmentViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                   
                    var check = db.StylistTreatments.Find(model.TreatmentId, model.StylistId);
                    if (check != null)
                    {
                        check.Active = true;
                        check.IsDeleted = false;
                        db.Entry(check).State = EntityState.Modified;
                        db.SaveChanges();
                        return check.TreatmentId;
                    }
                    else
                    {
                        var config = new MapperConfiguration(cfg => { cfg.CreateMap<StylistTreatmentViewModel, StylistTreatment>(); });
                        var iMapper = config.CreateMapper();
                        var stylistTreatment = iMapper.Map<StylistTreatmentViewModel, StylistTreatment>(model);
                        var result = db.StylistTreatments.Add(stylistTreatment);
                        db.SaveChanges();
                        return result.TreatmentId;
                    }
                 
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }

        public Guid Edit(StylistTreatmentViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.StylistTreatments.Where(m => m.TreatmentId == model.TreatmentId && m.StylistId == model.StylistId && m.Active && !m.IsDeleted).FirstOrDefault();
                    record.PercentageCommission = model.PercentageCommission;
                    record.Speciality = model.Speciality;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    return record.TreatmentId;
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }

        public Guid Delete(StylistTreatmentViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.StylistTreatments.Where(m => m.TreatmentId == model.TreatmentId && m.StylistId == model.StylistId && m.Active && !m.IsDeleted).FirstOrDefault();
                    record.Active = false;
                    record.IsDeleted = true;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    return record.TreatmentId;
                }
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }
    }
}
