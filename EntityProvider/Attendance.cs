﻿using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public string ScanClients(string id, string action)
        {
            using(ApplicationDbContext db = new ApplicationDbContext())
            {
                try
                {
                    new Helpers.LogGeneration().CreateGeneralLog(db, new Log { Action = action, AddedBy = "Client", DateTime = DateTime.Now, ModelId = id });
                    return id;
                }
                catch (Exception)
                {
                    return "";
                }
            }
        }

        public List<UserAttendanceViewModel> GetPresentUsers(string role, string action)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    if (action.Contains("Check"))
                    {
                        var query = db.Database.SqlQuery<UserAttendanceViewModel>(@"with lg1 as (select l.*, ROW_NUMBER()over(partition by ModelId order by DateTime DESC) as rn from GeneralLogs as l
where (UPPER(Action) = UPPER('Checked In') or UPPER(Action) = UPPER('Checked Out')) and AddedBy = {0} and DateTime >= {1})
select u.Id,u.ImageUrl, u.Name,u.PhoneNumber, u.Email as Email, tb.AddedBy, tb.Action, tb.DateTime as AttendanceDate from
((select * from lg1 where rn = 1) as tb 
join AspNetUsers as u
on tb.ModelId = u.Id) where tb.Action = {2} and u.Active = 1", role, DateTime.Now.Date, action).ToList();
                        return query;
                    }
                    else if(action.Contains("All Clients"))
                    {
                        var query = db.Database.SqlQuery<UserAttendanceViewModel>(@"select u.Id,u.ImageUrl, u.Name,u.PhoneNumber, tb.Id, u.Email as Email, tb.AddedBy, tb.Action, tb.DateTime as AttendanceDate from
((select * from GeneralLogs where AddedBy = {0} and (UPPER(Action) = UPPER('checked out') or UPPER(Action) = UPPER('checked in')) and DateTime >= {1}) as tb 
join AspNetUsers as u
on tb.ModelId = u.Id) where u.Active = 1", role, DateTime.Now.Date).ToList();
                      return query;
                    }
                    else if(action.Contains("All Stylists"))
                    {
                        var query = db.Database.SqlQuery<UserAttendanceViewModel>(@"select u.Id,u.ImageUrl, u.Name, tb.Id, u.Email as Email, tb.AddedBy, tb.Action, tb.DateTime as AttendanceDate from
((select * from GeneralLogs where AddedBy = {0} and (UPPER(Action) = UPPER('log out') or UPPER(Action) = UPPER('login')) and DateTime >= {1}) as tb 
join AspNetUsers as u
on tb.ModelId = u.Id) where u.Active = 1", role, DateTime.Now.Date).ToList();
                        return query;
                    }
                    else
                    {
                        var query = db.Database.SqlQuery<UserAttendanceViewModel>
                        (
                            @"with lg1 as 
                                (
                                    select 
                                    l.*, 
                                    ROW_NUMBER()over(partition by ModelId order by DateTime DESC) as rn 
                                    from GeneralLogs as l

                                    where (Action = 'Login' or Action = 'Log Off') and AddedBy = {0} and DateTime >= {1}
                                )
                            select u.Id,u.ImageUrl, u.Name, tb.ModelId as Email, tb.AddedBy, tb.Action, tb.DateTime as AttendanceDate 
                            from
                            (
                                (select * from lg1 where rn = 1) as tb 
                                join AspNetUsers as u on tb.ModelId = u.Email
                            )

                                where 
                                tb.Action = {2} and u.Active = 1", role, DateTime.Now.Date, action
                       ).ToList();

                        return query;
                    }
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = string.Format("Fetching present {0}", role), StackTrace = ex.ToString() });
                return new List<UserAttendanceViewModel>();
            }
        }

       


    }
}
