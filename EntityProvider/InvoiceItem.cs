﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.ViewModels;
using Models;
using Models.Identity;
using Helpers;
using AutoMapper;
using System.Data.Entity;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<T> GetInvoices<T>(InvoiceItemSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var queryableResult = db.InvoiceItems.Where(m => !m.IsDeleted
                                            && (m.Id == search.Id || search.Id == new Guid())
                                            && (m.DateTime == search.DateTime || search.DateTime == new DateTime())
                                            && (m.AppointmentId == search.Appointment.Id || search.Appointment.Id == new Guid() || search.Appointment == null)
                                            && (m.AddedBy == search.PersonAddingRecord.Id || string.IsNullOrEmpty(search.PersonAddingRecord.Id) || search.PersonAddingRecord == null))
                                            .Select(m => new InvoiceItemViewModel
                                            {
                                                PersonAddingRecord = new ApplicationUser { Id = m.AddedBy },
                                                Appointment = new AppointmentViewModel { Id = m.AppointmentId.Value },
                                                DateTime = m.DateTime,
                                                DiscountedPrice = m.DiscountedPrice,
                                                Id = m.Id,
                                                IsDeleted = m.IsDeleted,
                                                ItemPrice = m.ItemPrice,
                                                PersonUpdatingRecord = new ApplicationUser { Id = m.UpdatedBy },
                                                UpdatedOn = m.UpdatedOn
                                            }).OrderBy(m => m.DateTime).AsQueryable();
                    var result = queryableResult as IQueryable<T>;
                    return result.Paginate(search, db);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Create(InvoiceItemViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<InvoiceItemViewModel, InvoiceItem>().ForMember(x => x.AddedBy, m => m.MapFrom(n => n.PersonAddingRecord.Id))
                                                                          .ForMember(d => d.AppointmentId, m => m.MapFrom(s => s.Appointment.Id))
                                                                          .ForMember(d => d.Appointment, m => m.Ignore())
                                                                          .ForMember(d => d.InvoiceRecord, m => m.Ignore())
                                                                          .ForMember(d => d.PersonAddingRecord, m => m.Ignore())
                                                                          .ForMember(d => d.PersonUpdatingRecord, m => m.Ignore())
                                                                          .ForMember(x => x.UpdatedBy, m => m.MapFrom(n => n.PersonUpdatingRecord.Id))
                                                                          .ForMember(x => x.InvoiceId, m => m.MapFrom(n => n.InvoiceRecord.Id));
                    });
                    var iMapper = config.CreateMapper();
                    var invoiceRecord = iMapper.Map<InvoiceItemViewModel, InvoiceItem>(model);
                    //var invoiceRecord = new InvoiceItem
                    //{
                    //    AddedBy = model.PersonAddingRecord.Id,
                    //    AmountRecieved = model.AmountRecieved.Value,
                    //    AppointmentId = model.Appointment.Id,
                    //    DateTime = model.DateTime,
                    //    DiscountedPrice = model.DiscountedPrice.Value,
                    //    Id = model.Id,
                    //    InvoiceId = model.InvoiceRecord.Id,
                    //    IsDeleted = model.IsDeleted,
                    //    TreatmentPrice = model.TreatmentPrice,
                    //    UpdatedBy = model.PersonUpdatingRecord.Id,
                    //    UpdatedOn = model.UpdatedOn
                    //};
                    var record = db.InvoiceItems.Add(invoiceRecord);
                    db.SaveChanges();
                    return record.Id;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Create Invoice Item", DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                throw;
            }
        }

        public Guid Edit(InvoiceItemViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.InvoiceItems.Find(model.Id);
                    if (!record.IsDeleted)
                    {
                        record.DiscountedPrice = model.DiscountedPrice != null ? model.DiscountedPrice.Value : record.DiscountedPrice;
                        record.UpdatedBy = model.PersonUpdatingRecord.Id;
                        record.UpdatedOn = model.UpdatedOn;
                        db.Entry(record).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    new Helpers.LogGeneration().CreateGeneralLog(new ApplicationDbContext(), new Log { Action = "Edit Invoice Item", DateTime = DateTime.Now, ModelId = model.Id.ToString(), AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId() });
                    return record.Id;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Edit Invoice Item", DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                throw;
            }
        }

        public Guid Delete(InvoiceItemViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.InvoiceItems.Find(model.Id);
                    if (!record.IsDeleted)
                    {
                        record.IsDeleted = true;
                        db.Entry(record).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    new Helpers.LogGeneration().CreateGeneralLog(new ApplicationDbContext(), new Log { Action = "Delete Invoice Item", DateTime = DateTime.Now, ModelId = model.Id.ToString(), AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId() });
                    return record.Id;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Delete Invoice Item", DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                throw;
            }
        }
    }
}
