﻿using AutoMapper;
using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<T> GetCurrency<T>(CurrencySearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var queryableResult = db.Currencies.Where(
                                            m => (m.Id == new Guid() || m.Id == search.Id) &&
                                                (string.IsNullOrEmpty(search.Name) || m.Name == search.Name))
                                            .Select(m => new CurrencyViewModel
                                            {
                                                Id = m.Id,
                                                Name = m.Name,
                                                Value = m.Value
                                            }).AsQueryable();
                    var result = queryableResult as IQueryable<T>;
                    return result.Paginate(search, db);
                }

            }
            catch (Exception ex)
            {

                return new SearchResultViewModel<T>();
            }
        }

        public Guid Create(CurrencyViewModel model)
        {
            try
            {
               using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<CurrencyViewModel, Currency>(); });
                    var iMapper = config.CreateMapper();
                    var currency = iMapper.Map<CurrencyViewModel, Currency>(model);
                    var result = db.Currencies.Add(currency);
                    if(result.Id != new Guid())
                    {
                        return result.Id;
                    }
                    else
                    {
                        return new Guid();
                    }
                } 
            }
            catch (Exception ex)
            {
                return new Guid();
            }
        }

        public Guid Edit(CurrencyViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.Currencies.Find(model.Id);
                    if(record != null)
                    {
                        record.Name = string.IsNullOrEmpty(model.Name) ? record.Name : model.Name;
                        record.Value = string.IsNullOrEmpty(model.Value) ? record.Value : model.Value;
                        db.Entry(record).State = EntityState.Modified;
                        db.SaveChanges();
                        return record.Id;
                    }
                    return new Guid();
                }
            }
            catch (Exception ex)
            {

                return new Guid();
            }
        }
    }
}
