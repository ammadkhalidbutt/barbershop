﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Enums;
using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<DiscountViewModel> GetDiscounts(DiscountSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    IQueryable<DiscountViewModel> queryableResult = db.Discounts.Where(m => m.IsActive == true
                                                                    && m.IsDeleted == false
                                                                    &&
                                                                    (m.EntityType != (int)DiscountEntityTypeCatalog.Customer
                                                                    || m.EntityId == search.ClientId)
                                                        )
                                                       .Select(m => new DiscountViewModel
                                                       {
                                                           Id = m.Id,
                                                           Name = m.Name,
                                                           PromoCode = m.PromoCode,
                                                           Entity = new BriefEntityViewModel()
                                                           {
                                                               Id = m.EntityId,
                                                           },
                                                           EntityType = (DiscountEntityTypeCatalog)m.EntityType,
                                                           Priority = m.Priority,
                                                           StartDateTime = m.StartDateTime,
                                                           EndDateTime = m.EndDateTime,
                                                           DiscountPercentage = m.DiscountPercentage,
                                                           ImageUrl = m.ImageUrl,
                                                           Description = m.Description,
                                                           NotificationTitle = m.NotificationTitle,
                                                           NotificationBody = m.NotificationBody,
                                                           IsActive = m.IsActive,
                                                           IsApproved = m.IsApproved,
                                                           CreatedOn = m.CreatedOn,
                                                           UpdatedOn = m.UpdatedOn,
                                                           CreatedBy = m.CreatedBy,
                                                           UpdatedBy = m.UpdatedBy,
                                                       }).Distinct().OrderByDescending(x => x.CreatedOn).AsQueryable();



                    var paginatedResult = queryableResult.Paginate(search, db);
                    if (paginatedResult.ResultList != null && paginatedResult.ResultList.Count > 0)
                    {
                        foreach (DiscountViewModel item in paginatedResult.ResultList.Cast<DiscountViewModel>().ToList())
                        {
                            item.Entity.Name = GetEntityName(item.EntityType.ToString(), item.Entity.Id);
                        }
                    }
                    return paginatedResult;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public DiscountViewModel GetDiscount(Guid id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var discount = db.Discounts
                        .Where(m => m.Id == id)
                         .Select(m => new DiscountViewModel
                         {
                             Id = m.Id,
                             Name = m.Name,
                             PromoCode = m.PromoCode,
                             Entity = new BriefEntityViewModel()
                             {
                                 Id = m.EntityId,
                             },
                             EntityType = (DiscountEntityTypeCatalog)m.EntityType,
                             Priority = m.Priority,
                             StartDateTime = m.StartDateTime,
                             EndDateTime = m.EndDateTime,
                             DiscountPercentage = m.DiscountPercentage,
                             ImageUrl = m.ImageUrl,
                             Description = m.Description,
                             NotificationTitle = m.NotificationTitle,
                             NotificationBody = m.NotificationBody,
                             IsActive = m.IsActive,
                             IsApproved = m.IsApproved,
                             CreatedOn = m.CreatedOn,
                             UpdatedOn = m.UpdatedOn,
                             CreatedBy = m.CreatedBy,
                             UpdatedBy = m.UpdatedBy,
                         }).FirstOrDefault();
                    discount.Entity.Name = GetEntityName(discount.EntityType.ToString(), discount.Entity.Id);
                    return discount;

                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Create(DiscountViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<DiscountViewModel, Discount>(); });
                    var iMapper = config.CreateMapper();
                    var Discount = iMapper.Map<DiscountViewModel, Discount>(model);
                    var result = db.Discounts.Add(Discount);
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Edit(DiscountViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Discounts.Find(model.Id);
                    result.Name = model.Name;
                    result.DiscountPercentage = model.DiscountPercentage;
                    result.ImageUrl = !string.IsNullOrEmpty(model.ImageUrl) ? model.ImageUrl : result.ImageUrl;
                    if (model.Entity != null)
                        result.EntityId = model.Entity.Id;
                    result.EntityType = (int)model.EntityType;
                    result.Priority = (int)model.EntityType;
                    result.StartDateTime = model.StartDateTime;
                    result.EndDateTime = model.EndDateTime;
                    result.Description = model.Description;
                    result.NotificationTitle = model.NotificationTitle;
                    result.NotificationBody = model.NotificationBody;
                    result.IsActive = model.IsActive;
                    db.Entry(result).State = EntityState.Modified;

                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Delete(DiscountViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Discounts.Find(model.Id);
                    result.IsDeleted = true;
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public bool Approve(DiscountViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Discounts.Find(model.Id);
                    result.IsApproved = true;
                    db.Entry(result).State = EntityState.Modified;
                    return db.SaveChanges() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Dictionary<string, double> GetDiscountedItems(Guid customerId, string promoCode, List<Guid> selectedTreatments)
        {
            DiscountViewModel discount = null;
            Dictionary<string, double> discountedItems = new Dictionary<string, double>();
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    discount = db.Discounts
                       .Where(m => m.PromoCode == promoCode && m.StartDateTime <= DateTime.Now && m.EndDateTime >= DateTime.Now)
                        .Select(m => new DiscountViewModel
                        {
                            Id = m.Id,
                            Name = m.Name,
                            PromoCode = m.PromoCode,
                            Entity = new BriefEntityViewModel()
                            {
                                Id = m.EntityId,
                            },
                            EntityType = (DiscountEntityTypeCatalog)m.EntityType,
                            Priority = m.Priority,
                            StartDateTime = m.StartDateTime,
                            EndDateTime = m.EndDateTime,
                            DiscountPercentage = m.DiscountPercentage,
                            ImageUrl = m.ImageUrl,
                            Description = m.Description,
                            IsActive = m.IsActive,
                            IsApproved = m.IsApproved,
                            CreatedOn = m.CreatedOn,
                            UpdatedOn = m.UpdatedOn,
                            CreatedBy = m.CreatedBy,
                            UpdatedBy = m.UpdatedBy,
                        }).FirstOrDefault();
                    if (discount == null)
                        throw new KnownException("No Such Promo Code Exist");
                }
                discountedItems = ApplyPromo(discount, selectedTreatments, customerId);

            }
            catch (KnownException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {

                throw;
            }
            return discountedItems;
        }
        private Dictionary<string, double> ApplyPromo(DiscountViewModel discount, List<Guid> selectedTreatments, Guid customerId)
        {
            Dictionary<string, double> discountedItems = new Dictionary<string, double>();
            if (discount.EntityType == DiscountEntityTypeCatalog.Category)
            {
                var treatments = GetCategoriesItems(discount.Entity.Id, selectedTreatments);
                foreach (var treatment in treatments)
                {
                    discountedItems.Add(treatment.ToString(), discount.DiscountPercentage);
                }
            }
            else if (discount.EntityType == DiscountEntityTypeCatalog.Customer && discount.Entity.Id == customerId)
            {
                foreach (var treatment in selectedTreatments)
                {
                    discountedItems.Add(treatment.ToString(), discount.DiscountPercentage);
                }
            }
            else if (discount.EntityType == DiscountEntityTypeCatalog.Item)
            {
                var treatment = selectedTreatments.Where(x => x == discount.Entity.Id).FirstOrDefault();
                if (treatment != null)
                {
                    discountedItems.Add(treatment.ToString(), discount.DiscountPercentage);
                }
            }
            else if (discount.EntityType == DiscountEntityTypeCatalog.All)
            {
                foreach (var treatment in selectedTreatments)
                {
                    discountedItems.Add(treatment.ToString(), discount.DiscountPercentage);
                }
            }
            return discountedItems;
        }
        private List<Guid> GetCategoriesItems(Guid categoryId, List<Guid> selectedTreatments)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                return db.Treatments.Join(db.TreatmentCategories, t => t.CategoryId, tc => tc.Id, (t, tc) => new { t, tc }).Where(m => m.tc.Id == categoryId && selectedTreatments.Contains(m.t.Id)).Select(x => x.t.Id).ToList();
            }
        }


    }
}
