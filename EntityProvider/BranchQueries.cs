﻿using AutoMapper;
using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<T> GetBranches<T>(BranchSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    if (search.Select2)
                    {
                        SearchResultViewModel<Select2OptionModel> searchResult = new SearchResultViewModel<Select2OptionModel>();
                        IQueryable<Select2OptionModel> QueryableResult = db.Branches.Where(m => m.Active
                                  && (string.IsNullOrEmpty(search.Name) || m.Name.Contains(search.Name)))
                                .Select(m => new Select2OptionModel
                                {
                                    id = m.Id.ToString(),
                                    text = m.Name
                                }).AsQueryable();
                       var result = QueryableResult as IQueryable<T>;
                        return result.Paginate(search, db);
                    }
                    else
                    {
                        SearchResultViewModel<T> searchResult = new SearchResultViewModel<T>();
                        IQueryable<BranchViewModel> QueryableResult = (from b in db.Branches
                                                                       join a in db.Addresses on b.AddressId equals a.Id
                                                                       join c in db.Currencies on b.CurrencyId equals c.Id
                                                                       where (b.Active
                                                                       && (search.BranchId == defaultGuid || search.BranchId == b.Id)
                                                                       && (search.ApplicationId == defaultGuid || search.ApplicationId == b.ApplicationId)
                                                                       && (string.IsNullOrEmpty(search.Name) || b.Name.Contains(search.Name)))
                                                                       select new BranchViewModel
                                                                       {
                                                                           Id = b.Id,
                                                                           Name = b.Name,
                                                                           AddressId = b.AddressId,
                                                                           Active = b.Active,
                                                                           Address = new AddressViewModel()
                                                                           {
                                                                               Id = a.Id,
                                                                               Area = a.Area,
                                                                               City = a.City,
                                                                               House = a.House,
                                                                               Street = a.Street,
                                                                               Active = a.Active,
                                                                           },
                                                                           Currency = new CurrencyViewModel
                                                                           {
                                                                               Id = c.Id,
                                                                               Name = c.Name,
                                                                               Value = c.Value
                                                                           },
                                                                           ApplicationId = b.ApplicationId,
                                                                           OpeningTime = b.OpeningTime,
                                                                           ClosingTime = b.ClosingTime,
                                                                           VAT = b.VAT,
                                                                           LogoUrl = b.ImageUrl
                                                                       }).AsQueryable();
                        QueryableResult = QueryableResult.OrderBy(x => x.Name);
                        var result = QueryableResult as IQueryable<T>;
                        return result.Paginate(search, db);
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Guid Create(BranchViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<BranchViewModel, Branch>().ForMember(destination=>destination.ImageUrl, opt=>opt.MapFrom(source=>source.LogoUrl)); });
                    IMapper iMapper = config.CreateMapper();
                    var branch = iMapper.Map<BranchViewModel, Branch>(model);
                    var result = db.Branches.Add(branch);
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Edit(BranchViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Branches.Find(model.Id);
                    result.Name = string.IsNullOrEmpty(model.Name)? result.Name: model.Name;
                    result.ImageUrl = string.IsNullOrEmpty(model.LogoUrl) ? result.ImageUrl : model.LogoUrl;
                    result.OpeningTime = model.OpeningTime == new TimeSpan() ? result.OpeningTime : model.OpeningTime;
                    result.ClosingTime = model.ClosingTime == new TimeSpan() ? result.ClosingTime : model.ClosingTime;
                    result.VAT = model.VAT;
                    if (model.Address != null)
                    {
                        var address = db.Addresses.Find(result.AddressId);
                        address.Area = string.IsNullOrEmpty(model.Address.Area) ? address.Area : model.Address.Area;
                        address.City = string.IsNullOrEmpty(model.Address.City) ? address.City : model.Address.City;
                        address.House = string.IsNullOrEmpty(model.Address.House) ? address.House : model.Address.House;
                        address.Street = string.IsNullOrEmpty(model.Address.Street) ? address.Street : model.Address.Street;
                        db.Entry(address).State = EntityState.Modified;
                    }
                    if(model.Currency != null)
                    {
                        var currency = db.Currencies.Find(model.Currency.Id);
                        currency.Name = string.IsNullOrEmpty(model.Name) ? currency.Name : model.Currency.Name;
                        currency.Value = string.IsNullOrEmpty(model.Currency.Value) ? currency.Value : model.Currency.Value;
                        db.Entry(currency).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Delete(Branch model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Branches.Find(model.Id);
                    result.Active = false;
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
