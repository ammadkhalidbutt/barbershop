﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<T> GetTreatments<T>(TreatmentSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    if (search.Select2)
                    {
                        //if (string.IsNullOrEmpty(search.StylistId))
                        //{
                            var queryableResult = db.Treatments
                                                                .Join(db.TreatmentCategories, t => t.CategoryId, tc => tc.Id, (t, tc) => new { t, tc })
                                                                .GroupJoin(db.Users, t => t.t.AddedBy, u => u.Id, (tr, us) => new { t = tr.t, tc = tr.tc, u = us.FirstOrDefault() })
                                                                .GroupJoin(db.StylistTreatments, t => t.t.Id, st => st.TreatmentId, (t, st) => new { t = t.t, u = t.u, tc = t.tc, sts = st }).SelectMany(x => x.sts.DefaultIfEmpty(), (x, y) => new { t = x.t, st = y, u = x.u, tc = x.tc })
                                                                .GroupJoin(db.Users, st => st.st.StylistId, s => s.Id, (st, s) => new { st = st.st, s = s.FirstOrDefault(), t = st.t, u = st.u, tc = st.tc })
                                                                .Where(m => m.t.Active == true && m.t.IsDeleted == false &&
                                                                (string.IsNullOrEmpty(search.Name) || m.t.Name.Contains(search.Name)) &&
                                                                (search.Id == new Guid() || m.t.Id == search.Id) &&
                                                                (search.CategoryId == new Guid()|| m.t.CategoryId == search.CategoryId) &&
                                                                (string.IsNullOrEmpty(search.UserId) || m.t.AddedBy == search.UserId) &&
                                                                (string.IsNullOrEmpty(search.UserName)|| m.u.Name.Contains(search.UserName)) &&
                                                                    (
                                                                        string.IsNullOrEmpty(search.StylistName) 
                                                                        || (m.s.Name.Contains(search.StylistName) && (m.st == null || m.st.IsDeleted == false))
                                                                    ) &&
                                                                    (
                                                                        string.IsNullOrEmpty(search.StylistId)
                                                                        || (m.s.Id == search.StylistId && (m.st == null || m.st.IsDeleted == false))
                                                                    )
                                                                 )
                                                                .Select(m => new Select2OptionModel
                                                                {
                                                                    id = m.t.Id.ToString(),
                                                                    text = m.t.Name
                                                                }).Distinct().OrderBy(x => x.text).AsQueryable();
                            var result = queryableResult as IQueryable<T>;
                            return result.Paginate(search, db);
                        //}
                        //else
                        //{
                        //    var queryableResult = db.Treatments
                        //                                        .Join(db.StylistTreatments, t => t.Id, st => st.TreatmentId, (t, st) => new { t = t, st = st })
                        //                                        .Join(db.Users, st => st.st.StylistId, s => s.Id, (st, s) => new { st = st.st, s = s, t = st.t })
                        //                                        .Where(m => m.t.Active == true && m.t.IsDeleted == false &&
                        //                                        (m.st == null || m.st.IsDeleted == false) &&
                        //                                        (m.t.Name.Contains(search.Name) || string.IsNullOrEmpty(search.Name)) &&
                        //                                        (m.t.Id == search.Id || search.Id == new Guid()) &&
                        //                                        (m.t.CategoryId == search.CategoryId || search.CategoryId == new Guid()) &&
                        //                                        (m.t.AddedBy == search.UserId || string.IsNullOrEmpty(search.UserId)) &&
                        //                                        (m.s.Name.Contains(search.StylistName) || string.IsNullOrEmpty(search.StylistName)) &&
                        //                                        (m.s.Id == search.StylistId && m.st.StylistId == search.StylistId || string.IsNullOrEmpty(search.StylistId)))
                        //                                        .Select(m => new Select2OptionModel
                        //                                        {
                        //                                            id = m.t.Id.ToString(),
                        //                                            text = m.t.Name
                        //                                        }).OrderBy(x => x.text).Distinct().AsQueryable();
                        //    var result = queryableResult as IQueryable<T>;
                        //    return result.Paginate(search, db);
                        //}
                    }
                    else
                    {

                        IQueryable<TreatmentViewModel> queryableResult = db.Treatments.Include(m=>m.Tax)
                                                                 .Join(db.TreatmentCategories, t => t.CategoryId, tc => tc.Id, (t, tc) => new { t, tc })
                                                                 .GroupJoin(db.Users, t => t.t.AddedBy, u => u.Id, (tr, us) => new { t = tr.t, tc = tr.tc, u = us.FirstOrDefault() })
                                                                 .GroupJoin(db.StylistTreatments, t => t.t.Id, st => st.TreatmentId, (t, st) => new { t = t.t, u = t.u, tc = t.tc, sts = st }).SelectMany(x => x.sts.DefaultIfEmpty(), (x, y) => new { t = x.t, st = y, u = x.u, tc = x.tc })
                                                                 .GroupJoin(db.Users, st => st.st.StylistId, s => s.Id, (st, s) => new { st = st.st, s = s.FirstOrDefault(), t = st.t, u = st.u, tc = st.tc })
                                                                 .Where(m => m.t.Active == true && m.t.IsDeleted == false &&
                                                                 (m.st == null || m.st.IsDeleted == false) &&
                                                                 (m.t.Name.Contains(search.Name) || string.IsNullOrEmpty(search.Name)) &&
                                                                 (m.t.Id == search.Id || search.Id == new Guid()) &&
                                                                 (m.t.CategoryId == search.CategoryId || search.CategoryId == new Guid()) &&
                                                                 (m.tc.Name.Contains(search.CategoryName) || string.IsNullOrEmpty(search.CategoryName)) &&
                                                                 (m.t.AddedBy == search.UserId || string.IsNullOrEmpty(search.UserId)) &&
                                                                 (m.u.Name.Contains(search.UserName) || string.IsNullOrEmpty(search.UserName)) &&
                                                                 (m.s.Name.Contains(search.StylistName) || string.IsNullOrEmpty(search.StylistName)) &&
                                                                 (m.s.Id == search.StylistId || string.IsNullOrEmpty(search.StylistId)))
                                                           .Select(m => new TreatmentViewModel
                                                           {
                                                               Id = m.t.Id,
                                                               Active = m.t.Active,
                                                               AddedBy = m.t.AddedBy,
                                                               ApproximateTime = m.t.ApproximateTime,
                                                               Name = m.t.Name,
                                                               Points = m.t.Points,
                                                               Price = (float)m.t.Price,
                                                               Description = m.t.Description,
                                                               ImageUrl = m.t.ImageUrl,
                                                               Date = m.t.Date,
                                                               LastUpdatedOn = m.t.LastUpdatedOn,
                                                               CategoryId = m.t.CategoryId,
                                                               Category = m.tc,
                                                               Tax = m.t.Tax,
                                                               TaxId = m.t.TaxId
                                                           }).Distinct().OrderBy(x => x.Name).AsQueryable();
                        

                        var result = queryableResult as IQueryable<T>;
                        return result.Paginate(search, db);
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public TreatmentViewModel GetTreatment(Guid id)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    return db.Treatments.Include(m => m.Category).Include(m => m.Tax)
                        .Where(m => m.Id == id)
                        .Select(m => new TreatmentViewModel
                        {
                            Id = m.Id,
                            Name = m.Name,
                            Category = m.Category,
                            CategoryId = m.CategoryId,
                            AddedBy = m.AddedBy,
                            ApproximateTime = m.ApproximateTime,
                            Date = m.Date,
                            Description = m.Description,
                            ImageUrl = m.ImageUrl,
                            LastUpdatedOn = m.LastUpdatedOn,
                            Points = m.Points,
                            Price = (float)m.Price,
                            Tax = m.Tax,
                            TaxId = m.TaxId
                        })
                        .FirstOrDefault();
                        
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Create(TreatmentViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<TreatmentViewModel, Treatment>(); });
                    var iMapper = config.CreateMapper();
                    var treatment = iMapper.Map<TreatmentViewModel, Treatment>(model);

                    var result = db.Treatments.Add(treatment);
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Edit(TreatmentViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Treatments.Find(model.Id);
                    result.Name = string.IsNullOrEmpty(model.Name) ? result.Name : model.Name;
                    result.Points = model.Points.Equals(default(int)) ? result.Points : model.Points;
                    result.Price = model.Price.Equals(default(int)) ? result.Price : model.Price;
                    result.ApproximateTime = model.ApproximateTime.Equals(default(int)) ? result.ApproximateTime : model.ApproximateTime;
                    result.ImageUrl = !string.IsNullOrEmpty(model.ImageUrl) ? model.ImageUrl : result.ImageUrl;
                    result.LastUpdatedOn = model.LastUpdatedOn != new DateTime() ? model.LastUpdatedOn : DateTime.Now;
                    result.CategoryId = model.CategoryId != new Guid() ? model.CategoryId : result.CategoryId;
                    result.Description = string.IsNullOrEmpty(model.Description) ? result.Description : model.Description;
                    db.Entry(result).State = EntityState.Modified;

                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Delete(TreatmentViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Treatments.Find(model.Id);
                    result.Active = false;
                    result.IsDeleted = true;
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Treatment GetTreatmentDetails(Guid id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Treatments.Find(id);
                    if (result != null)
                    {
                        return result;
                    }
                    return new Treatment();
                }
            }
            catch (Exception ex)
            {

                return new Treatment();
            }
        }

        public void FixCategoryId()
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var categories = db.Treatments.Where(m => m.Active && m.CategoryId == null).ToList();
                    //categories.ForEach(m => m.CategoryId == );
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
