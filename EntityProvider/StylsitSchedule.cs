﻿using AutoMapper;
using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<T> GetStylistSchedule<T>(StylistScheduleSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var queryableResult = db.StylistSchedules.Join(db.Users, s => s.StylistId, u => u.Id, (s, u) => new { s, u })
                                          .Where(m =>
                                            (m.s.Day == search.Day || search.Day == 0) && m.s.IsDeleted == search.IsDeleted
                                            && (m.s.Id == search.Id || search.Id == new Guid())
                                            && (m.s.StylistId == search.Stylist.Id || string.IsNullOrEmpty(search.Stylist.Id))
                                            && (m.u.Name.Contains(search.Stylist.Name) || string.IsNullOrEmpty(search.Stylist.Name))
                                            && (m.s.StartingTime == search.StartingTime || search.StartingTime == new TimeSpan()))
                                          .Select(m => new StylistScheduleViewModel
                                          {
                                              Id = m.s.Id,
                                              Day = m.s.Day,
                                              EndingTime = m.s.EndingTime,
                                              StartingTime = m.s.StartingTime,
                                              Stylist = m.u,
                                          }).AsQueryable().OrderBy(x => x.StartingTime);
                    var result = queryableResult as IQueryable<T>;
                    return result.Paginate(search, db);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Create(StylistScheduleViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<StylistScheduleViewModel, StylistSchedule>().ForMember(d => d.StylistId, s => s.MapFrom(x => x.Stylist.Id)); });
                    var iMapper = config.CreateMapper();
                    var schedule = iMapper.Map<StylistScheduleViewModel, StylistSchedule>(model);
                    schedule.Stylist = null;
                    var result = db.StylistSchedules.Add(schedule);
                    db.SaveChanges();
                    new Helpers.LogGeneration().CreateGeneralLog(new ApplicationDbContext(), new Log { Action = "Create Schedule", DateTime = DateTime.Now, ModelId = model.Id.ToString(), AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId() });
                    return result.Id;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Create Schedule", DateTime = DateTime.Now, ModelId = "none", StackTrace = ex.StackTrace });
                throw;
            }
        }

        public Guid Edit(StylistScheduleViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.StylistSchedules.Find(model.Id);
                    if (!record.IsDeleted)
                    {
                        record.StartingTime = model.StartingTime == new TimeSpan() ? record.StartingTime : model.StartingTime;
                        record.EndingTime = model.EndingTime == new TimeSpan() ? record.EndingTime : model.EndingTime;
                        record.Day = model.Day == 0 ? record.Day : model.Day;
                        db.Entry(record).State = EntityState.Modified;
                        db.SaveChanges();
                        new Helpers.LogGeneration().CreateGeneralLog(new ApplicationDbContext(), new Log { Action = "Edit Schedule", DateTime = DateTime.Now, ModelId = model.Id.ToString(), AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId() });
                        return record.Id;

                    }
                    else
                    {
                        return new Guid();
                    }

                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Edit Schedule", DateTime = DateTime.Now, ModelId = "none", StackTrace = ex.StackTrace });
                throw;
            }
        }

        public Guid Delete(StylistScheduleViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var records = db.StylistSchedules.Where(m => m.StylistId == model.Stylist.Id).ToList();
                    var recordId = new Guid();
                    foreach (var record in records)
                    {
                        if (!record.IsDeleted)
                        {
                            record.IsDeleted = true;
                            db.Entry(record).State = EntityState.Modified;
                            db.SaveChanges();
                            new Helpers.LogGeneration().CreateGeneralLog(new ApplicationDbContext(), new Log { Action = "Delete Schedule", DateTime = DateTime.Now, ModelId = model.Id.ToString(), AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId() });
                            recordId = record.Id;
                        }
                    }
                    return recordId;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Delete Schedule", DateTime = DateTime.Now, ModelId = "none", StackTrace = ex.StackTrace });
                throw;
            }
        }
    }
}
