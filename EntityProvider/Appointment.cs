﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Enums;
using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;
using Itenso.TimePeriod;
using System.Data.Entity.Core.Objects;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<T> GetAppointments<T>(AppointmentSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    if (search.GetDetailedResult)
                    {
                        var result = GetDetailedResult(search, db) as IQueryable<T>;
                        return result.Paginate(search, db);
                    }
                    else if (search.TimeSlotsResult)
                    {
                        var result = TimeSlotsResult(search, db) as IQueryable<T>;
                        return result.Paginate(search, db);
                    }

                    else if (search.Select2)
                    {
                        var result = Select2Result(search, db) as IQueryable<T>;
                        return result.Paginate(search, db);
                    }
                    else if (search.VerifyTime)
                    {
                        var result = VerifyTime(search, db) as IQueryable<T>;
                        return result.Paginate(search, db);
                    }

                    else
                    {
                        var result = GetRegularSearchResult(search, db) as IQueryable<T>;
                        return result.Paginate(search, db);
                    }

                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Search Appointment", DateTime = DateTime.Now, ModelId = search.Id.ToString(), StackTrace = ex.StackTrace });
                throw;
            }
        }

        public Guid Create(AppointmentViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<AppointmentViewModel, Appointment>().ForMember(a => a.Status, x => x.MapFrom(av => av.AppointmentTreatmentStatus))
                                                                          .ForMember(a => a.BranchId, x => x.MapFrom(av => av.Branch.Id));
                    });
                    var iMapper = config.CreateMapper();
                    var appointment = iMapper.Map<AppointmentViewModel, Appointment>(model);
                    appointment.Client = null;
                    appointment.Treatment = null;
                    appointment.Stylist = null;
                    appointment.Branch = null;
                    var approval = new AppointmentApproval
                    {
                        Active = true,
                        Appointment = appointment,
                        AppointmentId = model.Id.Value,
                        ApprovalDateTime = DateTime.Now,
                        ApproverId = string.IsNullOrEmpty(model.Approver.Id) ? model.StylistId : model.Approver.Id,
                        LastUpdatedOn = DateTime.Now,
                        Status = model.Status == 0 ? AppointmentStatus.Processing : model.Status
                    };
                    var result = db.AppointmentApprovals.Add(approval);
                    db.SaveChanges();
                    return result.AppointmentId;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Edit(AppointmentViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.Appointments.Find(model.Id);
                    if (record.TreatmentId != model.Treatment.Id && model.Treatment.Id != new Guid())
                    {
                        var treatment = db.Treatments.Include(m => m.Tax).FirstOrDefault(m => m.Id == model.TreatmentId);
                        model.Treatment = new TreatmentViewModel
                        {
                            Id = treatment.Id,
                            AddedBy = treatment.AddedBy,
                            ApproximateTime = treatment.ApproximateTime,
                            Category = treatment.Category,
                            CategoryId = treatment.CategoryId,
                            Date = treatment.Date,
                            Description = treatment.Description,
                            ImageUrl = treatment.ImageUrl,
                            IsDeleted = treatment.IsDeleted,
                            Name = treatment.Name,
                            Points = treatment.Points,
                            LastUpdatedOn = treatment.LastUpdatedOn,
                            Price = (float)treatment.Price,
                            Tax = treatment.Tax,
                            TaxId = treatment.TaxId,
                            Active = treatment.Active
                        };
                    }
                    else
                    {
                        var treatment = db.Treatments.Include(m => m.Tax).FirstOrDefault(m => m.Id == record.TreatmentId);
                        model.Treatment = new TreatmentViewModel
                        {
                            Id = treatment.Id,
                            AddedBy = treatment.AddedBy,
                            ApproximateTime = treatment.ApproximateTime,
                            Category = treatment.Category,
                            CategoryId = treatment.CategoryId,
                            Date = treatment.Date,
                            Description = treatment.Description,
                            ImageUrl = treatment.ImageUrl,
                            IsDeleted = treatment.IsDeleted,
                            Name = treatment.Name,
                            Points = treatment.Points,
                            LastUpdatedOn = treatment.LastUpdatedOn,
                            Price = (float)treatment.Price,
                            Tax = treatment.Tax,
                            TaxId = treatment.TaxId,
                            Active = treatment.Active
                        };
                    }
                    record.AppointmentStartingTime = model.AppointmentStartingTime != new DateTime() ? model.AppointmentStartingTime : record.AppointmentStartingTime;
                    record.AppointmentEndingTime = model.AppointmentEndingTime != new DateTime() ? model.AppointmentEndingTime : record.AppointmentEndingTime;
                    record.StylistId = string.IsNullOrEmpty(model.StylistId) ? record.StylistId : model.StylistId;
                    record.TreatmentId = model.TreatmentId != new Guid() ? model.TreatmentId : record.TreatmentId;
                    record.Discount = model.Discount;
                    record.LastUpdatedOn = model.LastUpdatedOn;
                    if (record.Status != AppointmentTreatmentStatus.Ended)
                    {
                        record.Status = model.AppointmentTreatmentStatus != 0 ? model.AppointmentTreatmentStatus : record.Status;
                    }
                    record.ActualStartingTime = model.ActualStartingTime != new DateTime() ? model.ActualStartingTime : record.ActualStartingTime;
                    record.ActualEndingTime = model.ActualEndingTime != new DateTime() ? model.ActualEndingTime : record.ActualEndingTime;
                    //record.TimeElapsed = model.ElapsedTime + record.TimeElapsed;
                    record.TimeElapsed = model.ElapsedTime;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    var approval = db.AppointmentApprovals.Where(m => m.AppointmentId == model.Id).FirstOrDefault();
                    if (approval != null)
                    {
                        approval.Status = model.Status != 0 ? model.Status : approval.Status;
                        approval.LastUpdatedOn = DateTime.Now;
                        db.Entry(approval).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return record.Id;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Edit appointment", DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                throw;
            }
        }

        public Guid Delete(AppointmentViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.Appointments.Find(model.Id);
                    record.LastUpdatedOn = model.LastUpdatedOn;
                    record.IsDeleted = true;
                    record.Active = false;
                    db.Entry(record).State = EntityState.Modified;
                    var approval = db.AppointmentApprovals.Where(m => m.AppointmentId == model.Id).FirstOrDefault();
                    if (approval != null)
                    {
                        approval.LastUpdatedOn = model.LastUpdatedOn;
                        approval.Status = AppointmentStatus.Cancelled;
                        db.Entry(approval).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return record.Id;
                }
            }
            catch (Exception ex)
            {

                return new Guid();
            }
        }

        public AppointmentTimeSlotsViewModel GetFreeSlots(AppointmentSlotSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var scheduledTimings = new List<AppointmentViewModel>();

                    var branch = GetBranches<BranchViewModel>(new BranchSearchViewModel()).ResultList.First();
                    var day = (int)(search.DesiredDate.DayOfWeek);
                    var freeSlots = new List<AppointmentTimeSlot>();
                    var appointedSlots = new List<AppointmentTimeSlot>();
                    var saloonOpeiningTime = search.DesiredDate.Date.Add(branch.OpeningTime);
                    var saloonClosingTime = search.DesiredDate.Date.Add(branch.ClosingTime);
                    var lastAppointmentEndingTime = saloonOpeiningTime;
                    var stylist = GetUsers<RegisterViewModel>(new AccountSearchViewModel { Id = search.SytlistId }).ResultList.GroupBy(x => x.Email).Select(m => m.FirstOrDefault()).FirstOrDefault();

                    var approvedAppointments = GetAppointments<AppointmentViewModel>(new AppointmentSearchViewModel { BookingDate = search.DesiredDate.Date, StylistId = search.SytlistId, ClientId = search.ClientId, DiscardAppointment = search.DiscardAppointment, TimeSlotsResult = true, GetAllSlots = search.GetAllSlots }).ResultList;
                    approvedAppointments.ForEach(x => x.SlotStatus = SlotStatus.Appointment);
                    var schedules = GetStylistSchedule<StylistScheduleViewModel>(new StylistScheduleSearchViewModel { Stylist = new ApplicationUser { Id = search.SytlistId }, Day = (WeekDays)Enum.Parse(typeof(WeekDays), day.ToString()) }).ResultList;

                    scheduledTimings = schedules.Select(m => new AppointmentViewModel { AppointmentStartingTime = search.DesiredDate.Date.Add(m.StartingTime), AppointmentEndingTime = search.DesiredDate.Date.Add(m.EndingTime), SlotStatus = SlotStatus.Schedule }).ToList();

                    //foreach (var schedule in schedules)
                    //{
                    //    scheduledTimings.Add(new AppointmentViewModel { AppointmentStartingTime = search.DesiredDate.Date.Add(schedule.StartingTime), AppointmentEndingTime = search.DesiredDate.Date.Add(schedule.EndingTime), SlotStatus = SlotStatus.Schedule });
                    //    //approvedAppointments.Add(new AppointmentViewModel { AppointmentStartingTime = schedule.StartingTime < branch.OpeningTime ? search.DesiredDate.Date.Add(branch.OpeningTime) : search.DesiredDate.Date.Add(schedule.StartingTime), AppointmentEndingTime = schedule.EndingTime > branch.ClosingTime ? search.DesiredDate.Date.Add(branch.ClosingTime) : search.DesiredDate.Date.Add(schedule.EndingTime), SlotStatus = SlotStatus.Schedule });
                    //}
                    var leaves = new List<LeaveViewModel>();
                    var nonRecursiveLeaves = GetLeaves<LeaveViewModel>(new LeaveSearchViewModel { Stylist = new ApplicationUser { Id = search.SytlistId }, StartDate = search.DesiredDate.Date, Recursion = Recursion.Never }).ResultList;
                    var recursiveLeaves = GetLeaves<LeaveViewModel>(new LeaveSearchViewModel { Stylist = new ApplicationUser { Id = search.SytlistId }, StartDate = search.DesiredDate.Date, RecursionBeside = Recursion.Never }).ResultList;
                    var recursiveContinuousLeaves = GetLeaves<LeaveViewModel>(new LeaveSearchViewModel { Stylist = new ApplicationUser { Id = search.SytlistId }, StartDate = search.DesiredDate.Date, IsContinuous = true, RecursionBeside = Recursion.Never }).ResultList;
                    leaves.AddRange(recursiveContinuousLeaves);
                    leaves.AddRange(recursiveLeaves);
                    leaves.AddRange(nonRecursiveLeaves);
                    GetRecursiveLeavesForToday(leaves, approvedAppointments, search.DesiredDate, branch);




                    //TimePeriodCollection BookedTimePeriods = new TimePeriodCollection();
                    //BookedTimePeriods.AddAll(approvedAppointments.Select(b => new TimeRange() { Start = b.Start, End = b.End }).ToList());



                    TimePeriodCollection ScheduledTimePeriods = new TimePeriodCollection();
                    TimePeriodCollection BranchTimnings = new TimePeriodCollection();
                    ScheduledTimePeriods.AddAll(scheduledTimings.Where(m => m.SlotStatus == SlotStatus.Schedule).Select(b => new TimeRange() { Start = b.Start, End = b.End }).ToList());
                    BranchTimnings.Add(new TimeRange() { Start = search.DesiredDate.Add(branch.OpeningTime), End = search.DesiredDate.Add(branch.ClosingTime) });
                    TimeGapCalculator<TimeRange> gapCalculator = new TimeGapCalculator<TimeRange>(new TimeCalendar());
                    ITimePeriodCollection freeTimes = gapCalculator.GetGaps(ScheduledTimePeriods, BranchTimnings);
                    var unscheduledTime = freeTimes.Select(m => new AppointmentViewModel
                    {
                        AppointmentStartingTime = m.Start,
                        AppointmentEndingTime = m.End,
                        SlotStatus = SlotStatus.UnScheduled
                    });
                    approvedAppointments.AddRange(unscheduledTime);
                    approvedAppointments = approvedAppointments.OrderBy(x => x.AppointmentStartingTime).ThenByDescending(x => x.AppointmentEndingTime).ToList();

                    if (approvedAppointments.Count() > 0)
                    {
                        foreach (var appointment in approvedAppointments)
                        {
                            var appointedSlot = new AppointmentTimeSlot();
                            appointedSlot.Id = appointment.Id.Value;
                            appointedSlot.StartingTime = appointment.AppointmentStartingTime;
                            appointedSlot.EndingTime = appointment.AppointmentEndingTime;
                            appointedSlot.Status = appointment.SlotStatus;
                            appointedSlot.Stylist = stylist.Name;
                            if (appointedSlot.Status == SlotStatus.Appointment)
                            {
                                appointedSlot.Client = appointment.Client.Name;
                                appointedSlot.Treatment = appointment.Treatment.Name;
                                appointedSlot.Description = appointment.Treatment.Description;
                            }
                            if (appointedSlot.Status == SlotStatus.Leave)
                            {
                                appointedSlot.Description = db.Leaves.Where(x => x.ReferenceId == appointedSlot.Id).FirstOrDefault().Note;
                            }

                            appointedSlots.Add(appointedSlot);
                            //var comparisonSlots = freeSlots.Where(x => x.StartingTime >= appointment.AppointmentStartingTime).ToList();
                            //if (comparisonSlots.Count() > 0)
                            //{
                            //    foreach (var comparisonSlot in comparisonSlots)
                            //    {
                            //        freeSlots.Remove(comparisonSlot);
                            //    }
                            //}
                            var freeSlot = new AppointmentTimeSlot();
                            if (((appointment.AppointmentStartingTime - lastAppointmentEndingTime).TotalMinutes >= search.TimeRequiredForTreatment))
                            {
                                freeSlot.StartingTime = lastAppointmentEndingTime;
                                freeSlot.EndingTime = appointment.AppointmentStartingTime;
                                freeSlot.Stylist = stylist.Name;
                                freeSlot.Status = SlotStatus.Available;
                                lastAppointmentEndingTime = appointment.AppointmentEndingTime;
                                freeSlots.Add(freeSlot);
                            }
                            else if (appointment.AppointmentEndingTime.Ticks >= lastAppointmentEndingTime.Ticks)
                            {
                                lastAppointmentEndingTime = appointment.AppointmentEndingTime;
                            }
                        }
                        if (((saloonClosingTime - lastAppointmentEndingTime).TotalMinutes >= search.TimeRequiredForTreatment))
                        {
                            var freeSlot = new AppointmentTimeSlot();
                            freeSlot.StartingTime = lastAppointmentEndingTime;
                            freeSlot.EndingTime = saloonClosingTime;
                            freeSlot.Stylist = stylist.Name;
                            freeSlot.Status = SlotStatus.Available;
                            freeSlots.Add(freeSlot);
                        }
                    }
                    else
                    {
                        var freeSlot = new AppointmentTimeSlot { StartingTime = saloonOpeiningTime, EndingTime = saloonClosingTime, Stylist = stylist.Name, Status = SlotStatus.Available };
                        freeSlots.Add(freeSlot);
                    }
                    return new AppointmentTimeSlotsViewModel { AppointedSlots = appointedSlots, FreeSlots = freeSlots };
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<InvoiceRecordViewModel> GetUnpaidAppointments()
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    //var check = db.Appointments.Join(db.Treatments, a => a.TreatmentId, t => t.Id, (a, t) => new { a, t })
                    //                       .Join(db.Users, a => a.a.ClientId, c => c.Id, (a, c) => new { a, c })
                    //                       .Select(m => new AppointmentViewModel
                    //                       {
                    //                           Client = m.c,
                    //                           Treatment = m.a.t,
                    //                           BookingDate = m.a.a.BookingDate
                    //                       }).ToList().GroupBy(grp => grp.Client.Id)
                    //                       .Select(m => new InvoiceRecordViewModel
                    //                       {
                    //                           Client = m.FirstOrDefault().Client,
                    //                           GrandTotal = m.Sum(x=>x.Treatment.Price),
                    //                           DateTime = m.Max().BookingDate
                    //                       }).ToList();

                    var check = (from a in db.Appointments
                                 join b in db.Branches on a.BranchId equals b.Id
                                 join ta in db.AppointmentApprovals on a.Id equals ta.AppointmentId
                                 join t in db.Treatments on a.TreatmentId equals t.Id
                                 join c in db.Users on a.ClientId equals c.Id
                                 where
                                 ta.Status == AppointmentStatus.Approved
                                 && a.Status == AppointmentTreatmentStatus.Ended
                                 select new AppointmentViewModel
                                 {
                                     Treatment = new TreatmentViewModel
                                     {
                                         Id = t.Id,
                                         AddedBy = t.AddedBy,
                                         ApproximateTime = t.ApproximateTime,
                                         Category = t.Category,
                                         CategoryId = t.CategoryId,
                                         Date = t.Date,
                                         Description = t.Description,
                                         ImageUrl = t.ImageUrl,
                                         IsDeleted = t.IsDeleted,
                                         Name = t.Name,
                                         Points = t.Points,
                                         LastUpdatedOn = t.LastUpdatedOn,
                                         Price = (float)t.Price,
                                         TaxId = t.TaxId,
                                         Active = t.Active
                                     },
                                     Client = c,
                                     Branch = b,
                                     AppointmentEndingTime = a.AppointmentEndingTime,
                                     BookingDate = a.BookingDate,
                                     StylistId = a.StylistId,
                                     AppointmentTreatmentStatus = a.Status
                                 }).GroupBy(grp => grp.Client.Id).ToList();

                    var result = check.Select(m => new InvoiceRecordViewModel
                    {
                        Client = m.FirstOrDefault().Client,
                        AppointmentStatus = m.Any(x => x.AppointmentTreatmentStatus == AppointmentTreatmentStatus.Paused ||
                        x.AppointmentTreatmentStatus == AppointmentTreatmentStatus.Pending ||
                        x.AppointmentTreatmentStatus == AppointmentTreatmentStatus.Started) ?
                                AppointmentTreatmentStatus.Pending : AppointmentTreatmentStatus.Ended,
                        Total = m.Sum(s => s.Treatment.Price),
                        Vat = m.Select(x => x.Branch.VAT).FirstOrDefault(),
                        DateTime = m.FirstOrDefault().AppointmentEndingTime,
                        StylistId = m.FirstOrDefault().StylistId
                    }).OrderByDescending(x => x.DateTime).ToList();


                    return result;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Read unpaid appointment", DateTime = DateTime.Now, ModelId = "none", StackTrace = ex.StackTrace });
                throw;
            }
        }

        public IQueryable<AppointmentViewModel> GetDetailedResult(AppointmentSearchViewModel search, ApplicationDbContext db)
        {
            try
            {

                var queryableResult = (from a in db.Appointments
                                       join b in db.Branches on a.BranchId equals b.Id
                                       join ba in db.Addresses on b.AddressId equals ba.Id
                                       join c in db.Users on a.ClientId equals c.Id
                                       join ca in db.Addresses on c.AddressId equals ca.Id
                                       join s in db.Users on a.StylistId equals s.Id
                                       join t in db.Treatments on a.TreatmentId equals t.Id
                                       join tax in db.Taxes on t.Tax.Id equals tax.Id
                                       join ap in db.AppointmentApprovals on a.Id equals ap.AppointmentId
                                       join apr in db.Users on ap.ApproverId equals apr.Id

                                       where a.Active && !a.IsDeleted
                                           && (a.Id == search.Id || search.Id == new Guid())
                                           && (c.Id == search.ClientId || string.IsNullOrEmpty(search.ClientId))
                                           && (a.Status == search.AppointmentTreatmentStatus || search.AppointmentTreatmentStatus == 0)
                                           && (a.AppointmentStartingTime >= search.BookingDate && a.AppointmentEndingTime <= search.BookingDateEnding || search.BookingDate == new DateTime())
                                           && (ap.Status == search.Status || search.Status == 0)
                                       select new AppointmentViewModel
                                       {
                                           Active = a.Active,
                                           ApplicationId = a.ApplicationId,
                                           Branch = b,
                                           BranchAddress = ba,
                                           ClientId = a.ClientId,
                                           BookingDate = a.BookingDate,
                                           AppointmentStartingTime = a.ActualStartingTime,
                                           AppointmentEndingTime = a.ActualEndingTime,
                                           IsDeleted = a.IsDeleted,
                                           AppointmentTreatmentStatus = a.Status,
                                           Status = ap.Status,
                                           StylistId = a.StylistId,
                                           TreatmentId = a.TreatmentId,
                                           TotalTime = t.ApproximateTime,
                                           TreatmentTax = tax.Percentage,
                                           Id = a.Id,
                                           Client = c,
                                           ClientAddress = ca,
                                           Stylist = s,
                                           Treatment = new TreatmentViewModel
                                           {
                                               Id = t.Id,
                                               AddedBy = t.AddedBy,
                                               ApproximateTime = t.ApproximateTime,
                                               Category = t.Category,
                                               CategoryId = t.CategoryId,
                                               Date = t.Date,
                                               Description = t.Description,
                                               ImageUrl = t.ImageUrl,
                                               IsDeleted = t.IsDeleted,
                                               Name = t.Name,
                                               Points = t.Points,
                                               LastUpdatedOn = t.LastUpdatedOn,
                                               Price = (float)t.Price,
                                               Tax = tax,
                                               TaxId = t.TaxId,
                                               Active = t.Active
                                           },
                                           Approver = apr
                                       }).AsQueryable();

                queryableResult = queryableResult.OrderBy(x => x.AppointmentStartingTime);
                return queryableResult;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public IQueryable<AppointmentViewModel> TimeSlotsResult(AppointmentSearchViewModel search, ApplicationDbContext db)
        {
            try
            {
                IQueryable<AppointmentViewModel> queryableResult;
                if (search.GetAllSlots)
                {
                    queryableResult = (from a in db.Appointments
                                       join b in db.Branches on a.BranchId equals b.Id
                                       join c in db.Users on a.ClientId equals c.Id
                                       join s in db.Users on a.StylistId equals s.Id
                                       join t in db.Treatments on a.TreatmentId equals t.Id
                                       join ap in db.AppointmentApprovals on a.Id equals ap.AppointmentId
                                       join apr in db.Users on ap.ApproverId equals apr.Id
                                       where a.Active && !a.IsDeleted
                                           && (c.Name.Contains(search.ClientName) || string.IsNullOrEmpty(search.ClientName))
                                           && (s.Id == search.StylistId || string.IsNullOrEmpty(search.StylistId))
                                           && (c.Id == search.ClientId || string.IsNullOrEmpty(search.ClientId))
                                           && (s.Name.Contains(search.StylistName) || string.IsNullOrEmpty(search.StylistName))
                                           && (apr.Id == search.ApproverId || string.IsNullOrEmpty(search.ApproverId))
                                           && (apr.Name.Contains(search.ApproverName) || string.IsNullOrEmpty(search.ApproverName))
                                           && (a.Id == search.Id || search.Id == new Guid())
                                           && (a.Id != search.DiscardAppointment || search.DiscardAppointment == new Guid())
                                           && (a.Status == search.AppointmentTreatmentStatus || search.AppointmentTreatmentStatus == 0)
                                           && (ap.Status == search.Status || search.Status == 0 && ap.Status != AppointmentStatus.Cancelled)
                                           && (t.Name.Contains(search.TreatmentName) || string.IsNullOrEmpty(search.TreatmentName))
                                           && (a.AppointmentStartingTime >= search.BookingDate && a.AppointmentEndingTime <= search.BookingDateEnding || search.BookingDate == new DateTime())
                                           && ((DbFunctions.TruncateTime(a.AppointmentStartingTime) <= DbFunctions.TruncateTime(search.StartingDateTime) && DbFunctions.TruncateTime(a.AppointmentEndingTime) >= DbFunctions.TruncateTime(search.StartingDateTime)) || search.StartingDateTime == new DateTime())
                                       select new AppointmentViewModel
                                       {
                                           Active = a.Active,
                                           ApplicationId = a.ApplicationId,
                                           Branch = b,
                                           ClientId = a.ClientId,
                                           BookingDate = a.BookingDate,
                                           AppointmentStartingTime = a.AppointmentStartingTime,
                                           AppointmentEndingTime = a.AppointmentEndingTime,
                                           IsDeleted = a.IsDeleted,
                                           AppointmentTreatmentStatus = a.Status,
                                           Status = ap.Status,
                                           StylistId = a.StylistId,
                                           TreatmentId = a.TreatmentId,
                                           TotalTime = t.ApproximateTime,
                                           Id = a.Id,
                                           Client = c,
                                           Stylist = s,
                                           Treatment = new TreatmentViewModel
                                           {
                                               Id = t.Id,
                                               AddedBy = t.AddedBy,
                                               ApproximateTime = t.ApproximateTime,
                                               Category = t.Category,
                                               CategoryId = t.CategoryId,
                                               Date = t.Date,
                                               Description = t.Description,
                                               ImageUrl = t.ImageUrl,
                                               IsDeleted = t.IsDeleted,
                                               Name = t.Name,
                                               Points = t.Points,
                                               LastUpdatedOn = t.LastUpdatedOn,
                                               Price = (float)t.Price,
                                               TaxId = t.TaxId,
                                               Active = t.Active
                                           },
                                           Approver = apr
                                       }).OrderBy(x => x.AppointmentStartingTime).AsQueryable();
                }
                else
                {
                    queryableResult = (from a in db.Appointments
                                       join b in db.Branches on a.BranchId equals b.Id
                                       join c in db.Users on a.ClientId equals c.Id
                                       join s in db.Users on a.StylistId equals s.Id
                                       join t in db.Treatments on a.TreatmentId equals t.Id
                                       join ap in db.AppointmentApprovals on a.Id equals ap.AppointmentId
                                       join apr in db.Users on ap.ApproverId equals apr.Id
                                       where a.Active && !a.IsDeleted
                                           && (c.Name.Contains(search.ClientName) || string.IsNullOrEmpty(search.ClientName))
                                           && (s.Id == search.StylistId || c.Id == search.ClientId)
                                           && (s.Name.Contains(search.StylistName) || string.IsNullOrEmpty(search.StylistName))
                                           && (apr.Id == search.ApproverId || string.IsNullOrEmpty(search.ApproverId))
                                           && (apr.Name.Contains(search.ApproverName) || string.IsNullOrEmpty(search.ApproverName))
                                           && (a.Id == search.Id || search.Id == new Guid())
                                           && (a.Id != search.DiscardAppointment || search.DiscardAppointment == new Guid())
                                           && (a.Status == search.AppointmentTreatmentStatus || search.AppointmentTreatmentStatus == 0)
                                           && (ap.Status == search.Status || search.Status == 0 && ap.Status != AppointmentStatus.Cancelled)
                                           && (t.Name.Contains(search.TreatmentName) || string.IsNullOrEmpty(search.TreatmentName))
                                           && (a.AppointmentStartingTime >= search.BookingDate && a.AppointmentEndingTime <= search.BookingDateEnding || search.BookingDate == new DateTime())
                                           && ((DbFunctions.TruncateTime(a.AppointmentStartingTime) <= DbFunctions.TruncateTime(search.StartingDateTime) && DbFunctions.TruncateTime(a.AppointmentEndingTime) >= DbFunctions.TruncateTime(search.StartingDateTime)) || search.StartingDateTime == new DateTime())
                                       select new AppointmentViewModel
                                       {
                                           Active = a.Active,
                                           ApplicationId = a.ApplicationId,
                                           Branch = b,
                                           ClientId = a.ClientId,
                                           BookingDate = a.BookingDate,
                                           AppointmentStartingTime = a.AppointmentStartingTime,
                                           AppointmentEndingTime = a.AppointmentEndingTime,
                                           IsDeleted = a.IsDeleted,
                                           AppointmentTreatmentStatus = a.Status,
                                           Status = ap.Status,
                                           StylistId = a.StylistId,
                                           TreatmentId = a.TreatmentId,
                                           TotalTime = t.ApproximateTime,
                                           Id = a.Id,
                                           Client = c,
                                           Stylist = s,
                                           Treatment = new TreatmentViewModel
                                           {
                                               Id = t.Id,
                                               AddedBy = t.AddedBy,
                                               ApproximateTime = t.ApproximateTime,
                                               Category = t.Category,
                                               CategoryId = t.CategoryId,
                                               Date = t.Date,
                                               Description = t.Description,
                                               ImageUrl = t.ImageUrl,
                                               IsDeleted = t.IsDeleted,
                                               Name = t.Name,
                                               Points = t.Points,
                                               LastUpdatedOn = t.LastUpdatedOn,
                                               Price = (float)t.Price,
                                               TaxId = t.TaxId,
                                               Active = t.Active
                                           },
                                           Approver = apr
                                       }).OrderBy(x => x.AppointmentStartingTime).AsQueryable();
                }

                return queryableResult;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public IQueryable<Select2OptionModel> Select2Result(AppointmentSearchViewModel search, ApplicationDbContext db)
        {
            try
            {
                var queryableResult = (from a in db.Appointments
                                       join c in db.Users on a.ClientId equals c.Id
                                       join s in db.Users on a.StylistId equals s.Id
                                       join t in db.Treatments on a.TreatmentId equals t.Id
                                       join ap in db.AppointmentApprovals on a.Id equals ap.AppointmentId
                                       join u in db.Users on ap.ApproverId equals u.Id
                                       where (c.Id == search.ClientId || string.IsNullOrEmpty(search.ClientId))
                                           && (c.Name.Contains(search.ClientName) || string.IsNullOrEmpty(search.ClientName))
                                           && (s.Id == search.StylistId || string.IsNullOrEmpty(search.StylistId))
                                           && (s.Name.Contains(search.StylistName) || string.IsNullOrEmpty(search.StylistName))
                                           && (u.Id == search.ApproverId || string.IsNullOrEmpty(search.ApproverId))
                                           && (u.Name.Contains(search.ApproverName) || string.IsNullOrEmpty(search.ApproverName))
                                           && (a.Id == search.Id || search.Id == new Guid())
                                           && (a.Id != search.DiscardAppointment || search.DiscardAppointment == new Guid())
                                           && (DbFunctions.TruncateTime(a.AppointmentStartingTime) == search.BookingDate || search.BookingDate == new DateTime())
                                           && (a.Status == search.AppointmentTreatmentStatus || search.AppointmentTreatmentStatus == 0)
                                           && (ap.Status == search.Status || search.Status == 0)
                                           && (t.Id == search.TreatmentId || search.TreatmentId == new Guid())
                                           && (t.Name.Contains(search.TreatmentName) || string.IsNullOrEmpty(search.TreatmentName))
                                           && ((DbFunctions.TruncateTime(a.AppointmentStartingTime) <= DbFunctions.TruncateTime(search.StartingDateTime) && DbFunctions.TruncateTime(a.AppointmentEndingTime) >= DbFunctions.TruncateTime(search.StartingDateTime)) || search.StartingDateTime == new DateTime())
                                       select new Select2OptionModel
                                       {
                                           id = a.Id.ToString(),
                                           text = a.BookingDate.ToString()
                                       }).OrderBy(x => x.text).AsQueryable();
                return queryableResult;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public IQueryable<AppointmentViewModel> GetRegularSearchResult(AppointmentSearchViewModel search, ApplicationDbContext db)
        {
            try
            {
                var queryableResult = (from a in db.Appointments
                                       join b in db.Branches on a.BranchId equals b.Id
                                       join c in db.Users on a.ClientId equals c.Id
                                       join s in db.Users on a.StylistId equals s.Id
                                       join t in db.Treatments on a.TreatmentId equals t.Id
                                       join ap in db.AppointmentApprovals on a.Id equals ap.AppointmentId
                                       join apr in db.Users on ap.ApproverId equals apr.Id
                                       //join iit in db.InvoiceItems on a.Id equals iit.AppointmentId into iits
                                       //from iit in iits.DefaultIfEmpty()
                                       //join ir in db.InvoiceRecords on iit.InvoiceId equals ir.Id into irs
                                       //from ir in irs.DefaultIfEmpty()
                                       //join cp in db.ClientPoints on ir.Id equals cp.InvoiceId into cps
                                       //from cp in cps.DefaultIfEmpty()
                                       where a.Active && !a.IsDeleted
                                           && (c.Id == search.ClientId || string.IsNullOrEmpty(search.ClientId))
                                           && (c.Name.Contains(search.ClientName) || string.IsNullOrEmpty(search.ClientName))
                                           && (s.Id == search.StylistId || string.IsNullOrEmpty(search.StylistId))
                                           && (s.Name.Contains(search.StylistName) || string.IsNullOrEmpty(search.StylistName))
                                           && (apr.Id == search.ApproverId || string.IsNullOrEmpty(search.ApproverId))
                                           && (apr.Name.Contains(search.ApproverName) || string.IsNullOrEmpty(search.ApproverName))
                                           && (a.Id == search.Id || search.Id == new Guid())
                                           && (a.Id != search.DiscardAppointment || search.DiscardAppointment == new Guid())
                                           && (search.MultiAppointmentTreatmentStatus.Count == 0 || search.MultiAppointmentTreatmentStatus.Contains((int)a.Status))
                                           && (a.Status == search.AppointmentTreatmentStatus || search.AppointmentTreatmentStatus == 0)
                                           && (ap.Status == search.Status || search.Status == 0)
                                           && (t.Id == search.TreatmentId || search.TreatmentId == new Guid())
                                           && (t.Name.Contains(search.TreatmentName) || string.IsNullOrEmpty(search.TreatmentName))
                                           && (search.SkipDateCheck || (a.AppointmentStartingTime >= search.BookingDate && a.AppointmentEndingTime <= search.BookingDateEnding || search.BookingDate == new DateTime()))
                                           && ((DbFunctions.TruncateTime(a.AppointmentStartingTime) <= DbFunctions.TruncateTime(search.StartingDateTime) && DbFunctions.TruncateTime(a.AppointmentEndingTime) >= DbFunctions.TruncateTime(search.StartingDateTime)) || search.StartingDateTime == new DateTime())
                                       select new AppointmentViewModel
                                       {
                                           Active = a.Active,

                                           ApplicationId = a.ApplicationId,
                                           Branch = b,
                                           ClientId = a.ClientId,
                                           BookingDate = a.BookingDate,
                                           AppointmentStartingTime = a.AppointmentStartingTime,
                                           AppointmentEndingTime = a.AppointmentEndingTime,
                                           IsDeleted = a.IsDeleted,
                                           AppointmentTreatmentStatus = a.Status,
                                           Status = ap.Status,
                                           StylistId = a.StylistId,
                                           TreatmentId = a.TreatmentId,
                                           TotalTime = t.ApproximateTime,
                                           ElapsedTime = (float)a.TimeElapsed,
                                           ActualStartingTime = a.ActualStartingTime,
                                           ActualEndingTime = a.ActualEndingTime,
                                           Id = a.Id,
                                           Client = c,
                                           Stylist = s,
                                           Treatment = new TreatmentViewModel
                                           {
                                               Id = t.Id,
                                               AddedBy = t.AddedBy,
                                               ApproximateTime = t.ApproximateTime,
                                               Category = t.Category,
                                               CategoryId = t.CategoryId,
                                               Date = t.Date,
                                               Description = t.Description,
                                               ImageUrl = t.ImageUrl,
                                               IsDeleted = t.IsDeleted,
                                               Name = t.Name,
                                               Points = t.Points,
                                               LastUpdatedOn = t.LastUpdatedOn,
                                               Price = (float)t.Price,
                                               TaxId = t.TaxId,
                                               Active = t.Active
                                           },
                                           //ClientPoints = cp,
                                           //GrandTotal = ir != null ? ir.GrandTotal : 0,
                                           //AmountRecieved = ir != null ? ir.AmountRecieved : 0,
                                           Approver = apr,
                                           IsEditAllowed = search.IsEditAllowed,
                                       })
                                       .OrderBy(x => x.AppointmentStartingTime).AsQueryable();
                return queryableResult;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public List<AppointmentViewModel> GetAppointmentCount(AppointmentSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    var queryableResult = (from a in db.Appointments
                                           join b in db.Branches on a.BranchId equals b.Id
                                           join c in db.Users on a.ClientId equals c.Id
                                           join s in db.Users on a.StylistId equals s.Id
                                           join t in db.Treatments on a.TreatmentId equals t.Id
                                           join ap in db.AppointmentApprovals on a.Id equals ap.AppointmentId
                                           join apr in db.Users on ap.ApproverId equals apr.Id
                                           where a.Active && !a.IsDeleted
                                               && (c.Id == search.ClientId || string.IsNullOrEmpty(search.ClientId))
                                               && (c.Name.Contains(search.ClientName) || string.IsNullOrEmpty(search.ClientName))
                                               && (s.Id == search.StylistId || string.IsNullOrEmpty(search.StylistId))
                                               && (s.Name.Contains(search.StylistName) || string.IsNullOrEmpty(search.StylistName))
                                               && (apr.Id == search.ApproverId || string.IsNullOrEmpty(search.ApproverId))
                                               && (apr.Name.Contains(search.ApproverName) || string.IsNullOrEmpty(search.ApproverName))
                                               && (a.Id == search.Id || search.Id == new Guid())
                                               && (a.Id != search.DiscardAppointment || search.DiscardAppointment == new Guid())
                                               && (a.Status == search.AppointmentTreatmentStatus || search.AppointmentTreatmentStatus == 0)
                                               && (ap.Status == search.Status || search.Status == 0)
                                               && (t.Id == search.TreatmentId || search.TreatmentId == new Guid())
                                               && (t.Name.Contains(search.TreatmentName) || string.IsNullOrEmpty(search.TreatmentName))
                                               && (a.AppointmentStartingTime >= search.BookingDate && a.AppointmentEndingTime <= search.BookingDateEnding || search.BookingDate == new DateTime())
                                               && ((DbFunctions.TruncateTime(a.AppointmentStartingTime) <= DbFunctions.TruncateTime(search.StartingDateTime) && DbFunctions.TruncateTime(a.AppointmentEndingTime) >= DbFunctions.TruncateTime(search.StartingDateTime)) || search.StartingDateTime == new DateTime())
                                           select new AppointmentViewModel
                                           {
                                               AppointmentTreatmentStatus = a.Status,
                                               Status = ap.Status,
                                           }).ToList();
                    return queryableResult;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public IQueryable<FullCalendarViewModel> VerifyTime(AppointmentSearchViewModel search, ApplicationDbContext db)
        {
            try
            {
                var queryableResult = (from a in db.Appointments
                                       join b in db.Branches on a.BranchId equals b.Id
                                       join c in db.Users on a.ClientId equals c.Id
                                       join s in db.Users on a.StylistId equals s.Id
                                       join t in db.Treatments on a.TreatmentId equals t.Id

                                       where a.Active && !a.IsDeleted
                                           && (c.Name.Contains(search.ClientName) || string.IsNullOrEmpty(search.ClientName))
                                           && (s.Id == search.StylistId || (c.Id == search.ClientId))
                                           && (s.Name.Contains(search.StylistName) || string.IsNullOrEmpty(search.StylistName))
                                           && (a.Id == search.Id || search.Id == new Guid())
                                           && (a.Id != search.DiscardAppointment || search.DiscardAppointment == new Guid())
                                           && (a.Status == search.AppointmentTreatmentStatus || search.AppointmentTreatmentStatus == 0)
                                           && (t.Id == search.TreatmentId || search.TreatmentId == new Guid())
                                           && (t.Name.Contains(search.TreatmentName) || string.IsNullOrEmpty(search.TreatmentName))
                                           && ((a.AppointmentStartingTime >= search.BookingDate && a.AppointmentEndingTime <= search.BookingDateEnding || search.BookingDate == new DateTime()))
                                           && ((DbFunctions.TruncateTime(a.AppointmentStartingTime) <= DbFunctions.TruncateTime(search.StartingDateTime) && DbFunctions.TruncateTime(a.AppointmentEndingTime) >= DbFunctions.TruncateTime(search.StartingDateTime)) || search.StartingDateTime == new DateTime())
                                       select new FullCalendarViewModel
                                       {
                                           Start = a.AppointmentStartingTime,
                                           End = a.AppointmentEndingTime,
                                           Subject = c.Name,
                                           Description = t.Name,
                                           ThemeColor = "blue",
                                           IsFullDay = false
                                       }).OrderBy(x => x.Start).AsQueryable();
                return queryableResult;

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public List<AppointmentHistoryViewModel> GetAppointmentHistory(AppointmentSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = (from a in db.Appointments
                                  join b in db.Branches on a.BranchId equals b.Id
                                  join c in db.Users on a.ClientId equals c.Id
                                  join s in db.Users on a.StylistId equals s.Id
                                  join t in db.Treatments on a.TreatmentId equals t.Id
                                  join ap in db.AppointmentApprovals on a.Id equals ap.AppointmentId
                                  join apr in db.Users on ap.ApproverId equals apr.Id
                                  join iit in db.InvoiceItems on a.Id equals iit.AppointmentId into iits
                                  from iit in iits.DefaultIfEmpty()
                                  join ir in db.InvoiceRecords on iit.InvoiceId equals ir.Id into irs
                                  from ir in irs.DefaultIfEmpty()
                                  join cp in db.ClientPoints on ir.Id equals cp.InvoiceId into cps
                                  from cp in cps.DefaultIfEmpty()
                                  where a.Active && !a.IsDeleted
                                    && (c.Id == search.ClientId || string.IsNullOrEmpty(search.ClientId))
                                    && (c.Name.Contains(search.ClientName) || string.IsNullOrEmpty(search.ClientName))
                                    && (s.Id == search.StylistId || string.IsNullOrEmpty(search.StylistId))
                                    && (s.Name.Contains(search.StylistName) || string.IsNullOrEmpty(search.StylistName))
                                    && (apr.Id == search.ApproverId || string.IsNullOrEmpty(search.ApproverId))
                                    && (apr.Name.Contains(search.ApproverName) || string.IsNullOrEmpty(search.ApproverName))
                                    && (a.Id == search.Id || search.Id == new Guid())
                                    && (a.Id != search.DiscardAppointment || search.DiscardAppointment == new Guid())
                                    && (search.MultiAppointmentTreatmentStatus.Count == 0 || search.MultiAppointmentTreatmentStatus.Contains((int)a.Status))
                                    && (a.Status == search.AppointmentTreatmentStatus || search.AppointmentTreatmentStatus == 0)
                                    && (ap.Status == search.Status || search.Status == 0)
                                    && (t.Id == search.TreatmentId || search.TreatmentId == new Guid())
                                    && (t.Name.Contains(search.TreatmentName) || string.IsNullOrEmpty(search.TreatmentName))
                                    && (search.SkipDateCheck || (a.AppointmentStartingTime >= search.BookingDate && a.AppointmentEndingTime <= search.BookingDateEnding || search.BookingDate == new DateTime()))
                                    && ((DbFunctions.TruncateTime(a.AppointmentStartingTime) <= DbFunctions.TruncateTime(search.StartingDateTime) && DbFunctions.TruncateTime(a.AppointmentEndingTime) >= DbFunctions.TruncateTime(search.StartingDateTime)) || search.StartingDateTime == new DateTime())

                                  select new
                                  {
                                      Id = a.Id,
                                      TreatmentId = t.Id,
                                      StylistId = s.Id,
                                      TreatmentName = t.Name,
                                      StylistName = s.Name,
                                      ClientId = c.Id,
                                      Cost = iit != null ? iit.ItemPrice - iit.DiscountedPrice : 0,
                                      PointsGained = ir != null && ir.PointsUsed == 0 ? t.Points : 0,
                                      PointsUsed = ir != null ? ir.PointsUsed : 0,
                                      AppointmentStart = a.ActualStartingTime,
                                      ApppointmentEnd = a.ActualEndingTime,
                                      ClientName = c.Name,
                                      Date = a.BookingDate,
                                      Discount = iit != null ? iit.DiscountedPrice : 0
                                  })
                                         .GroupBy(m => new { m.Id, m.TreatmentId }).ToList()
                                         .Select(m => new AppointmentHistoryViewModel
                                         {
                                             Id = m.FirstOrDefault().Id,
                                             TreatmentId = m.FirstOrDefault().TreatmentId,
                                             StylistId = m.FirstOrDefault().StylistId,
                                             TreatmentName = m.FirstOrDefault().TreatmentName,
                                             StylistName = m.FirstOrDefault().StylistName,
                                             ClientId = m.FirstOrDefault().ClientId,
                                             Cost = m.FirstOrDefault().Cost,
                                             PointsGained = m.FirstOrDefault().PointsGained,
                                             PointsUsed = m.FirstOrDefault().PointsUsed,
                                             AppointmentStart = m.FirstOrDefault().AppointmentStart,
                                             AppointmentEnd = m.FirstOrDefault().ApppointmentEnd,
                                             ClientName = m.FirstOrDefault().ClientName,
                                             Date = m.FirstOrDefault().Date,
                                             Discount = m.FirstOrDefault().Discount
                                         }).ToList();
                    return result;

                }
            }
            catch (Exception ex)
            {
                return new List<AppointmentHistoryViewModel>();
            }
        }

        #region Functions
        private List<LeaveViewModel> GetRecursiveLeavesForToday(List<LeaveViewModel> Leaves, List<AppointmentViewModel> ApprovedAppointments, DateTime DesiredDate, BranchViewModel branch)
        {
            List<LeaveViewModel> TodayRecursiveLeaves = new List<LeaveViewModel>();
            foreach (var leave in Leaves)
            {
                bool AddLeave = false;
                bool IsRecursive = true;
                DateTime AppointmentStartingTime = DateTime.Now;
                DateTime AppointmentEndingTime = DateTime.Now;
                SlotStatus AppointmentSlotStatus = SlotStatus.Leave;
                Guid leaveId = leave.ReferenceId;
                if (leave.Recursion == Recursion.Never)
                {
                    AddLeave = true;
                    IsRecursive = false;
                    AppointmentStartingTime = leave.StartingDate < DesiredDate.Add(branch.OpeningTime) ? DesiredDate.Add(branch.OpeningTime) : leave.StartingDate;
                    AppointmentEndingTime = leave.EndingDate > DesiredDate.Add(branch.ClosingTime) ? DesiredDate.Add(branch.ClosingTime) : leave.EndingDate;

                }
                else
                {
                    AppointmentStartingTime = leave.StartingDate.TimeOfDay < branch.OpeningTime ? DesiredDate.Add(branch.OpeningTime) : DesiredDate.Date.Add(leave.StartingDate.TimeOfDay);
                    if (leave.EndingDate.Date > DesiredDate.Date)
                        AppointmentEndingTime = DesiredDate.Add(leave.EndingDate.TimeOfDay);
                    else
                        AppointmentEndingTime = leave.EndingDate.TimeOfDay > branch.ClosingTime ? DesiredDate.Add(branch.ClosingTime) : DesiredDate.Date.Add(leave.EndingDate.TimeOfDay);
                    if (leave.Recursion == Recursion.Weekly)
                    {
                        if (leave.RecursiveDays.Contains((((int)DesiredDate.DayOfWeek) + 1).ToString()))//+1 Because our Enum is starting from one. We are starting from one because of search puposes
                        {
                            AddLeave = true;
                        }
                    }
                    else if (leave.Recursion == Recursion.EveryOtherWeek)
                    {
                        var startingWeek = leave.StartingDate.DayOfYear < 7 ? 1 : leave.StartingDate.DayOfYear / 7;
                        var desiredWeek = DesiredDate.DayOfYear < 7 ? 1 : DesiredDate.DayOfYear / 7;
                        var difference = (desiredWeek - startingWeek) % 2;
                        if (difference == 0)
                        {
                            if (leave.RecursiveDays.Contains((((int)DesiredDate.DayOfWeek) + 1).ToString()))
                            {
                                AddLeave = true;
                            }
                        }
                    }
                    else if (leave.Recursion == Recursion.Daily)
                    {
                        AddLeave = true;
                    }
                }
                if (AddLeave)
                {
                    ApprovedAppointments.Add(new AppointmentViewModel
                    {
                        Id = leave.ReferenceId,
                        Stylist = leave.Stylist,
                        StylistId = leave.Stylist.Id,
                        IsReccursive = IsRecursive,
                        Description = leave.Note,
                        BookingDate = AppointmentStartingTime,
                        AppointmentStartingTime = AppointmentStartingTime,
                        AppointmentEndingTime = AppointmentEndingTime,
                        SlotStatus = AppointmentSlotStatus,
                    });
                }
            }
            return TodayRecursiveLeaves;
        }
        public List<FullCalendarEvents> GetFreeSlotsForAllStylist(AppointmentSlotSearchViewModel search)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var scheduledTimings = new List<AppointmentViewModel>();
                    var freeSlots = new List<AppointmentTimeSlot>();
                    var appointedSlots = new List<AppointmentTimeSlot>();
                    var day = (int)(search.DesiredDate.DayOfWeek);
                    var branch = GetBranches<BranchViewModel>(new BranchSearchViewModel()).ResultList.First();
                    var saloonOpeiningTime = search.DesiredDate.Date.Add(branch.OpeningTime);
                    var saloonClosingTime = search.DesiredDate.Date.Add(branch.ClosingTime);
                    var RoleId = this.GetRoles<RoleViewModel>(new RoleSearchViewModel { Name = "Stylist" }).ResultList.FirstOrDefault().Id;

                    //===================Get All Stylist=========================
                    List<RegisterViewModel> Stylists = GetUsers<RegisterViewModel>(new AccountSearchViewModel { RoleId = RoleId }).ResultList.GroupBy(x => x.Email).Select(o => o.FirstOrDefault()).ToList();

                    //================ Get Appointments==========================
                    var approvedAppointments = GetAppointments<AppointmentViewModel>(new AppointmentSearchViewModel { BookingDate = search.DesiredDate.Date, StylistId = search.SytlistId, ClientId = search.ClientId, DiscardAppointment = search.DiscardAppointment, TimeSlotsResult = true, GetAllSlots = search.GetAllSlots }).ResultList;
                    approvedAppointments.ForEach(x => x.SlotStatus = SlotStatus.Appointment);

                    //================ Get Schedule==========================
                    var schedules = GetStylistSchedule<StylistScheduleViewModel>(new StylistScheduleSearchViewModel { Stylist = new ApplicationUser { Id = search.SytlistId }, Day = (WeekDays)Enum.Parse(typeof(WeekDays), day.ToString()) }).ResultList;
                    foreach (var schedule in schedules)
                    {
                        scheduledTimings.Add(new AppointmentViewModel { Stylist = schedule.Stylist, AppointmentStartingTime = search.DesiredDate.Date.Add(schedule.StartingTime), AppointmentEndingTime = search.DesiredDate.Date.Add(schedule.EndingTime), SlotStatus = SlotStatus.Schedule });
                    }

                    //================ Get Leaves==========================
                    var leaves = new List<LeaveViewModel>();
                    var nonRecursiveLeaves = GetLeaves<LeaveViewModel>(new LeaveSearchViewModel { Stylist = new ApplicationUser { Id = search.SytlistId }, StartDate = search.DesiredDate.Date, Recursion = Recursion.Never }).ResultList;
                    var recursiveLeaves = GetLeaves<LeaveViewModel>(new LeaveSearchViewModel { Stylist = new ApplicationUser { Id = search.SytlistId }, StartDate = search.DesiredDate.Date, RecursionBeside = Recursion.Never }).ResultList;
                    var recursiveContinuousLeaves = GetLeaves<LeaveViewModel>(new LeaveSearchViewModel { Stylist = new ApplicationUser { Id = search.SytlistId }, StartDate = search.DesiredDate.Date, IsContinuous = true, RecursionBeside = Recursion.Never }).ResultList;
                    leaves.AddRange(recursiveContinuousLeaves);
                    leaves.AddRange(recursiveLeaves);
                    leaves.AddRange(nonRecursiveLeaves);
                    GetRecursiveLeavesForToday(leaves, approvedAppointments, search.DesiredDate, branch);


                    return GetEventsSlots(approvedAppointments, saloonOpeiningTime, saloonClosingTime, appointedSlots, freeSlots, scheduledTimings, Stylists);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        private List<FullCalendarEvents> GetEventsSlots(List<AppointmentViewModel> approvedAppointments, DateTime saloonOpeiningTime, DateTime saloonClosingTime, List<AppointmentTimeSlot> appointedSlots, List<AppointmentTimeSlot> freeSlots, List<AppointmentViewModel> scheduledTimings, List<RegisterViewModel> Stylists)
        {
            List<FullCalendarEvents> events = new List<FullCalendarEvents>();
            foreach (var Stylist in Stylists)
            {

                var lastAppointmentEndingTime = saloonOpeiningTime;
                var StylistApprovedAppointment = approvedAppointments.Where(x => x.StylistId == Stylist.Id).ToList();
                // Get UnScheduled Timings

                TimePeriodCollection ScheduledTimePeriods = new TimePeriodCollection();
                TimePeriodCollection BranchTimnings = new TimePeriodCollection();

                ScheduledTimePeriods.AddAll(scheduledTimings.Where(m => m.SlotStatus == SlotStatus.Schedule && m.Stylist.Id == Stylist.Id).Select(b => new TimeRange() { Start = b.Start, End = b.End }).ToList());
                BranchTimnings.Add(new TimeRange() { Start = saloonOpeiningTime, End = saloonClosingTime });
                TimeGapCalculator<TimeRange> gapCalculator = new TimeGapCalculator<TimeRange>(new TimeCalendar());
                ITimePeriodCollection freeTimes = gapCalculator.GetGaps(ScheduledTimePeriods, BranchTimnings);
                var unscheduledTime = freeTimes.Select(m => new AppointmentViewModel
                {
                    AppointmentStartingTime = m.Start,
                    AppointmentEndingTime = m.End,
                    SlotStatus = SlotStatus.UnScheduled
                });
                StylistApprovedAppointment.AddRange(unscheduledTime);
                StylistApprovedAppointment = StylistApprovedAppointment.OrderBy(x => x.AppointmentStartingTime).ThenByDescending(x => x.AppointmentEndingTime).ToList();



                if (StylistApprovedAppointment.Count() > 0)
                {
                    foreach (var appointment in StylistApprovedAppointment)
                    {
                        var treatment = appointment.Treatment;
                        FullCalendarEvents AppointmentEvent = new FullCalendarEvents();
                        AppointmentEvent.id = appointment.Id.ToString();
                        AppointmentEvent.stylist = Stylist.Name;
                        AppointmentEvent.bookingDate = appointment.BookingDate.ToString("yyyy-MM-dd");
                        AppointmentEvent.isRecurring = appointment.IsReccursive;
                        AppointmentEvent.resourceId = Stylist.Id;
                        AppointmentEvent.title = EnumHelper.GetDescription<SlotStatus>(appointment.SlotStatus);
                        AppointmentEvent.startTime = appointment.AppointmentStartingTime.ToString("HH:mm");
                        AppointmentEvent.endTime = appointment.AppointmentEndingTime.ToString("HH:mm");
                        AppointmentEvent.start = appointment.AppointmentStartingTime.ToString("yyyy-MM-ddTHH:mm:ss");
                        AppointmentEvent.end = appointment.AppointmentEndingTime.ToString("yyyy-MM-ddTHH:mm:ss");

                        if (appointment.SlotStatus == SlotStatus.Appointment)
                        {

                            AppointmentEvent.treatment = treatment.Name;
                            AppointmentEvent.client = appointment.Client.Name;
                            AppointmentEvent.description = treatment.Description;
                            AppointmentEvent.bordercolor = "green";
                        }
                        if (appointment.SlotStatus == SlotStatus.Leave)
                        {
                            AppointmentEvent.description = appointment.Description;
                            AppointmentEvent.isRecurring = appointment.IsReccursive;
                            AppointmentEvent.bordercolor = "#FEC828";
                        }
                        if (appointment.SlotStatus == SlotStatus.UnScheduled)
                        {
                            AppointmentEvent.bordercolor = "gray";
                        }
                        events.Add(AppointmentEvent);

                        if (((appointment.AppointmentStartingTime - lastAppointmentEndingTime).TotalMinutes > 0))//Replace 0 By minimum treatment time
                        {
                            FullCalendarEvents FreeSlotEvent = new FullCalendarEvents();
                            FreeSlotEvent.stylist = Stylist.Name;
                            FreeSlotEvent.resourceId = Stylist.Id;
                            FreeSlotEvent.title = EnumHelper.GetDescription<SlotStatus>(SlotStatus.Available);
                            FreeSlotEvent.startTime = lastAppointmentEndingTime.ToString("HH:mm");
                            FreeSlotEvent.endTime = appointment.AppointmentStartingTime.ToString("HH:mm");
                            FreeSlotEvent.start = lastAppointmentEndingTime.ToString("yyyy-MM-ddTHH:mm:ss");
                            FreeSlotEvent.end = appointment.AppointmentStartingTime.ToString("yyyy-MM-ddTHH:mm:ss");
                            lastAppointmentEndingTime = appointment.AppointmentEndingTime;
                            events.Add(FreeSlotEvent);
                        }
                        else if (appointment.AppointmentEndingTime.Ticks >= lastAppointmentEndingTime.Ticks)
                        {
                            lastAppointmentEndingTime = appointment.AppointmentEndingTime;
                        }
                    }
                    if (((saloonClosingTime - lastAppointmentEndingTime).TotalMinutes > 0)) //Replace 0 By minimum Treatment Time
                    {
                        FullCalendarEvents FreeSlotEvent = new FullCalendarEvents();
                        FreeSlotEvent.stylist = Stylist.Name;
                        FreeSlotEvent.resourceId = Stylist.Id;
                        FreeSlotEvent.title = EnumHelper.GetDescription<SlotStatus>(SlotStatus.Available);
                        FreeSlotEvent.startTime = lastAppointmentEndingTime.ToString("HH:mm");
                        FreeSlotEvent.endTime = saloonClosingTime.ToString("HH:mm");
                        FreeSlotEvent.start = lastAppointmentEndingTime.ToString("yyyy-MM-ddTHH:mm:ss");
                        FreeSlotEvent.end = saloonClosingTime.ToString("yyyy-MM-ddTHH:mm:ss");
                        events.Add(FreeSlotEvent);
                    }
                }
                else
                {
                    FullCalendarEvents FreeSlotEvent = new FullCalendarEvents();
                    FreeSlotEvent.stylist = Stylist.Name;
                    FreeSlotEvent.resourceId = Stylist.Id;
                    FreeSlotEvent.title = EnumHelper.GetDescription<SlotStatus>(SlotStatus.Available);
                    FreeSlotEvent.startTime = saloonOpeiningTime.ToString("HH:mm");
                    FreeSlotEvent.endTime = saloonClosingTime.ToString("HH:mm");
                    FreeSlotEvent.start = saloonOpeiningTime.ToString("yyyy-MM-ddTHH:mm:ss");
                    FreeSlotEvent.end = saloonClosingTime.ToString("yyyy-MM-ddTHH:mm:ss");
                    events.Add(FreeSlotEvent);
                }
            }
            return events;
        }

        #endregion

    }
}
