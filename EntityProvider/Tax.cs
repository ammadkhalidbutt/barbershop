﻿using AutoMapper;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public TaxViewModel GetTax(Guid id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.Taxes.FirstOrDefault(x => x.Id == id);
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<Tax, TaxViewModel>(); });
                    var iMapper = config.CreateMapper();
                    var Tax = iMapper.Map<Tax, TaxViewModel>(record);
                    return Tax;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public List<TaxViewModel> GetTaxes(Guid taxId)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var result = db.Taxes
                        .Where(m =>
                                  (taxId == new Guid() || m.Id == taxId)
                                  &&
                                  m.IsDeleted == false
                              )
                        .Select(m => new TaxViewModel
                        {
                            Id = m.Id,
                            CreatedOn = m.CreatedOn,
                            CreatedBy = m.CreatedBy,
                            Name = m.Name,
                            IsExclusive = m.IsExclusive,
                            Percentage = m.Percentage,
                            UpdatedBy = m.UpdatedBy,
                            UpdatedOn = m.UpdatedOn
                        }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return new List<TaxViewModel>();
            }
        }

        public Guid Create(TaxViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<TaxViewModel, Tax>(); });
                    var iMapper = config.CreateMapper();
                    var Tax = iMapper.Map<TaxViewModel, Tax>(model);
                    var result = db.Taxes.Add(Tax);
                    db.SaveChanges();
                    new Helpers.LogGeneration().CreateGeneralLog(db, new Log { Action = "Create Tax", DateTime = DateTime.Now, AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), ModelId = model.Id.ToString() });
                    return result.Id;

                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Create Taxes", AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                return new Guid();
            }
        }

        public Guid Edit(TaxViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.Taxes.Find(model.Id);
                    record.Name = string.IsNullOrEmpty(model.Name) ? record.Name : model.Name;
                    record.Percentage = model.Percentage == 0 ? record.Percentage : model.Percentage;
                    record.IsExclusive = model.IsExclusive;
                    record.UpdatedBy = model.UpdatedBy;
                    record.UpdatedOn = model.UpdatedOn;
                    record.IsDeleted = model.IsDeleted;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    new Helpers.LogGeneration().CreateGeneralLog(db, new Log { Action = "Edit Tax", DateTime = DateTime.Now, AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), ModelId = model.Id.ToString() });
                    return record.Id;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Edit Tax", AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                return new Guid();
            }
        }

        public bool Remove(TaxViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var tax = db.Taxes.Find(model.Id);
                    if (tax != null)
                        db.Taxes.Remove(tax);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
