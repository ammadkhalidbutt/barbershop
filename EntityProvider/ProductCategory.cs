﻿using AutoMapper;
using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public ProductCategoryViewModel GetProductCategory(Guid id)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.ProductCategories.FirstOrDefault(x => x.Id == id);
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<ProductCategory, ProductCategoryViewModel>(); });
                    var iMapper = config.CreateMapper();
                    var ProductCategory = iMapper.Map<ProductCategory, ProductCategoryViewModel>(record);
                    return ProductCategory;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public SearchResultViewModel<T> GetProductCategories<T>(ProductCategorySearchViewModel search)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var queryableResult = db.ProductCategories
                        .Where(m =>
                                m.IsDeleted == false
                                &&
                                (search.IdList.Count() == 0 || search.IdList.Contains(m.Id))
                                &&
                                (string.IsNullOrEmpty(search.Name) || m.Name == search.Name)
                             ).AsQueryable();

                    var result = new List<T>() as IQueryable<T>;
                    if(search.Select2)
                    {
                        result = queryableResult.Select(m => new Select2OptionModel
                        {
                            id = m.Id.ToString(),
                            text = m.Name
                        }).AsQueryable() as IQueryable<T>;
                    }
                    else
                    {
                        result = queryableResult.Select(m => new ProductCategoryViewModel
                        {
                            Id = m.Id,
                            Description = m.Description,
                            Image = m.Image,
                            Name = m.Name,
                        }).AsQueryable() as IQueryable<T>;
                    }
                    return result.Paginate(search, db);
                }
            }
            catch (Exception ex)
            {
                return new SearchResultViewModel<T>();
            }
        }

        public Guid Create(ProductCategoryViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    //if (db.ProductCategories.Count() == 0)
                    //{
                        var config = new MapperConfiguration(cfg => { cfg.CreateMap<ProductCategoryViewModel, ProductCategory>(); });
                        var iMapper = config.CreateMapper();
                        var ProductCategory = iMapper.Map<ProductCategoryViewModel, ProductCategory>(model);
                        var result = db.ProductCategories.Add(ProductCategory);
                        db.SaveChanges();
                        new Helpers.LogGeneration().CreateGeneralLog(db, new Log { Action = "Create ProductCategory", DateTime = DateTime.Now, AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), ModelId = model.Id.ToString() });
                        return result.Id;
                    //}
                    //else
                    //{
                    //    new Helpers.LogGeneration().CreateExceptionLog(db, new Log { Action = "ProductCategory already exists", DateTime = DateTime.Now, AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), ModelId = model.Id.ToString() });
                    //    return new Guid();
                    //}
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Create ProductCategories", AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                return new Guid();
            }
        }

        public Guid Edit(ProductCategoryViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.ProductCategories.Find(model.Id);
                    record.Name = string.IsNullOrEmpty(model.Name) ? record.Name : model.Name;
                    record.Image = string.IsNullOrEmpty(model.Image) ? record.Image : model.Image;
                    record.UpdatedOn = model.UpdatedOn;
                    record.UpdatedBy = model.UpdatedBy;
                    record.IsDeleted = model.IsDeleted;
                    
record.Description = string.IsNullOrEmpty(model.Description) ? record.Description : model.Description;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    new Helpers.LogGeneration().CreateGeneralLog(db, new Log { Action = "Edit ProductCategory", DateTime = DateTime.Now, AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), ModelId = model.Id.ToString() });
                    return record.Id;
                }
            }
            catch (Exception ex)
            {
                new Helpers.LogGeneration().CreateExceptionLog(new ApplicationDbContext(), new Log { Action = "Edit Product", AddedBy = new Helpers.LoggedInUserInfo().GetLoggedInUserId(), DateTime = DateTime.Now, ModelId = model.Id.ToString(), StackTrace = ex.StackTrace });
                return new Guid();
            }
        }

        public bool Remove(ProductCategoryViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.ProductCategories.Find(model.Id);
                    db.ProductCategories.Remove(record);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
