﻿using AutoMapper;
using Helpers;
using Models;
using Models.Identity;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityProvider
{
    public partial class DataAccess
    {
        public SearchResultViewModel<T> GetClientPoints<T>(ClientPointSearchViewModel search)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    
                    var queryableResult = (from cp in db.ClientPoints
                                           join c in db.Users on cp.ClientId equals c.Id
                                           join ir in db.InvoiceRecords on cp.InvoiceId equals ir.Id
                                           where !cp.IsDeleted
                                               && (cp.ClientId == search.Client.Id || string.IsNullOrEmpty(search.Client.Id))
                                               && (cp.Date == search.Date || search.Date == new DateTime())
                                               && (cp.Date >= search.FromDate && cp.Date <= DateTime.Now || search.FromDate == new DateTime())
                                               && (ir.Id == search.InvoiceId || search.InvoiceId == new Guid())
                                           select new ClientPointViewModel
                                           {
                                               Client = c,
                                               InvoiceRecord = ir,
                                               Date = cp.Date,
                                               Points = cp.Points
                                           }).OrderBy(x => x.Date).AsQueryable();
                    var result = queryableResult as IQueryable<T>;
                    return result.Paginate(search, db);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Create(ClientPointViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var config = new MapperConfiguration(cfg => { cfg.CreateMap<ClientPointViewModel, ClientPoint>().ForMember(x=>x.InvoiceId, m=>m.MapFrom(s=>s.InvoiceRecord.Id))
                                                                                                                    .ForMember(d=>d.InvoiceRecord, m=>m.Ignore())
                                                                                                                    .ForMember(d=>d.Client, m=>m.Ignore()); });
                    var iMapper = config.CreateMapper();
                    var clientPoint = iMapper.Map<ClientPointViewModel, ClientPoint>(model);
                    var result = db.ClientPoints.Add(clientPoint);
                    db.SaveChanges();
                    return result.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Edit(ClientPointViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.ClientPoints.Where(m => m.ClientId == model.Client.Id && m.InvoiceId == model.InvoiceRecord.Id && !m.IsDeleted).FirstOrDefault();
                    record.Points = model.Points;
                    record.LastUpdatedOn = model.LastUpdatedOn;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    return record.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Guid Delete(ClientPointViewModel model)
        {
            try
            {
                using(ApplicationDbContext db = new ApplicationDbContext())
                {
                    var record = db.ClientPoints.Where(m => m.ClientId == model.Client.Id && m.InvoiceId == model.InvoiceRecord.Id && !m.IsDeleted).FirstOrDefault();
                    record.IsDeleted = true;
                    record.LastUpdatedOn = model.LastUpdatedOn;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                    return record.Id;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
